%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Full
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Models
    m_Weight: 0
  - m_Path: Models/Body
    m_Weight: 0
  - m_Path: Models/Head
    m_Weight: 0
  - m_Path: Models/LArmDown
    m_Weight: 0
  - m_Path: Models/LArmPaim1
    m_Weight: 0
  - m_Path: Models/LArmPaim2
    m_Weight: 0
  - m_Path: Models/LArmUp
    m_Weight: 0
  - m_Path: Models/LArmWrist
    m_Weight: 0
  - m_Path: Models/LFootDown
    m_Weight: 0
  - m_Path: Models/LFootUp
    m_Weight: 0
  - m_Path: Models/RArmDown
    m_Weight: 0
  - m_Path: Models/RArmPaim1
    m_Weight: 0
  - m_Path: Models/RArmPaim2
    m_Weight: 0
  - m_Path: Models/RArmUp
    m_Weight: 0
  - m_Path: Models/RArmWrist
    m_Weight: 0
  - m_Path: Models/RFootDown
    m_Weight: 0
  - m_Path: Models/RFootUp
    m_Weight: 0
  - m_Path: Models/Tors
    m_Weight: 0
  - m_Path: Root_M
    m_Weight: 0
  - m_Path: Root_M/Hip_L
    m_Weight: 1
  - m_Path: Root_M/Hip_L/joint9_L
    m_Weight: 1
  - m_Path: Root_M/Hip_L/joint9_L/foot_L
    m_Weight: 1
  - m_Path: Root_M/Hip_L/joint9_L/foot_L/Toes_L
    m_Weight: 1
  - m_Path: Root_M/Hip_R
    m_Weight: 1
  - m_Path: Root_M/Hip_R/joint9_R
    m_Weight: 1
  - m_Path: Root_M/Hip_R/joint9_R/foot_R
    m_Weight: 1
  - m_Path: Root_M/Hip_R/joint9_R/foot_R/Toes_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint5_M
    m_Weight: 0
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint5_M/joint6_M
    m_Weight: 0
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L/joint16_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L/joint16_L/joint17_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L/joint16_L/joint17_L/joint18_1_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L/joint16_L/joint17_L/joint18_1_L/joint19_1_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L/joint16_L/joint20_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L/joint16_L/joint20_L/joint18_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_L/shoulder_L/joint14_L/hand_L/joint16_L/joint20_L/joint18_L/joint19_L
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R/joint16_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R/joint16_R/joint17_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R/joint16_R/joint17_R/joint18_1_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R/joint16_R/joint17_R/joint18_1_R/joint19_1_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R/joint16_R/joint20_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R/joint16_R/joint20_R/joint18_R
    m_Weight: 1
  - m_Path: Root_M/Spine1_M/Mid_M/ChestMid_M/joint21_R/shoulder_R/joint14_R/hand_R/joint16_R/joint20_R/joint18_R/joint19_R
    m_Weight: 1

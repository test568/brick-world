﻿Shader "Normal /LegacyDiffuseRadius" {

	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}

	//Radius
	_Center("Center", Vector) = (0,0,0,0)
		_Radius("Radius", Float) = 0.5
		_RadiusColor("Radius Color", Color) = (1,0,0,1)
		_RadiusWidth("Radius Width", Float) = 2

	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert
#pragma surface surf Standard fullforwardshadows
#pragma target 3.5

		sampler2D _MainTex;
	fixed4 _Color;
	//Radius
	float3 _Center;
	float _Radius;
	fixed4 _RadiusColor;
	float _RadiusWidth;


	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		float d = distance(_Center, IN.worldPos);
		if (d > _Radius && d < _Radius + _RadiusWidth)
			o.Albedo = _RadiusColor;
		else
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		o.Albedo = c.rgb;
		o.Alpha = c.a;
	}
	ENDCG
	}

		Fallback "Legacy Shaders/VertexLit"
}
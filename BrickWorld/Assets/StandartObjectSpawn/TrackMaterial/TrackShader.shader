﻿Shader "Custom/TrackShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Cutoff("Cutoff",Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { 
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
            "IgnoreProjector"="True"
             }

        LOD 200
        Cull off


        CGPROGRAM

        #pragma surface surf Lambert alphatest:_Cutoff


        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        fixed4 _Color;


 //       UNITY_INSTANCING_BUFFER_START(Props)

  //      UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutput o)
        {

            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;

           // o.Metallic = _Metallic;
           // o.Smoothness = _Glossiness;
            o.Alpha = c.a;
            
        }
        ENDCG
    }
    FallBack "Diffuse"
}

﻿Shader "Custom/TraficControllerShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)


        _Bump("Normal Map", 2D) = "Bump" {}
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Oclussion("Aimbent Oclussion",2D) = "white" {}
        _ForceOclussion("Force Oclussion",Range(0,1)) = 0.5
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Emissive("Emissive Mask RGB", 2D) = "red" {}
        _R ("Color R channel",Color) = (1,1,1,1)
        _G ("Color G channel",Color) = (1,1,1,1)
        _B ("Color B channel",Color) = (1,1,1,1)
        _IsR("Active R", Range(0,1)) = 0.0
        _IsG("Active G", Range(0,1)) = 0.0
        _IsB("Active B", Range(0,1)) = 0.0
    

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM

        #pragma surface surf Standard fullforwardshadows

        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _Bump;
        sampler2D _Emissive;
        sampler2D _Oclussion;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        float4 _R : Color;
        float4 _G : Color;
        float4 _B : Color;

        float _IsR;
        float _IsG;
        float _IsB;

        float _ForceOclussion;

        UNITY_INSTANCING_BUFFER_START(Props)

        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {

            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb * ( tex2D(_Oclussion,IN.uv_MainTex) * _ForceOclussion).rgb;
            o.Normal = UnpackNormal(tex2D(_Bump,IN.uv_MainTex));
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness; 
            

            float4 mask = tex2D(_Emissive,IN.uv_MainTex);
            float4 r_canal = mask.r * _R * _IsR; 
            float4 g_canal = mask.g * _G * _IsG; 
            float4 b_canal = mask.b * _B * _IsB; 
            o.Emission = r_canal + g_canal + b_canal;

            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}

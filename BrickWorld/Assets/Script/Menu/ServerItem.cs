﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon;
using Photon.Pun;
using Photon.Realtime;

public class ServerItem: MonoBehaviour {//Сервер в списке серверов
    [SerializeField] Text Name;
    [SerializeField] Text PlayerCount;
    [SerializeField] Text MaxPlayer;
    [SerializeField] Text Map;
    [SerializeField] Text Ping;
    [SerializeField] Button Detected;
    [SerializeField] GameObject Fon;

    public RoomInfo Server;
    private string password;
    BaseMenu menu;

    bool is_take;

    private void Awake() {
        Detected.onClick.AddListener(delegate { UpdateDetected(); });
        }

    public void Init(RoomInfo room, BaseMenu menu) {//Иницыализировать класс
        Name.text=room.Name;
        PlayerCount.text=""+room.PlayerCount;
        MaxPlayer.text=""+room.MaxPlayers;
        Map.text=(string)room.CustomProperties["Map"];
        password=(string)room.CustomProperties["Psw"];
        Server=room;
        Detected.onClick.AddListener(delegate { UpdateDetected(); });
        this.menu=menu;
        }

    public void TakeServer() {
        if (is_take)
            return;
        Fon.SetActive(true);
        is_take=true;

        if (menu.TakeServer)
            menu.TakeServer.PutServer();

        menu.TakeServer=this;
        }

    public void PutServer() {
        Fon.SetActive(false);
        is_take=false;
        }

    public bool CheckPassword(string password) {
        if (this.password=="") {
            return true;
            }
        else
            return this.password==password;
        }

    private void UpdateDetected() {//Вызываеться при нажатии на кнопку.
        TakeServer();
        }
    }

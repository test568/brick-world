﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MapItem: MonoBehaviour {//Иконка карты, спавниться с помощью BaseMenu.cs иницыализирует карту которую можно будет выбрать. При срабатывании функции TakeMap обращаеться к BaseMenu и заменяет у него поле с сылкой на MapItem. 
    [SerializeField] Image MapSprite;
    [SerializeField] Text MapName;
    [SerializeField] Button Detected;
    [SerializeField] GameObject Fon;

    public string NameMap;
    public string CodMap;
    private BaseMenu menu;
    bool is_take;


    public void Init(MapDataItem MapOption, BaseMenu menu) {//Иницыализировать класс
        MapSprite.sprite=MapOption.IconMap;
        MapName.text=MapOption.Name;
        NameMap=MapOption.Name;
        CodMap=MapOption.NameMapBuild;
        Detected.onClick.AddListener(delegate { UpdateDetected(); });
        this.menu=menu;
        }

    public void TakeMap() {//Выбрать данную карту
        if (is_take)
            return;

        Fon.SetActive(true);
        is_take=true;

        if (menu.TakeMap)
            menu.TakeMap.PutMap();

        menu.TakeMap=this;
        }

    public void PutMap() {//Убрать выбранную карту
        Fon.SetActive(false);
        is_take=false;
        }

    private void UpdateDetected() {//Вызываеться при нажатии на кнопку.
        TakeMap();
        }
    }

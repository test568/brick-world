﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using Photon;
using ExitGames.Client.Photon;

public class BaseMenu: MonoBehaviourPunCallbacks {//Контроллер игрового меню

    [Header("Play")]//Обьекты для вкладки "Играть"
    [SerializeField] Button Play;
    [SerializeField] MapDataBase ListMap;
    [SerializeField] GameObject MapIcon;
    [SerializeField] GameObject ContentObjMaps;
    [SerializeField] GameObject MultiplayerOptions;
    [SerializeField] Toggle isMultiplayer;
    [SerializeField] InputField NameServer;
    [SerializeField] InputField CountPlayer;
    [SerializeField] InputField Password;
    [SerializeField] Button StartGame;

    [Header("Server")]//Обьекты для вкладки сервер
    [SerializeField] Button Connect;
    [SerializeField] GameObject ContentServerList;
    [SerializeField] GameObject ServerItem;
    [SerializeField] InputField PasswordConnect;
    [SerializeField] Button UpdateListServers;

    [HideInInspector]
    public MapItem TakeMap;

    [HideInInspector]
    public ServerItem TakeServer;

    private List<GameObject> IconMaps = new List<GameObject>();//Список карт

    private List<GameObject> Servers = new List<GameObject>();//Список серверов

    bool is_connect_master = false;//Подключился ли игрок к мастер клиенту. Для подключение и создания серверов

    private string map_connet; //Карта которая будет на сервере если подключение или создание сервера пройдет успешно.

    private bool is_connect_singleplay = false; //Хочет ли игрок зайти в одиночную игру

    List<RoomInfo> ListServerUpdate;
    private void Awake() {
        PhotonCreateInfo();

        LoadListMap();

        UpdateServerList();
        isMultiplayer.onValueChanged.AddListener(delegate { GetSetMultiplayerOptions(); });
        UpdateListServers.onClick.AddListener(delegate { UpdateServerList(); });
        Connect.onClick.AddListener(delegate { ConnectRoom(); });
        StartGame.onClick.AddListener(delegate { StartGameLobby(); });
        }

    private void PhotonCreateInfo() {//Создает информацыю о клиенте
        PhotonNetwork.NickName="Player: "+Random.Range(1000, 10000);
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.GameVersion="1";
        PhotonNetwork.AutomaticallySyncScene=true;
        }

    public override void OnConnectedToMaster() {
        is_connect_master=true;//Подтверждение что клиент подключился
        PhotonNetwork.JoinLobby();
        }

    public override void OnJoinedLobby() {
        }

    public override void OnDisconnected(DisconnectCause cause) {
        if (is_connect_singleplay)
            SceneManager.LoadScene(map_connet);
        }

    public override void OnJoinedRoom() {//Загрузка карты
        SceneManager.LoadScene(map_connet);
        }

    private void LoadMapSinglePlayer() {//Загрузка карты в одиночном режиме
        if (is_connect_master) {
            PhotonNetwork.Disconnect();
            }
        else {
            PhotonNetwork.AutomaticallySyncScene=false;
            SceneManager.LoadScene(map_connet);
            }
        }

    private void StartGameLobby() {//Создает игру либо одиночную либо сервер
        if (isMultiplayer.isOn)
            CreateServer();
        else {
            is_connect_singleplay=true;
            map_connet=TakeMap.CodMap;
            LoadMapSinglePlayer();
            }
        }

    private void CreateServer() {//Создание сервера если игрок выбрал создать многопользовательскую игру
        if (is_connect_master&&isMultiplayer.isOn) {
            ExitGames.Client.Photon.Hashtable prop = new ExitGames.Client.Photon.Hashtable();
            prop.Add("Map", TakeMap.NameMap);//Указание в масив дополнительных свойств карту на сервере.
            prop.Add("Psw", Password.text);//Указания в масив пароля от сервера.
            prop.Add("CodMap", TakeMap.CodMap);//Указания в масив код карты в файлах
            RoomOptions room = new RoomOptions();
            room.CustomRoomProperties=prop;
            room.MaxPlayers=byte.Parse(CountPlayer.text);
            PhotonNetwork.CreateRoom(NameServer.text, new Photon.Realtime.RoomOptions
                {
                CustomRoomProperties=prop,
                CustomRoomPropertiesForLobby=new string[3] { "Map", "Psw", "CodMap" },//Ключи доступных для всех перменных в масиве с кастомными параметрами для сервера
                MaxPlayers=byte.Parse(CountPlayer.text),
                });
            map_connet=TakeMap.CodMap;
            }
        }

    private void ConnectRoom() {//Подключение к серверу
        if (is_connect_master&&TakeServer&&TakeServer.CheckPassword(PasswordConnect.text)) {
            PhotonNetwork.JoinRoom(TakeServer.Server.Name);
            map_connet=(string)TakeServer.Server.CustomProperties[3];
            }
        }

    public override void OnRoomListUpdate(List<RoomInfo> roomList) {//Указания списка сервера если он обновился
        ListServerUpdate=roomList;
        }

    private void LoadListMap() {//Загрузка списка карты
        MapDataItem[] maps = ListMap.ListMaps;

        for (int i = 0; i<maps.Length; i++) {
            GameObject obj = Instantiate(MapIcon, ContentObjMaps.transform);
            obj.GetComponent<MapItem>().Init(maps[i], this);
            IconMaps.Add(obj);
            }
        IconMaps[0].GetComponent<MapItem>().TakeMap();
        }

    private void UpdateServerList() {//Обновить список серверов
        if (is_connect_master) {
            for (int i = 0; i<Servers.Count; i++) {
                Destroy(Servers[i]);
                }
            if (ListServerUpdate!=null) {
                for (int i = 0; i<ListServerUpdate.Count; i++) {
                    GameObject obj = Instantiate(ServerItem, ContentServerList.transform);
                    ServerItem server = obj.GetComponent<ServerItem>();
                    server.Init(ListServerUpdate[i], this);
                    Servers.Add(obj);
                    }
                if (Servers.Count>0) {
                    Servers[0].GetComponent<ServerItem>().TakeServer();
                    }
                }
            }
        }

    private void GetSetMultiplayerOptions() {//Включить / выключить настройки для создания сервера
        if (isMultiplayer.isOn) {
            MultiplayerOptions.SetActive(true);
            }
        else {
            MultiplayerOptions.SetActive(false);
            }
        }
    }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingTest : MonoBehaviour {
    [SerializeField] private GameObject Rigid;

    [SerializeField] private Vector2 dimensions;

    [SerializeField] private WingCurves curves;

    [SerializeField] private Vector3 up;
    [SerializeField] private Vector3 forward;

    public float liftMultiplier = 1f;

    public float dragMultiplier = 1f;

    public float WingArea {
        get { return dimensions.x * dimensions.y; }
    }

    private void FixedUpdate() {
        Vector3 forceApplyPos = transform.position;

        Rigidbody _rigidbody = Rigid.GetComponent<Rigidbody>();

        Vector3 localVelocity = transform.InverseTransformDirection(_rigidbody.GetPointVelocity(transform.position));


        localVelocity.x = 0;

        Debug.DrawRay(transform.position, transform.forward * 10f, Color.green);

        Debug.DrawRay(transform.position, transform.up * 10f, Color.blue);

        Debug.DrawRay(transform.position, transform.right * 10f, Color.red);

        // Angle of attack is used as the look up for the lift and drag curves.
        float angleOfAttack = Vector3.Angle(Vector3.forward, localVelocity);
        float liftCoefficient = curves.GetLiftAtAOA(angleOfAttack);
        float dragCoefficient = curves.GetDragAtAOA(angleOfAttack);

        // Calculate lift/drag.
        float liftForce = localVelocity.sqrMagnitude * liftCoefficient * WingArea * liftMultiplier;
        float dragForce = localVelocity.sqrMagnitude * dragCoefficient * WingArea * dragMultiplier;

        // Vector3.Angle always returns a positive value, so add the sign back in.
        // liftForce *= -Mathf.Sign(Vector3.Dot(up, localVelocity));
        liftForce *= -Mathf.Sign(localVelocity.y);

        // Lift is always perpendicular to air flow.

        Vector3 liftDirection = Vector3.Cross(_rigidbody.velocity, transform.right).normalized;

        Vector3 force = ( liftDirection * liftForce ) + ( -_rigidbody.velocity.normalized * dragForce );


        //  Debug.DrawRay(transform.position, liftDirection * liftForce * 0.001f, Color.green);
        //  Debug.DrawRay(transform.position, -_rigidbody.velocity.normalized * dragForce * 0.001f, Color.blue);

        _rigidbody.AddForceAtPosition(force, forceApplyPos, ForceMode.Force);

    }
}

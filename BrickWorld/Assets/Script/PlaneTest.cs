﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneTest : MonoBehaviour {
    public Rigidbody Rigid;

    [SerializeField] float Force;

    private void FixedUpdate() {
        //  if(Rigid.velocity.sqrMagnitude < 600)
        Rigid.AddForce(transform.forward * Force, ForceMode.Impulse);
    }
}

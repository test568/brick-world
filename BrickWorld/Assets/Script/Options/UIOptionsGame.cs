﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIOptionsGame : MonoBehaviour {//Загрузка и выгрузка настроек для игры

    public static bool IsPause = false;

    public bool isMenu;//Позволяет использовать не полные возможности скрипта и изменять настройки из базового меню игры

    [SerializeField] private GameObject PanelPause;

    [SerializeField] private ControllerPlayer Player;
    [SerializeField] private Button ExitGame;
    [SerializeField] private CanvasScaler Scaler;

    [Header("Sound")]
    [SerializeField] private Slider AllVolume;
    [SerializeField] private Slider BlastVolume;
    [SerializeField] private Slider EngineVolume;
    [SerializeField] private Slider EffectVolume;

    [Header("Play")]
    [SerializeField] private GameObject MasterOption;
    [SerializeField] private GameObject ClientOption;
    [SerializeField] private Button ClearAllMyTechincClient;
    [SerializeField] private Button ClearTechincClient;

    [SerializeField] private Button RestartMaster;
    [SerializeField] private Button ClearAllTechincMaster;
    [SerializeField] private Button ClearAllMyTechincMaster;
    [SerializeField] private Button ClearTechincMaster;
    [SerializeField] private Slider TimeDayMaster;
    [SerializeField] private Slider DensityCloudsMaster;
    [SerializeField] private Toggle SimulateTimeMaster;

    [Header("Graphic")]
    [SerializeField] private Dropdown ScreenResolution;
    [SerializeField] private Dropdown ScreenMode;
    [SerializeField] private Dropdown Graphics_Level;

    [SerializeField] private Dropdown ShadowLevel;
    [SerializeField] private Dropdown DistanceLoad;
    [SerializeField] private Dropdown PostProcsesing;
    [SerializeField] private Dropdown AntiAliasing;
    [SerializeField] private Dropdown TextureQuality;
    [SerializeField] private Button SetParamsGraphic;
    [SerializeField] private PostProcessingProfile HighPost;
    [SerializeField] private PostProcessingProfile MediumPost;
    [SerializeField] private PostProcessingProfile LowPost;



    public const string name_file = "option";
    public const string format_file = "bwo";

    public OptionGame Options;

    private static GameObject Editor { get => Editor_Controller.singleton.gameObject; }

    private void Start() {//Назначения событий для кнопок
        Graphics_Level.SetValueWithoutNotify(0);
        QualitySettings.SetQualityLevel(5);


        if (!isMenu) {
            AllVolume.onValueChanged.AddListener(delegate { AudioPlayer.Audio.AllVolume = AllVolume.value; });
            EngineVolume.onValueChanged.AddListener(delegate { AudioPlayer.Audio.EngineVolume = EngineVolume.value; });
            BlastVolume.onValueChanged.AddListener(delegate { AudioPlayer.Audio.BlastVolume = BlastVolume.value; });
            EffectVolume.onValueChanged.AddListener(delegate { AudioPlayer.Audio.EffectVolume = EffectVolume.value; });

            RestartMaster.onClick.AddListener(delegate { RestartScene(); });
            ClearAllTechincMaster.onClick.AddListener(delegate { DestroyAllTechnic(); });
            ClearTechincMaster.onClick.AddListener(delegate { DestroyTechnic(); });
            TimeDayMaster.onValueChanged.AddListener(delegate { EditTime(); });
            DensityCloudsMaster.onValueChanged.AddListener(delegate { EditDensity(); });
            SimulateTimeMaster.onValueChanged.AddListener(delegate { WeatherController.Weater.SimulateTime = SimulateTimeMaster.isOn; });
            ExitGame.onClick.AddListener(delegate { Application.Quit(); });
        }

        Graphics_Level.onValueChanged.AddListener(delegate {
            if (Graphics_Level.value == 0) {
                ShadowLevel.SetValueWithoutNotify(0);
                DistanceLoad.SetValueWithoutNotify(0);
                PostProcsesing.SetValueWithoutNotify(0);
                AntiAliasing.SetValueWithoutNotify(0);
                TextureQuality.SetValueWithoutNotify(0);
            }
            else if (Graphics_Level.value == 1) {
                ShadowLevel.SetValueWithoutNotify(1);
                DistanceLoad.SetValueWithoutNotify(1);
                PostProcsesing.SetValueWithoutNotify(1);
                AntiAliasing.SetValueWithoutNotify(1);
                TextureQuality.SetValueWithoutNotify(1);
            }
            else if (Graphics_Level.value == 2) {
                ShadowLevel.SetValueWithoutNotify(2);
                DistanceLoad.SetValueWithoutNotify(2);
                PostProcsesing.SetValueWithoutNotify(2);
                AntiAliasing.SetValueWithoutNotify(2);
                TextureQuality.SetValueWithoutNotify(2);
            }
        });

        ShadowLevel.onValueChanged.AddListener(delegate { Graphics_Level.SetValueWithoutNotify(3); });
        DistanceLoad.onValueChanged.AddListener(delegate { Graphics_Level.SetValueWithoutNotify(3); });
        PostProcsesing.onValueChanged.AddListener(delegate { Graphics_Level.SetValueWithoutNotify(3); });
        AntiAliasing.onValueChanged.AddListener(delegate { Graphics_Level.SetValueWithoutNotify(3); });
        TextureQuality.onValueChanged.AddListener(delegate { Graphics_Level.SetValueWithoutNotify(3); });

        SetParamsGraphic.onClick.AddListener(delegate { Options = GetUIOption(); UpdateParams(); SaveOption(Options); });

        if (!isMenu) {
            ClearTechincClient.onClick.AddListener(delegate { CallDestroyThisTechnic(); });
            ClearTechincMaster.onClick.AddListener(delegate { CallDestroyThisTechnic(); });
            ClearAllTechincMaster.onClick.AddListener(delegate { CallDestroyAllTechnic(); });
            ClearAllMyTechincClient.onClick.AddListener(delegate { CallDestroyAllMyTechnic(); });
            ClearAllMyTechincMaster.onClick.AddListener(delegate { CallDestroyAllMyTechnic(); });
        }
        LoadOption();
    }

    private void CallDestroyThisTechnic() {
        SyncBuilds.singleton.DestroyThisTechnic();
    }

    private void CallDestroyAllTechnic() {
        SyncBuilds.singleton.DestroyAllTechnic();
    }

    private void CallDestroyAllMyTechnic() {
        SyncBuilds.singleton.DestroyAllMyTechnic();
    }

    private void Update() {//Установка паузы, обновление ползунков
        if (!isMenu) {
            if (Input.GetKeyUp(KeyCode.Escape) && !IsPause) {
                SetPause();
            }
            else if (Input.GetKeyUp(KeyCode.Escape) && IsPause) {
                SetUnPause();
            }

            TimeDayMaster.SetValueWithoutNotify(WeatherController.Weater.TimeDay / 1440f);
            DensityCloudsMaster.SetValueWithoutNotify(WeatherController.Weater.Density);
        }
    }

    public void UpdateMasterOptions(bool isMaster) {
        if (isMaster == false) {
            MasterOption.SetActive(false);
            ClientOption.SetActive(true);
        }
        else {
            MasterOption.SetActive(true);
            ClientOption.SetActive(false);
        }
    }

    private void UpdateCanvas(int width, int height) {//Обновление размеров канванса
        Scaler.referenceResolution = new Vector2(Mathf.Clamp(width, 1280, 2560), Mathf.Clamp(height, 720, 1440));
    }

    public void UpdateParams() {//Установка параметров в игре.
        if (!isMenu) {
            if (Options.ShadowLevel == LevelList.High) {
                QualitySettings.shadows = ShadowQuality.All;
                QualitySettings.shadowDistance = 150f;
            }
            else if (Options.ShadowLevel == LevelList.Medium) {
                QualitySettings.shadows = ShadowQuality.HardOnly;
                QualitySettings.shadowDistance = 30f;
            }
            else if (Options.ShadowLevel == LevelList.Low) {
                QualitySettings.shadows = ShadowQuality.Disable;
            }

            if (Options.DistanceLoad == LevelList.High) {
                QualitySettings.lodBias = 4f;
            }
            else if (Options.DistanceLoad == LevelList.Medium) {
                QualitySettings.lodBias = 3f;
            }
            else if (Options.DistanceLoad == LevelList.Low) {
                QualitySettings.lodBias = 2.5f;
            }

            if (Options.PostProcsesing == LevelList.High) {
                EffectOnCamera.effectOnCamera.PostProcessing.profile = HighPost;
                EffectOnCamera.effectOnCamera.PostProcessing.enabled = true;
            }
            else if (Options.PostProcsesing == LevelList.Medium) {
                EffectOnCamera.effectOnCamera.PostProcessing.profile = MediumPost;
                EffectOnCamera.effectOnCamera.PostProcessing.enabled = true;
            }
            else if (Options.PostProcsesing == LevelList.Low) {
                EffectOnCamera.effectOnCamera.PostProcessing.profile = LowPost;
                EffectOnCamera.effectOnCamera.PostProcessing.enabled = true;
            }
            else if (Options.PostProcsesing == LevelList.Disable) {
                EffectOnCamera.effectOnCamera.PostProcessing.enabled = false;
            }

            if (Options.AntiAliasing == AntiAliasingLevel.x8) {
                QualitySettings.antiAliasing = 8;
            }
            else if (Options.AntiAliasing == AntiAliasingLevel.x4) {
                QualitySettings.antiAliasing = 4;
            }
            else if (Options.AntiAliasing == AntiAliasingLevel.x2) {
                QualitySettings.antiAliasing = 2;
            }
            else if (Options.AntiAliasing == AntiAliasingLevel.Disable) {
                QualitySettings.antiAliasing = 0;
            }

            if (Options.TextureQuality == LevelList.High) {
                QualitySettings.masterTextureLimit = 0;
            }
            else if (Options.TextureQuality == LevelList.Medium) {
                QualitySettings.maxQueuedFrames = 1;
            }
            else if (Options.TextureQuality == LevelList.Low) {
                QualitySettings.maxQueuedFrames = 2;
            }


            AudioPlayer.Audio.AllVolume = Options.AllVolume;
            AudioPlayer.Audio.BlastVolume = Options.BlastVolume;
            AudioPlayer.Audio.EffectVolume = Options.EffectVolume;
            AudioPlayer.Audio.EngineVolume = Options.EngineVolume;
        }

        Vector2 size = GetSizeFromEnum(Options.ScreenResolution);
        Vector2 this_size = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);
        if (size != this_size) {
            Screen.SetResolution((int) size.x, (int) size.y, true);
        }
        if (Options.ScreenMode == ScreenModeGame.FullScreen) {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        }
        else if (Options.ScreenMode == ScreenModeGame.Window) {
            Screen.fullScreenMode = FullScreenMode.Windowed;
        }
        UpdateCanvas((int) size.x, (int) size.y);
    }

    public void SetPause() {//Включить паузу
        if (!IsPause) {
            if (!GameRoom.isMultiplayer)
                Time.timeScale = 0f;
            IsPause = true;
            PanelPause.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public void SetUnPause() {//Выключить паузу
        if (IsPause) {
            //if (( Player.Status!=PlayerStatus.inEdtior)) {
            //    Cursor.visible=false;
            //    Cursor.lockState=CursorLockMode.Locked;
            //    }
            //else {
            //    Cursor.visible=true;
            //    Cursor.lockState=CursorLockMode.None;
            //    }

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            if (!GameRoom.isMultiplayer)
                Time.timeScale = 1f;

            IsPause = false;
            PanelPause.SetActive(false);
        }
    }

    private void SetParams(OptionGame options) {//Принять настройки
        Options = options;
    }

    private void SetUIOption(OptionGame options) {//Установить настройки на елементы UI
        AllVolume.value = options.AllVolume;
        BlastVolume.value = options.BlastVolume;
        EffectVolume.value = options.EffectVolume;
        EngineVolume.value = options.EngineVolume;

        switch (options.ScreenResolution) {
            case ScreenResolutionGame.W2560H1440:
                ScreenResolution.SetValueWithoutNotify(0);
                break;
            case ScreenResolutionGame.W2048H1152:
                ScreenResolution.SetValueWithoutNotify(1);
                break;
            case ScreenResolutionGame.W1920H1080:
                ScreenResolution.SetValueWithoutNotify(2);
                break;
            case ScreenResolutionGame.W1900H1200:
                ScreenResolution.SetValueWithoutNotify(3);
                break;
            case ScreenResolutionGame.W1680H1050:
                ScreenResolution.SetValueWithoutNotify(4);
                break;
            case ScreenResolutionGame.W1280H1024:
                ScreenResolution.SetValueWithoutNotify(5);
                break;
            case ScreenResolutionGame.W1280H720:
                ScreenResolution.SetValueWithoutNotify(6);
                break;
            case ScreenResolutionGame.W1024H768:
                ScreenResolution.SetValueWithoutNotify(7);
                break;
            case ScreenResolutionGame.W800H600:
                ScreenResolution.SetValueWithoutNotify(8);
                break;
            case ScreenResolutionGame.W640H480:
                ScreenResolution.SetValueWithoutNotify(9);
                break;
        }
        switch (options.ScreenMode) {
            case ScreenModeGame.FullScreen:
                ScreenMode.SetValueWithoutNotify(0);
                break;
            case ScreenModeGame.Window:
                ScreenMode.SetValueWithoutNotify(1);
                break;
        }
        switch (options.GraphicsLevel) {
            case LevelList.High:
                Graphics_Level.SetValueWithoutNotify(0);
                break;
            case LevelList.Low:
                Graphics_Level.SetValueWithoutNotify(2);
                break;
            case LevelList.Medium:
                Graphics_Level.SetValueWithoutNotify(1);
                break;
            case LevelList.Disable:
                Graphics_Level.SetValueWithoutNotify(3);
                break;
        }
        switch (options.ShadowLevel) {
            case LevelList.High:
                ShadowLevel.SetValueWithoutNotify(0);
                break;
            case LevelList.Low:
                ShadowLevel.SetValueWithoutNotify(2);
                break;
            case LevelList.Medium:
                ShadowLevel.SetValueWithoutNotify(1);
                break;
        }
        switch (options.DistanceLoad) {
            case LevelList.High:
                DistanceLoad.SetValueWithoutNotify(0);
                break;
            case LevelList.Low:
                DistanceLoad.SetValueWithoutNotify(2);
                break;
            case LevelList.Medium:
                DistanceLoad.SetValueWithoutNotify(1);
                break;
        }
        switch (options.PostProcsesing) {
            case LevelList.High:
                PostProcsesing.SetValueWithoutNotify(0);
                break;
            case LevelList.Low:
                PostProcsesing.SetValueWithoutNotify(2);
                break;
            case LevelList.Medium:
                PostProcsesing.SetValueWithoutNotify(1);
                break;
            case LevelList.Disable:
                PostProcsesing.SetValueWithoutNotify(3);
                break;
        }

        switch (options.AntiAliasing) {
            case AntiAliasingLevel.x8:
                AntiAliasing.SetValueWithoutNotify(0);
                break;
            case AntiAliasingLevel.x4:
                AntiAliasing.SetValueWithoutNotify(1);
                break;
            case AntiAliasingLevel.x2:
                AntiAliasing.SetValueWithoutNotify(2);
                break;
            case AntiAliasingLevel.Disable:
                AntiAliasing.SetValueWithoutNotify(3);
                break;
        }
        switch (options.TextureQuality) {
            case LevelList.High:
                TextureQuality.SetValueWithoutNotify(0);
                break;
            case LevelList.Low:
                TextureQuality.SetValueWithoutNotify(2);
                break;
            case LevelList.Medium:
                TextureQuality.SetValueWithoutNotify(1);
                break;
        }
    }

    private OptionGame GetUIOption() {//Взять настройки с элеметов UI
        OptionGame option = new OptionGame();
        option.AllVolume = AllVolume.value;
        option.BlastVolume = BlastVolume.value;
        option.EffectVolume = EffectVolume.value;
        option.EngineVolume = EngineVolume.value;

        switch (ScreenResolution.value) {
            case 0:
                option.ScreenResolution = ScreenResolutionGame.W2560H1440;
                break;
            case 1:
                option.ScreenResolution = ScreenResolutionGame.W2048H1152;
                break;
            case 2:
                option.ScreenResolution = ScreenResolutionGame.W1920H1080;
                break;
            case 3:
                option.ScreenResolution = ScreenResolutionGame.W1900H1200;
                break;
            case 4:
                option.ScreenResolution = ScreenResolutionGame.W1680H1050;
                break;
            case 5:
                option.ScreenResolution = ScreenResolutionGame.W1280H1024;
                break;
            case 6:
                option.ScreenResolution = ScreenResolutionGame.W1280H720;
                break;
            case 7:
                option.ScreenResolution = ScreenResolutionGame.W1024H768;
                break;
            case 8:
                option.ScreenResolution = ScreenResolutionGame.W800H600;
                break;
            case 9:
                option.ScreenResolution = ScreenResolutionGame.W640H480;
                break;
        }

        switch (ScreenMode.value) {
            case 0:
                option.ScreenMode = ScreenModeGame.FullScreen;
                break;
            case 1:
                option.ScreenMode = ScreenModeGame.Window;
                break;
        }

        switch (Graphics_Level.value) {
            case 0:
                option.GraphicsLevel = LevelList.High;
                break;
            case 2:
                option.GraphicsLevel = LevelList.Low;
                break;
            case 1:
                option.GraphicsLevel = LevelList.Medium;
                break;
            case 3:
                option.GraphicsLevel = LevelList.Disable;
                break;
        }

        switch (ShadowLevel.value) {
            case 0:
                option.ShadowLevel = LevelList.High;
                break;
            case 2:
                option.ShadowLevel = LevelList.Low;
                break;
            case 1:
                option.ShadowLevel = LevelList.Medium;
                break;
        }

        switch (DistanceLoad.value) {
            case 0:
                option.DistanceLoad = LevelList.High;
                break;
            case 2:
                option.DistanceLoad = LevelList.Low;
                break;
            case 1:
                option.DistanceLoad = LevelList.Medium;
                break;
        }

        switch (PostProcsesing.value) {
            case 0:
                option.PostProcsesing = LevelList.High;
                break;
            case 2:
                option.PostProcsesing = LevelList.Low;
                break;
            case 1:
                option.PostProcsesing = LevelList.Medium;
                break;
            case 3:
                option.PostProcsesing = LevelList.Disable;
                break;
        }

        switch (AntiAliasing.value) {
            case 0:
                option.AntiAliasing = AntiAliasingLevel.x8;
                break;
            case 1:
                option.AntiAliasing = AntiAliasingLevel.x4;
                break;
            case 2:
                option.AntiAliasing = AntiAliasingLevel.x2;
                break;
            case 3:
                option.AntiAliasing = AntiAliasingLevel.Disable;
                break;
        }

        switch (TextureQuality.value) {
            case 0:
                option.TextureQuality = LevelList.High;
                break;
            case 2:
                option.TextureQuality = LevelList.Low;
                break;
            case 1:
                option.TextureQuality = LevelList.Medium;
                break;
        }

        return option;
    }

    public void LoadOption() {//Загрузить настройки из файлов
        OptionGame option;
        string path = Application.dataPath + "/" + name_file + "." + format_file;
        if (File.Exists(path)) {
            FileStream file = File.Open(path, FileMode.Open);
            option = (OptionGame) new BinaryFormatter().Deserialize(file);
            SetParams(option);
            file.Close();
        }
        else {
            option = CreateDefaultOption();
            SaveOption(option);
        }
        Options = option;
        SetUIOption(option);
        UpdateParams();
    }

    public void SaveOption(OptionGame options) {//Сохранить настройки в файл
        string path = Application.dataPath + "/" + name_file + "." + format_file;
        FileStream file = File.Create(path);
        new BinaryFormatter().Serialize(file, options);
        file.Close();
    }

    static OptionGame CreateDefaultOption() {//Создать стандартные настройки графики
        OptionGame new_option = new OptionGame();
        new_option.Language = LanguageEnum.Russian;
        new_option.AllVolume = 1f;
        new_option.BlastVolume = 1f;
        new_option.EngineVolume = 1f;
        new_option.EffectVolume = 1f;
        new_option.ScreenResolution = ScreenResolutionGame.W1920H1080;
        new_option.ScreenMode = ScreenModeGame.FullScreen;
        new_option.GraphicsLevel = LevelList.Medium;
        new_option.ShadowLevel = LevelList.Medium;
        new_option.DistanceLoad = LevelList.Medium;
        new_option.PostProcsesing = LevelList.Medium;
        new_option.AntiAliasing = AntiAliasingLevel.x4;
        new_option.TextureQuality = LevelList.Medium;
        return new_option;
    }

    private void EditTime() {//Изменить время
        WeatherController.Weater.TimeDay = 1440f * TimeDayMaster.value;
    }

    private void EditDensity() {//Изменить густоту облаков
        WeatherController.Weater.Density = DensityCloudsMaster.value;
    }

    private void RestartScene() {//Перезапустить игру
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void DestroyTechnic() {//Удалить технику
        if (ControllerPlayer.MainPlayer.Status == PlayerStatus.inChair) {
            GameObject gm = PlayerChair.ChairFromPlayer.GetComponent<DetailBuild>().gameObject;
            ControllerPlayer.MainPlayer.Status = PlayerStatus.Move;
            Destroy(gm.GetComponent<DetailBuild>().Building.gameObject);
        }
    }

    private void DestroyAllTechnic() {//Удалить всю технику
        List<GameObject> gm = new List<GameObject>();
        if (ControllerPlayer.MainPlayer.Status == PlayerStatus.inChair) {
            ControllerPlayer.MainPlayer.Status = PlayerStatus.Move;
        }
        if (SyncBuilds.singleton.Builds != null) {
            for (int i = 0; i < SyncBuilds.singleton.Builds.ToArray().Length; i++) {
                gm.Add(SyncBuilds.singleton.Builds.ToArray()[i].gameObject);
            }
            for (int i = 0; i < gm.Count; i++) {
                Destroy(gm[i]);
            }
        }
    }

    private static Vector2 GetSizeFromEnum(ScreenResolutionGame Size) {//Взять размер екрана из перечисления ScreenResolutionGame
        string text = Size.ToString();
        string x = "";
        string y = "";
        for (int i = 0; i < text.Length; i++) {
            if (text[i] == 'H') {
                x = text.Substring(1, i - 1);
                y = text.Substring(i + 1, text.Length - i - 1);
                break;
            }
        }
        return new Vector2(int.Parse(x), int.Parse(y));
    }

}

[System.Serializable]
public enum LanguageEnum {
    Russian,
    English
}

[System.Serializable]
public enum LevelList {
    High,
    Medium,
    Low,
    Disable,
}

[System.Serializable]
public enum ScreenResolutionGame {
    W2560H1440,
    W2048H1152,
    W1920H1080,
    W1900H1200,
    W1680H1050,
    W1280H1024,
    W1280H720,
    W1024H768,
    W800H600,
    W640H480,
}

[System.Serializable]
public enum ScreenModeGame {
    FullScreen,
    Window,
}

[System.Serializable]
public enum AntiAliasingLevel {
    x8,
    x4,
    x2,
    Disable,
}

[System.Serializable]
public class OptionGame {
    public LanguageEnum Language;

    public float AllVolume;
    public float BlastVolume;
    public float EngineVolume;
    public float EffectVolume;

    public ScreenResolutionGame ScreenResolution;
    public ScreenModeGame ScreenMode;
    public LevelList GraphicsLevel;
    public LevelList ShadowLevel;
    public LevelList DistanceLoad;
    public LevelList PostProcsesing;
    public AntiAliasingLevel AntiAliasing;
    public LevelList TextureQuality;
}

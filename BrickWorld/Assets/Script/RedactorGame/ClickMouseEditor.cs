﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ClickMouseEditor: MonoBehaviour {

    public EventSystem eventSystem;


    private LayerMask layer_detail;
    private bool move;

    private Editor_Controller editor { get => Editor_Controller.singleton; }

    private void Awake() {
        layer_detail=LayerMask.GetMask(new string[1] { "Detail" });
        }
    private void Update() {
        if (eventSystem.currentSelectedGameObject==null&&!editor.move_group) {
            if (Input.GetMouseButtonDown(0)) {//Проверка на то что игрок возможно нажал по обьекту
                RaycastHit hit;
                if (Physics.Raycast(RaycastCamera(), out hit, 1000f, layer_detail)) {// Игрок выбрал обьект
                    editor.TakeDetail(hit.transform.gameObject, false);
                    }
                else {
                    editor.TakeDetail(null, false);
                    }
                }
            }
        }
    private Ray RaycastCamera() {// Создание рейкаста от камеры
        Ray ray = ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        return ray;
        }
    }

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MassCenterAxis : MonoBehaviour {

    public TextMesh Text;

    const float multiplay_scale = 0.2f;

    private void Update() {
        Camera camera = EffectOnCamera.effectOnCamera.objCamera;
        transform.localScale = new Vector3(1,1,1) * (Vector3.Distance(transform.position,camera.transform.position)) * multiplay_scale;

        Text.transform.rotation = Quaternion.LookRotation( ( Text.transform.position -camera.transform.position).normalized,Vector3.up);
    }
    public void SetLook() {
        gameObject.SetActive(true);
    }

    public void UnSetLook() {
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingForceCenter : MonoBehaviour {

    const float multiplay_scale = 0.2f;

    private void Update() {
        Camera camera = EffectOnCamera.effectOnCamera.objCamera;
        transform.localScale = new Vector3(2, 2, 1) * ( Vector3.Distance(transform.position, camera.transform.position) ) * multiplay_scale;
    }
    public void SetLook() {
        gameObject.SetActive(true);
    }

    public void UnSetLook() {
        gameObject.SetActive(false);
    }
}

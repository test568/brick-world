﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingData : MonoBehaviour {

    public Vector2 Dimensions = new Vector2();

    public WingCurves Wing;

    public float LiftMultiplier = 1f;

    public float DragMultiplier = 1f;

    public float Area { get { return Dimensions.x * Dimensions.y; } }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected() {
        Matrix4x4 oldMatrix = Gizmos.matrix;

        Gizmos.color = Color.white;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(Dimensions.x, 0f, Dimensions.y));
        Gizmos.color = Color.green;
        Gizmos.DrawLine(Vector3.zero, Vector3.forward * 5f);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(Vector3.zero, Vector3.up * 5f);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(Vector3.zero, Vector3.right * 5f);


        Gizmos.matrix = oldMatrix;
    }
#endif
}

﻿using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;


public class TankWheelData : MonoBehaviour {
    public float Radius;
    public bool IsLeadTankWheel;

    [HideInInspector]
    public bool IsFullConnect = false;

    [HideInInspector]
    public List<TankWheelData> Connects = new List<TankWheelData>();

    [HideInInspector]
    public List<Transform> Points = new List<Transform>();
}

[System.Serializable]
[RussianName("Тип гусеницы")]
public enum TypeTrack {
    [RussianName("Гусеница 1")]
    Track1,
}

[System.Serializable]
[RussianName("Сторона")]
public enum Side {
    [RussianName("Лево")]
    Left,
    [RussianName("Право")]
    Right
}


[System.Serializable]
public class TankTrackEditor : TankTrack {
    public static TankWheelTrack[] FromDetailDateToTankWheel(DetailDate[] details) {
        List<TankWheelTrack> tankWheels = new List<TankWheelTrack>();
        for (int i = 0; i < details.Length; i++) {
            TankWheelTrack wheel = new TankWheelTrack();
            //    wheel.CodesTankWheels = ( (ITankWheel) details[i].DetailComponent ).CodesTankWheels;
            wheel.Height = ( (ITankWheel) details[i].DetailComponent ).Height;
            wheel.IsLeader = ( (ITankWheel) details[i].DetailComponent ).IsLead;
            wheel.Type = ( (ITankWheel) details[i].DetailComponent ).Track;
            wheel.Widht = ( (ITankWheel) details[i].DetailComponent ).Widht;
            wheel.transform = details[i].transform;
            wheel.WheelObject = details[i].transform.gameObject;
            wheel.Radius = ( (TankWheelData) details[i].ParameterDetail ).Radius;
            tankWheels.Add(wheel);
        }
        return tankWheels.ToArray();
    }

    protected override void DestroyTrackCallBack() {
        for (int i = 0; i < Wheels.Count; i++) {
            Wheels[i].Connects.Clear();
            Wheels[i].IsFullConnect = false;
            Wheels[i].Points.Clear();
            ( (ITankWheel) Wheels[i].WheelObject.GetComponent<DetailDate>().DetailComponent ).UpdateParamsNoDetected(null, false, new string[0],
                Wheels[i].Type,
                Wheels[i].Widht, Wheels[i].Height);
        }
    }

    public void UpdateMesh(float widht, float height, TypeTrack Type) {
        if (IsTrack) {
            Material mat = TrackMaterialDataBase.DataBase.Materials[TrackMaterialDataBase.DataBase.Type.IndexOf(Type)];

            this.widht = widht;
            this.height = height;
            widhtProjection = widht * 0.1f;

            meshObject.DestroyTrack();
            meshObject.CreateMesh(Wheels, widht, height, widhtProjection, mat);
            UpdateDetailTankWheel();
        }
    }

    protected override void CreateNewTrackCallBack(bool isCreate) {
        if (isCreate) {
            UpdateDetailTankWheel();
        }
    }

    private void UpdateDetailTankWheel() {
        ( (ITankWheel) Wheels[0].WheelObject.GetComponent<DetailDate>().DetailComponent ).UpdateParamsNoDetected(this, true, ( (ITankWheel) Wheels[0].WheelObject.GetComponent<DetailDate>().DetailComponent ).CodesTankWheels, Wheels[0].Type, widht, height);
        for (int i = 1; i < Wheels.Count; i++) {
            ( (ITankWheel) Wheels[i].WheelObject.GetComponent<DetailDate>().DetailComponent ).UpdateParamsNoDetected(this, false, ( (ITankWheel) Wheels[0].WheelObject.GetComponent<DetailDate>().DetailComponent ).CodesTankWheels, Wheels[0].Type, widht, height);
        }
    }

}

[System.Serializable]
public class TankTrackBuild : TankTrack {
    public void UpdateTrack() {
        meshObject.UpdateMesh();

    }

    public void MoveUpdate(float move) {
        meshObject.MoveTrack(move);
    }
}


[System.Serializable]
public class TankWheelTrack {
    public Transform transform;

    public GameObject WheelObject;

    public float Height;

    public float Widht;

    public TypeTrack Type;

    public bool IsLeader;

    public float Radius;

    // public string[] CodesTankWheels;

    public bool IsFullConnect;

    public List<TankWheelTrack> Connects = new List<TankWheelTrack>();

    public List<Transform> Points = new List<Transform>();
}

[System.Serializable]
public abstract class TankTrack {

    public bool IsTrack = false;
    public List<TankWheelTrack> Wheels = new List<TankWheelTrack>();

    public const float WidhtDivider = 2;

    public const float HeightDivider = 5;

    protected List<Transform> points = new List<Transform>();

    protected MeshTrack meshObject;

    protected const float minAnglePoints = 15f;

    protected int count_points = 0;

    protected float widht = 0f;

    protected float widhtProjection = 0f;

    protected float height = 0f;

    public static TankWheelTrack[] SortWheelsNormal(TankWheelTrack[] details) {
        TankWheelTrack leader = details[0];
        if (details.Length == 2) {
            if (leader.transform.up == details[1].transform.up) {
                return details;
            }
            else {
                return new TankWheelTrack[0];
            }
        }
        else if (details.Length > 2) {

            Plane plane1 = new Plane();
            Plane plane2 = new Plane();
            List<TankWheelTrack> new_details = new List<TankWheelTrack>();
            new_details.Add(leader);
            for (int i = 1; i < details.Length; i++) {
                plane1.Set3Points(details[0].transform.position, details[0].transform.position + details[0].transform.right * 10f, details[i].transform.position);
                plane2.Set3Points(details[0].transform.position, details[0].transform.position + details[0].transform.forward * 10f, details[i].transform.position);

                float normal1 = Mathf.Abs(Vector3.Dot(details[i].transform.up, plane1.normal));
                float normal2 = Mathf.Abs(Vector3.Dot(details[i].transform.up, plane2.normal));

                if (normal1 >= 0.92f || normal2 >= 0.92f)
                    new_details.Add(details[i]);
            }
            return new_details.ToArray();
        }
        return new TankWheelTrack[0];

    }

    public void DestroyTrack() {
        IsTrack = false;
        if (meshObject != null)
            meshObject.DestroyTrack();
        for (int i = 0; i < points.Count; i++) {
            Object.Destroy(points[i].gameObject);
        }
        for (int i = 0; i < Wheels.Count; i++) {
            Wheels[i].Connects.Clear();
            Wheels[i].Points.Clear();
            Wheels[i].IsFullConnect = false;
        }

        points.Clear();

        DestroyTrackCallBack();

        Wheels.Clear();
        meshObject = null;
        count_points = 0;
    }

    protected virtual void DestroyTrackCallBack() {
    }

    public bool CreateNewTrack(TankWheelTrack[] details) {
        if (SortListTankWheels(details)) {
            IsTrack = true;
            CreatePoints();
            SmothPoints();
            meshObject = new MeshTrack();
            Material mat = TrackMaterialDataBase.DataBase.Materials[TrackMaterialDataBase.DataBase.Type.IndexOf(Wheels[0].Type)];

            widht = Wheels[0].Widht;
            height = Wheels[0].Height;
            widhtProjection = widht * 0.1f;


            meshObject.CreateMesh(Wheels.ToList(), widht, height, widhtProjection, mat);
            CreateNewTrackCallBack(true);
            return true;
        }
        else {
            CreateNewTrackCallBack(false);
            return false;
        }
    }

    protected virtual void CreateNewTrackCallBack(bool isCreate) {

    }



    protected bool SortListTankWheels(TankWheelTrack[] wheels) {
        Vector3 pos_center = GetCenterWheels(wheels);

        TankWheelTrack max_dist_wheel = null;
        float dist = 0f;
        for (int i = 0; i < wheels.Length; i++) {
            if (dist < Vector3.Distance(wheels[i].transform.position, pos_center)) {
                dist = Vector3.Distance(wheels[i].transform.position, pos_center);
                max_dist_wheel = wheels[i];
            }
        }

        TankWheelTrack wheel_update = max_dist_wheel;
        for (int i = 0; i < wheels.Length; i++) {
            TankWheelTrack min_dist_wheel = null;
            dist = float.MaxValue;
            if (i != wheels.Length - 1) {
                for (int i2 = 0; i2 < wheels.Length; i2++) {

                    if (!wheels[i2].IsFullConnect && wheels[i2].Connects.IndexOf(wheel_update) == -1 && wheel_update != wheels[i2] && dist > Vector3.Distance(wheel_update.transform.position, wheels[i2].transform.position)) {
                        dist = Vector3.Distance(wheel_update.transform.position, wheels[i2].transform.position);
                        min_dist_wheel = wheels[i2];
                    }
                }
            }
            else {
                min_dist_wheel = max_dist_wheel;
            }

            if (min_dist_wheel == null) {
                return false;
            }

            wheel_update.Connects.Add(min_dist_wheel);
            min_dist_wheel.Connects.Add(wheel_update);
            if (wheel_update.Connects.Count > 1) {
                wheel_update.IsFullConnect = true;
            }
            if (min_dist_wheel.Connects.Count > 1) {
                min_dist_wheel.IsFullConnect = true;
            }

            wheel_update = min_dist_wheel;
            Wheels.Add(wheel_update);
        }
        return true;
    }

    private static Vector3 GetCenterWheels(TankWheelTrack[] wheels) {
        Vector3 pos_center = new Vector3();
        for (int i = 0; i < wheels.Length; i++) {
            pos_center += wheels[i].transform.position;
        }
        pos_center = pos_center / wheels.Length;
        return pos_center;
    }

    protected void CreatePoints() {
        Vector3 pos_center = GetCenterWheels(Wheels.ToArray());

        for (int i = 0; i < Wheels.Count; i++) {
            TankWheelTrack wheel1 = null;
            TankWheelTrack wheel2 = null;

            if (i != Wheels.Count - 1) {
                wheel1 = Wheels[i];
                wheel2 = Wheels[i + 1];
            }
            else {
                wheel1 = Wheels[i];
                wheel2 = Wheels[0];
            }

            Vector3 direct_wheel = wheel2.transform.position - wheel1.transform.position;
            Vector3 center_point = direct_wheel / 2f + wheel1.transform.position;

            Vector3 paralel_direct = Vector3.Cross(wheel1.transform.up, direct_wheel.normalized).normalized;

            float res_scalar = Vector3.Dot(paralel_direct, ( center_point - pos_center ).normalized);

            if (res_scalar < 0) {
                paralel_direct = -paralel_direct;
            }

            Vector3 pos1 = wheel1.transform.position + ( paralel_direct * wheel1.Radius );
            Vector3 pos2 = wheel2.transform.position + ( paralel_direct * wheel2.Radius );


            bool it = true;
            for (int i2 = 0; i2 < wheel1.Points.Count; i2++) {
                if (Vector3.Angle(wheel1.Points[i2].position - wheel1.transform.position, pos1 - wheel1.transform.position) < minAnglePoints) {
                    it = false;
                    break;
                }
            }

            if (it) {
                count_points++;
                wheel1.Points.Add(new GameObject("Point " + count_points).GetComponent<Transform>());
                points.Add(wheel1.Points[wheel1.Points.Count - 1]);
                wheel1.Points[wheel1.Points.Count - 1].transform.parent = wheel1.transform;
                wheel1.Points[wheel1.Points.Count - 1].position = pos1;
                wheel1.Points[wheel1.Points.Count - 1].rotation = Quaternion.LookRotation(paralel_direct, Vector3.Cross(paralel_direct, -Wheels[0].transform.up));
            }

            it = true;
            for (int i2 = 0; i2 < wheel2.Points.Count; i2++) {
                if (Vector3.Angle(wheel2.Points[i2].position - wheel2.transform.position, pos2 - wheel2.transform.position) < minAnglePoints) {
                    it = false;
                    break;
                }

            }

            if (it) {
                count_points++;
                wheel2.Points.Add(new GameObject("Point " + count_points).GetComponent<Transform>());
                points.Add(wheel2.Points[wheel2.Points.Count - 1]);
                wheel2.Points[wheel2.Points.Count - 1].transform.parent = wheel2.transform;
                wheel2.Points[wheel2.Points.Count - 1].position = pos2;
                wheel2.Points[wheel2.Points.Count - 1].rotation = Quaternion.LookRotation(paralel_direct, Vector3.Cross(paralel_direct, -Wheels[0].transform.up));
            }
        }
        Wheels[0].Points.Reverse();
    }

    protected void SmothPoints() {
        for (int i = 0; i < Wheels.Count; i++) {
            if (Wheels[i].Points.Count == 2) {
                float ang = Vector3.Angle(Wheels[i].Points[0].position - Wheels[i].transform.position, Wheels[i].Points[1].position - Wheels[i].transform.position);
                if (ang > 60) {
                    int count = (int) ( ang / 30f );
                    Vector3 direct1 = Wheels[i].Points[0].position - Wheels[i].transform.position;
                    Vector3 direct2 = Wheels[i].Points[1].position - Wheels[i].transform.position;
                    Vector3 cross_direct = Vector3.Cross(direct1, Wheels[i].transform.up);
                    for (int i2 = 1; i2 < count; i2++) {
                        Vector3 new_direct = new Vector3();
                        if ((int) ang == 180) {
                            if (i2 <= count / 2) {
                                new_direct = Vector3.Slerp(direct1, cross_direct, (float) i2 / (float) ( count / 2 ));
                            }
                            else {
                                new_direct = Vector3.Slerp(direct2, cross_direct, ( (float) ( i2 / 2 ) / (float) ( count / 2 ) ));
                            }
                        }
                        else {
                            new_direct = Vector3.Slerp(direct1, direct2, (float) i2 / (float) count);
                        }
                        count_points++;
                        Transform new_trans = new GameObject("Point " + count_points).GetComponent<Transform>();
                        points.Add(new_trans);
                        new_trans.position = Wheels[i].transform.position + new_direct;
                        new_trans.rotation = Quaternion.LookRotation(new_direct, Vector3.Cross(new_direct, -Wheels[0].transform.up));
                        new_trans.parent = Wheels[i].transform;
                        Wheels[i].Points.Add(new_trans);
                    }

                    Transform point_move = Wheels[i].Points[1];
                    Wheels[i].Points.Remove(point_move);
                    Wheels[i].Points.Add(point_move);
                }
            }
        }
    }

    protected class MeshTrack {
        List<Vector3> points = new List<Vector3>();
        List<int> polygons = new List<int>();
        List<Vector2> uv = new List<Vector2>();
        GameObject meshObject;
        Mesh mesh;
        Material mat;
        List<GetPosPoint> getPoints = new List<GetPosPoint>();

        const float move_track_multiplier = 0.0012f;

        float posTrack = 0f;

        delegate Vector3 GetPosPoint();

        readonly Vector2 scale_material = new Vector2(5.6f, 3.55f);

        const float widht_multiplier = 5f;

        public void CreateMesh(List<TankWheelTrack> tankWheels, float width, float height, float wightProjections, Material material) {
            meshObject = new GameObject("MeshTrack");

            Vector3 pos_center = new Vector3();
            for (int i = 0; i < tankWheels.Count; i++) {
                pos_center += tankWheels[i].transform.position;
            }
            pos_center /= tankWheels.Count;

            width /= WidhtDivider;
            height /= HeightDivider;

            mesh = new Mesh();

            float dist = 0;
            for (int i = 0; i < tankWheels.Count; i++) {
                for (int i2 = 0; i2 < tankWheels[i].Points.Count; i2++) {
                    Transform one_point = tankWheels[i].Points[i2];
                    Transform two_point = null;
                    if (i2 == tankWheels[i].Points.Count - 1) {
                        if (i == tankWheels.Count - 1) {
                            two_point = tankWheels[0].Points[0];
                        }
                        else {
                            two_point = tankWheels[i + 1].Points[0];
                        }
                    }
                    else {
                        two_point = tankWheels[i].Points[i2 + 1];
                    }



                    dist += Vector3.Distance(one_point.position, two_point.position);
                    // Debug.Log(one_point.name+"   "+two_point.name);
                    //верхния пластина
                    Vector3 point1 = meshObject.transform.InverseTransformDirection(one_point.position + ( one_point.right * width ) + ( one_point.forward * height ));
                    Vector3 point2 = meshObject.transform.InverseTransformDirection(one_point.position - ( one_point.right * width ) + ( one_point.forward * height ));
                    Vector3 point3 = meshObject.transform.InverseTransformDirection(two_point.position + ( two_point.right * width ) + ( two_point.forward * height ));
                    Vector3 point4 = meshObject.transform.InverseTransformDirection(two_point.position - ( two_point.right * width ) + ( two_point.forward * height ));

                    //нижния пластина
                    Vector3 point5 = meshObject.transform.InverseTransformDirection(one_point.position + ( one_point.right * width ) - ( one_point.forward * height ));
                    Vector3 point6 = meshObject.transform.InverseTransformDirection(one_point.position - ( one_point.right * width ) - ( one_point.forward * height ));
                    Vector3 point7 = meshObject.transform.InverseTransformDirection(two_point.position + ( two_point.right * width ) - ( two_point.forward * height ));
                    Vector3 point8 = meshObject.transform.InverseTransformDirection(two_point.position - ( two_point.right * width ) - ( two_point.forward * height ));

                    //левые боковые 
                    Vector3 point9 = meshObject.transform.InverseTransformDirection(one_point.position - ( one_point.right * width ) - ( one_point.right * wightProjections ));
                    Vector3 point10 = meshObject.transform.InverseTransformDirection(two_point.position - ( two_point.right * width ) - ( two_point.right * wightProjections ));

                    //правые боковые
                    Vector3 point11 = meshObject.transform.InverseTransformDirection(one_point.position + ( one_point.right * width ) + ( one_point.right * wightProjections ));
                    Vector3 point12 = meshObject.transform.InverseTransformDirection(two_point.position + ( two_point.right * width ) + ( two_point.right * wightProjections ));

                    if (i == 0 && i2 == 0) {

                        points.Add(point1); //Cout -12
                        points.Add(point2);//Cout -11
                        points.Add(point5);//Cout -10
                        points.Add(point6);//Cout -9
                        points.Add(point9); //Cout -8
                        points.Add(point11);//Cout -7

                        points.Add(point3);//Cout -6
                        points.Add(point4);//Cout -5
                        points.Add(point7); //Cout -4
                        points.Add(point8);//Cout -3
                        points.Add(point10);//Cout -2
                        points.Add(point12);//Cout -1


                        uv.Add(new Vector2(0, wightProjections + width));
                        uv.Add(new Vector2(0, wightProjections));
                        uv.Add(new Vector2(0, wightProjections + width));
                        uv.Add(new Vector2(0, wightProjections));
                        uv.Add(new Vector2(0, 0));
                        uv.Add(new Vector2(0, wightProjections * 2 + width));

                        uv.Add(new Vector2(dist, wightProjections + width));
                        uv.Add(new Vector2(dist, wightProjections));
                        uv.Add(new Vector2(dist, wightProjections + width));
                        uv.Add(new Vector2(dist, wightProjections));
                        uv.Add(new Vector2(dist, 0));
                        uv.Add(new Vector2(dist, wightProjections * 2 + width));






                        //добавление поинтов для того как получить нужную позицию

                        getPoints.Add(delegate { return one_point.position + ( one_point.right * width ) + ( one_point.forward * height ); });
                        getPoints.Add(delegate { return one_point.position - ( one_point.right * width ) + ( one_point.forward * height ); });
                        getPoints.Add(delegate { return one_point.position + ( one_point.right * width ) - ( one_point.forward * height ); });
                        getPoints.Add(delegate { return one_point.position - ( one_point.right * width ) - ( one_point.forward * height ); });
                        getPoints.Add(delegate { return one_point.position - ( one_point.right * width ) - ( one_point.right * wightProjections ); });
                        getPoints.Add(delegate { return one_point.position + ( one_point.right * width ) + ( one_point.right * wightProjections ); });

                        getPoints.Add(delegate { return two_point.position + ( two_point.right * width ) + ( two_point.forward * height ); });
                        getPoints.Add(delegate { return two_point.position - ( two_point.right * width ) + ( two_point.forward * height ); });

                        getPoints.Add(delegate { return two_point.position + ( two_point.right * width ) - ( two_point.forward * height ); });
                        getPoints.Add(delegate { return two_point.position - ( two_point.right * width ) - ( two_point.forward * height ); });
                        getPoints.Add(delegate { return two_point.position - ( two_point.right * width ) - ( two_point.right * wightProjections ); });
                        getPoints.Add(delegate { return two_point.position + ( two_point.right * width ) + ( two_point.right * wightProjections ); });
                    }
                    else {
                        points.Add(point3);//Cout -6
                        points.Add(point4);//Cout -5
                        points.Add(point7); //Cout -4
                        points.Add(point8);//Cout -3
                        points.Add(point10);//Cout -2
                        points.Add(point12);//Cout -1

                        uv.Add(new Vector2(dist, wightProjections + width));
                        uv.Add(new Vector2(dist, wightProjections));
                        uv.Add(new Vector2(dist, wightProjections + width));
                        uv.Add(new Vector2(dist, wightProjections));
                        uv.Add(new Vector2(dist, 0));
                        uv.Add(new Vector2(dist, wightProjections * 2 + width));

                        //добавление поинтов для того как получить нужную позицию
                        getPoints.Add(delegate { return two_point.position + ( two_point.right * width ) + ( two_point.forward * height ); });
                        getPoints.Add(delegate { return two_point.position - ( two_point.right * width ) + ( two_point.forward * height ); });

                        getPoints.Add(delegate { return two_point.position + ( two_point.right * width ) - ( two_point.forward * height ); });
                        getPoints.Add(delegate { return two_point.position - ( two_point.right * width ) - ( two_point.forward * height ); });
                        getPoints.Add(delegate { return two_point.position - ( two_point.right * width ) - ( two_point.right * wightProjections ); });
                        getPoints.Add(delegate { return two_point.position + ( two_point.right * width ) + ( two_point.right * wightProjections ); });
                        //  Vector2 uv1 = new Vector2(Vector3.Distance(points[points.Count-4], points[points.Count-2])+uv[uv.Count-2].x, 0);
                        //  Vector2 uv2 = new Vector2(Vector3.Distance(points[points.Count-4], points[points.Count-2])+uv[uv.Count-1].x, width);
                        //  uv.Add(uv1);
                        //   uv.Add(uv2);
                    }
                    #region установка полигонов
                    //нижния пластина
                    polygons.Add(points.Count - 10);
                    polygons.Add(points.Count - 4);
                    polygons.Add(points.Count - 3);

                    polygons.Add(points.Count - 3);
                    polygons.Add(points.Count - 9);
                    polygons.Add(points.Count - 10);

                    //верхния пластина
                    polygons.Add(points.Count - 12);
                    polygons.Add(points.Count - 11);
                    polygons.Add(points.Count - 6);

                    polygons.Add(points.Count - 6);
                    polygons.Add(points.Count - 11);
                    polygons.Add(points.Count - 5);

                    //левые боковые
                    polygons.Add(points.Count - 8);
                    polygons.Add(points.Count - 5);
                    polygons.Add(points.Count - 11);

                    polygons.Add(points.Count - 2);
                    polygons.Add(points.Count - 5);
                    polygons.Add(points.Count - 8);

                    polygons.Add(points.Count - 9);
                    polygons.Add(points.Count - 2);
                    polygons.Add(points.Count - 8);

                    polygons.Add(points.Count - 9);
                    polygons.Add(points.Count - 3);
                    polygons.Add(points.Count - 2);


                    //правые боковые
                    polygons.Add(points.Count - 12);
                    polygons.Add(points.Count - 6);
                    polygons.Add(points.Count - 7);

                    polygons.Add(points.Count - 6);
                    polygons.Add(points.Count - 1);
                    polygons.Add(points.Count - 7);

                    polygons.Add(points.Count - 7);
                    polygons.Add(points.Count - 1);
                    polygons.Add(points.Count - 10);

                    polygons.Add(points.Count - 1);
                    polygons.Add(points.Count - 4);
                    polygons.Add(points.Count - 10);
                    #endregion


                }
            }
            // 2 6 7
            Vector3 vec1 = points[6] - points[2];
            Vector3 vec2 = points[6] - points[7];
            Vector3 direct_trip = meshObject.transform.TransformPoint(points[2]) - meshObject.transform.TransformPoint(GetCenterWheels(tankWheels.ToArray()));

            float delta_error = Vector3.Dot(Vector3.Cross(vec1, vec2), direct_trip);

            if (delta_error < 0 && tankWheels.Count > 2) {//если полигон повернут не в ту сторону, тогда все полигоны выворачиваються
                for (int i = 0; i < polygons.Count; i += 3) {
                    int num = polygons[i];
                    polygons[i] = polygons[i + 2];
                    polygons[i + 2] = num;
                }
            }

            MeshRenderer mr = meshObject.AddComponent<MeshRenderer>();
            MeshFilter mf = meshObject.AddComponent<MeshFilter>();
            mesh.vertices = points.ToArray();
            mesh.triangles = polygons.ToArray();
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.uv = uv.ToArray();
            mf.sharedMesh = mesh;
            mr.material = new Material(material);
            mat = mr.material;


            mat.SetTextureScale("_MainTex", Vector3.Scale(scale_material, new Vector2(1, 1 / ( width * widht_multiplier ))));
        }

        public void UpdateMesh() {
            for (int i = 0; i < mesh.vertexCount; i++) {
                points[i] = meshObject.transform.InverseTransformPoint(getPoints[i]());
            }
            mesh.vertices = points.ToArray();
            mesh.triangles = polygons.ToArray();
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
        }

        public void MoveTrack(float target_speed) {
            posTrack -= target_speed * move_track_multiplier;
            mat.SetTextureOffset("_MainTex", new Vector2(posTrack, 0));
        }

        public void DestroyTrack() {
            if (meshObject)
                Object.Destroy(meshObject);
            polygons.Clear();
            points.Clear();
            getPoints.Clear();
            uv.Clear();

        }
        ~MeshTrack() {
            DestroyTrack();
        }
    }
}
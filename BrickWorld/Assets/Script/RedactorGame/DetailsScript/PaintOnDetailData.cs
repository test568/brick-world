﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PaintOnDetailData: MonoBehaviour {
    public TextMeshPro Text;
    public SpriteRenderer Sprite;
    public float MinSize;
    public float MaxSize;
    }

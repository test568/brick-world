﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using HighlightPlus;
using System.Linq;



public class TypeBracingDate {
    public string Name;
    public Vector3[] BracingPos;
    public Vector3[] BracingDirection;
    public GameObject[] Bracing;
}

public class DetailDate : MonoBehaviour {

    public enum TypeDetail {

        [ClassTypeDetail(typeof(StandartD))]
        StandartD,

        [ClassTypeDetail(typeof(Chair))]
        Chair,

        [ClassTypeDetail(typeof(Lamp))]
        Lamp,

        [ClassTypeDetail(typeof(Axis))]
        Axis,

        [ClassTypeDetail(typeof(WheelParams))]
        Wheel,

        [ClassTypeDetail(typeof(EngineParams))]
        Engine,

        [ClassTypeDetail(typeof(Gearbox4ST_R))]
        Gearbox,

        [ClassTypeDetail(typeof(DifferentialParam))]
        Differential,

        [ClassTypeDetail(typeof(Scalabe))]
        Scalabe,

        [ClassTypeDetail(typeof(FuelTankParams))]
        FuelTank,

        [ClassTypeDetail(typeof(BeepParams))]
        Beep,

        [ClassTypeDetail(typeof(ConnectorBaseParams))]
        ConnectorBase,

        [ClassTypeDetail(typeof(StandartD))]
        ConnectorCon,

        [ClassTypeDetail(typeof(PaintOnDetail))]
        Paint,

        [ClassTypeDetail(typeof(ChipSpeedometer))]
        ChipSpeedometer,

        [ClassTypeDetail(typeof(ExtractorParams))]
        Extractor,

        [ClassTypeDetail(typeof(WheelAxis))]
        WheelAxis,

        [ClassTypeDetail(typeof(TankWheelParams))]
        TankWheel,

        [ClassTypeDetail(typeof(WingParams))]
        Wing,

        [ClassTypeDetail(typeof(PropellerParams))]
        Propeller,

        [ClassTypeDetail(typeof(PropellerAxisParams))]
        PropellerAxis,

        [ClassTypeDetail(typeof(AileronParams))]
        Aileron
    }


    [Header("DetailSprite")]
    public string Name;
    public GroupBase Group;
    public Sprite Sprite;


    [Header("Misc")]
    [Space(20)]
    public string[] TypeBracing;
    public bool Blocked;
    public bool TakeDetailMove;
    public MeshRenderer DetailMesh;
    public MeshRenderer[] MinorMeshes;

    public HighlightEffect Highlight;


    [HideInInspector]
    public TypeBracingDate[] Date;

    public string CodeDetail;

    public TypeDetail Type;

    [Header("Axis Option")]
    public Vector3 LocalAxisPos;

    public Vector3 LocalAxisRot;

    //[HideInInspector]
    public string GenerateCod;

    public MonoBehaviour ParameterDetail;

    public DetailStandart DetailComponent {
        get {
            if (_detail_component == null) {
                MemberInfo enum_member = Type.GetType().GetMember(Type.GetType().GetEnumName(Type))[0];

                Type type = ( (ClassTypeDetail) enum_member.GetCustomAttribute(typeof(ClassTypeDetail), false) ).Class;
                LoadParameter((DetailStandart) Activator.CreateInstance(type), false);
                return _detail_component;

            }
            return _detail_component;
        }
        set {
            LoadParameter(value, true);
        }
    }

    private DetailStandart _detail_component;

    public GameObject Editor { get; set; }

    public bool isHide { get; private set; }

    public int SetHighlight {
        get { return _set_highlight; }
        set {
            if (value != _set_highlight && Highlight) {
                TakeDetailMove = false;
                if (value == 0) {
                    Highlight.SetHighlighted(false);
                    Highlight.enabled = false;
                }
                else if (value == 1) {
                    Highlight.enabled = true;
                    Highlight.outlineColor = Color.yellow;
                    Highlight.SetHighlighted(true);
                }
                else if (value == 2) {
                    Highlight.enabled = true;
                    Highlight.outlineColor = Color.green;
                    Highlight.SetHighlighted(true);
                    TakeDetailMove = true;
                }
                _set_highlight = value;
            }
        }
    }

    int _set_highlight;

    private Vector3 old_pos;

    private Quaternion olr_rot;


    private void Awake() {
        GenerateCod = CodeGenerate();

        Date = new TypeBracingDate[TypeBracing.Length];

        for (int i = 0; i < TypeBracing.Length; i++) {
            TypeBracingDate date = new TypeBracingDate();
            List<Vector3> pos = new List<Vector3>();
            List<Vector3> diirection = new List<Vector3>();
            List<GameObject> objs = new List<GameObject>();
            date.Name = TypeBracing[i];
            for (int i2 = 0; i2 < gameObject.transform.childCount; i2++) {
                if (gameObject.transform.GetChild(i2).name.IndexOf(TypeBracing[i]) != -1) {
                    pos.Add(gameObject.transform.GetChild(i2).transform.localPosition);
                    diirection.Add(gameObject.transform.GetChild(i2).transform.localRotation.eulerAngles);
                    objs.Add(gameObject.transform.GetChild(i2).gameObject);
                }
            }
            date.BracingDirection = new Vector3[diirection.Count];
            date.BracingPos = new Vector3[pos.Count];
            date.Bracing = new GameObject[objs.Count];
            for (int i2 = 0; i2 < pos.Count; i2++) {
                objs[i2].name = date.Name + " " + i2;
                date.BracingDirection[i2] = diirection[i2];
                date.BracingPos[i2] = pos[i2];
                date.Bracing[i2] = objs[i2];
            }
            Date[i] = date;
        }

        if (GetComponent<ScalabeData>()) {
            GetComponent<ScalabeData>().Load();
        }
    }
    private string CodeGenerate() {
        return "" + UnityEngine.Random.Range(long.MinValue, long.MaxValue);
    }

    public void HideDetail() {
        gameObject.layer = LayerMask.NameToLayer("HideDetail");
        DetailMesh.enabled = false;
        for (int i = 0; i < MinorMeshes.Length; i++) {
            MinorMeshes[i].enabled = false;
        }
        SetHighlight = 0;
        isHide = true;
    }
    public void UnHideDetail() {
        gameObject.layer = LayerMask.NameToLayer("Detail");
        DetailMesh.enabled = true;
        for (int i = 0; i < MinorMeshes.Length; i++) {
            MinorMeshes[i].enabled = true;
        }
        SetHighlight = 0;
        isHide = false;
    }

    public void LoadParameter(DetailStandart type, bool isPassParameters) {

        if (isPassParameters) {
            _detail_component = (DetailStandart) Activator.CreateInstance(type.GetType());

            _detail_component.Detail = gameObject;
            _detail_component.Editor = Editor;

            _detail_component.Cod = type.Cod;
            for (int i = 0; i < type.GetType().GetProperties().Length; i++) {
                if (_detail_component.GetType().GetProperties()[i].CanWrite) {
                    try {
                        PropertyInfo parameter = _detail_component.GetType().GetProperties()[i];
                        _detail_component.GetType().GetProperties()[i].SetValue(_detail_component, type.GetType().GetProperties()[i].GetValue(type));
                    }
                    catch {
                    }
                }
            }
        }
        else {
            _detail_component = type;
            _detail_component.Detail = gameObject;
            _detail_component.Editor = Editor;
        }
    }

    private void LateUpdate() {
        if (old_pos != transform.position || olr_rot != transform.rotation) {
            old_pos = transform.position;
            olr_rot = transform.rotation;
            DetailComponent.UpdateTrasform();
        }
    }

    public void StartInEditorBuild() {
        old_pos = transform.position;
        olr_rot = transform.rotation;
        DetailComponent.StartInEditorBuilds();
    }

    public DetailStandart CopyDetailComponent() {
        return (DetailStandart) DetailComponent.Clone();
    }

    [Serializable]

    private class StandartD : DetailStandart, IStandartDetail {

    }

    [Serializable]
    [HaveWireConnect]
    private class Chair : DetailStandart, IStandartDetail, IChair {

        public WireConnect Speed { get; set; }

        public WireConnect Gear { get; set; }

        public WireConnect RPM { get; set; }

        public WireConnect FButton { get; set; }

        public WireConnect AButton { get; set; }

        public WireConnect DButton { get; set; }

        public WireConnect WButton { get; set; }

        public WireConnect SButton { get; set; }


        public WireConnect Steering { get; set; }

        public WireConnect Throttle { get; set; }

        public WireConnect Brake { get; set; }

        public WireConnect GearShift { get; set; }

        public WireConnect EngineActive { get; set; }

        public WireConnect TechnicalMode { get; set; }

        public WireConnect FrontLight { get; set; }

        public WireConnect RearLight { get; set; }
    }

    [Serializable]
    [HaveWireConnect]
    private class Lamp : DetailStandart, IStandartDetail, ILamp {

        public float Distance {
            get => _distance;
            set => _distance = Mathf.Max(0, value);
        }
        float _distance;

        public float Angle {
            get {
                if (_angle < 1f || _angle > 180f && _angle != 360f) {
                    _angle = 1f;
                }
                else if (_angle == 360f) {
                    _angle = 360f;
                }
                return _angle;
            }
            set {
                if (value < 1f || value > 180f && value != 360f) {
                    _angle = 1f;
                }
                else if (value == 360f) {
                    _angle = 360f;
                }
                else {
                    _angle = value;
                }
            }
        }
        float _angle;

        public float Intensive {
            get => _intensive;
            set => _intensive = Mathf.Max(0, value);
        }
        float _intensive;

        public ColorSerializable LampColor {
            get {
                if (_lamp_color == new Color()) {
                    _lamp_color = new Color(1, 1, 1, 1);
                }
                return new ColorSerializable(_lamp_color);
            }
            set {
                _lamp_color = ColorSerializable.ToColor(value);
            }
        }

        Color _lamp_color;
        public WireConnect Active { get; set; }
    }

    [Serializable]
    [HaveWireConnect]
    private class Axis : DetailStandart, IStandartDetail, IAxis, ISpring, ILimits, IRotateAxis {
        public bool Spring { get; set; }

        public float Elasticity {
            get => _elasticity;
            set => _elasticity = Mathf.Max(0, value);
        }

        float _elasticity;

        public float Damping {
            get => _damping;
            set => _damping = Mathf.Max(0, value);
        }

        float _damping;

        public bool Active { get; set; }

        public float Min {
            get => _min;
            set => _min = Mathf.Clamp(value, -180f, 180f);
        }
        float _min;

        public float Max {
            get => _max;
            set => _max = Mathf.Clamp(value, -180f, 180f);
        }

        float _max;

        public bool RotationDrivers { get; set; }

        public float SpeedRotate {
            get => _speed;
            set => _speed = Mathf.Clamp(value, 0f, 400f);
        }

        float _speed;

        public WireConnect LeftRotate { get; set; }

        public WireConnect RightRotate { get; set; }

        public bool RotateToTransmission { get; set; }

        public WireConnect GetRotate { get; set; }
    }

    [Serializable]
    private class WheelParams : DetailStandart, IStandartDetail, ITire {
        public float TireResilience {
            get => _tire_resilience;
            set => _tire_resilience = Mathf.Clamp(value, 0.1f, 1f);
        }

        float _tire_resilience = 0.8f;
    }

    [Serializable]
    [HaveWireConnect]
    private class EngineParams : DetailStandart, IStandartDetail, IEngine {

        public float EngineVolume {
            get {
                return Detail.GetComponent<EngineData>().EngineVolume;
            }

            private set {

            }
        }
        public string Fuel {
            get {
                TypeFuel t = Detail.GetComponent<EngineData>().Fuel;
                if (t == TypeFuel.Diesel) {
                    return "Дизель";
                }
                else if (t == TypeFuel.Petrol) {
                    return "Бензин";
                }
                else {
                    return "";
                }
            }
            private set {

            }
        }
        public float minRPM {
            get {
                return Detail.GetComponent<EngineData>().minRPM;
            }
            private set {

            }
        }
        public float maxRPM {
            get {
                return Detail.GetComponent<EngineData>().maxRPM;
            }
            private set {

            }
        }
        public float Power {
            get {
                return Detail.GetComponent<EngineData>().Power;
            }
            private set {

            }
        }
        public float Torque {
            get {
                return Detail.GetComponent<EngineData>().Torque;
            }
            private set {

            }
        }
        public float FuelConsumption {
            get {
                return Detail.GetComponent<EngineData>().FuelConsumption;
            }
            private set {

            }
        }

        public WireConnect Active { get; set; }

        public WireConnect Gas { get; set; }

        public WireConnect MoveRotate { get; set; }

        public WireConnect RPM { get; set; }

        public WireConnect CountFuel { get; set; }

        public WireConnect Extractor { get; set; }
    }

    [Serializable]
    public class All_R_GearboxParams : DetailStandart {
        public float Gear1 {
            get => _gear1;
            set => _gear1 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear1 = 0.1f;
        public float Gear2 {
            get => _gear2;
            set => _gear2 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear2 = 0.2f;
        public float Gear3 {
            get => _gear3;
            set => _gear3 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear3 = 0.3f;
        public float Gear4 {
            get => _gear4;
            set => _gear4 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear4 = 0.4f;
        public float Gear5 {
            get => _gear5;
            set => _gear5 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear5 = 0.5f;
        public float Gear6 {
            get => _gear6;
            set => _gear6 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear6 = 0.6f;
        public float Gear7 {
            get => _gear7;
            set => _gear7 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear7 = 0.7f;

        public float Gear8 {
            get => _gear8;
            set => _gear8 = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear8 = 0.8f;

        public float GearR {
            get => _gearR;
            set => _gearR = Mathf.Clamp(value, -1f, 1f);
        }
        float _gearR = -0.1f;

        public float MaxTorque {
            get {
                return Detail.GetComponent<GearboxData>().MaxTorque;
            }
            private set {

            }
        }
        public WireConnect GearShift { get; set; }

        public WireConnect Clutch { get; set; }

        public WireConnect GetRotate { get; set; }

        public WireConnect Rotate { get; set; }

        public WireConnect Gear { get; set; }

    }

    [Serializable]
    [HaveWireConnect]
    private class Gearbox4ST_R : All_R_GearboxParams, IStandartDetail, IGearbox4ST_R {

    }

    [Serializable]
    [HaveWireConnect]
    private class DifferentialParam : DetailStandart, IStandartDetail, IDifferentialParam {
        public float GearFinal {
            get => _gear_final;
            set => _gear_final = Mathf.Clamp(value, -1f, 1f);
        }
        float _gear_final = 1f;

        public WireConnect GetRotate { get; set; }

        public WireConnect LeftRotate { get; set; }

        public WireConnect RightRotate { get; set; }
    }

    [Serializable]
    private class Scalabe : DetailStandart, IStandartDetail, IScalabe {
        public Vector3Serializable Scale {
            get {
                return new Vector3Serializable(_scale);
            }
            set {
                _scale = ( (ScalabeData) Detail.GetComponent<DetailDate>().ParameterDetail ).SetScale(Vector3Serializable.ToVector3(value));
                _scale.x = (int) _scale.x;
                _scale.y = (int) _scale.y;
                _scale.z = (int) _scale.z;
                Detail.GetComponent<Rigidbody>().mass = ( (ScalabeData) Detail.GetComponent<DetailDate>().ParameterDetail ).BasseMass * ( _scale.x * _scale.y * _scale.z );
            }
        }
        Vector3 _scale = new Vector3(1, 1, 1);
    }

    [Serializable]
    private class FuelTankParams : DetailStandart, IStandartDetail, IFuelTank {
        public TypeFuel FuelType { get => _typeFuel; set => _typeFuel = value; }

        private TypeFuel _typeFuel;
        public float Capacity {
            get {
                return Detail.GetComponent<FuelTankData>().Capacity;
            }
        }
    }

    [Serializable]
    [HaveWireConnect]
    private class BeepParams : DetailStandart, IStandartDetail, IBeep {
        public WireConnect Activated { get; set; }
    }

    [SerializeField]
    [HaveWireConnect]
    private class ConnectorBaseParams : DetailStandart, IStandartDetail, IConnectorBase {
        public WireConnect Fasten { get; set; }

        public WireConnect Unfasten { get; set; }
    }

    [Serializable]
    private class PaintOnDetail : DetailStandart, IStandartDetail, IPaintOnDetail {

        public string Text {
            get {
                return _text;
            }
            set {
                Detail.GetComponent<PaintOnDetailData>().Text.text = value;
                _text = value;
            }
        }
        string _text;

        public float Size {
            get {
                return _size;
            }
            set {
                int v = (int) Mathf.Max(0, value);
                Detail.GetComponent<PaintOnDetailData>().Text.fontSize = v;
                _size = v;
            }
        }
        int _size = 25;

        public ColorSerializable ColorText {
            get {
                _color_text = Detail.GetComponent<PaintOnDetailData>().Text.color;
                return new ColorSerializable(_color_text);
            }
            set {
                Detail.GetComponent<PaintOnDetailData>().Text.color = ColorSerializable.ToColor(value);
                _color_text = ColorSerializable.ToColor(value);
            }
        }

        Color _color_text;

        public PaintSprite Sprite {
            get {
                return _sprite;
            }
            set {
                PaintOnDetailData data = Detail.GetComponent<PaintOnDetailData>();
                if (value.name == "null" | value.name == null | value.name == "") {
                    data.Sprite.sprite = null;
                }
                data.Sprite.sprite = SpritePaintDataBase.DataBase.FindSprite(value.name);
                _sprite = value;
            }
        }

        PaintSprite _sprite = new PaintSprite();

        public float SizeSprite {
            get {
                _size_sprite = Detail.GetComponent<PaintOnDetailData>().Sprite.transform.localScale.x;
                return _size_sprite;
            }
            set {
                PaintOnDetailData data = Detail.GetComponent<PaintOnDetailData>();
                _size_sprite = Mathf.Clamp(value, data.MinSize, data.MaxSize);
                data.Sprite.transform.localScale = new Vector3(_size_sprite, _size_sprite, _size_sprite);
            }
        }

        float _size_sprite;

        public ColorSerializable ColorSprite {
            get {
                _color_sprite = Detail.GetComponent<PaintOnDetailData>().Sprite.color;
                return new ColorSerializable(_color_sprite);
            }
            set {
                Detail.GetComponent<PaintOnDetailData>().Sprite.color = ColorSerializable.ToColor(value);
                _color_sprite = ColorSerializable.ToColor(value);
            }
        }

        Color _color_sprite;

    }

    [Serializable]
    [HaveWireConnect]
    private class ChipSpeedometer : DetailStandart, IStandartDetail, ISpeedometer {
        public WireConnect GetSpeed { get; set; }
    }

    [Serializable]
    [HaveWireConnect]
    private class ExtractorParams : DetailStandart, IStandartDetail, IExtractor {
        public WireConnect Extractor { get; set; }
    }

    [Serializable]
    [HaveWireConnect]
    private class WheelAxis : DetailStandart, IStandartDetail, IWheelAxis, ISide {
        public float DistanceSuspension {
            get {
                return _distance_suspension;
            }
            set {
                _distance_suspension = Mathf.Clamp(value, 0f, 10f);
            }
        }

        float _distance_suspension = 0.5f;

        public float Spring {
            get {
                return _spring;
            }
            set {
                _spring = Mathf.Max(0, value);
            }
        }

        float _spring = 20000f;

        public float Damper {
            get {
                return _damper;
            }
            set {
                _damper = Mathf.Max(0, value);
            }
        }

        float _damper = 1500f;

        public float SteerAngle {
            get {
                return _steer_angle;
            }
            set {
                _steer_angle = Mathf.Clamp(value, 0f, 90f);
            }
        }

        float _steer_angle = 30f;

        public float SpeedRotate {
            get {
                return _speed_rotate;
            }
            set {
                _speed_rotate = Mathf.Clamp(value, 0f, 400f);
            }
        }

        float _speed_rotate = 100f;

        public bool Revers {
            get {
                return _revers;
            }
            set {
                _revers = value;
            }
        }

        bool _revers;


        public WireConnect Steering { get; set; }

        public WireConnect Break { get; set; }

        public float ForceBreak {
            get {
                return _force_break;
            }
            set {
                _force_break = Mathf.Clamp(value, 0f, 100f);
            }
        }

        float _force_break = 50f;

        public WireConnect GetRotate { get; set; }
        public Side TrackSide { get; set; }
    }

    [Serializable]
    private class TankWheelParams : DetailStandart, IStandartDetail, ITankWheel {

        public TypeTrack Track {
            get {
                return _track;
            }
            set {
                _track = value;
                if (MeshTrack != null) {
                    MeshTrack.UpdateMesh(_widht, _height, _track);
                }

            }
        }

        TypeTrack _track = TypeTrack.Track1;

        public string[] CodesTankWheels { get; set; }

        public TankTrackEditor MeshTrack { get; set; }

        public bool IsLead { get; set; }

        public float Widht {
            get {
                return _widht;
            }
            set {

                _widht = Mathf.Clamp(value, 0.01f, 1f);
                if (MeshTrack != null) {
                    MeshTrack.UpdateMesh(_widht, _height, _track);
                }

            }
        }

        float _widht = 0.2f;

        public float Height {
            get {
                return _height;
            }
            set {
                _height = Mathf.Clamp(value, 0.01f, 1f);
                if (MeshTrack != null) {
                    MeshTrack.UpdateMesh(_widht, _height, _track);
                }

            }
        }

        float _height = 0.04f;

        public void CreateTrack(DetailDate[] details) {
            if (MeshTrack == null)
                GenerateTrack(TankTrackEditor.FromDetailDateToTankWheel(details));
        }

        public void DestroyTrack() {
            if (MeshTrack != null)
                MeshTrack.DestroyTrack();
            MeshTrack = null;
        }

        private void GenerateTrack(TankWheelTrack[] details) {
            MeshTrack = new TankTrackEditor();
            TankWheelTrack[] sort_wheels = TankTrack.SortWheelsNormal(details);
            CodesTankWheels = sort_wheels.Select((x) => x.WheelObject.GetComponent<DetailDate>().DetailComponent.Cod).ToArray();

            for (int i = 0; i < sort_wheels.Length; i++) {
                ( (ITankWheel) sort_wheels[i].WheelObject.GetComponent<DetailDate>().DetailComponent ).CodesTankWheels = CodesTankWheels;
            }


            bool is_create = MeshTrack.CreateNewTrack(sort_wheels);
            if (!is_create) {
                CodesTankWheels = new string[0];
                IsLead = false;
                MeshTrack = null;
            }
        }

        public void UpdateParamsNoDetected(TankTrackEditor Track, bool isLeadTankWheel, string[] codes_tank_wheel, TypeTrack type, float widht, float height) {
            MeshTrack = Track;
            CodesTankWheels = codes_tank_wheel;
            IsLead = isLeadTankWheel;
            _track = type;
            _widht = widht;
            _height = height;
        }

        public override void DestroyDetail() {
            DestroyTrack();
        }

        public override void StartInEditorBuilds() {
            if (MeshTrack == null) {
                if (CodesTankWheels == null) {
                    CodesTankWheels = new string[0];
                }
                List<DetailDate> details = new List<DetailDate>();
                for (int i = 0; i < Editor_Controller.singleton.DetailList.Count; i++) {
                    if (CodesTankWheels.Length == CodesTankWheels.Length) {
                        for (int i2 = 0; i2 < CodesTankWheels.Length; i2++) {
                            if (Editor_Controller.singleton.DetailList[i].DetailComponent.Cod == CodesTankWheels[i2]) {
                                details.Add(Editor_Controller.singleton.DetailList[i]);
                                break;
                            }
                        }
                    }
                    else {
                        break;
                    }
                }

                if (details.Count > 1) {
                    GenerateTrack(TankTrackEditor.FromDetailDateToTankWheel(details.ToArray()));
                }
            }
        }

        public override void UpdateHistory() {
            DestroyTrack();
        }


        public override void UpdateTrasform() {
            if (MeshTrack != null) {
                TankWheelTrack[] detail = MeshTrack.Wheels.ToArray();
                if (detail.Length > 1) {
                    DestroyTrack();
                    GenerateTrack(detail);
                }
            }
        }
    }

    [Serializable]
    private class WingParams : DetailStandart, IStandartDetail, IWing {

        public float Intensity {
            get {
                return _intensity;
            }
            set {
                _intensity = Mathf.Clamp(value, 0, 100f);
            }
        }

        float _intensity = 1f;

        public Vector3 GetForce(Vector3 velocity) {
            return Wing.CalculateForce(velocity, Detail.GetComponent<WingData>().Wing, Detail.transform, Detail.GetComponent<WingData>().Area * _intensity, 1, 1);
        }
    }

    [Serializable]
    private class PropellerParams : DetailStandart, IStandartDetail, IPropeller {
        public float CountBlade {
            get {
                return _count;
            }
            set {
                _count = Mathf.Clamp((int) value, 2, 5);
                if (blades.Count != 0) {
                    DestroyMesh();
                }
                CreateMesh();
            }
        }

        int _count = 3;

        private List<GameObject> blades = new List<GameObject>();

        private void CreateMesh() {
            Debug.Log(300);
            for (int i = 0; i < _count; i++) {
                GameObject obj = Instantiate(Detail.GetComponent<PropellerData>().PrafabBlade, Detail.transform);
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localEulerAngles = new Vector3(0, 360 / _count * i, 90);
                obj.GetComponent<MeshRenderer>().material = MaterialDataBase.DataBase.FindMaterila(TypeMaterial).Material;
                obj.GetComponent<MeshRenderer>().material.color = ColorSerializable.ToColor(ColorDetail);
                blades.Add(obj);
            }
        }

        private void UpdateMesh() {
            for (int i = 0; i < blades.Count; i++) {
                blades[i].GetComponent<MeshRenderer>().material = MaterialDataBase.DataBase.FindMaterila(TypeMaterial).Material;
                blades[i].GetComponent<MeshRenderer>().material.color = ColorSerializable.ToColor(ColorDetail);
            }
        }

        private void DestroyMesh() {
            for (int i = 0; i < blades.Count; i++) {
                Destroy(blades[i]);
            }
            blades.Clear();
        }

        public override void DestroyDetail() {
            DestroyMesh();
        }

        public override void UpdateHistory() {
            DestroyMesh();
            CreateMesh();
        }

        protected override void UpdateColor() {
            UpdateMesh();
        }

        protected override void UpdateMaterial() {
            UpdateMesh();
        }

    }

    [Serializable]
    [HaveWireConnect]
    private class PropellerAxisParams : DetailStandart, IStandartDetail, IPropellerAxis {
        public WireConnect GetRotate { get; set; }
    }


    [Serializable]
    [HaveWireConnect]
    private class AileronParams : DetailStandart, IStandartDetail, IAileron {

        protected override void UpdateColor() {
            Detail.GetComponent<AileronData>().AileronMesh.GetComponent<MeshRenderer>().material.color = ColorSerializable.ToColor(ColorDetail);
        }

        protected override void UpdateMaterial() {
            Detail.GetComponent<AileronData>().AileronMesh.GetComponent<MeshRenderer>().material = MaterialDataBase.DataBase.FindMaterila(TypeMaterial).Material;
            UpdateColor();
        }

        public Vector3 GetForce(Vector3 velocity) {
            return Wing.CalculateForce(velocity, Detail.GetComponent<AileronData>().Wing, Detail.GetComponent<AileronData>().PosRotate.transform, Detail.GetComponent<AileronData>().Area * _intensity, 1, 1);
        }

        public float Intensity {
            get {
                return _intensity;
            }
            set {
                _intensity = Mathf.Clamp(value, 0, 100f);
            }
        }

        float _intensity = 1f;

        public bool Revers { get; set; }



        public WireConnect Rotate { get; set; }
    }
}

public static class Bracing {
    public static bool CheckContactBracing(string Bracing_one, string Bracing_two) {
        if (Bracing_one == "internal_weld" && Bracing_two == "convex_weld") {
            return true;
        }
        else if (Bracing_one == "convex_weld" && Bracing_two == "internal_weld") {
            return true;
        }
        else if (Bracing_one == "wheel_axle_m" && Bracing_two == "axle_weld_m") {
            return true;
        }
        else if (Bracing_one == "axle_weld_m" && Bracing_two == "wheel_axle_m") {
            return true;
        }
        else if (Bracing_one == "base_top_axis" && Bracing_two == "base_down_axis") {
            return true;
        }
        else if (Bracing_one == "base_down_axis" && Bracing_two == "base_top_axis") {
            return true;
        }
        else if (Bracing_one == "base_down_axis_m" && Bracing_two == "base_top_axis_m") {
            return true;
        }
        else if (Bracing_one == "base_top_axis_m" && Bracing_two == "base_down_axis_m") {
            return true;
        }
        else if (Bracing_one == "axis_nano_base" && Bracing_two == "axis_nano_top") {
            return true;
        }
        else if (Bracing_one == "axis_nano_top" && Bracing_two == "axis_nano_base") {
            return true;
        }
        else if (Bracing_one == "top_nano_b_axis" && Bracing_two == "base_nano_b_axis") {
            return true;
        }
        else if (Bracing_one == "base_nano_b_axis" && Bracing_two == "top_nano_b_axis") {
            return true;
        }
        else if (Bracing_one == "connector_con" && Bracing_two == "connector_base") {
            return true;
        }
        else if (Bracing_one == "connector_base" && Bracing_two == "connector_con") {
            return true;
        }
        else if (Bracing_one == "scalabe_bracing" || Bracing_two == "scalabe_bracing") {
            return true;
        }
        else if (Bracing_one == "propeller_connect_con" && Bracing_two == "propeller_connect_base") {
            return true;
        }
        else if (Bracing_one == "propeller_connect_base" && Bracing_two == "propeller_connect_con") {
            return true;
        }
        else {
            return false;
        }
    }

    public static bool CheckDirectionBracing(string Bracing_one, string Bracing_two) {
        bool two_way_direction = false;
        if (Bracing_one == "top_nano_b_axis" && Bracing_two == "base_nano_b_axis") {
            two_way_direction = true;
        }
        else if (Bracing_one == "base_nano_b_axis" && Bracing_two == "top_nano_b_axis") {
            two_way_direction = true;
        }
        else if (Bracing_one == "scalabe_bracing" || Bracing_two == "scalabe_bracing") {
            two_way_direction = true;
        }
        return two_way_direction;
    }

    public static bool CheckIsConnector(string Bracing_one, string Bracing_two) {
        bool connector = false;
        if (Bracing_one == "connector_con" && Bracing_two == "connector_base") {
            connector = true;
        }
        else if (Bracing_one == "connector_base" && Bracing_two == "connector_con") {
            connector = true;
        }
        return connector;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


public static class MetodsUpdate {
    public static Vector3Serializable GetVector3Serializable(this Vector3 vec) {
        return new Vector3Serializable(vec.x, vec.y, vec.z);
    }

    public static QuaterionSerializable GetQuaterionSerializable(this Quaternion qua) {
        return new QuaterionSerializable { x = qua.x, y = qua.y, z = qua.z, w = qua.w };
    }
}

[System.Serializable]
public struct Vector3Serializable {
    public float x;
    public float y;
    public float z;
    public Vector3Serializable(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector3Serializable(Vector3 vector3) {
        x = vector3.x;
        y = vector3.y;
        z = vector3.z;
    }
    public static Vector3 ToVector3(Vector3Serializable vector3) {
        return new Vector3(vector3.x, vector3.y, vector3.z);
    }
    public static bool operator ==(Vector3Serializable a, Vector3Serializable b) {
        if (a.x == b.x && a.y == b.y && a.z == b.z) {
            return true;
        }
        else {
            return false;
        }
    }



    public static bool operator !=(Vector3Serializable a, Vector3Serializable b) {
        if (a.x != b.x | a.y != b.y | a.z != b.z) {
            return true;
        }
        else {
            return false;
        }
    }
    public override bool Equals(object obj) {
        Vector3Serializable col = (Vector3Serializable) obj;
        if (col.x == x && col.y == y && col.z == z) {
            return true;
        }
        else {
            return false;
        }
    }
    public override int GetHashCode() {
        return base.GetHashCode();
    }
    public override string ToString() {
        return base.ToString();
    }
}

[System.Serializable]
public struct ColorSerializable {
    public float r;
    public float g;
    public float b;
    public float a;
    public ColorSerializable(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
        a = 1;
    }
    public ColorSerializable(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
    public ColorSerializable(Color color) {
        r = color.r;
        g = color.g;
        b = color.b;
        a = color.a;
    }
    public static Color ToColor(ColorSerializable colorSerializable) {
        return new Color(colorSerializable.r, colorSerializable.g, colorSerializable.b, colorSerializable.a);
    }
    public static bool operator ==(ColorSerializable a, ColorSerializable b) {
        if (a.a == b.a && a.g == b.g && a.b == b.b && a.r == b.r) {
            return true;
        }
        else {
            return false;
        }
    }
    public static bool operator !=(ColorSerializable a, ColorSerializable b) {
        if (a.a != b.a | a.g != b.g | a.b != b.b | a.r != b.r) {
            return true;
        }
        else {
            return false;
        }
    }
    public override bool Equals(object obj) {
        ColorSerializable col = (ColorSerializable) obj;
        if (col.a == a && col.b == b && col.g == g && col.r == r) {
            return true;
        }
        else {
            return false;
        }
    }
    public override int GetHashCode() {
        return base.GetHashCode();
    }
    public override string ToString() {
        return base.ToString();
    }
}

[System.Serializable]
public struct WireConnect {
    public string CodConnectDetail;
    public PropertyInfo ConnectParameter;
}

[System.Serializable]
public struct PaintSprite {
    public string name;
}

[System.Serializable]
public abstract class DetailStandart {
    public TypeMaterialDetail TypeMaterial {
        get {
            return _type_material;
        }
        set {
            _type_material = value;
            SetMaterial(_type_material);
            WeightSet();
            BuoyancySet();
            UpdateMaterial();
        }
    }


    public ColorSerializable ColorDetail {
        get {
            if (_color == new Color()) {
                if (Detail.GetComponent<DetailDate>().Blocked) {
                    _color = Color.white;
                }
                else {
                    _color = Detail.GetComponent<DetailDate>().DetailMesh.material.color;
                }
                return new ColorSerializable(_color);
            }
            else {
                Detail.GetComponent<DetailDate>().DetailMesh.material.color = _color;
                return new ColorSerializable(_color);
            }
        }
        set {
            Detail.GetComponent<DetailDate>().DetailMesh.material.color = ColorSerializable.ToColor(value);
            _color = ColorSerializable.ToColor(value);
            UpdateColor();
        }
    }

    public float ArmorDetail {
        get {
            if (_armor_detail == -1) {
                float base_mass = Detail.GetComponent<Rigidbody>().mass;
                _armor_detail = ( base_mass / 2f ) / 1.4f / GetDensity() / ( base_mass / 2f );
                _wheight = base_mass;
            }
            return _armor_detail;
        }
        set {
            _armor_detail = Mathf.Max(1f, value);
            WeightSet();
            BuoyancySet();
        }
    }

    public float Health {
        get {
            if (_health == -1) {
                float base_mass = Detail.GetComponent<Rigidbody>().mass;
                _health = ( base_mass / 2f ) / 2.1f / GetDensity() / ( base_mass / 2f );
                _wheight = base_mass;
            }
            return _health;
        }
        set {
            _health = Mathf.Max(1f, value);
            WeightSet();
            BuoyancySet();
        }
    }

    public float Weight {
        get {
            WeightSet();
            return _wheight;
        }
        private set => _wheight = Mathf.Max(1f, value);
    }

    public float Buoyancy { get; private set; }

    public string Name {
        get {
            if (_name == "" || _name == null) {
                _name = "Деталь";
            }
            return _name;
        }
        set {
            if (value == "") {
                _name = "Деталь";
            }
            else {
                _name = value;
            }
        }
    }

    public string Cod;

    private string _name;

    private float _health = -1f;

    private float _wheight;

    private float _armor_detail = -1f;

    private TypeMaterialDetail _type_material = TypeMaterialDetail.Plastic;

    private Color _color;

    public GameObject Detail;

    public GameObject Editor;

    private void WeightSet() {
        float base_mass = Detail.GetComponent<Rigidbody>().mass;
        Weight = ( base_mass / 2 * _armor_detail * 1.4f * GetDensity() ) + ( base_mass / 2f * _health * 2.1f * GetDensity() );
    }

    private void BuoyancySet() {

    }

    private void SetMaterial(TypeMaterialDetail material) {
        Material mat = MaterialDataBase.DataBase.FindMaterila(material).Material;
        Detail.GetComponent<DetailDate>().DetailMesh.material = mat;
        ColorDetail = new ColorSerializable(ColorDetail.r, ColorDetail.g, ColorDetail.b, mat.color.a);
    }

    public virtual void StartInEditorBuilds() {

    }

    public virtual void UpdateTrasform() {

    }

    public virtual void UpdateHistory() {

    }

    public virtual void DestroyDetail() {

    }

    protected virtual void UpdateColor() {

    }

    protected virtual void UpdateMaterial() {

    }

    public object Clone() {
        return this.MemberwiseClone();
    }

    public float GetDensity() {
        return MaterialDataBase.DataBase.FindMaterila(TypeMaterial).Density;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BracingSphereSetLook : MonoBehaviour
{
    void Update()
    {
        gameObject.transform.localScale=new Vector3(1,1,1) * (Vector3.Distance(gameObject.transform.position, Camera.main.transform.position)*0.011f);
    }
}

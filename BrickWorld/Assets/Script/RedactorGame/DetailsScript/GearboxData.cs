﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearboxData: MonoBehaviour {
    public int CountGear;
    public float MaxTorque;
    public bool Automatic;
    }

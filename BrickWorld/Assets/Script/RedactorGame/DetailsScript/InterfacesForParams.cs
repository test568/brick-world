﻿
using UnityEngine;

[RussianName("Ось")]
[NumberInterface(0)]
public interface IAxis {
    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

}

[RussianName("Сиденье")]
[NumberInterface(1)]
public interface IChair {
    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Скорость")]
    [NumberProperty(1)]
    [Input("Speed", "Number")]
    WireConnect Speed { get; set; }

    [RussianName("Передача")]
    [NumberProperty(2)]
    [Input("Gear", "GearType")]
    WireConnect Gear { get; set; }

    [RussianName("Обороты двигателя")]
    [NumberProperty(3)]
    [Input("RPM", "Number")]
    WireConnect RPM { get; set; }

    [Output("GetButtonF", "FWire", "Number")]
    [NumberProperty(4)]
    [RussianName("F")]
    WireConnect FButton { get; set; }

    [Output("GetButtonA", "AWire", "Number")]
    [NumberProperty(5)]
    [RussianName("A")]
    WireConnect AButton { get; set; }

    [Output("GetButtonW", "WWire", "Number")]
    [NumberProperty(6)]
    [RussianName("W")]
    WireConnect WButton { get; set; }

    [Output("GetButtonS", "SWire", "Number")]
    [NumberProperty(7)]
    [RussianName("S")]
    WireConnect SButton { get; set; }

    [Output("GetButtonD", "DWire", "Number")]
    [NumberProperty(8)]
    [RussianName("D")]
    WireConnect DButton { get; set; }

    [Output("GetSteering", "SteeringWire", "Number")]
    [NumberProperty(9)]
    [RussianName("Рулевое управление")]
    WireConnect Steering { get; set; }

    [Output("GetThrottle", "ThrottleWire", "Number")]
    [NumberProperty(10)]
    [RussianName("Газ")]
    WireConnect Throttle { get; set; }

    [Output("GetBrake", "BrakeWire", "Number")]
    [NumberProperty(11)]
    [RussianName("Тормоз")]
    WireConnect Brake { get; set; }

    [Output("GetGearShift", "GearShiftWire", "Number")]
    [NumberProperty(12)]
    [RussianName("Смена передачи")]
    WireConnect GearShift { get; set; }

    [Output("GetEngineActive", "EngineActiveWire", "Number")]
    [NumberProperty(13)]
    [RussianName("Запуск двигателя")]
    WireConnect EngineActive { get; set; }



    [Output("GetTechnicalMode", "TechnicalModeWire", "Number")]
    [NumberProperty(14)]
    [RussianName("Техничиский режим")]
    WireConnect TechnicalMode { get; set; }

    [Output("GetFrontLight", "FrontLightWire", "Number")]
    [NumberProperty(15)]
    [RussianName("Передний свет")]
    WireConnect FrontLight { get; set; }

    [Output("GetRearLight", "RearLightWire", "Number")]
    [NumberProperty(16)]
    [RussianName("Задний свет")]
    WireConnect RearLight { get; set; }

}


[RussianName("Лампа")]
[NumberInterface(2)]
public interface ILamp {

    [RussianName("Дистанция")]
    [NumberProperty(0)]
    float Distance { get; set; }

    [RussianName("Угол")]
    [NumberProperty(1)]
    float Angle { get; set; }

    [RussianName("Интенсивность")]
    [NumberProperty(2)]
    float Intensive { get; set; }

    [RussianName("Цвет света")]
    [NumberProperty(3)]
    ColorSerializable LampColor { get; set; }

    [RussianName("Имя")]
    [NumberProperty(4)]
    string Name { get; set; }

    [Input("Active", "Number")]
    [NumberProperty(5)]
    [RussianName("Активация")]
    WireConnect Active { get; set; }
}

[RussianName("Лимит")]
[NumberInterface(3)]
public interface ILimits {

    [RussianName("Лимит")]
    [NumberProperty(0)]
    bool Active { get; set; }

    [RussianName("Минимум")]
    [NumberProperty(1)]
    float Min { get; set; }

    [RussianName("Максимум")]
    [NumberProperty(2)]
    float Max { get; set; }
}

[RussianName("Приводы поворота")]
[NumberInterface(4)]
public interface IRotateAxis {
    [RussianName("Приводы поворота")]
    [NumberProperty(0)]
    bool RotationDrivers { get; set; }

    [RussianName("Скорость вращения")]
    [NumberProperty(1)]
    float SpeedRotate { get; set; }

    [RussianName("В лево")]
    [NumberProperty(2)]
    [Input("LeftRotate", "Number")]
    WireConnect LeftRotate { get; set; }

    [RussianName("В право")]
    [NumberProperty(3)]
    [Input("RightRotate", "Number")]
    WireConnect RightRotate { get; set; }

    [RussianName("Вращение от трансмиссии")]
    [NumberProperty(4)]
    bool RotateToTransmission { get; set; }

    [RussianName("GetRotate")]
    [NumberProperty(5)]
    [Output("TransmissionUpdate", "RotateTransmission", "ForceRotate")]
    WireConnect GetRotate { get; set; }
}

[RussianName("Пружина")]
[NumberInterface(5)]
public interface ISpring {

    [RussianName("Пружина")]
    [NumberProperty(0)]
    bool Spring { get; set; }

    [RussianName("Эластичность")]
    [NumberProperty(1)]
    float Elasticity { get; set; }

    [RussianName("Затухание")]
    [NumberProperty(2)]
    float Damping { get; set; }
}

[RussianName("Стандартные параметры")]
[NumberInterface(6)]
public interface IStandartDetail {
    [RussianName("Материала")]
    [NumberProperty(0)]
    TypeMaterialDetail TypeMaterial { get; set; }
    [RussianName("Цвет")]
    [NumberProperty(1)]
    ColorSerializable ColorDetail { get; set; }
    [RussianName("Броня")]
    [NumberProperty(2)]
    float ArmorDetail { get; set; }
    [RussianName("Здоровье")]
    [NumberProperty(3)]
    float Health { get; set; }
    [RussianName("Вес")]
    [NumberProperty(4)]
    float Weight { get; }
    [RussianName("Плавучесть")]
    [NumberProperty(5)]
    float Buoyancy { get; }
}

[RussianName("Шина")]
[NumberInterface(7)]
public interface ITire {

    [RussianName("Упругость шины")]
    [NumberProperty(0)]
    float TireResilience { get; set; }
}

[RussianName("Двигатель")]
[NumberInterface(8)]
public interface IEngine {

    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Обьем двигателья л.")]
    [NumberProperty(1)]
    float EngineVolume { get; }

    [RussianName("Тип топлива")]
    [NumberProperty(2)]
    string Fuel { get; }

    [RussianName("Миним. Обороты")]
    [NumberProperty(3)]
    float minRPM { get; }

    [RussianName("Макс. Обороты")]
    [NumberProperty(4)]
    float maxRPM { get; }

    [RussianName("Мощность двигателя л. с")]
    [NumberProperty(5)]
    float Power { get; }

    [RussianName("Крутящий момент")]
    [NumberProperty(6)]
    float Torque { get; }

    [RussianName("Расход топлива л/ч")]
    [NumberProperty(7)]
    float FuelConsumption { get; }

    [RussianName("Включить/Выключить")]
    [NumberProperty(8)]
    [Input("ControllEngine", "Number")]
    WireConnect Active { get; set; }


    [RussianName("Газовать")]
    [NumberProperty(9)]
    [Input("FullGas", "Number")]
    WireConnect Gas { get; set; }

    [RussianName("Передать вращение")]
    [NumberProperty(10)]
    [Input("Rotation", "ForceRotate")]
    WireConnect MoveRotate { get; set; }

    [RussianName("RPM")]
    [NumberProperty(11)]
    [Output("GetRPM", "GetRPMWire", "Number")]
    WireConnect RPM { get; set; }

    [RussianName("CountFuel")]
    [NumberProperty(12)]
    [Output("GetCountFuel", "GetCountFuelWire", "Number")]
    WireConnect CountFuel { get; set; }

    [RussianName("Extractor")]
    [NumberProperty(13)]
    [Output("GetExtractor", "GetExtractorWire", "Effect")]
    WireConnect Extractor { get; set; }
}

[RussianName("Ручная четырехступенчатая коробка")]
[NumberInterface(9)]
public interface IGearbox4ST_R {

    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Первая передача")]
    [NumberProperty(1)]
    float Gear1 { get; set; }

    [RussianName("Вторая передача")]
    [NumberProperty(2)]
    float Gear2 { get; set; }

    [RussianName("Третия передача")]
    [NumberProperty(3)]
    float Gear3 { get; set; }

    [RussianName("Четвертая передача")]
    [NumberProperty(4)]
    float Gear4 { get; set; }

    [RussianName("Задняя передача")]
    [NumberProperty(5)]
    float GearR { get; set; }

    [RussianName("Макс вращение")]
    [NumberProperty(6)]
    float MaxTorque { get; }

    [RussianName("Переключение передачи")]
    [NumberProperty(7)]
    [Input("GearShift", "Number")]
    WireConnect GearShift { get; set; }

    [RussianName("Сцепление")]
    [NumberProperty(8)]
    [Input("Clutch", "Number")]
    WireConnect Clutch { get; set; }

    [RussianName("GetRotate")]
    [NumberProperty(9)]
    [Output("GetRotate", "GetRotateWire", "ForceRotate")]
    WireConnect GetRotate { get; set; }

    [RussianName("Передать вращение")]
    [NumberProperty(10)]
    [Input("Rotate", "ForceRotate")]
    WireConnect Rotate { get; set; }

    [RussianName("Gear")]
    [NumberProperty(11)]
    [Output("GetGear", "GetGearWire", "GearType")]
    WireConnect Gear { get; set; }


}

[RussianName("Дифференциал")]
[NumberInterface(10)]
public interface IDifferentialParam {

    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Выходная передача")]
    [NumberProperty(1)]
    float GearFinal { get; set; }

    [RussianName("GetRotate")]
    [NumberProperty(2)]
    [Output("GetRotate", "GetRotateWire", "ForceRotate")]
    WireConnect GetRotate { get; set; }

    [RussianName("Вращение на лево")]
    [NumberProperty(3)]
    [Input("LeftRotate", "ForceRotate")]
    WireConnect LeftRotate { get; set; }

    [RussianName("Вращение на право")]
    [NumberProperty(4)]
    [Input("RightRotate", "ForceRotate")]
    WireConnect RightRotate { get; set; }
}

[RussianName("Масштаб")]
[NumberInterface(11)]
public interface IScalabe {
    [RussianName("Размер")]
    [NumberProperty(0)]
    Vector3Serializable Scale { get; set; }
}

[RussianName("Топливный бак")]
[NumberInterface(12)]

public interface IFuelTank {

    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Тип топлива")]
    [NumberProperty(1)]
    TypeFuel FuelType { get; set; }

    [RussianName("Обьем")]
    [NumberProperty(2)]
    float Capacity { get; }
}

[RussianName("Гудок")]
[NumberInterface(13)]
public interface IBeep {
    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Активация")]
    [NumberProperty(1)]
    [Input("Activated", "Number")]
    WireConnect Activated { get; set; }
}

[RussianName("Коннектор")]
[NumberInterface(14)]
public interface IConnectorBase {
    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Закрепить")]
    [NumberProperty(1)]
    [Input("Fasten", "Number")]
    WireConnect Fasten { get; set; }

    [RussianName("Открепить")]
    [NumberProperty(2)]
    [Input("Unfasten", "Number")]
    WireConnect Unfasten { get; set; }
}

[RussianName("Разрисовка")]
[NumberInterface(15)]
public interface IPaintOnDetail {
    [RussianName("Текст")]
    [NumberProperty(0)]
    string Text { get; set; }

    [RussianName("Размер")]
    [NumberProperty(1)]
    float Size { get; set; }

    [RussianName("Цвет текста")]
    [NumberProperty(2)]
    ColorSerializable ColorText { get; set; }

    [RussianName("Картинка")]
    [NumberProperty(3)]
    PaintSprite Sprite { get; set; }

    [RussianName("Размер картинки")]
    [NumberProperty(4)]
    float SizeSprite { get; set; }

    [RussianName("Цвет картинки")]
    [NumberProperty(5)]
    ColorSerializable ColorSprite { get; set; }
}

[RussianName("Спидометер")]
[NumberInterface(16)]
public interface ISpeedometer {
    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Speed")]
    [NumberProperty(1)]
    [Output("GetSpeed", "GetSpeedWire", "Number")]
    WireConnect GetSpeed { get; set; }
}

[RussianName("Выхлоп")]
[NumberInterface(17)]
public interface IExtractor {
    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Выхлоп")]
    [NumberProperty(1)]
    [Input("GetExtractor", "Effect")]
    WireConnect Extractor { get; set; }
}

[RussianName("Ось")]
[NumberInterface(18)]
public interface IWheelAxis {

    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Длина подвески")]
    [NumberProperty(1)]
    float DistanceSuspension { get; set; }

    [RussianName("Пружина")]
    [NumberProperty(2)]
    float Spring { get; set; }

    [RussianName("Амортизатор")]
    [NumberProperty(3)]
    float Damper { get; set; }

    [RussianName("Скорость поворота")]
    [NumberProperty(4)]
    float SpeedRotate { get; set; }

    [RussianName("Угол поворота")]
    [NumberProperty(5)]
    float SteerAngle { get; set; }

    [RussianName("Реверс")]
    [NumberProperty(6)]
    bool Revers { get; set; }

    [RussianName("Рулевое управление")]
    [NumberProperty(7)]
    [Input("Steering", "Number")]
    WireConnect Steering { get; set; }

    [RussianName("Тормоз")]
    [NumberProperty(8)]
    [Input("Break", "Number")]
    WireConnect Break { get; set; }

    [RussianName("Сила торможения")]
    [NumberProperty(9)]
    float ForceBreak { get; set; }

    [RussianName("GetRotate")]
    [NumberProperty(10)]
    [Output("TransmissionUpdate", "RotateTransmission", "ForceRotate")]
    WireConnect GetRotate { get; set; }
}

[RussianName("Каток")]
[NumberInterface(19)]
public interface ITankWheel {
    [RussianName("Тип гусеницы")]
    [NumberProperty(0)]
    TypeTrack Track { get; set; }

    [RussianName("")]
    [NumberProperty(1)]
    [NoShow]
    [NoSave]
    TankTrackEditor MeshTrack { get; set; }

    [RussianName("")]
    [NumberProperty(2)]
    [NoShow]
    string[] CodesTankWheels { get; set; }

    [RussianName("")]
    [NumberProperty(3)]
    [NoShow]
    bool IsLead { get; set; }

    [RussianName("Ширина")]
    [NumberProperty(4)]
    float Widht { get; set; }


    [RussianName("Высота")]
    [NumberProperty(5)]
    float Height { get; set; }

    void UpdateParamsNoDetected(TankTrackEditor Track, bool isLeadTankWheel, string[] codes_tank_wheel, TypeTrack type, float widht, float height);

    void CreateTrack(DetailDate[] details);
    void DestroyTrack();


}

[RussianName("Танк")]
[NumberInterface(20)]
public interface ISide {

    [RussianName("Сторона гусеницы")]
    [NumberProperty(0)]
    Side TrackSide { get; set; }
}

[RussianName("Крыло")]
[NumberInterface(21)]
public interface IWing {

    [RussianName("Интенсивность")]
    [NumberProperty(0)]
    float Intensity { get; set; }

    Vector3 GetForce(Vector3 velocity);
}

[RussianName("Пропеллер")]
[NumberInterface(22)]
public interface IPropeller {

    [RussianName("Кол - лопастей")]
    [NumberProperty(0)]
    float CountBlade { get; set; }
}

[RussianName("Ось")]
[NumberInterface(23)]
public interface IPropellerAxis {

    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("GetRotate")]
    [NumberProperty(1)]
    [Output("TransmissionUpdate", "RotateTransmission", "ForceRotate")]
    WireConnect GetRotate { get; set; }
}

[RussianName("Элерон")]
[NumberInterface(24)]
public interface IAileron {

    [RussianName("Имя")]
    [NumberProperty(0)]
    string Name { get; set; }

    [RussianName("Интенсивность")]
    [NumberProperty(1)]
    float Intensity { get; set; }

    [RussianName("Реверс")]
    [NumberProperty(3)]
    bool Revers { get; set; }

    [RussianName("Вращение")]
    [NumberProperty(2)]
    [Input("Rotate", "Number")]
    WireConnect Rotate { get; set; }

    Vector3 GetForce(Vector3 velocity);
}
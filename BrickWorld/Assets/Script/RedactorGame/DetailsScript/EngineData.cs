﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RussianName("Тип топлива")]
public enum TypeFuel {
    [RussianName("Бензин")]
    Petrol,
    [RussianName("Дизель")]
    Diesel
    }
public class EngineData: MonoBehaviour {
    public float EngineVolume;
    public TypeFuel Fuel;
    public float minRPM;
    public float maxRPM;
    public float Power;
    public float Torque;
    public float FuelConsumption;
    public GameObject SoundEngine;
    public GameObject EffectObject;
    }

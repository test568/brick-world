﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeScalabe {
    Brick,
    Panel,
    }


public class ScalabeData: MonoBehaviour {
    public Vector3 minSize;
    public Vector3 maxSize;

    public Collider ObjectCollider;
    public GameObject Mesh;
    public GameObject ScaleObject;

    public float BasseMass;

    public TypeScalabe Type;

    private DetailDate data;

    private const float dist = 0.02f;

    public void Load() {
        BasseMass=GetComponent<Rigidbody>().mass;
        UpdateScale();
        data=GetComponent<DetailDate>();
        }

    public void UpdateScale() {
        Vector3 scale = ScaleObject.transform.localScale;
        Vector3 mesh_scale = Mesh.transform.localScale;
        try {
            ( (BoxCollider)ObjectCollider ).size=new Vector3(scale.x*mesh_scale.x-0.004f, scale.y*mesh_scale.y-0.004f, scale.z*mesh_scale.z-0.004f);
            }
        catch {

            }
        }

    public Vector3 SetScale(Vector3 scale) {
            Vector3 set_scale = new Vector3(Mathf.Clamp(scale.x, minSize.x, maxSize.x), Mathf.Clamp(scale.y, minSize.y, maxSize.y), Mathf.Clamp(scale.z, minSize.z, maxSize.z));
            ScaleObject.transform.localScale=set_scale;
            Vector3 scale_s = ScaleObject.transform.localScale;
            Vector3 mesh_scale = Mesh.transform.localScale;
            if (Type==TypeScalabe.Panel) {
                PlateBracing(scale);
                }
            try {
                ( (BoxCollider)ObjectCollider ).size=new Vector3(scale_s.x*mesh_scale.x+0.004f, scale_s.y*mesh_scale.y+0.004f, scale_s.z*mesh_scale.z+0.004f);
                }
            catch {
                }
            return set_scale;
        }

    private void PlateBracing(Vector3 scale) {
        for (int i = 0; i<data.Date[0].Bracing.Length; i++) {
            Destroy(data.Date[0].Bracing[i]);
            }

        List<GameObject> objs = new List<GameObject>();
        int count = 0;
        for (int i = 0; i<scale.x; i++) {
            for (int i2 = 0; i2<scale.z; i2++) {
                count++;
                GameObject obj = new GameObject(count+"");
                obj.transform.parent=transform;
                obj.transform.localPosition=new Vector3(( i-( scale.x/2f ) )*0.2f+0.1f, scale.y/2f*0.08f, ( i2-( scale.z/2f ) )*0.2f+0.1f);
                obj.transform.eulerAngles=new Vector3(-90f, 0, 0);
                objs.Add(obj);
                }
            }
        for (int i = 0; i<scale.x; i++) {
            for (int i2 = 0; i2<scale.z; i2++) {
                count++;
                GameObject obj = new GameObject(count+"");
                obj.transform.parent=transform;
                obj.transform.localPosition=new Vector3(( i-( scale.x/2f ) )*0.2f+0.1f, -scale.y/2f*0.08f, ( i2-( scale.z/2f ) )*0.2f+0.1f);
                obj.transform.eulerAngles=new Vector3(90f, 0, 0);
                objs.Add(obj);
                }
            }
        data.Date[0].Bracing=objs.ToArray();
        }
    }

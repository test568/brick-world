﻿using System;
using UnityEngine;

public class Attributes : MonoBehaviour {

}
internal class RussianNameAttribute : Attribute {
    public string Name { get; set; }

    public RussianNameAttribute(string text) {
        Name = text;
    }
}
internal class NoSaveAttribute : Attribute {
}
internal class NoShowAttribute : Attribute {
}
internal class InputAttribute : Attribute {
    public string NameParameter;

    public string Type;
    public InputAttribute(string NameParameter, string Type) {
        this.NameParameter = NameParameter;
        this.Type = Type;
    }
}
internal class OutputAttribute : Attribute {
    public string name_function;
    public string name_event;
    public string Type;
    public OutputAttribute(string name_function, string name_event, string Type) {
        this.name_function = name_function;
        this.name_event = name_event;
        this.Type = Type;
    }
}

internal class NumberInterfaceAttribute : Attribute {
    public byte Number;
    public NumberInterfaceAttribute(byte number) {
        this.Number = number;
    }
}

internal class NumberPropertyAttribute : Attribute {
    public byte Number;
    public NumberPropertyAttribute(byte number) {
        this.Number = number;
    }
}

internal class ClassTypeDetail : Attribute {
    public Type Class;
    public ClassTypeDetail(Type typeClass) {
        this.Class = typeClass;
    }
}

internal class HaveWireConnect : Attribute { }
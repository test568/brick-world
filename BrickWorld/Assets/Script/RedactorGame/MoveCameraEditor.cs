﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class MoveCameraEditor: MonoBehaviour {

    public EventSystem eventSystem;
    [Range(0, 40)]
    public float Speed;
    [Range(40, 140)]
    public float SpeedRotate;

    [HideInInspector]
    public bool Move;

    private bool LockCamera = false;
    private Vector3 limit;
    private Vector3 local_pos;

    private float rotY = 0.0f;
    private float rotX = 0.0f;


    private Editor_Controller Editor { get => Editor_Controller.singleton; }
    private void Start() {
        limit=Editor.LimitCameraMove;
        }

    private void OnEnable() {
        gameObject.transform.localRotation=new Quaternion();
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY=rot.y;
        rotX=rot.x;
        }
    private void Update() {
        if (!LockCamera&&eventSystem.currentSelectedGameObject==null&&Input.GetMouseButton(1)&&!Editor.move_group) {
            if (Input.GetKey(KeyCode.W)) {
                gameObject.transform.position+=gameObject.transform.forward*( Speed*Time.deltaTime );
                }
            if (Input.GetKey(KeyCode.S)) {
                gameObject.transform.position+=gameObject.transform.forward*( -Speed*Time.deltaTime );
                }
            if (Input.GetKey(KeyCode.A)) {
                gameObject.transform.position+=gameObject.transform.right*( -Speed*Time.deltaTime );
                }
            if (Input.GetKey(KeyCode.D)) {
                gameObject.transform.position+=gameObject.transform.right*( Speed*Time.deltaTime );
                }
            if (Input.GetKey(KeyCode.Space)) {
                gameObject.transform.position+=gameObject.transform.up*( Speed*Time.deltaTime );
                }
            if (Input.GetKey(KeyCode.LeftAlt)) {
                gameObject.transform.position+=gameObject.transform.up*( -Speed*Time.deltaTime );
                }

            local_pos=gameObject.transform.position-Editor.transform.position;

            gameObject.transform.position=Editor.transform.position+new Vector3(Mathf.Clamp(local_pos.x, -limit.x, limit.x),
                Mathf.Clamp(local_pos.y, -limit.y, limit.y),
                Mathf.Clamp(local_pos.z, -limit.z, limit.z));

            if (Input.GetMouseButton(1)&&Time.timeScale!=0f) {
                float mouseX = Input.GetAxis("Mouse X");
                float mouseY = -Input.GetAxis("Mouse Y");

                rotY+=mouseX*SpeedRotate*Time.deltaTime;
                rotX+=mouseY*SpeedRotate*Time.deltaTime;
                rotX=Mathf.Clamp(rotX, -90, 90);
                Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
                transform.rotation=localRotation;
                Move=true;
                }
            else {
                Move=false;
                }
            }
        if (Input.GetMouseButtonDown(1)&&!ControllerPlayer.PlayerOnPause) {
            Cursor.lockState=CursorLockMode.Confined;
            Cursor.visible=false;
            Move=true;
            }
        else if (Input.GetMouseButtonUp(1)&&!Editor.move_group) {
            Cursor.lockState=CursorLockMode.None;
            Cursor.visible=true;
            Move=false;
            }
        }

    public void TeleportCameraOnTakeDetails() {
        Bounds bounds = Editor.Bounds_group;
        float dist = bounds.size.magnitude*0.1f;
        Vector3 pos = Editor.Group.transform.position;
        Vector3 direct = ( Vector3.forward+Vector3.right+Vector3.up ).normalized;
        pos=pos+( direct*dist );
        gameObject.transform.position=pos;
        transform.rotation=Quaternion.LookRotation(( Editor.Group.transform.position-pos ), Vector3.up);
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY=rot.y;
        rotX=rot.x;
        }

    private void OnDisable() {

        gameObject.transform.rotation=Quaternion.Euler(new Vector3(0, 0, 0));
        }
    }

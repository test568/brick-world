﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class ClickKeyboardEditor : MonoBehaviour {

    public UIEditor uiEditor;
    public EventSystem eventSystem;
    public MoveCameraEditor MoveCamera;

    private Editor_Controller editor { get => Editor_Controller.singleton; }

    private void Update() {
        if (Time.timeScale != 0f) {
            if (( eventSystem.currentSelectedGameObject == null || eventSystem.currentSelectedGameObject.GetComponent<Button>() ) && !uiEditor.IsOpenPanel) {
                DeletDetail();
                StoryBreak();
                StoryNext();
                MirroringDetail();
                TakeDetailInGroup();
                PutDetailForGroup();
                CopyDetailMove();
                SaveToBufer();
                LoadToBufer();
                MoveDetail();
                if (!MoveCamera.Move) {
                    TakeCod();
                    TakeColor();
                    TakeDetailInGroup();
                    TakeMaterial();
                    TakeParameter();
                    TakeAll();
                    TakeInverse();
                    TakeBracingOne();
                    TakeBracingAll();
                    CenterMass();
                    HideIndicators();
                    HideDetails();
                    UnHideDetails();
                    CreateTrack();
                    RemoveTrack();
                }
                CameraSetOnGroupDetail();

            }
        }
    }
    private void DeletDetail() {
        if (Input.GetKeyDown(KeyCode.Delete)) {
            editor.DestroyDetal();
        }
    }
    private void StoryBreak() {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Z)) {
            editor.PreviousStory();
        }
#else
        if (Input.GetKey(KeyCode.LeftControl)&&Input.GetKeyDown(KeyCode.Z)) {
            editor.PreviousStory();
            }
#endif
    }
    private void StoryNext() {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Y)) {
            editor.FutureStory();
        }
#else
        if (Input.GetKey(KeyCode.LeftControl)&&Input.GetKeyDown(KeyCode.Y)) {
            editor.FutureStory();
            }
#endif
    }
    private void MoveDetail() {
        if (Input.GetKeyDown(KeyCode.G) && !editor.move_group) {
            editor.move_group = true;
        }
        else if (Input.GetMouseButtonUp(0) || Input.GetKeyDown(KeyCode.G) && editor.move_group) {
            editor.move_group = false;
        }
    }
    private void MirroringDetail() {
        if (Input.GetKeyDown(KeyCode.N)) {
            editor.DetailMirroring();
        }
    }

    private void CenterMass() {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.M)) {
            editor.UpdateCenterMass();
        }
    }

    private void HideIndicators() {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.K)) {
            editor.HideIndicators();
        }
    }

    private void TakeDetailInGroup() {
        if (Input.GetKey(KeyCode.LeftShift)) {
            editor.take_detail_in_groupe = true;
        }
        else {
            editor.take_detail_in_groupe = false;
        }
    }

    private void TakeColor() {
        if (Input.GetKeyDown(KeyCode.P) && !editor.move_group) {
            editor.TakeDetailColor(false);
        }
    }

    private void TakeMaterial() {
        if (Input.GetKeyDown(KeyCode.M) && !editor.move_group) {
            editor.TakeDetailMaterial(false);
        }
    }

    private void TakeParameter() {
        if (!Input.GetKeyDown(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R) && !editor.move_group) {
            editor.TakeDetailParams(false);
        }
    }

    private void TakeCod() {
        if (Input.GetKeyDown(KeyCode.B) && !editor.move_group) {
            editor.TakeDetailCod(false);
        }
    }

    private void TakeAll() {
        if (Input.GetKeyDown(KeyCode.A) && !editor.move_group && !MoveCamera.Move) {
            editor.TakeAllDetails(false);
        }
    }

    private void TakeInverse() {
        if (Input.GetKeyDown(KeyCode.I) && !editor.move_group) {
            editor.TakeInverse(false);
        }
    }

    private void UnHideDetails() {
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.H) && !editor.move_group) {
            editor.UnHideDetails(false);
        }
    }

    private void HideDetails() {
        if (Input.GetKeyDown(KeyCode.H) && !editor.move_group) {
            editor.HideDetails(false);
        }
    }

    private void PutDetailForGroup() {
        if (Input.GetKey(KeyCode.LeftControl) && !Input.GetKeyDown(KeyCode.V)) {
            editor.put_detail_for_groupe = true;
        }
        else {
            editor.put_detail_for_groupe = false;
        }
    }

    private void CopyDetailMove() {
        if (Input.GetKey(KeyCode.LeftControl)) {
            editor.copy_detail_move = true;
        }
        else {
            editor.copy_detail_move = false;
        }
    }

    private void SaveToBufer() {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C)) {
            editor.SaveBufer();
        }
    }

    private void LoadToBufer() {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V)) {
            editor.LoadBufer();
        }
    }
    private void CameraSetOnGroupDetail() {
        if (Input.GetKeyDown(KeyCode.F) && !editor.move_group) {
            MoveCamera.TeleportCameraOnTakeDetails();
        }
    }

    private void TakeBracingOne() {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.L)) {
            editor.TakeBracingDetails(false);
        }
    }
    private void TakeBracingAll() {
        if (!Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.L)) {
            editor.TakeBracingDetailsAll(false);
        }
    }

    private void CreateTrack() {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.T)) {
            editor.CreateTrack();
        }
    }

    private void RemoveTrack() {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.U)) {
            editor.DestroyTrack();
        }
    }
}

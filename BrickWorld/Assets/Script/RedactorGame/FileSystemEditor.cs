﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.Reflection;

static class FileSystemEditor {
    public static LoadFile[] LoadList() {
        if (!Directory.Exists(Application.dataPath + "/BrickWorldSave")) {
            Directory.CreateDirectory(Application.dataPath + "/BrickWorldSave");
            return new LoadFile[0];
        }
        List<LoadFile> filesList = new List<LoadFile>();
        string[] files = Directory.GetFiles(Application.dataPath + "/BrickWorldSave");

        for (int i = 0; i < files.Length; i++) {
            FileStream file;
            file = File.Open(files[i], FileMode.Open);
            //   try {
            if (file.Name.IndexOf(".bwb") != -1 && file.Name.IndexOf(".meta") == -1) {
                LoadFile lf = new LoadFile();
                lf.Name = Path.GetFileName(file.Name).Remove(Path.GetFileName(file.Name).Length - 4);

                string file_image_png = files[i].Remove(files[i].Length - 4) + ".png";
                string file_image_jpg = files[i].Remove(files[i].Length - 4) + ".jpg";
                Texture2D texture = new Texture2D(2, 2);
                byte[] file_data = new byte[0];
                if (File.Exists(file_image_png)) {
                    file_data = File.ReadAllBytes(file_image_png);
                }
                else if (File.Exists(file_image_jpg)) {
                    file_data = File.ReadAllBytes(file_image_jpg);
                }
                if (file_data.Length != 0 && texture.LoadImage(file_data)) {
                    lf.Image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0), 100.0f);
                }

                filesList.Add(lf);
            }
            // }
            // catch {
            // }
            file.Close();
        }
        return filesList.ToArray();
        // return new string[14] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14" };
    }

    public static void LoadFile(string name, Editor_Controller editor) {
        if (!Directory.Exists(Application.dataPath + "/BrickWorldSave")) {
            Directory.CreateDirectory(Application.dataPath + "/BrickWorldSave");
        }
        if (File.Exists(Application.dataPath + "/BrickWorldSave/" + name + ".bwb")) {
            FileStream file;
            file = File.Open(Application.dataPath + "/BrickWorldSave/" + name + ".bwb", FileMode.Open);
            SaveEditorBuild sv;
            sv = (SaveEditorBuild) new BinaryFormatter().Deserialize(file);
            if (sv.CodeGame == "X1-E5424-EhGfd") {
                editor.LoadBuild(sv);
            }
            file.Close();

        }
    }

    public static void RemoveFile(string name) {
        if (!Directory.Exists(Application.dataPath + "/BrickWorldSave")) {
            Directory.CreateDirectory(Application.dataPath + "/BrickWorldSave");
        }
        if (File.Exists(Application.dataPath + "/BrickWorldSave/" + name + ".bwb")) {
            File.Delete(Application.dataPath + "/BrickWorldSave/" + name + ".bwb");
        }
    }

    public static void SaveFile(string name, Editor_Controller editor) {
        if (!Directory.Exists(Application.dataPath + "/BrickWorldSave")) {
            Directory.CreateDirectory(Application.dataPath + "/BrickWorldSave");
        }
        FileStream file;
        file = File.Create(Application.dataPath + "/BrickWorldSave/" + name + ".bwb");
        SaveEditorBuild sv = new SaveEditorBuild();

        sv.DetailsCodes = new string[editor.DetailList.Count];
        sv.DetailGameGenerateCodes = new string[editor.DetailList.Count];
        sv.ParametersObject = new SaveEditorBuild.ObjectSave[editor.DetailList.Count];
        sv.TypeDetails = new Type[editor.DetailList.Count];
        sv.Position = new Vector3Serializable[editor.DetailList.Count];
        sv.Angle = new QuaterionSerializable[editor.DetailList.Count];

        for (int i = 0; i < sv.TypeDetails.Length; i++) {
            sv.DetailsCodes[i] = editor.DetailList[i].CodeDetail;
            sv.Position[i] = new Vector3Serializable(editor.DetailList[i].transform.position - editor.gameObject.transform.position);
            QuaterionSerializable q = new QuaterionSerializable();
            q.x = editor.DetailList[i].transform.rotation.x;
            q.y = editor.DetailList[i].transform.rotation.y;
            q.z = editor.DetailList[i].transform.rotation.z;
            q.w = editor.DetailList[i].transform.rotation.w;
            sv.Angle[i] = q;

            sv.TypeDetails[i] = editor.DetailList[i].DetailComponent.GetType();
            sv.ParametersObject[i] = new SaveEditorBuild.ObjectSave();
            sv.DetailGameGenerateCodes[i] = ( (DetailStandart) editor.DetailList[i].DetailComponent ).Cod;


            Type[] interafaces = editor.DetailList[i].DetailComponent.GetType().GetInterfaces();
            for (int i2 = 0; i2 < interafaces.Length; i2++) {
                PropertyInfo[] properties = interafaces[i2].GetProperties();
                for (int i3 = 0; i3 < properties.Length; i3++) {
                    if (properties[i3].CanWrite && properties[i3].GetCustomAttribute(typeof(NoSaveAttribute)) == null) {
                        sv.ParametersObject[i].NumbersInterfaces.Add(( (NumberInterfaceAttribute) interafaces[i2].GetCustomAttribute(typeof(NumberInterfaceAttribute)) ).Number);
                        sv.ParametersObject[i].NumbersPropertysfaces.Add(( (NumberPropertyAttribute) properties[i3].GetCustomAttribute(typeof(NumberPropertyAttribute)) ).Number);
                        sv.ParametersObject[i].Save.Add(properties[i3].GetValue(editor.DetailList[i].DetailComponent));
                    }
                }
            }
        }
        sv.CodeGame = "X1-E5424-EhGfd";
        new BinaryFormatter().Serialize(file, sv);
        file.Close();
    }
}

public class LoadFile {
    public string Name;
    public Sprite Image;
}

[Serializable]
public class SaveEditorBuild : ConstParameterSave {
    public string[] DetailsCodes;
    public string[] DetailGameGenerateCodes;
    public Vector3Serializable[] Position;
    public QuaterionSerializable[] Angle;

    public Type[] TypeDetails;

    public ObjectSave[] ParametersObject;
    [Serializable]
    public class ObjectSave {
        public List<byte> NumbersInterfaces = new List<byte>();
        public List<byte> NumbersPropertysfaces = new List<byte>();
        public List<object> Save = new List<object>();
    }


}
[Serializable]
public class QuaterionSerializable {
    public float x, y, z, w;

    public Quaternion ToQuaterion() {
        return new Quaternion(x, y, z, w);
    }
}

[Serializable]
public abstract class ConstParameterSave {
    public string Version;
    public string CodeGame;
}



﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Editor_Controller : MonoBehaviour {
    public Vector3 LimitCameraMove;
    public Vector3 LimitDetalMove;
    [Range(0, 10)]
    public float DistanceSpawn;

    public FullDatabase FullDatabase;

    [SerializeField] MassCenterAxis MassCenter;
    [SerializeField] WingForceCenter WingForce;

    [HideInInspector]
    public GameObject DetailMove;//взятая деталь для движения


    public bool take_detail_in_groupe { get; set; }//Добавлять ли обьект в группу выделения
    public bool put_detail_for_groupe { get; set; }//Удалить ли обьект из группы выделения

    public static Editor_Controller singleton;

    public bool move_group {
        get {
            return _move_group;
        }
        set {
            _move_group = value;
            if (value == true) {
                if (DetailsGroup.Count != 0) {
                    mouse_pos = Group.transform.position;
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                    StartCoroutine(MoveDetail());
                }
                else {
                    _move_group = false;
                }
            }
            else {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                StopCoroutine(MoveDetail());
                List<DetailDate> dd = new List<DetailDate>();
                for (int i = 0; i < DetailsGroup.Count; i++) {
                    dd.Add(DetailsGroup[i].GetComponent<DetailDate>());
                }
                SaveStory(TypeHistory.EditParameterDetail, dd.ToArray());
            }
        }
    }// Включено ли движение

    private bool _move_group;

    private Vector3 mouse_pos;

    public bool copy_detail_move { get; set; }

    [HideInInspector]
    public GameObject Group;//обьект группы

    public Bounds Bounds_group;

    public List<GameObject> DetailsGroup = new List<GameObject>();//список деталей в группе

    //  [HideInInspector]
    public List<DetailDate> DetailList = new List<DetailDate>();

    // [HideInInspector]
    public int SetStory = -1; //На каком участке истории находиться редактор

    List<ActionHistory> StoryList = new List<ActionHistory>();

    private BuferBuild Bufer;

    private MassCenterAxis object_axis;

    private WingForceCenter object_wing;

    public const int MaxCountHistory = 40;

    public const float MaxDistBracing = 0.05f;

    public static readonly Vector3 AddSizeBounds = new Vector3(0.02f, 0.02f, 0.02f);

    private const float dist_bracing = 0.07999992f;




    private void Awake() {
        if (singleton == null)
            singleton = this;
        else
            Destroy(gameObject);
        SetStory = -1;
        Group = new GameObject("Groupe Derails");
        Group.transform.parent = transform;
        DetailList.Clear();
        DetailsGroup.Clear();

        CreateAxisMass();
    }



    #region Установка деталей
    public void TakeDetail(GameObject detail, bool isNoHistory) {
        if (detail) {
            if (!take_detail_in_groupe && !put_detail_for_groupe) {
                for (int i = 0; i < DetailsGroup.Count; i++) {
                    DetailsGroup[i].transform.parent = transform;
                    DetailsGroup[i].GetComponent<DetailDate>().SetHighlight = 0;
                }
                DetailsGroup.Clear();
                DetailsGroup.Add(detail);
                Group.transform.position = detail.transform.position;
                Group.transform.rotation = detail.transform.rotation;
                detail.transform.parent = Group.transform;
                DetailMove = detail;
                detail.GetComponent<DetailDate>().SetHighlight = 2;
            }
            else if (take_detail_in_groupe && DetailsGroup.IndexOf(detail) == -1) {
                DetailsGroup.Add(detail);
                detail.GetComponent<DetailDate>().SetHighlight = 2;
                for (int i = 0; i < DetailsGroup.Count; i++) {
                    DetailsGroup[i].transform.parent = Group.transform;
                }
            }
            else if (put_detail_for_groupe && DetailsGroup.IndexOf(detail) != -1) {
                if (DetailsGroup.Count != 1) {
                    DetailsGroup.Remove(detail);
                    detail.transform.parent = transform;
                    detail.GetComponent<DetailDate>().SetHighlight = 0;
                    for (int i = 0; i < DetailsGroup.Count; i++) {
                        DetailsGroup[i].transform.parent = transform;
                    }
                    Group.transform.position = DetailsGroup[0].transform.position;
                    Group.transform.rotation = Quaternion.identity;
                    for (int i = 0; i < DetailsGroup.Count; i++) {
                        DetailsGroup[i].transform.parent = Group.transform;
                    }
                }
            }
        }
        else {
            for (int i = 0; i < DetailsGroup.Count; i++) {
                DetailsGroup[i].GetComponent<DetailDate>().SetHighlight = 0;
                DetailsGroup[i].transform.parent = transform;
            }
            DetailsGroup.Clear();
            DetailMove = null;
        }
        if (DetailsGroup.Count != 0) {
            DetailMove = DetailsGroup[0];
        }
        else {
            DetailMove = null;
        }
        if (!isNoHistory) {
            List<DetailDate> details = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                details.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, details.ToArray());
        }
        UpdateBoundsGroup();
    }//Взятие детали для всех типов

    public void ArrayTakeDetail(GameObject[] details, bool isNoHistory) {

        if (take_detail_in_groupe) {
            for (int i = 0; i < details.Length; i++) {
                DetailsGroup.Add(details[i]);
                details[i].GetComponent<DetailDate>().SetHighlight = 2;
                details[i].transform.parent = Group.transform;
            }
        }
        else if (put_detail_for_groupe) {
            if (DetailsGroup.Count != 1) {
                for (int i = 0; i < details.Length; i++) {
                    DetailsGroup.Remove(details[i]);
                    details[i].transform.parent = transform;
                    details[i].GetComponent<DetailDate>().SetHighlight = 0;
                }
            }
        }
        else {
            TakeDetail(default, true);
            for (int i = 0; i < details.Length; i++) {
                DetailsGroup.Add(details[i]);
                details[i].GetComponent<DetailDate>().SetHighlight = 2;
                details[i].transform.parent = Group.transform;
            }
        }
        if (DetailsGroup.Count != 0) {
            for (int i = 0; i < DetailsGroup.Count; i++) {
                DetailsGroup[i].transform.parent = transform;
            }
            Group.transform.position = DetailsGroup[0].transform.position;
            Group.transform.rotation = DetailsGroup[0].transform.rotation;
            for (int i = 0; i < DetailsGroup.Count; i++) {
                DetailsGroup[i].transform.parent = Group.transform;
            }
        }
        if (!isNoHistory) {
            List<DetailDate> detailL = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                detailL.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, detailL.ToArray());
        }
        UpdateBoundsGroup();
        if (DetailsGroup.Count > 0)
            DetailMove = DetailsGroup[0].gameObject;
    }

    public void TakeDetailColor(bool isNoHistory) {
        List<GameObject> gms = new List<GameObject>();
        for (int i = 0; i < DetailList.Count; i++) {
            for (int i2 = 0; i2 < DetailsGroup.Count; i2++) {
                if (DetailList[i].DetailComponent.ColorDetail == DetailsGroup[i2].GetComponent<DetailDate>().DetailComponent.ColorDetail) {
                    gms.Add(DetailList[i].gameObject);
                }
            }
        }
        ArrayTakeDetail(gms.ToArray(), true);
        if (!isNoHistory) {
            List<DetailDate> detailL = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                detailL.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, detailL.ToArray());
        }
    }

    public void TakeDetailMaterial(bool isNoHistory) {
        List<GameObject> gms = new List<GameObject>();
        for (int i = 0; i < DetailList.Count; i++) {
            for (int i2 = 0; i2 < DetailsGroup.Count; i2++) {
                if (DetailList[i].DetailComponent.TypeMaterial == DetailsGroup[i2].GetComponent<DetailDate>().DetailComponent.TypeMaterial) {
                    gms.Add(DetailList[i].gameObject);
                }
            }
        }
        ArrayTakeDetail(gms.ToArray(), true);
        if (!isNoHistory) {
            List<DetailDate> detailL = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                detailL.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, detailL.ToArray());
        }
    }

    public void TakeDetailCod(bool isNoHistory) {
        List<GameObject> gms = new List<GameObject>();
        for (int i = 0; i < DetailList.Count; i++) {
            for (int i2 = 0; i2 < DetailsGroup.Count; i2++) {
                if (DetailList[i].CodeDetail == DetailsGroup[i2].GetComponent<DetailDate>().CodeDetail) {
                    gms.Add(DetailList[i].gameObject);
                }
            }
        }
        ArrayTakeDetail(gms.ToArray(), true);
        if (!isNoHistory) {
            List<DetailDate> detailL = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                detailL.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, detailL.ToArray());
        }
    }

    public void TakeDetailParams(bool isNoHistory) {
        List<GameObject> gms = new List<GameObject>();
        for (int i = 0; i < DetailList.Count; i++) {
            for (int i2 = 0; i2 < DetailsGroup.Count; i2++) {
                if (DetailList[i].Type == DetailsGroup[i2].GetComponent<DetailDate>().Type) {
                    Type one = DetailList[i].DetailComponent.GetType();
                    DetailDate data_two = DetailsGroup[i2].GetComponent<DetailDate>();
                    List<PropertyInfo> properties_one = new List<PropertyInfo>();
                    Type[] interfaces = one.GetInterfaces();
                    for (int i3 = 0; i3 < interfaces.Length; i3++) {
                        properties_one.AddRange(interfaces[i3].GetProperties());
                    }
                    bool on = true;
                    for (int i3 = 0; i3 < properties_one.Count; i3++) {
                        if (properties_one[i3].GetCustomAttribute(typeof(NoShowAttribute)) == null && !properties_one[i3].GetValue(DetailList[i].DetailComponent).Equals(properties_one[i3].GetValue(data_two.DetailComponent))) {
                            on = false;
                        }
                    }
                    if (on)
                        gms.Add(DetailList[i].gameObject);
                }
            }
        }
        ArrayTakeDetail(gms.ToArray(), true);
        if (!isNoHistory) {
            List<DetailDate> detailL = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                detailL.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, detailL.ToArray());
        }
    }

    public void TakeAllDetails(bool isNoHistory) {
        List<GameObject> gms = new List<GameObject>();
        for (int i = 0; i < DetailList.Count; i++) {
            gms.Add(DetailList[i].gameObject);
        }
        ArrayTakeDetail(gms.ToArray(), true);
        if (!isNoHistory) {
            List<DetailDate> detailL = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                detailL.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, detailL.ToArray());
        }
    }

    public void TakeInverse(bool isNoHistory) {
        List<GameObject> gms = new List<GameObject>();
        for (int i = 0; i < DetailList.Count; i++) {
            if (!DetailList[i].TakeDetailMove)
                gms.Add(DetailList[i].gameObject);
        }
        TakeDetail(default, true);
        ArrayTakeDetail(gms.ToArray(), true);
        if (!isNoHistory) {
            List<DetailDate> detailL = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                detailL.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.TakeDetail, detailL.ToArray());
        }
    }

    public void TakeBracingDetails(bool isNoHistory) {
        List<GameObject> take = new List<GameObject>();
        for (int i = 0; i < DetailsGroup.Count; i++) {
            Bounds bounds = DetailsGroup[i].GetComponent<DetailDate>().DetailMesh.bounds;
            bounds.size += AddSizeBounds;
            for (int i2 = 0; i2 < DetailList.Count; i2++) {
                if (!DetailList[i2].TakeDetailMove && DetailList[i2].DetailMesh.bounds.Intersects(bounds)) {
                    DetailDate one = DetailsGroup[i].GetComponent<DetailDate>();
                    DetailDate two = DetailList[i2];
                    for (int i3 = 0; i3 < one.Date.Length; i3++) {
                        for (int i4 = 0; i4 < two.Date.Length; i4++) {
                            bool on = false;
                            if (Bracing.CheckContactBracing(one.Date[i3].Name, two.Date[i4].Name)) {
                                on = true;
                            }
                            if (on) {
                                for (int i5 = 0; i5 < one.Date[i3].Bracing.Length; i5++) {
                                    for (int i6 = 0; i6 < two.Date[i4].Bracing.Length; i6++) {
                                        float ds = Vector3.Distance(one.Date[i3].Bracing[i5].transform.position, two.Date[i4].Bracing[i6].transform.position);
                                        if (ds < MaxDistBracing) {
                                            if (take.IndexOf(two.gameObject) == -1) {
                                                take.Add(two.gameObject);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            take.Add(DetailsGroup[i]);
        }
        if (isNoHistory) {
            ArrayTakeDetail(take.ToArray(), true);
        }
        else {
            ArrayTakeDetail(take.ToArray(), false);
        }
    }

    public void TakeBracingDetailsAll(bool isNoHistory) {
        List<GameObject> take = new List<GameObject>();
        take.AddRange(DetailsGroup.ToArray());

        for (int i = 0; i < take.Count; i++) {
            Bounds bounds = take[i].GetComponent<DetailDate>().DetailMesh.bounds;
            bounds.size += AddSizeBounds;
            for (int i2 = 0; i2 < DetailList.Count; i2++) {
                if (!DetailList[i2].TakeDetailMove && DetailList[i2].DetailMesh.bounds.Intersects(bounds)) {
                    DetailDate one = take[i].GetComponent<DetailDate>();
                    DetailDate two = DetailList[i2];
                    for (int i3 = 0; i3 < one.Date.Length; i3++) {
                        for (int i4 = 0; i4 < two.Date.Length; i4++) {
                            bool on = false;
                            if (Bracing.CheckContactBracing(one.Date[i3].Name, two.Date[i4].Name)) {
                                on = true;
                            }
                            if (on) {
                                for (int i5 = 0; i5 < one.Date[i3].Bracing.Length; i5++) {
                                    for (int i6 = 0; i6 < two.Date[i4].Bracing.Length; i6++) {
                                        float ds = Vector3.Distance(one.Date[i3].Bracing[i5].transform.position, two.Date[i4].Bracing[i6].transform.position);
                                        if (ds < MaxDistBracing) {
                                            if (take.IndexOf(two.gameObject) == -1) {
                                                take.Add(two.gameObject);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (isNoHistory) {
            ArrayTakeDetail(take.ToArray(), true);
        }
        else {
            ArrayTakeDetail(take.ToArray(), false);
        }
    }

    public void SpawnDetail(GameObject PrefabDetal) {
        if (PrefabDetal != null) {

            GameObject obj = Instantiate(PrefabDetal, ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().transform.forward * DistanceSpawn + ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().transform.position, Quaternion.identity);
            obj.name = obj.name + " " + ( DetailList.Count + 1 );
            obj.GetComponent<DetailDate>().Editor = gameObject;
            obj.GetComponent<DetailDate>().enabled = true;
            obj.GetComponent<DetailDate>().DetailComponent.Cod = CreateCodDetail();
            obj.transform.parent = gameObject.transform;
            mouse_pos = obj.transform.position;
            DetailList.Add(obj.GetComponent<DetailDate>());
            obj.GetComponent<DetailDate>().StartInEditorBuild();
            CorectPosGroupDetail(obj);
            TakeDetail(obj, true);
            SaveStory(TypeHistory.SpawnDetail, new DetailDate[1] { obj.GetComponent<DetailDate>() });
        }
    }//Спавн детали

    public void DetailMirroring() {
        if (DetailsGroup.Count != 0 && move_group) {
            List<DetailDate> details = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                if (DetailsGroup[i].transform.position.z != 0) {
                    Vector3 edit_pos = gameObject.transform.position;
                    Vector3 local_detail = edit_pos - DetailsGroup[i].transform.position;
                    Vector3 copy_detail_pos = DetailsGroup[i].transform.position - new Vector3(0, 0, -local_detail.z * 2);
                    GameObject go = FullDatabase.FindGroup(DetailsGroup[i].GetComponent<DetailDate>().Group).Find(DetailsGroup[i].GetComponent<DetailDate>().CodeDetail);
                    GameObject obj = Instantiate(go, copy_detail_pos, Quaternion.Inverse(DetailsGroup[i].transform.rotation));
                    obj.transform.parent = gameObject.transform;
                    obj.name = obj.name + " " + ( DetailList.Count + 1 );
                    obj.GetComponent<DetailDate>().Editor = DetailsGroup[i].GetComponent<DetailDate>().Editor;
                    obj.GetComponent<DetailDate>().DetailComponent = DetailsGroup[i].GetComponent<DetailDate>().DetailComponent;
                    obj.GetComponent<DetailDate>().DetailComponent.Cod = CreateCodDetail();
                    details.Add(obj.GetComponent<DetailDate>());
                    DetailList.Add(obj.GetComponent<DetailDate>());
                    obj.GetComponent<DetailDate>().StartInEditorBuild();
                }
            }
            SaveStory(TypeHistory.SpawnDetail, details.ToArray());
        }
    }//отзеркаливание детали

    public void DestroyDetal() {
        if (DetailsGroup.Count != 0) {
            List<DetailDate> details = new List<DetailDate>();
            for (int i = 0; i < DetailsGroup.Count; i++) {
                details.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
            SaveStory(TypeHistory.RemoveDetail, details.ToArray());
            for (int i = 0; i < DetailsGroup.Count; i++) {
                RemoveDetail(DetailsGroup[i].GetComponent<DetailDate>());
            }
            DetailMove = null;
            move_group = false;
            DetailsGroup.Clear();
            TakeDetail(default, true);
        }
    }//Уничтожение детали

    public void SaveBufer() {
        if (DetailsGroup.Count != 0) {
            Bufer.Cods = new string[DetailsGroup.Count];
            Bufer.Groups = new GroupBase[DetailsGroup.Count];
            Bufer.Params = new DetailStandart[DetailsGroup.Count];
            Bufer.Pos = new Vector3[DetailsGroup.Count];
            Bufer.Rot = new Quaternion[DetailsGroup.Count];
            for (int i = 0; i < DetailsGroup.Count; i++) {
                Bufer.Cods[i] = DetailsGroup[i].GetComponent<DetailDate>().CodeDetail;
                Bufer.Groups[i] = DetailsGroup[i].GetComponent<DetailDate>().Group;
                Bufer.Params[i] = DetailsGroup[i].GetComponent<DetailDate>().CopyDetailComponent();
                Bufer.Pos[i] = DetailsGroup[i].transform.position;
                Bufer.Rot[i] = DetailsGroup[i].transform.rotation;
            }
        }
        else {
            Bufer.Cods = new string[0];
            Bufer.Groups = new GroupBase[0];
            Bufer.Params = new DetailStandart[0];
            Bufer.Pos = new Vector3[0];
            Bufer.Rot = new Quaternion[0];
        }
    }

    public void LoadBufer() {
        if (Bufer.Cods == null)
            return;

        List<DetailDate> details = new List<DetailDate>();
        List<GameObject> details_obj = new List<GameObject>();
        for (int i = 0; i < Bufer.Cods.Length; i++) {
            GameObject obj = Instantiate(FullDatabase.FindGroup(Bufer.Groups[i]).Find(Bufer.Cods[i]));
            obj.GetComponent<DetailDate>().Editor = gameObject;
            obj.GetComponent<DetailDate>().DetailComponent = Bufer.Params[i];
            obj.GetComponent<DetailDate>().DetailComponent.Cod = CreateCodDetail();
            DetailList.Add(obj.GetComponent<DetailDate>());
            obj.transform.position = Bufer.Pos[i];
            obj.transform.rotation = Bufer.Rot[i];
            details_obj.Add(obj);
            details.Add(obj.GetComponent<DetailDate>());
        }
        ArrayTakeDetail(details_obj.ToArray(), true);
        SaveStory(TypeHistory.SpawnDetail, details.ToArray());
    }

    public void HideDetails(bool isNoHistory) {
        List<DetailDate> gms = new List<DetailDate>();
        for (int i = 0; i < DetailsGroup.Count; i++) {
            DetailDate d = DetailsGroup[i].GetComponent<DetailDate>();
            d.HideDetail();
            gms.Add(d);
        }
        TakeDetail(default, true);
        if (!isNoHistory) {
            SaveStory(TypeHistory.HideDetail, gms.ToArray());
        }
    }

    public void UnHideDetails(bool isNoHistory) {
        List<DetailDate> gms = new List<DetailDate>();
        for (int i = 0; i < DetailList.Count; i++) {
            if (DetailList[i].isHide) {
                DetailList[i].UnHideDetail();
                gms.Add(DetailList[i]);
            }
        }
        if (!isNoHistory) {
            SaveStory(TypeHistory.UnHideDetail, gms.ToArray());
        }
    }

    public void CreateTrack() {
        List<DetailDate> tankWheels = new List<DetailDate>();
        for (int i = 0; i < DetailsGroup.Count; i++) {
            if (DetailsGroup[i].GetComponent<DetailDate>().Type == DetailDate.TypeDetail.TankWheel) {
                tankWheels.Add(DetailsGroup[i].GetComponent<DetailDate>());
            }
        }
        if (tankWheels.Count > 1) {
            ( (ITankWheel) tankWheels[0].DetailComponent ).CreateTrack(tankWheels.ToArray());
        }
    }

    public void DestroyTrack() {
        for (int i = 0; i < DetailsGroup.Count; i++) {
            if (DetailsGroup[i].GetComponent<DetailDate>().Type == DetailDate.TypeDetail.TankWheel) {
                ( (ITankWheel) DetailsGroup[i].GetComponent<DetailDate>().DetailComponent ).DestroyTrack();
            }
        }
    }


    private string CreateCodDetail() {
        return UnityEngine.Random.Range(0, int.MaxValue).ToString() + UnityEngine.Random.Range(0, int.MaxValue).ToString();
    }

    private void RemoveDetail(DetailDate detail) {
        Destroy(detail.gameObject);
        DetailList.Remove(detail);
        detail.DetailComponent.DestroyDetail();
    }

    private void CloneDetails() {
        List<DetailDate> details = new List<DetailDate>();
        for (int i = 0; i < DetailsGroup.Count; i++) {
            GameObject go = FullDatabase.FindGroup(DetailsGroup[i].GetComponent<DetailDate>().Group).Find(DetailsGroup[i].GetComponent<DetailDate>().CodeDetail);
            GameObject obj = Instantiate(go, DetailsGroup[i].transform.position, DetailsGroup[i].transform.rotation);
            obj.transform.parent = gameObject.transform;
            obj.name = obj.name + " " + ( DetailList.Count + 1 );
            details.Add(obj.GetComponent<DetailDate>());
            obj.GetComponent<DetailDate>().Editor = DetailsGroup[i].GetComponent<DetailDate>().Editor;
            obj.GetComponent<DetailDate>().DetailComponent = DetailsGroup[i].GetComponent<DetailDate>().DetailComponent;
            obj.GetComponent<DetailDate>().DetailComponent.Cod = CreateCodDetail();
            DetailList.Add(obj.GetComponent<DetailDate>());
        }
        SaveStory(TypeHistory.SpawnDetail, details.ToArray());
    }

    private void CorectPosGroupDetail(GameObject gameObject) {
        Vector3 old_pos = gameObject.transform.position;
        Vector3 pos = new Vector3();
        pos.x = mouse_pos.x - ( mouse_pos.x % ( dist_bracing / 2f ) );
        pos.y = mouse_pos.y - ( mouse_pos.y % ( dist_bracing ) );
        pos.z = mouse_pos.z - ( mouse_pos.z % ( dist_bracing / 2f ) );

        gameObject.transform.position = pos;
        if (old_pos != pos) {
            MagnitGroup();
        }
    }

    public IEnumerator MoveDetail() {
        while (true) {
            if (!Input.GetMouseButtonUp(1)) {
                float mouse_x = Input.GetAxis("Mouse X") * 2f;
                float mouse_y = Input.GetAxis("Mouse Y") * 2f;
                Vector3 normal_ang_camera_f = new Vector3(ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().transform.forward.x, 0, ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().transform.forward.z).normalized;
                Vector3 normal_ang_camera_r = new Vector3(ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().transform.right.x, 0, ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().transform.right.z).normalized;
                Vector3 cursor_move = ( new Vector3(normal_ang_camera_r.x, 1, normal_ang_camera_r.z) * mouse_x + new Vector3(normal_ang_camera_f.x, 1, normal_ang_camera_f.z) * mouse_y ) * Time.deltaTime;
                if (!Input.GetKey(KeyCode.LeftShift)) {
                    mouse_pos += new Vector3(cursor_move.x, 0, cursor_move.z);
                }
                else {
                    mouse_pos += new Vector3(0, cursor_move.y, 0);
                }
                CorectPosGroupDetail(Group);

                if (Input.GetKeyDown(KeyCode.W))
                    StartCoroutine(RotateDetail(KeyCode.W, new Vector3(1, 0, 0)));
                if (Input.GetKeyDown(KeyCode.S))
                    StartCoroutine(RotateDetail(KeyCode.S, new Vector3(-1, 0, 0)));
                if (Input.GetKeyDown(KeyCode.A))
                    StartCoroutine(RotateDetail(KeyCode.A, new Vector3(0, -1, 0)));
                if (Input.GetKeyDown(KeyCode.D))
                    StartCoroutine(RotateDetail(KeyCode.D, new Vector3(0, 1, 0)));
                if (Input.GetKeyDown(KeyCode.Q))
                    StartCoroutine(RotateDetail(KeyCode.Q, new Vector3(0, 0, 1)));
                if (Input.GetKeyDown(KeyCode.E))
                    StartCoroutine(RotateDetail(KeyCode.E, new Vector3(0, 0, -1)));
            }
            if (_move_group == false) {
                yield break;
            }
            else {
                yield return null;
            }
        }
    }

    public IEnumerator RotateDetail(KeyCode key, Vector3 direct) {
        while (Input.GetKey(key)) {
            if (!Input.GetMouseButtonUp(1))
                if (Input.GetKey(KeyCode.LeftShift)) {
                    Group.transform.Rotate(direct * 1.4f);
                    UpdateBoundsGroup();
                }
                else {
                    Group.transform.Rotate(direct * 22.5f);
                    UpdateBoundsGroup();
                }
            if (_move_group == false) {
                yield break;
            }
            else {
                yield return null;
            }
            yield return new WaitForSeconds(0.2f);
        }
    }

    public void MagnitGroup() {
        Bounds_group.center = Group.transform.position;
        for (int i = 0; i < DetailList.Count; i++) {
            if (!DetailList[i].TakeDetailMove) {
                Bounds bounds = new Bounds(DetailList[i].DetailMesh.bounds.center, DetailList[i].DetailMesh.bounds.size);
                bounds.center = DetailList[i].transform.position;
                if (Bounds_group.Intersects(bounds)) {
                    for (int i2 = 0; i2 < DetailsGroup.Count; i2++) {
                        Bounds b = DetailsGroup[i2].GetComponent<DetailDate>().DetailMesh.bounds;
                        b.center = DetailsGroup[i2].transform.position;
                        b.size += AddSizeBounds;
                        if (b.Intersects(bounds)) {
                            DetailDate one = DetailsGroup[i2].GetComponent<DetailDate>();
                            DetailDate two = DetailList[i];
                            GameObject one_b = null;
                            GameObject two_b = null;
                            float dist = 10000f;
                            for (int i3 = 0; i3 < one.Date.Length; i3++) {
                                for (int i4 = 0; i4 < two.Date.Length; i4++) {
                                    bool on = Bracing.CheckContactBracing(one.Date[i3].Name, two.Date[i4].Name);
                                    bool two_way_direction = Bracing.CheckDirectionBracing(one.Date[i3].Name, two.Date[i4].Name);
                                    bool connector = Bracing.CheckIsConnector(one.Date[i3].Name, two.Date[i4].Name);
                                    if (on) {
                                        for (int i5 = 0; i5 < one.Date[i3].Bracing.Length; i5++) {
                                            for (int i6 = 0; i6 < two.Date[i4].Bracing.Length; i6++) {
                                                float ds = Vector3.Distance(one.Date[i3].Bracing[i5].transform.position, two.Date[i4].Bracing[i6].transform.position);
                                                if (one.Date[i3].Bracing[i5].transform.forward == two.Date[i4].Bracing[i6].transform.forward && ds < 0.12f && ds < dist) {
                                                    one_b = one.Date[i3].Bracing[i5];
                                                    two_b = two.Date[i4].Bracing[i6];
                                                    dist = ds;
                                                }
                                                if (two_way_direction) {
                                                    if (-one.Date[i3].Bracing[i5].transform.forward == two.Date[i4].Bracing[i6].transform.forward && ds < 0.12f && ds < dist) {
                                                        one_b = one.Date[i3].Bracing[i5];
                                                        two_b = two.Date[i4].Bracing[i6];
                                                        dist = ds;
                                                    }
                                                }
                                                if (connector) {
                                                    if (ds < 0.2f && ds < dist) {
                                                        one_b = one.Date[i3].Bracing[i5];
                                                        two_b = two.Date[i4].Bracing[i6];
                                                        dist = ds;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (one_b) {
                                Group.transform.position += two_b.transform.position - one_b.transform.position;
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    private void UpdateBoundsGroup() {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        for (int i = 0; i < DetailsGroup.Count; i++) {
            bounds.Encapsulate(DetailsGroup[i].GetComponent<DetailDate>().DetailMesh.GetComponent<Renderer>().bounds);
        }
        Bounds_group = bounds;
        if (DetailsGroup.Count != 0)
            Bounds_group.size += AddSizeBounds;
    }

    #endregion

    #region Дополнительные данные
    #region Центр массы и подьемной силы

    private void CreateAxisMass() {
        object_axis = Instantiate(MassCenter, transform);
        object_axis.transform.localPosition = Vector3.zero;
        object_axis.transform.localEulerAngles = Vector3.zero;
        object_axis.UnSetLook();

        object_wing = Instantiate(WingForce, transform);
        object_wing.transform.localPosition = Vector3.zero;
        object_wing.transform.localEulerAngles = Vector3.zero;
        object_wing.UnSetLook();
    }

    public void UpdateCenterMass() {
        if (DetailList.Count > 0) {
            float mass = 0;
            Vector3 center = Vector3.zero;


            Vector3 directs = new Vector3();
            int count = 0;
            float Force = 0;
            Vector3 pos = new Vector3();
            for (int i = 0; i < DetailList.Count; i++) {
                mass += DetailList[i].DetailComponent.Weight;
                center += DetailList[i].transform.position * DetailList[i].DetailComponent.Weight;


                if (DetailList[i].Type == DetailDate.TypeDetail.Aileron) {
                    Vector3 force = ( (IAileron) DetailList[i].DetailComponent ).GetForce(-Vector3.up * 100f);
                    Force += force.sqrMagnitude;
                    directs += force.normalized;
                    pos += ( (AileronData) DetailList[i].ParameterDetail ).AileronMesh.transform.position * force.sqrMagnitude;
                    count++;
                }
                else if (DetailList[i].Type == DetailDate.TypeDetail.Wing) {
                    Vector3 force = ( (IWing) DetailList[i].DetailComponent ).GetForce(-Vector3.up * 100f);
                    Force += force.sqrMagnitude;
                    pos += DetailList[i].transform.position * force.sqrMagnitude;
                    directs += force.normalized;
                    count++;
                }
            }
            object_axis.transform.position = center / mass;
            object_axis.Text.text = mass + ".kg";
            object_axis.SetLook();

            if (count > 0) {
                object_wing.transform.position = ( pos / Force );

                object_wing.transform.rotation = Quaternion.LookRotation(directs / count);
                object_wing.SetLook();
            }

        }
    }


    #endregion

    public void HideIndicators() {
        object_axis.UnSetLook();
        object_wing.UnSetLook();
    }
    #endregion

    #region История
    private class ActionHistory {
        private Story story;

        public void LoadStory(Editor_Controller Editor) {
            story.LoadStory(Editor);
            LoadDop(Editor);
            TakeOldDatail(Editor);
        }

        public void UnLoadStory(Editor_Controller Editor) {
            story.UnLoadStory(Editor);
            LoadDop(Editor);
            TakeOldDatail(Editor);
        }

        private void LoadDop(Editor_Controller Editor) {
            Editor.TakeDetail(default, true);
        }

        private void TakeOldDatail(Editor_Controller Editor) {
            Editor.take_detail_in_groupe = true;
            for (int i = 0; i < story.cods_group_details.Length; i++) {
                for (int i2 = 0; i2 < Editor.DetailList.Count; i2++) {
                    if (story.cods_group_details[i] == Editor.DetailList[i2].DetailComponent.Cod) {
                        Editor.TakeDetail(Editor.DetailList[i2].gameObject, true);
                    }
                }
            }
            Editor.take_detail_in_groupe = false;
        }

        public ActionHistory(TypeHistory Type, Editor_Controller Editor, params DetailDate[] Details) {
            if (Type == TypeHistory.SpawnDetail) {
                story = new SpawnDetail();
            }
            else if (Type == TypeHistory.RemoveDetail) {
                story = new DestroyDetail();
            }
            else if (Type == TypeHistory.EditParameterDetail) {
                story = new EditParameter();
            }
            else if (Type == TypeHistory.TakeDetail) {
                story = new TakeDatils();
            }
            else if (Type == TypeHistory.HideDetail) {
                story = new HideDetails();
            }
            else if (Type == TypeHistory.UnHideDetail) {
                story = new UnHideDetails();
            }

            story.Details = new DetailStandart[Details.Length];
            story.Position = new Vector3[Details.Length];
            story.Rotate = new Quaternion[Details.Length];
            story.Cods = new string[Details.Length];
            story.Groups = new GroupBase[Details.Length];
            for (int i = 0; i < Details.Length; i++) {
                story.Details[i] = Details[i].CopyDetailComponent();
                story.Position[i] = ( Details[i].gameObject.transform.position );
                story.Rotate[i] = ( Details[i].gameObject.transform.rotation );
                story.Cods[i] = ( Details[i].CodeDetail );
                story.Groups[i] = ( Details[i].Group );
            }

            story.cods_group_details = new string[Editor.DetailsGroup.Count];
            for (int i = 0; i < Editor.DetailsGroup.Count; i++) {
                story.cods_group_details[i] = Editor.DetailsGroup[i].GetComponent<DetailDate>().DetailComponent.Cod;
            }
        }

        private abstract class Story {

            public string[] Cods;

            public GroupBase[] Groups;

            public DetailStandart[] Details;

            public Vector3[] Position;

            public Quaternion[] Rotate;

            public string[] cods_group_details;

            public abstract void LoadStory(Editor_Controller Editor);

            public abstract void UnLoadStory(Editor_Controller Editor);

        }

        private class SpawnDetail : Story {
            public override void LoadStory(Editor_Controller Editor) {

                for (int i = 0; i < Details.Length; i++) {
                    string cod = Cods[i];
                    GroupBase groupBase = Groups[i];
                    GameObject detail = Editor.FullDatabase.FindGroup(groupBase).Find(cod);
                    if (detail) {
                        GameObject obj = Instantiate(detail, Position[i], Rotate[i]);
                        obj.GetComponent<DetailDate>().Editor = Editor.gameObject;
                        obj.GetComponent<DetailDate>().DetailComponent = Details[i];
                        obj.transform.parent = Editor.transform;
                        Editor.DetailList.Add(obj.transform.GetComponent<DetailDate>());
                        obj.GetComponent<DetailDate>().DetailComponent.UpdateHistory();
                    }
                }
            }

            public override void UnLoadStory(Editor_Controller Editor) {
                List<DetailDate> details = new List<DetailDate>();
                for (int i = 0; i < Editor.DetailList.Count; i++) {
                    for (int i2 = 0; i2 < Details.Length; i2++) {
                        if (Editor.DetailList[i].DetailComponent.Cod == Details[i2].Cod) {
                            details.Add(Editor.DetailList[i]);
                        }
                    }
                }
                for (int i = 0; i < details.Count; i++) {
                    Editor.RemoveDetail(details[i]);
                    details[i].DetailComponent.UpdateHistory();
                }
            }
        }

        private class DestroyDetail : Story {
            public override void LoadStory(Editor_Controller Editor) {

                List<DetailDate> details = new List<DetailDate>();
                for (int i = 0; i < Editor.DetailList.Count; i++) {
                    for (int i2 = 0; i2 < Details.Length; i2++) {
                        if (Editor.DetailList[i].DetailComponent.Cod == Details[i2].Cod) {
                            details.Add(Editor.DetailList[i]);
                        }
                    }
                }
                for (int i = 0; i < details.Count; i++) {
                    Editor.RemoveDetail(details[i]);
                    details[i].DetailComponent.UpdateHistory();
                }
            }

            public override void UnLoadStory(Editor_Controller Editor) {
                for (int i = 0; i < Details.Length; i++) {
                    string cod = Cods[i];
                    GroupBase groupBase = Groups[i];
                    GameObject detail = Editor.FullDatabase.FindGroup(groupBase).Find(cod);
                    if (detail) {
                        GameObject obj = Instantiate(detail, Position[i], Rotate[i]);
                        obj.GetComponent<DetailDate>().Editor = Editor.gameObject;
                        obj.GetComponent<DetailDate>().DetailComponent = Details[i];
                        obj.transform.parent = Editor.transform;
                        Editor.DetailList.Add(obj.transform.GetComponent<DetailDate>());
                        obj.GetComponent<DetailDate>().DetailComponent.UpdateHistory();
                    }
                }
            }
        }

        private class EditParameter : Story {
            public override void LoadStory(Editor_Controller Editor) {
                for (int i = 0; i < Editor.DetailList.Count; i++) {
                    for (int i2 = 0; i2 < Details.Length; i2++) {
                        if (Details[i2].Cod == Editor.DetailList[i].DetailComponent.Cod) {
                            Editor.DetailList[i].DetailComponent = Details[i2];
                            Editor.DetailList[i].transform.position = Position[i2];
                            Editor.DetailList[i].transform.rotation = Rotate[i2];
                            Editor.DetailList[i].DetailComponent.UpdateHistory();
                        }
                    }
                }
            }

            public override void UnLoadStory(Editor_Controller Editor) {
                if (Editor.SetStory - 1 >= 0 && Editor.StoryList.Count != 0) {
                    Story story = Editor.StoryList[Editor.SetStory - 1].story;
                    for (int i = 0; i < Editor.DetailList.Count; i++) {
                        for (int i2 = 0; i2 < story.Details.Length; i2++) {
                            if (Editor.DetailList[i].DetailComponent.Cod == story.Details[i2].Cod) {
                                Editor.DetailList[i].DetailComponent = story.Details[i2];
                                Editor.DetailList[i].transform.position = story.Position[i2];
                                Editor.DetailList[i].transform.rotation = story.Rotate[i2];
                                Editor.DetailList[i].DetailComponent.UpdateHistory();
                            }
                        }
                    }
                }
            }
        }

        private class TakeDatils : Story {
            public override void LoadStory(Editor_Controller Editor) {
            }

            public override void UnLoadStory(Editor_Controller Editor) {
            }
        }

        private class HideDetails : Story {
            public override void LoadStory(Editor_Controller Editor) {
                for (int i = 0; i < Editor.DetailList.Count; i++) {
                    for (int i2 = 0; i2 < Details.Length; i2++) {
                        if (Details[i2].Cod == Editor.DetailList[i].DetailComponent.Cod) {
                            Editor.DetailList[i].HideDetail();
                        }
                    }
                }
            }

            public override void UnLoadStory(Editor_Controller Editor) {
                for (int i = 0; i < Editor.DetailList.Count; i++) {
                    for (int i2 = 0; i2 < Details.Length; i2++) {
                        if (Details[i2].Cod == Editor.DetailList[i].DetailComponent.Cod) {
                            Editor.DetailList[i].UnHideDetail();
                        }
                    }
                }
            }
        }

        private class UnHideDetails : Story {
            public override void LoadStory(Editor_Controller Editor) {
                for (int i = 0; i < Editor.DetailList.Count; i++) {
                    for (int i2 = 0; i2 < Details.Length; i2++) {
                        if (Details[i2].Cod == Editor.DetailList[i].DetailComponent.Cod) {
                            Editor.DetailList[i].UnHideDetail();
                        }
                    }
                }
            }

            public override void UnLoadStory(Editor_Controller Editor) {
                for (int i = 0; i < Editor.DetailList.Count; i++) {
                    for (int i2 = 0; i2 < Details.Length; i2++) {
                        if (Details[i2].Cod == Editor.DetailList[i].DetailComponent.Cod) {
                            Editor.DetailList[i].HideDetail();
                        }
                    }
                }
            }
        }

    }

    public enum TypeHistory {
        SpawnDetail,
        RemoveDetail,
        EditParameterDetail,
        TakeDetail,
        HideDetail,
        UnHideDetail
    }

    public void PreviousStory() {
        if (StoryList.Count >= 1 & SetStory > 0) {
            StoryList[SetStory].UnLoadStory(this);
            SetStory--;
            //  LoadStory(set_story);
        }
    }

    public void FutureStory() {
        if (SetStory + 1 < StoryList.Count) {
            SetStory++;
            StoryList[SetStory].LoadStory(this);
            //  LoadStory(set_story);
        }
    }

    public void SaveStory(TypeHistory typeHistory, params DetailDate[] Details) {
        if (SetStory + 1 != StoryList.Count) {
            ClearStory(SetStory + 1);
        }
        ActionHistory story = new ActionHistory(typeHistory, this, Details);
        SetStory++;
        StoryList.Add(story);
        if (StoryList.Count > MaxCountHistory) {
            StoryList.Remove(StoryList[0]);
            SetStory--;
        }
    }

    private void ClearStory() {
        SetStory = -1;
        StoryList.Clear();
        //SaveStory("Start");
    }

    private void ClearStory(int number) {
        StoryList.RemoveRange(number, StoryList.Count - number);
        SetStory = number - 1;
    }
    #endregion

    #region FileController
    public void ClearEditor() {
        TakeDetail(default, true);
        List<DetailDate> list = new List<DetailDate>(DetailList.ToArray());
        for (int i = 0; i < list.Count; i++) {
            RemoveDetail(list[i]);
        }
        HideIndicators();
        ClearStory();
    }

    public void LoadBuild(SaveEditorBuild sv) {
        ClearEditor();

        for (int i = 0; i < sv.DetailsCodes.Length; i++) {
            for (int i2 = 0; i2 < FullDatabase.ToArray.Length; i2++) {
                GameObject detail = FullDatabase.ToArray[i2].Find(sv.DetailsCodes[i]);
                if (detail != null) {
                    GameObject go = detail;
                    GameObject obj = Instantiate(go, gameObject.transform.position + Vector3Serializable.ToVector3(sv.Position[i]), sv.Angle[i].ToQuaterion());
                    obj.name = obj.name + " " + ( DetailList.Count + 1 );
                    obj.GetComponent<DetailDate>().Editor = gameObject;
                    object t = obj.GetComponent<DetailDate>().DetailComponent;
                    obj.GetComponent<DetailDate>().enabled = true;
                    obj.transform.parent = gameObject.transform;
                    DetailList.Add(obj.GetComponent<DetailDate>());
                    obj.GetComponent<DetailDate>().DetailComponent.Cod = sv.DetailGameGenerateCodes[i];
                    if (sv.TypeDetails[i].Name == t.GetType().Name) {
                        Type[] interfaces = t.GetType().GetInterfaces();
                        for (int i3 = 0; i3 < interfaces.Length; i3++) {
                            int number_interfase = ( (NumberInterfaceAttribute) interfaces[i3].GetCustomAttribute(typeof(NumberInterfaceAttribute)) ).Number;
                            int index_interface = sv.ParametersObject[i].NumbersInterfaces.IndexOf((byte) number_interfase);
                            PropertyInfo[] properties = interfaces[i3].GetProperties();
                            if (index_interface != -1) {
                                for (int i4 = 0; i4 < properties.Length; i4++) {
                                    int num_property = ( (NumberPropertyAttribute) properties[i4].GetCustomAttribute(typeof(NumberPropertyAttribute)) ).Number;

                                    if (properties[i4].CanWrite) {
                                        for (int i5 = 0; i5 < sv.ParametersObject[i].Save.Count; i5++) {
                                            if (sv.ParametersObject[i].NumbersInterfaces[i5] == number_interfase && sv.ParametersObject[i].NumbersPropertysfaces[i5] == num_property) {

                                                properties[i4].SetValue(t, sv.ParametersObject[i].Save[i5]);

                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    else {
                        RemoveDetail(DetailList[i]);
                    }
                    //if (s < 2) {
                    //    s++;
                    //    sw.Stop();
                    //    print(sw.Elapsed);
                    //    sw.Reset();
                    //}
                    break;
                }
            }
        }
        for (int i = 0; i < DetailList.Count; i++) {
            DetailList[i].StartInEditorBuild();
        }
        ClearStory();
    }
    #endregion

    private void OnDestroy() {
        if (singleton = this)
            singleton = null;
    }
}

struct BuferBuild {
    public DetailStandart[] Params;
    public GroupBase[] Groups;
    public string[] Cods;
    public Vector3[] Pos;
    public Quaternion[] Rot;
}

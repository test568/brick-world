﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaterialItem: MonoBehaviour {
    public Image Sprite;
    public MaterialPanel Panel;
    public TypeMaterialDetail Type;
    public Text Name;
    public void Init() {
        GetComponent<Button>().onClick.AddListener(() => { Panel.SetMaterial(Type); });
        }
    }

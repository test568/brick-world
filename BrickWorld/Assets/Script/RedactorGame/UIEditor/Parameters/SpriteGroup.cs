﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SpriteGroup : MonoBehaviour {
    public SpritePanel Panel;
    public Image Sprite;
    public string NameGroup;
    public Text Name;
    public void Init() {
        GetComponent<Button>().onClick.AddListener(() => { Panel.PrintGroup(NameGroup); });
    }
}

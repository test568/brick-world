﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Collections.Generic;

public class StringPanel: MonoBehaviour {
    public Text Name;
    public InputField Text;
    [HideInInspector]
    public DetailDate Base;

    [HideInInspector]
    public UIEditor iEditor;

    public PropertyInfo Parameter;

    private object old_object;

    private void Start() {
        old_object=Parameter.GetValue(Base.DetailComponent);
        Text.SetTextWithoutNotify( (string)Parameter.GetValue(Base.DetailComponent) );
        if (Parameter.CanWrite) {
            Text.onEndEdit.AddListener(delegate { SetText(); });
            }
        else {
            Text.readOnly=true;
            }
        }
    private void Update() {
        if (!Equals(old_object, Parameter.GetValue(Base.DetailComponent))) {
            Text.SetTextWithoutNotify(( (string)Parameter.GetValue(Base.DetailComponent) ));
            old_object=Parameter.GetValue(Base.DetailComponent);
            }
        }
    private void SetText() {
        Parameter.SetValue(Base.DetailComponent, Text.text);
        old_object=Parameter.GetValue(Base.DetailComponent);
        Text.SetTextWithoutNotify((string)Parameter.GetValue(Base.DetailComponent));

        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();
        List<DetailDate> dates = new List<DetailDate>();
        for (int i = 0; i<details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date!=Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, Text.text);
                    dates.Add(date);
                    }
                catch { }
                }
            }
        dates.Add(Base);
        iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, dates.ToArray());
        }
    }

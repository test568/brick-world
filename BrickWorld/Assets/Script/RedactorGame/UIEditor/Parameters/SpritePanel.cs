﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class SpritePanel : MonoBehaviour {
    public Text Name;
    public Button BaseSprite;
    public Button DetectedOFF;
    public Image PanelSprite;

    public GameObject Content;

    [HideInInspector]
    public UIEditor iEditor;

    [HideInInspector]
    public DetailDate Base;

    public PropertyInfo Parameter;

    private object old_object;


    [SerializeField] Button brake;

    [SerializeField] SpriteItem baseSpriteItem;

    [SerializeField] SpriteGroup baseSpriteGroup;


    private List<GameObject> list_objects = new List<GameObject>();

    private void Awake() {
        old_object = Parameter.GetValue(Base.DetailComponent);
        BaseSprite.GetComponent<Image>().sprite = SpritePaintDataBase.DataBase.FindSprite(( (PaintSprite) Parameter.GetValue(Base.DetailComponent) ).name);



        if (Parameter.CanWrite) {
            BaseSprite.onClick.AddListener(delegate { ActivePanel(); });
            DetectedOFF.onClick.AddListener(delegate { ActivePanel(); });
            brake.onClick.AddListener(delegate { BrakeGroup(); });
        }
        BrakeGroup();

    }

    private void Update() {
        if (!Equals(old_object, Parameter.GetValue(Base.DetailComponent))) {
            BaseSprite.GetComponent<Image>().sprite = SpritePaintDataBase.DataBase.FindSprite(( (PaintSprite) Parameter.GetValue(Base.DetailComponent) ).name);
            old_object = Parameter.GetValue(Base.DetailComponent);
        }
    }

    public void PrintGroup(string name) {
        Clear();
        Sprite[] sprites;
        for (int i = 0; i < SpritePaintDataBase.DataBase.Sprites.Length; i++) {
            if (SpritePaintDataBase.DataBase.Sprites[i].RussianName == name) {
                sprites = SpritePaintDataBase.DataBase.Sprites[i].Sprites;
                for (int i2 = 0; i2 < sprites.Length; i2++) {
                    SpriteItem item = Instantiate(baseSpriteItem, Content.transform);
                    item.Panel = this;
                    item.GetComponent<Image>().sprite = sprites[i2];
                    item.name = sprites[i2].name;
                    item.Init();
                    item.gameObject.SetActive(true);
                    list_objects.Add(item.gameObject);
                }
                break;
            }
        }
    }

    public void BrakeGroup() {
        Clear();
        SpriteGroupData[] spriteGroups = SpritePaintDataBase.DataBase.Sprites;
        for (int i = 0; i < spriteGroups.Length; i++) {
            SpriteGroup group = Instantiate(baseSpriteGroup, Content.transform);
            group.Name.text = spriteGroups[i].RussianName;
            group.NameGroup = spriteGroups[i].RussianName;
            group.Panel = this;
            group.Sprite.sprite = spriteGroups[i].ImageSpriteGroup;
            group.Init();
            group.gameObject.SetActive(true);
            list_objects.Add(group.gameObject);
        }

    }

    public void Clear() {
        for (int i = 0; i < list_objects.Count; i++) {
            Destroy(list_objects[i]);
        }
        list_objects.Clear();
    }

    public void SetSprite(string name) {
        BaseSprite.GetComponent<Image>().sprite = SpritePaintDataBase.DataBase.FindSprite(name);


        PaintSprite sprite;
        sprite.name = name;
        Parameter.SetValue(Base.DetailComponent, sprite);
        old_object = Parameter.GetValue(Base.DetailComponent);

        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();
        List<DetailDate> dates = new List<DetailDate>();
        for (int i = 0; i < details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date != Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, sprite);
                    dates.Add(date);
                }
                catch { }
            }
        }
        dates.Add(Base);
        iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, dates.ToArray());
    }

    private void ActivePanel() {
        if (PanelSprite.gameObject.active == true && Input.GetMouseButtonUp(0) && iEditor.layer_set) {
            PanelSprite.gameObject.active = false;
            PanelSprite.gameObject.transform.parent = BaseSprite.transform;
            iEditor.layer_set = false;
            return;
        }
        if (PanelSprite.gameObject.active == false && Input.GetMouseButtonUp(0) && !iEditor.layer_set) {
            PanelSprite.gameObject.active = true;
            PanelSprite.gameObject.transform.parent = iEditor.layer_add.transform;
            iEditor.layer_set = true;
        }
    }
    private void OnDestroy() {
        Destroy(PanelSprite);
    }
}

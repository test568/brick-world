﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Collections.Generic;

public class SinglePanel: MonoBehaviour {
    public Text Name;
    public InputField Text;
    [HideInInspector]
    public DetailDate Base;

    [HideInInspector]
    public UIEditor iEditor;

    public PropertyInfo Parameter;

    private float old_object;

    private void Awake() {
        old_object=(float)Parameter.GetValue(Base.DetailComponent);
        Text.SetTextWithoutNotify(( (float)Parameter.GetValue(Base.DetailComponent) )+"");
        if (Parameter.CanWrite) {
            Text.onEndEdit.AddListener(delegate { SetText(); });
            }
        else {
            Text.readOnly=true;
            }
        }
    private void Update() {
        if (old_object!=(float)Parameter.GetValue(Base.DetailComponent)) {
            Text.SetTextWithoutNotify(( (float)Parameter.GetValue(Base.DetailComponent) )+"");
            old_object=(float)Parameter.GetValue(Base.DetailComponent);
            }
        }
    private void SetText() {
        try {
            float.Parse(Text.text);
            }
        catch {
            Text.text=0+"";
            }
        old_object=(float)Parameter.GetValue(Base.DetailComponent);
        Parameter.SetValue(Base.DetailComponent, float.Parse(Text.text));
        Text.SetTextWithoutNotify((float)Parameter.GetValue(Base.DetailComponent)+"");

        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();
        List<DetailDate> dates = new List<DetailDate>();
        for (int i = 0; i<details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date!=Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, float.Parse(Text.text));
                    dates.Add(date);
                    }
                catch { }
                }
            }
        dates.Add(Base);
        iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, dates.ToArray());
        }
    }

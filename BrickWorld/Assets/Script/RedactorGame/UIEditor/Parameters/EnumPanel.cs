﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Collections.Generic;
using System;
using System.Linq;

public class EnumPanel: MonoBehaviour {
    public Text Name;

    public Dropdown ListDrop;
    [HideInInspector]
    public DetailDate Base;

    [HideInInspector]
    public UIEditor iEditor;

    public PropertyInfo Parameter;

    private void Awake() {
        List<Dropdown.OptionData> opt = new List<Dropdown.OptionData>();
        Array value = Parameter.PropertyType.GetEnumValues();

        for (int i = 0; i<Parameter.PropertyType.GetEnumNames().Length; i++) {
            Dropdown.OptionData o = new Dropdown.OptionData();
            MemberInfo[] member = Parameter.PropertyType.GetMember(Parameter.PropertyType.GetEnumNames()[i]);
            MemberInfo enum_member = member.FirstOrDefault(m => m.DeclaringType==Parameter.PropertyType);
            o.text=( (RussianNameAttribute)enum_member.GetCustomAttribute(typeof(RussianNameAttribute), false) ).Name;
            opt.Add(o);
            }


        ListDrop.AddOptions(opt);
        if (Parameter.CanWrite)
            ListDrop.onValueChanged.AddListener(delegate { SetParams(); });
        ListDrop.SetValueWithoutNotify(GetParams());
        }
    private void Update() {
        int i = GetParams();
        if (i!=ListDrop.value) {
            ListDrop.SetValueWithoutNotify(i);
            }
        }
    public void SetParams() {
        object param = Enum.Parse(Parameter.PropertyType, Parameter.PropertyType.GetEnumNames()[ListDrop.value]);
        Parameter.SetValue(Base.DetailComponent, param);
        ListDrop.SetValueWithoutNotify(GetParams());

        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();

        List<DetailDate> dates = new List<DetailDate>();
        for (int i = 0; i<details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date!=Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, param);
                    dates.Add(date);
                    }
                catch { }
                }
            }
        dates.Add(Base);
        iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, dates.ToArray());
        }
    private int GetParams() {
        object param = Parameter.GetValue(Base.DetailComponent);
        for (int i = 0; i<param.GetType().GetEnumNames().Length; i++) {
            if (param.GetType().GetEnumNames()[i]==param.ToString()) {
                return i;
                }
            }
        return 0;
        }
    }

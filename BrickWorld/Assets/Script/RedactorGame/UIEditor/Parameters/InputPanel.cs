﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Collections.Generic;
using System;

public class InputPanel : MonoBehaviour {
    public Text Name;

    public Dropdown ListObject;
    public Dropdown ListOutput;

    [HideInInspector]
    public DetailDate Base;

    [HideInInspector]
    public UIEditor iEditor;

    [HideInInspector]
    public Editor_Controller Editor;

    public PropertyInfo Parameter;
    private object old_object;

    private bool Save = false;

    private List<string> cods = new List<string>();
    private List<PropertyInfo> properties = new List<PropertyInfo>();
    private void Start() {
        old_object = Parameter.GetValue(Base.DetailComponent);
        List<Dropdown.OptionData> option_data = new List<Dropdown.OptionData>();
        option_data.Add(new Dropdown.OptionData(""));
        cods.Add("0");
        for (int i = 0; i < Editor.DetailList.Count; i++) {
            if (Editor.DetailList[i].DetailComponent.GetType().GetCustomAttribute(typeof(HaveWireConnect))!=null) {

                option_data.Add(new Dropdown.OptionData(Editor.DetailList[i].DetailComponent.Name));
                cods.Add(Editor.DetailList[i].DetailComponent.Cod);
            }

        }
        ListObject.AddOptions(option_data);
        ListObject.onValueChanged.AddListener(delegate { UpdateTakeObjtect(); });
        ListOutput.onValueChanged.AddListener(delegate { SetParameter(); });

        if (( (WireConnect) old_object ).CodConnectDetail != null) {
            for (int i = 0; i < Editor.DetailList.Count; i++) {
                if (( (WireConnect) old_object ).CodConnectDetail == ( (DetailStandart) Editor.DetailList[i].DetailComponent ).Cod) {
                    for (int i2 = 0; i2 < cods.Count; i2++) {
                        if (cods[i2] == ( (WireConnect) old_object ).CodConnectDetail) {
                            ListObject.value = i2;
                        }
                    }
                    for (int i2 = 0; i2 < properties.Count; i2++) {
                        if (properties[i2] == ( (WireConnect) old_object ).ConnectParameter) {
                            ListOutput.value = i2;
                        }
                    }
                    break;
                }
                else if (i == Editor.DetailList.Count) {
                    Parameter.SetValue(Base.DetailComponent, null);
                }
            }
        }
        Save = true;
    }

    private void UpdateTakeObjtect() {
        properties.Clear();
        ListOutput.ClearOptions();
        if (ListObject.value != 0) {
            List<Dropdown.OptionData> option_data = new List<Dropdown.OptionData>();
            for (int i = 0; i < Editor.DetailList.Count; i++) {
                if (( (DetailStandart) Editor.DetailList[i].DetailComponent ).Cod == cods[ListObject.value]) {
                    Type[] interfaces_detail = Editor.DetailList[i].DetailComponent.GetType().GetInterfaces();
                    for (int i2 = 0; i2 < interfaces_detail.Length; i2++) {
                        for (int i3 = 0; i3 < interfaces_detail[i2].GetProperties().Length; i3++) {
                            if (interfaces_detail[i2].GetProperties()[i3].GetCustomAttribute(typeof(NoShowAttribute)) == null && interfaces_detail[i2].GetProperties()[i3].GetCustomAttribute(typeof(OutputAttribute)) != null
                                && ( (OutputAttribute) interfaces_detail[i2].GetProperties()[i3].GetCustomAttribute(typeof(OutputAttribute)) ).Type == ( (InputAttribute) Parameter.GetCustomAttribute(typeof(InputAttribute)) ).Type) {
                                option_data.Add(new Dropdown.OptionData(( (RussianNameAttribute) interfaces_detail[i2].GetProperties()[i3].GetCustomAttribute(typeof(RussianNameAttribute)) ).Name));
                                properties.Add(interfaces_detail[i2].GetProperties()[i3]);
                            }
                        }
                    }
                }
            }
            ListOutput.AddOptions(option_data);
            if (properties.Count > 0) {
                ListOutput.value = 0;
                SetParameter();
            }
            else {
                Parameter.SetValue(Base.DetailComponent, null);
            }
        }
        else {
            Parameter.SetValue(Base.DetailComponent, null);
        }
    }

    private void SetParameter() {
        if (Save) {
            WireConnect wire;
            wire.CodConnectDetail = cods[ListObject.value];
            wire.ConnectParameter = properties[ListOutput.value];
            Parameter.SetValue(Base.DetailComponent, wire);
            iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, new DetailDate[1] { Base });
        }
    }
}
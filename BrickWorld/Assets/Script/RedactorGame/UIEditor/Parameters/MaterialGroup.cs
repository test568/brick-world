﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MaterialGroup : MonoBehaviour {

    public MaterialPanel Panel;
    public Image Sprite;
    public TypeMaterialGroup TypeGroup;
    public Text Name;
    public void Init() {
        GetComponent<Button>().onClick.AddListener(() => { Panel.PrintMaterial(TypeGroup); });
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;
public class Vector3Panel: MonoBehaviour {
    public Text Name;
    public InputField X;
    public InputField Y;
    public InputField Z;

    [HideInInspector]
    public UIEditor iEditor;

    [HideInInspector]
    public DetailDate Base;

    public PropertyInfo Parameter;
    private Vector3Serializable old_object;

    private void Awake() {
        old_object=(Vector3Serializable)Parameter.GetValue(Base.DetailComponent);

        X.SetTextWithoutNotify(( (Vector3Serializable)Parameter.GetValue(Base.DetailComponent) ).x+"");
        Y.SetTextWithoutNotify(( (Vector3Serializable)Parameter.GetValue(Base.DetailComponent) ).y+"");
        Z.SetTextWithoutNotify(( (Vector3Serializable)Parameter.GetValue(Base.DetailComponent) ).z+"");
        if (Parameter.CanWrite) {
            X.onEndEdit.AddListener(delegate { SetText(); });
            Y.onEndEdit.AddListener(delegate { SetText(); });
            Z.onEndEdit.AddListener(delegate { SetText(); });
            }
        else {
            X.readOnly=true;
            Y.readOnly=true;
            Z.readOnly=true;
            }
        }
    private void Update() {
        if (old_object!=(Vector3Serializable)Parameter.GetValue(Base.DetailComponent)) {
            X.SetTextWithoutNotify(( (Vector3Serializable)Parameter.GetValue(Base.DetailComponent) ).x+"");
            Y.SetTextWithoutNotify(( (Vector3Serializable)Parameter.GetValue(Base.DetailComponent) ).y+"");
            Z.SetTextWithoutNotify(( (Vector3Serializable)Parameter.GetValue(Base.DetailComponent) ).z+"");
            old_object=(Vector3Serializable)Parameter.GetValue(Base.DetailComponent);
            }
        }

    private void SetText() {
        try {
            float.Parse(X.text);
            }
        catch {
            X.SetTextWithoutNotify(0+"");
            }
        try {
            float.Parse(Y.text);
            }
        catch {
            Y.SetTextWithoutNotify(0+"");
            }
        try {
            float.Parse(Z.text);
            }
        catch {
            Z.SetTextWithoutNotify(0+"");
            }

        Parameter.SetValue(Base.DetailComponent, new Vector3Serializable(float.Parse(X.text), float.Parse(Y.text), float.Parse(Z.text)));
        Vector3 s = Vector3Serializable.ToVector3((Vector3Serializable)Parameter.GetValue(Base.DetailComponent));
        X.SetTextWithoutNotify(Math.Round(s.x, 2)+"");
        Y.SetTextWithoutNotify(Math.Round(s.y, 2)+"");
        Z.SetTextWithoutNotify(Math.Round(s.z, 2)+"");


        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();
        List<DetailDate> dates = new List<DetailDate>();
        for (int i = 0; i<details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date!=Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, new Vector3Serializable(float.Parse(X.text), float.Parse(Y.text), float.Parse(Z.text)));
                    dates.Add(date);
                    }
                catch { }
                }
            }
        dates.Add(Base);
        iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, dates.ToArray());

        old_object=(Vector3Serializable)Parameter.GetValue(Base.DetailComponent);
        }
    }

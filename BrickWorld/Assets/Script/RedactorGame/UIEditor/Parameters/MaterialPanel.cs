﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class MaterialPanel : MonoBehaviour {
    public Text Name;
    public Button BaseText;
    public Button DetectedOFF;
    public Image PanelMaterial;

    public GameObject Content;

    [HideInInspector]
    public UIEditor iEditor;

    [HideInInspector]
    public DetailDate Base;

    public PropertyInfo Parameter;

    [SerializeField] Button bake;

    [SerializeField] GameObject BaseMaterialItem;

    [SerializeField] GameObject BaseMaterialGroup;

    private object old_object;

    private bool group_list = true;

    private List<Transform> object_list = new List<Transform>();

    private void Awake() {
        bake.onClick.AddListener(delegate { BrakeGroup(); });
        old_object = Parameter.GetValue(Base.DetailComponent);
        BaseText.GetComponent<Text>().text = MaterialDataBase.DataBase.FindMaterila((TypeMaterialDetail) Parameter.GetValue(Base.DetailComponent)).RussianName;
        if (Parameter.CanWrite) {
            BaseText.onClick.AddListener(delegate { ActivePanel(); });
            DetectedOFF.onClick.AddListener(delegate { ActivePanel(); });
            BrakeGroup();
        }
    }

    private void Update() {
        if (!Equals(old_object, Parameter.GetValue(Base.DetailComponent))) {
            BaseText.GetComponent<Text>().text = MaterialDataBase.DataBase.FindMaterila((TypeMaterialDetail) Parameter.GetValue(Base.DetailComponent)).RussianName;
            old_object = Parameter.GetValue(Base.DetailComponent);
        }
    }

    public void SetMaterial(TypeMaterialDetail type) {
        BaseText.GetComponent<Text>().text = MaterialDataBase.DataBase.FindMaterila(type).RussianName;

        Parameter.SetValue(Base.DetailComponent, type);
        old_object = Parameter.GetValue(Base.DetailComponent);

        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();
        List<DetailDate> dates = new List<DetailDate>();
        for (int i = 0; i < details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date != Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, type);
                    dates.Add(date);
                }
                catch { }
            }
        }
        dates.Add(Base);
        iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, dates.ToArray());
    }

    public void BrakeGroup() {
        group_list = true;

        Clear();

        MaterialGroupData[] groupDatas = MaterialDataBase.DataBase.GroupDatas;
        for (int i = 0; i < groupDatas.Length; i++) {
            MaterialGroup materialGroup = Instantiate(BaseMaterialGroup, Content.transform).GetComponent<MaterialGroup>();
            materialGroup.Panel = this;
            materialGroup.Name.text = groupDatas[i].RussianName;
            materialGroup.TypeGroup = groupDatas[i].Type;
            materialGroup.Init();
            materialGroup.Sprite.sprite = groupDatas[i].ImageMaterialGroup;
            materialGroup.gameObject.SetActive(true);
            object_list.Add(materialGroup.transform);
        }
    }

    public void PrintMaterial(TypeMaterialGroup TypeGroup) {
        group_list = false;

        Clear();
        MaterialGroupData groupData = null;
        for (int i = 0; i < MaterialDataBase.DataBase.GroupDatas.Length; i++) {
            if (TypeGroup == MaterialDataBase.DataBase.GroupDatas[i].Type) {
                groupData = MaterialDataBase.DataBase.GroupDatas[i];
                break;
            }
        }
        for (int i = 0; i < groupData.Materials.Length; i++) {
            MaterialDataItem materialData = groupData.Materials[i];
            MaterialItem materialItem = Instantiate(BaseMaterialItem, Content.transform).GetComponent<MaterialItem>();
            materialItem.Name.text = materialData.RussianName;
            materialItem.Panel = this;
            materialItem.Type = materialData.Type;
            materialItem.Sprite.sprite = materialData.ImageMaterial;
            materialItem.Init();
            materialItem.gameObject.SetActive(true);
            object_list.Add(materialItem.transform);
        }

    }

    public void Clear() {
        for (int i = 0; i < object_list.Count; i++) {
            Destroy(object_list[i].gameObject);
        }
        object_list.Clear();
    }

    private void ActivePanel() {
        if (PanelMaterial.gameObject.active == true && Input.GetMouseButtonUp(0) && iEditor.layer_set) {
            PanelMaterial.gameObject.active = false;
            PanelMaterial.gameObject.transform.parent = BaseText.transform;
            iEditor.layer_set = false;
            return;
        }
        if (PanelMaterial.gameObject.active == false && Input.GetMouseButtonUp(0) && !iEditor.layer_set) {
            PanelMaterial.gameObject.active = true;
            PanelMaterial.gameObject.transform.parent = iEditor.layer_add.transform;
            iEditor.layer_set = true;
        }
    }

    private void OnDestroy() {
        Destroy(PanelMaterial);
    }
}

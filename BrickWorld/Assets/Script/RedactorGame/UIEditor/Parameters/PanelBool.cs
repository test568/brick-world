﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Collections.Generic;
public class PanelBool: MonoBehaviour {
    public Text Name;
    public Toggle Text;
    [HideInInspector]
    public DetailDate Base;

    [HideInInspector]
    public UIEditor iEditor;

    public PropertyInfo Parameter;

    private bool old_object;

    private void Awake() {
        old_object=(bool)Parameter.GetValue(Base.DetailComponent);
        Text.SetIsOnWithoutNotify((bool)Parameter.GetValue(Base.DetailComponent));
        Text.onValueChanged.AddListener(delegate { SetBool(); });
        }
    private void Update() {
        if (old_object!=(bool)Parameter.GetValue(Base.DetailComponent)) {
            Text.SetIsOnWithoutNotify((bool)Parameter.GetValue(Base.DetailComponent));
            old_object=(bool)Parameter.GetValue(Base.DetailComponent);
            }
        }
    private void SetBool() {
        old_object=(bool)Parameter.GetValue(Base.DetailComponent);
        Parameter.SetValue(Base.DetailComponent, Text.isOn);
        Text.SetIsOnWithoutNotify((bool)Parameter.GetValue(Base.DetailComponent));

        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();
        List<DetailDate> dates = new List<DetailDate>();
        for (int i = 0; i<details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date!=Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, Text.isOn);
                    dates.Add(date);
                    }
                catch { }
                }
            }
        dates.Add(Base);
        iEditor.Editor.SaveStory(Editor_Controller.TypeHistory.EditParameterDetail, dates.ToArray());
        }
    }

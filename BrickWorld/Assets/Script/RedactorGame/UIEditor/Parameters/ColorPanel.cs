﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Text.RegularExpressions;

public class ColorPanel: MonoBehaviour {
    public Text Name;
    public Button BaseColor;
    public Button DetectorOFF;
    public Image PanelColor;

    public Image Gradient;
    public Slider ScrollbarHue;
    public Image Hue;
    public Slider R;
    public Slider G;
    public Slider B;
    public InputField RInput;
    public InputField GInput;
    public InputField BInput;
    public InputField Hex;

    public Image Joistick;

    private Texture2D HueTexture;
    private Texture2D GradientTexture;

    [HideInInspector]
    public UIEditor iEditor;

    [HideInInspector]
    public DetailDate Base;

    public PropertyInfo Parameter;

    private object old_object;

    private void Awake() {
        old_object=Parameter.GetValue(Base.DetailComponent);
        BaseColor.image.color=ColorSerializable.ToColor((ColorSerializable)old_object);
        if (Parameter.CanWrite) {
            BaseColor.onClick.AddListener(delegate { ActivePanel(); });
            DetectorOFF.onClick.AddListener(delegate { ActivePanel(); });
            }

        Color color = ColorSerializable.ToColor((ColorSerializable)Parameter.GetValue(Base.DetailComponent));
        float h, s, v;
        Color.RGBToHSV(color, out h, out s, out v);


        GradientTexture=new Texture2D((int)Gradient.rectTransform.rect.width, (int)Gradient.rectTransform.rect.height);
        Gradient.sprite=Sprite.Create(GradientTexture, new Rect(0, 0, GradientTexture.width, GradientTexture.height), new Vector2(0.5f, 0.5f), 100f);

        List<Color> pixels = new List<Color>();
        for (int i = 0; i<GradientTexture.height; i++) {
            for (int i2 = 0; i2<GradientTexture.width; i2++) {
                pixels.Add(Color.HSVToRGB(h, (float)i2/GradientTexture.width, (float)i/GradientTexture.height));
                }
            }

        GradientTexture.SetPixels(pixels.ToArray());
        GradientTexture.Apply();

        HueTexture=new Texture2D(1, (int)Hue.rectTransform.rect.height);

        for (int i = 0; i<HueTexture.height; i++) {
            HueTexture.SetPixel(0, i, Color.HSVToRGB((float)i/( HueTexture.height-1 ), 1f, 1f));
            }
        HueTexture.Apply();
        Hue.sprite=Sprite.Create(HueTexture, new Rect(0f, 0f, HueTexture.width, HueTexture.height), new Vector2(0.5f, 0.5f), 100f);

        RInput.SetTextWithoutNotify(""+color.r*255f);
        GInput.SetTextWithoutNotify(""+color.g*255f);
        BInput.SetTextWithoutNotify(""+color.b*255f);


        ScrollbarHue.onValueChanged.AddListener(delegate { UpdateColorHue(true); });
        R.onValueChanged.AddListener(delegate { UpdateSliderRGB(true); });
        G.onValueChanged.AddListener(delegate { UpdateSliderRGB(true); });
        B.onValueChanged.AddListener(delegate { UpdateSliderRGB(true); });
        RInput.onValueChanged.AddListener(delegate { UpdateInputRGB(true); });
        GInput.onValueChanged.AddListener(delegate { UpdateInputRGB(true); });
        BInput.onValueChanged.AddListener(delegate { UpdateInputRGB(true); });

        Hex.onValueChanged.AddListener(delegate { UpdateColorHex(true); });

        UpdateInputRGB(false);
        }

    private void Update() {
        if (!Equals(old_object, Parameter.GetValue(Base.DetailComponent))) {
            ColorSerializable col = (ColorSerializable)Parameter.GetValue(Base.DetailComponent);
            col.a=1f;
            BaseColor.image.color=ColorSerializable.ToColor(col);
            old_object=Parameter.GetValue(Base.DetailComponent);
            }
        if (PanelColor.gameObject.active)
            if (Input.GetMouseButton(0)) {
                Vector3 pos = ( iEditor.layer_add.transform.position-Gradient.transform.position )-( new Vector3(Screen.width/2f, Screen.height/2f, 0)-Input.mousePosition );
                if (Mathf.Abs(pos.x)<=Gradient.rectTransform.rect.width/2f&&Mathf.Abs(pos.y)<=Gradient.rectTransform.rect.height/2f) {
                    Joistick.rectTransform.anchoredPosition=( pos-new Vector3(-Gradient.rectTransform.rect.width, -Gradient.rectTransform.rect.height, 0)/2f );
                    UpdateColorJoistick(true);
                    }
                }
        }

    public void SetColor(Color color) {
        BaseColor.image.color=color;
        color.a=MaterialDataBase.DataBase.FindMaterila(Base.DetailComponent.TypeMaterial).Material.color.a;
        Parameter.SetValue(Base.DetailComponent, new ColorSerializable(color));
        old_object=Parameter.GetValue(Base.DetailComponent);
        GameObject[] details_group = Base.Editor.GetComponent<Editor_Controller>().DetailsGroup.ToArray();
        for (int i = 0; i<details_group.Length; i++) {
            DetailDate date = details_group[i].GetComponent<DetailDate>();
            if (date!=Base) {
                try {
                    Parameter.SetValue(date.DetailComponent, new ColorSerializable(color));
                    }
                catch { }
                }
            }
        Debug.Log(1);
        }

    private void ActivePanel() {
        if (PanelColor.gameObject.active==true&&Input.GetMouseButtonUp(0)&&iEditor.layer_set) {
            PanelColor.gameObject.active=false;
            PanelColor.gameObject.transform.parent=BaseColor.transform;
            iEditor.layer_set=false;
            return;
            }
        if (PanelColor.gameObject.active==false&&Input.GetMouseButtonUp(0)&&!iEditor.layer_set) {
            PanelColor.gameObject.active=true;
            PanelColor.gameObject.transform.parent=iEditor.layer_add.transform;
            iEditor.layer_set=true;
            }
        }

    private void OnDestroy() {
        Destroy(PanelColor);
        }
    private void UpdateColorHue(bool noSet) {
        List<Color> pixels = new List<Color>();
        for (int i = 0; i<GradientTexture.height; i++) {
            for (int i2 = 0; i2<GradientTexture.width; i2++) {
                pixels.Add(Color.HSVToRGB(ScrollbarHue.value, (float)i2/GradientTexture.width, (float)i/GradientTexture.height));
                }
            }

        GradientTexture.SetPixels(pixels.ToArray());
        GradientTexture.Apply();
        UpdateColorJoistick(noSet);
        }

    private void UpdateSliderRGB(bool noSet) {
        Color colorRGB = new Color(R.value, G.value, B.value);
        float value, x, y;
        Color.RGBToHSV(colorRGB, out value, out x, out y);
        ScrollbarHue.SetValueWithoutNotify(value);

        Joistick.rectTransform.anchoredPosition=new Vector2(x*Gradient.rectTransform.rect.width, y*Gradient.rectTransform.rect.height);

        List<Color> pixels = new List<Color>();
        for (int i = 0; i<GradientTexture.height; i++) {
            for (int i2 = 0; i2<GradientTexture.width; i2++) {
                pixels.Add(Color.HSVToRGB(value, (float)i2/GradientTexture.width, (float)i/GradientTexture.height));
                }
            }

        GradientTexture.SetPixels(pixels.ToArray());
        GradientTexture.Apply();

        RInput.SetTextWithoutNotify(""+(int)( R.value*255f ));
        GInput.SetTextWithoutNotify(""+(int)( G.value*255f ));
        BInput.SetTextWithoutNotify(""+(int)( B.value*255f ));
        Hex.SetTextWithoutNotify(ColorUtility.ToHtmlStringRGBA(colorRGB));
        if (noSet)
            SetColor(colorRGB);
        }

    private void UpdateInputRGB(bool noSet) {
        float r = 0;
        if (RInput.text!="")
            r=float.Parse(RInput.text);
        float g = 0;
        if (GInput.text!="")
            g=float.Parse(GInput.text);
        float b = 0;
        if (BInput.text!="")
            b=float.Parse(BInput.text);


        Color colorRGB = new Color(Mathf.Min(r*( 1f/255f ), 1f), Mathf.Min(g*( 1f/255f ), 1f), Mathf.Min(b*( 1f/255f ), 1f));
        R.SetValueWithoutNotify(r*( 1f/255f ));
        G.SetValueWithoutNotify(g*( 1f/255f ));
        B.SetValueWithoutNotify(b*( 1f/255f ));
        UpdateSliderRGB(noSet);
        Hex.SetTextWithoutNotify(ColorUtility.ToHtmlStringRGBA(colorRGB));
        if (noSet)
            SetColor(colorRGB);
        }

    private void UpdateColorJoistick(bool noSet) {
        Color color = Color.HSVToRGB(ScrollbarHue.value, ( Joistick.rectTransform.anchoredPosition.x/Gradient.rectTransform.rect.width ), ( Joistick.rectTransform.anchoredPosition.y/Gradient.rectTransform.rect.height ));

        R.SetValueWithoutNotify(color.r);
        G.SetValueWithoutNotify(color.g);
        B.SetValueWithoutNotify(color.b);
        RInput.SetTextWithoutNotify(""+(int)( color.r*255f ));
        GInput.SetTextWithoutNotify(""+(int)( color.g*255f ));
        BInput.SetTextWithoutNotify(""+(int)( color.b*255f ));

        Hex.SetTextWithoutNotify(ColorUtility.ToHtmlStringRGBA(color));
        if (noSet)
            SetColor(color);
        }

    private void UpdateColorHex(bool noSet) {
        string value = Hex.text;

        value=Regex.Replace(value.ToUpper(), "[^0-9A-F]", "");

        Hex.text=value;

        Color color;

        if (ColorUtility.TryParseHtmlString("#"+value, out color)&&value.Length==8) {
            RInput.SetTextWithoutNotify(""+color.r*255f);
            GInput.SetTextWithoutNotify(""+color.g*255f);
            BInput.text=""+color.b*255f;
            if (noSet)
                SetColor(color);
            }
        }
    }

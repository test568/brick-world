﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteItem: MonoBehaviour {
    public SpritePanel Panel;

    public void Init() {
        GetComponent<Button>().onClick.AddListener(() => { Panel.SetSprite(gameObject.name); });
        }
    }

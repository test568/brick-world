﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroupDetailPanel: MonoBehaviour {
    public Text text;
    public Image Sprite;
    public GroupBase page;
    public Button Detector;

    [HideInInspector]
    public UIEditor iEditor;

    private void Awake() {
        Detector.onClick.AddListener(delegate { iEditor.DrawPageDetails(page); });
        }
    }

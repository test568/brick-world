﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailPanel : MonoBehaviour {
    public Image Sprite;
    public Text Name;
    public Button Detector;

    [HideInInspector]
    public UIEditor iEditor;

    [HideInInspector]
    public GameObject PrefabDetail;

    private void Awake() {
        Detector.onClick.AddListener(delegate { Editor_Controller.singleton.SpawnDetail(PrefabDetail); });
    }
}

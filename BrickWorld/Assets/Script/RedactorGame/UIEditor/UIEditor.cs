﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class UIEditor : MonoBehaviour {

    public GameObject CanvasEditor;

    public Button Exit;
    public Button Spawn;

    [Header("Parameters List")]
    public GameObject panel;
    public GameObject old_take_detail = null;

    public GameObject layer_add;

    [HideInInspector]
    public bool layer_set;

    private List<GameObject> Parameters = new List<GameObject>();

    [Header("Panels draw")]
    public GameObject BoolPanel;
    public GameObject Vector3Panel;
    public GameObject SinglePanel;
    public GameObject StringPanel;
    public GameObject NameInterfacePanel;
    public GameObject ColorPanel;
    public GameObject MaterialPanel;
    public GameObject Input;
    public GameObject EnumPanel;
    public GameObject SpritePanel;

    [Header("Details List")]
    public GameObject PanelDetailList;
    public Button BreakList;
    public GameObject GroupDetail;
    public GameObject DetailUI;
    public FullDatabase Database;

    [Header("FileSystem")]
    public Button NewFile;
    public Button Load;
    public Button Save;

    public GameObject PanelConfirmation;
    public GameObject PanelLoadFile;
    public GameObject PanelSaveFile;

    public GameObject FileText;

    [Header("Details")]
    public Text TextCount;

    private GameObject take_file;
    private bool is_take_file;
    private List<GameObject> files = new List<GameObject>();

    public bool IsOpenPanel;

    public Editor_Controller Editor { get => Editor_Controller.singleton; }

    private void Start() {
        Exit.onClick.AddListener(delegate { ExitEditor(); });
        Spawn.onClick.AddListener(delegate { SpawnBuilding(); });
        AwakePanelDetail();
        AwakeSave();
    }

    private void Update() {
        try {
            if (old_take_detail != Editor.DetailMove) {
                old_take_detail = Editor.DetailMove;
                ClearPanel();
                if (old_take_detail != null) {
                    DrawPanel(old_take_detail.GetComponent<DetailDate>().DetailComponent);
                }
            }
        }
        catch (Exception ex) {
            Debug.Log(ex.Message);
        }

        TextCount.text = ( Editor.DetailsGroup.Count + "/" + Editor.DetailList.Count );
    }

    #region Панель параметров
    public void DrawPanel(object detail_date) {
        ClearPanel();
        Type type_detail = detail_date.GetType();
        Type[] interfaces_detail = type_detail.GetInterfaces();
        for (int i = 0; i < interfaces_detail.Length; i++) {
            SpawnNameInterface(interfaces_detail[i]);
            for (int i2 = 0; i2 < interfaces_detail[i].GetProperties().Length; i2++) {
                if (interfaces_detail[i].GetProperties()[i2].GetCustomAttribute(typeof(NoShowAttribute)) == null) {
                    string parameter_type = interfaces_detail[i].GetProperties()[i2].ToString();
                    if (parameter_type.IndexOf("Vector3Serializable") != -1) {
                        SpawnVector3(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (parameter_type.IndexOf("Single") != -1) {
                        SpawnSignle(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (parameter_type.IndexOf("Color") != -1) {
                        SpawnColor(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (parameter_type.IndexOf("TypeMaterialDetail") != -1) {
                        SpawnMaterial(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (parameter_type.IndexOf("String") != -1) {
                        SpawnString(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (parameter_type.IndexOf("Bool") != -1) {
                        SpawnBool(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (interfaces_detail[i].GetProperties()[i2].GetCustomAttribute(typeof(InputAttribute)) != null) {
                        SpawnInput(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (interfaces_detail[i].GetProperties()[i2].PropertyType.IsEnum) {
                        SpawnEnum(interfaces_detail[i].GetProperties()[i2]);
                    }
                    else if (parameter_type.IndexOf("PaintSprite") != -1) {
                        SpawnSprite(interfaces_detail[i].GetProperties()[i2]);
                    }
                }
            }
        }
    }

    private void SpawnVector3(System.Reflection.PropertyInfo parameter) {
        GameObject obj = Instantiate(Vector3Panel, panel.transform);
        Vector3Panel panelv = obj.GetComponent<Vector3Panel>();
        panelv.Name.text = GetName(parameter);
        panelv.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panelv.Parameter = parameter;
        panelv.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnSignle(System.Reflection.PropertyInfo parameter) {
        GameObject obj = Instantiate(SinglePanel, panel.transform);
        SinglePanel panels = obj.GetComponent<SinglePanel>();
        panels.Name.text = GetName(parameter);
        panels.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panels.Parameter = parameter;
        panels.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnString(System.Reflection.PropertyInfo parameter) {
        GameObject obj = Instantiate(StringPanel, panel.transform);
        StringPanel panels = obj.GetComponent<StringPanel>();
        panels.Name.text = GetName(parameter);
        panels.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panels.Parameter = parameter;
        panels.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnColor(System.Reflection.PropertyInfo parameter) {
        GameObject obj = Instantiate(ColorPanel, panel.transform);
        ColorPanel panelc = obj.GetComponent<ColorPanel>();
        panelc.Name.text = GetName(parameter);
        panelc.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panelc.Parameter = parameter;
        panelc.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnBool(System.Reflection.PropertyInfo parameter) {
        GameObject obj = Instantiate(BoolPanel, panel.transform);
        PanelBool panelb = obj.GetComponent<PanelBool>();
        panelb.Name.text = GetName(parameter);
        panelb.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panelb.Parameter = parameter;
        panelb.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnNameInterface(Type parameter) {
        GameObject obj = Instantiate(NameInterfacePanel, panel.transform);
        string text_russian = "";
        for (int i = 0; i < parameter.GetCustomAttributes(false).Length; i++) {
            if (parameter.GetCustomAttributes(false)[i].GetType().Name == "RussianNameAttribute") {
                text_russian = ( (RussianNameAttribute) parameter.GetCustomAttributes(false)[i] ).Name;
            }
        }
        obj.GetComponent<NameInterface>().Name.text = text_russian;
        obj.GetComponent<NameInterface>().iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnMaterial(System.Reflection.PropertyInfo parameter) {
        GameObject obj = Instantiate(MaterialPanel, panel.transform);
        MaterialPanel panelm = obj.GetComponent<MaterialPanel>();
        panelm.Name.text = GetName(parameter);
        panelm.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panelm.Parameter = parameter;
        panelm.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnInput(System.Reflection.PropertyInfo parameter) {
        GameObject obj = Instantiate(Input, panel.transform);
        InputPanel paneli = obj.GetComponent<InputPanel>();
        paneli.Name.text = GetName(parameter);
        paneli.Base = Editor.DetailMove.GetComponent<DetailDate>();
        paneli.Editor = Editor;
        paneli.Parameter = parameter;
        paneli.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnEnum(PropertyInfo parameter) {
        GameObject obj = Instantiate(EnumPanel, panel.transform);
        EnumPanel panele = obj.GetComponent<EnumPanel>();
        panele.Name.text = GetName(parameter);
        panele.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panele.Parameter = parameter;
        panele.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private void SpawnSprite(PropertyInfo parameter) {
        GameObject obj = Instantiate(SpritePanel, panel.transform);
        SpritePanel panels = obj.GetComponent<SpritePanel>();
        panels.Name.text = GetName(parameter);
        panels.Base = Editor.DetailMove.GetComponent<DetailDate>();
        panels.Parameter = parameter;
        panels.iEditor = GetComponent<UIEditor>();
        obj.SetActive(true);
    }

    private string GetName(System.Reflection.PropertyInfo parameter) {
        string text_russian = "";
        for (int i = 0; i < parameter.GetCustomAttributes(false).Length; i++) {
            if (parameter.GetCustomAttributes(false)[i].GetType().Name == "RussianNameAttribute") {
                text_russian = ( (RussianNameAttribute) parameter.GetCustomAttributes(false)[i] ).Name;
            }
        }
        return text_russian + ":";
    }

    private void ClearPanel() {
        int count = panel.transform.childCount;
        for (int i = 0; i < count; i++) {
            Destroy(panel.transform.GetChild(i).gameObject);
        }
        List<GameObject> gms = new List<GameObject>();
        for (int i = 0; i < layer_add.transform.childCount; i++) {
            gms.Add(layer_add.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < gms.Count; i++)
            Destroy(gms[i]);
        layer_set = false;
        Parameters.Clear();
    }
    #endregion

    #region Панель деталей
    private void AwakePanelDetail() {
        BreakList.onClick.AddListener(delegate { DrawPageStart(); });
        ClearDetailPanel();
        DrawPageStart();
    }

    public void DrawPageStart() {
        ClearDetailPanel();
        for (int i = 0; i < Database.ToArray.Length; i++) {
            DetailDatabase data = Database.ToArray[i];
            if (data.Count != 0) {
                GameObject obj = Instantiate(GroupDetail, PanelDetailList.transform);
                GroupDetailPanel groupDetail = obj.GetComponent<GroupDetailPanel>();
                groupDetail.text.text = data.Name;
                groupDetail.Sprite.sprite = data.Sprite;
                groupDetail.page = data.Group;
                groupDetail.iEditor = this;
                obj.SetActive(true);
            }
        }
    }

    public void DrawPageDetails(GroupBase groupBase) {
        ClearDetailPanel();
        GameObject[] details = Database.FindGroup(groupBase).ToArray;
        for (int i = 0; i < details.Length; i++) {
            GameObject obj = Instantiate(DetailUI, PanelDetailList.transform);
            DetailPanel detailPanel = obj.GetComponent<DetailPanel>();
            DetailDate detail = details[i].GetComponent<DetailDate>();
            detailPanel.iEditor = this;
            detailPanel.Sprite.sprite = detail.Sprite;
            detailPanel.Name.text = detail.Name;
            detailPanel.PrefabDetail = details[i];
            obj.SetActive(true);
        }
    }

    public void ClearDetailPanel() {
        int count = PanelDetailList.transform.childCount;
        for (int i = 0; i < count; i++) {
            Destroy(PanelDetailList.transform.GetChild(i).gameObject);
        }
    }

    #endregion

    #region Сохранение, загрузка, создание
    private void AwakeSave() {
        NewFile.onClick.AddListener(delegate { New(); });
        Load.onClick.AddListener(delegate { LoadFilePanel(); });
        Save.onClick.AddListener(delegate { SaveFilePanel(); });

        PanelLoadFile.GetComponent<LoadFileUI>().Load.onClick.AddListener(delegate { LoadTakeFile(); });
        PanelLoadFile.GetComponent<LoadFileUI>().Delet.onClick.AddListener(delegate { DeletTakeFile(); });
        PanelLoadFile.GetComponent<LoadFileUI>().InputField.onValueChanged.AddListener(delegate { UpdateFindList(); });

        PanelSaveFile.GetComponent<SaveFileUI>().FindInputField.onValueChanged.AddListener(delegate { UpdateFindList(); });
        PanelSaveFile.GetComponent<SaveFileUI>().Delet.onClick.AddListener(delegate { DeletTakeFile(); });
        PanelSaveFile.GetComponent<SaveFileUI>().Save.onClick.AddListener(delegate { SaveFile(); });
    }

    private void SpawnBuilding() {
        if (Editor.DetailList.Count != 0) {
            ControllerPlayer.MainPlayer.Status = PlayerStatus.SpawnBuild;
        }
        else {
            ExitEditor();
        }
    }

    private void ExitEditor() {
        ControllerPlayer.MainPlayer.Status = PlayerStatus.Move;
    }

    private void New() {
        if (!IsOpenPanel) {
            PanelConfirmation.SetActive(true);
            PanelConfirmation.GetComponent<ConfirmationPanelUI>().TextPanel.text = "Вы уверенны что хотите создать новый файл? Все не сохранненое будет удалено!";
            PanelConfirmation.GetComponent<ConfirmationPanelUI>().Yes.onClick.RemoveAllListeners();
            PanelConfirmation.GetComponent<ConfirmationPanelUI>().Yes.onClick.AddListener(delegate { CreateNew(); });
            PanelConfirmation.GetComponent<ConfirmationPanelUI>().No.onClick.RemoveAllListeners();
            PanelConfirmation.GetComponent<ConfirmationPanelUI>().No.onClick.AddListener(delegate { NoCreateNew(); });
            IsOpenPanel = true;
        }
    }

    private void CreateNew() {
        PanelConfirmation.SetActive(false);
        IsOpenPanel = false;
        Editor.ClearEditor();
    }

    private void NoCreateNew() {
        PanelConfirmation.SetActive(false);
        IsOpenPanel = false;
    }

    private void LoadFilePanel() {
        if (IsOpenPanel) {
            return;
        }
        IsOpenPanel = true;
        PanelLoadFile.SetActive(true);
        PanelLoadFile.GetComponent<LoadFileUI>().Close.onClick.RemoveAllListeners();
        PanelLoadFile.GetComponent<LoadFileUI>().Close.onClick.AddListener(delegate { ClosePanelFile(); });
        take_file = null;
        is_take_file = false;
        UpdateList("");
    }

    private void ClosePanelFile() {
        for (int i = 0; i < files.Count; i++) {
            Destroy(files[i]);
        }
        if (PanelLoadFile.activeSelf == true) {
            PanelLoadFile.SetActive(false);
        }
        else if (PanelSaveFile.activeSelf == true) {
            PanelSaveFile.SetActive(false);
        }
        IsOpenPanel = false;
        IsOpenPanel = false;
    }

    public void ClickButtonFIle(GameObject button) {
        if (button == take_file) {
            button.GetComponent<TextSaveUI>().SetFon.SetActive(false);
            take_file = null;
            is_take_file = false;
        }
        else {
            if (take_file) {
                take_file.GetComponent<TextSaveUI>().SetFon.SetActive(false);
            }
            button.GetComponent<TextSaveUI>().SetFon.SetActive(true);
            take_file = button;
            is_take_file = true;
            if (PanelSaveFile.activeSelf == true) {
                PanelSaveFile.GetComponent<SaveFileUI>().NameInputFeild.text = take_file.GetComponent<TextSaveUI>().file_name;
            }
        }
    }

    private void UpdateList(string find_name) {
        for (int i = 0; i < files.Count; i++) {
            Destroy(files[i]);
        }
        LoadFile[] list = FileSystemEditor.LoadList();

        for (int i = 0; i < list.Length; i++) {
            if (list.Where((x) => x.Name.IndexOf(find_name) != -1).FirstOrDefault() != null) {
                GameObject obj = null;
                if (PanelLoadFile.activeSelf == true) {
                    obj = Instantiate(FileText, PanelLoadFile.GetComponent<LoadFileUI>().PanelSpawn.transform);
                }
                else if (PanelSaveFile.activeSelf == true) {
                    obj = Instantiate(FileText, PanelSaveFile.GetComponent<SaveFileUI>().PanelSpawn.transform);
                }

                TextSaveUI text = obj.GetComponent<TextSaveUI>();
                text.file_name = list[i].Name;
                text.SetImage(list[i].Image);
                text.Name.text = list[i].Name;
                text.iEditor = GetComponent<UIEditor>();
                text.Detector.onClick.AddListener(delegate { obj.GetComponent<TextSaveUI>().Click(); });
                obj.SetActive(true);
                files.Add(obj);
            }
        }



    }

    private void SaveFilePanel() {
        if (IsOpenPanel) {
            return;
        }
        IsOpenPanel = true;
        PanelSaveFile.SetActive(true);
        PanelSaveFile.GetComponent<SaveFileUI>().Close.onClick.RemoveAllListeners();
        PanelSaveFile.GetComponent<SaveFileUI>().Close.onClick.AddListener(delegate { ClosePanelFile(); });
        take_file = null;
        is_take_file = false;
        UpdateList("");
    }

    private void UpdateFindList() {
        if (PanelLoadFile.activeSelf == true) {
            UpdateList(PanelLoadFile.GetComponent<LoadFileUI>().InputField.text);
        }
        else if (PanelSaveFile.activeSelf == true) {
            UpdateList(PanelSaveFile.GetComponent<SaveFileUI>().FindInputField.text);
        }
    }

    private void LoadTakeFile() {
        if (is_take_file) {
            FileSystemEditor.LoadFile(take_file.GetComponent<TextSaveUI>().file_name, Editor);
        }
    }

    private void DeletTakeFile() {
        if (is_take_file) {
            FileSystemEditor.RemoveFile(take_file.GetComponent<TextSaveUI>().file_name);
            UpdateFindList();
            is_take_file = false;
        }
    }

    private void SaveFile() {
        if (PanelSaveFile.GetComponent<SaveFileUI>().NameInputFeild.text != "") {
            FileSystemEditor.SaveFile(PanelSaveFile.GetComponent<SaveFileUI>().NameInputFeild.text, Editor);
            UpdateFindList();
        }
    }
    #endregion
}

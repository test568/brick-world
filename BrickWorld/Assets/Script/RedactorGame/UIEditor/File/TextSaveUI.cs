﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSaveUI : MonoBehaviour {
    public Text Name;
    public Button Detector;
    public GameObject SetFon;
    [HideInInspector]
    public UIEditor iEditor;
    [HideInInspector]
    public string file_name;
    [SerializeField] Sprite defaultImage;

    public void SetImage(Sprite sprite) {
        if (sprite) {
            GetComponent<Image>().sprite = sprite;
        }
        else {
            GetComponent<Image>().sprite = defaultImage;
        }
    }

    public void Click() {
        iEditor.ClickButtonFIle(gameObject);
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum GroupBase {
    Antenna, Arch, Bar, Bracket, Brick, Fence, Panel, Plate, Round, Slope, Stair, Support, Tile, Wing, NO_Group, Axle,
    TurntableBase, TurntableTop, Wheel, Lamp, Chair, Door, Window, Windscreen, Engine, Gearbox, Scalabe, Steering, FuelTank, Beep, Connector,
    Electronics, TankWheel, Propeller
    }

[CreateAssetMenu(fileName = "DetailDatabase", menuName = "Custom Data/Detail Database")]
public class DetailDatabase: ScriptableObject {
    [SerializeField] GroupBase groupBase;
    [SerializeField] string groupName;
    [SerializeField] Sprite spriteGroup;
    [SerializeField] List<GameObject> details = new List<GameObject>();

    public GameObject Find(string code, out int number) {
        for (int i = 0; i<details.Count; i++) {
            if (details[i].GetComponent<DetailDate>().CodeDetail==code) {
                number=i;
                return details[i];
                }
            }
        number = -1;
        return null;
        }

    public GameObject Find(string code) {
        for (int i = 0; i<details.Count; i++) {
            if (details[i].GetComponent<DetailDate>().CodeDetail==code) {
                return details[i];
                }
            }
        return null;
        }

    public int Count => details.Count;
    public GameObject[] ToArray => details.ToArray();
    public GameObject this[int i] => details[i];
    public GroupBase Group => groupBase;
    public string Name => groupName;
    public Sprite Sprite => spriteGroup;
    }

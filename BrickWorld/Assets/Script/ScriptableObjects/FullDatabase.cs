﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FullDatabase", menuName = "Custom Data/Full Database")]
public class FullDatabase : ScriptableObject
{
    [SerializeField] List<DetailDatabase> fullDatabases;
    public DetailDatabase[] ToArray => fullDatabases.ToArray();

    public DetailDatabase FindGroup(GroupBase group)
    {
        foreach (DetailDatabase detailDatabase in fullDatabases)
        {
            if (detailDatabase.Group == group)
                return detailDatabase;
        }

        return null;
    }

}
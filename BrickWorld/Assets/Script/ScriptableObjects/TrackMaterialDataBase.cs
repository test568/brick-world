﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " TrackMaterialData", menuName = "Custom Data/ Track Material DataBase")]
public class TrackMaterialDataBase : ScriptableObject {

        public static TrackMaterialDataBase DataBase {
        get {
            if (_DataBase==null) {
                _DataBase=Resources.Load<TrackMaterialDataBase>("DataBase/TrackMaterialData");
                }
            return _DataBase;
            }
        }

    static TrackMaterialDataBase _DataBase;

    public List<Material> Materials = new List<Material>();
    public List<TypeTrack> Type = new List<TypeTrack>();
}

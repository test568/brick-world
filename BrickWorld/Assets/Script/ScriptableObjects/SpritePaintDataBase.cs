﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " SpritePaintDataBase", menuName = "Custom Data/ Sprite Paint DataBase")]
public class SpritePaintDataBase : ScriptableObject {
    public SpriteGroupData[] Sprites;

    public static SpritePaintDataBase DataBase {
        get {
            if (_DataBase == null) {
                _DataBase = Resources.Load<SpritePaintDataBase>("DataBase/SpritePaintDataBase");
            }
            return _DataBase;
        }
    }
    static SpritePaintDataBase _DataBase;

    public Sprite FindSprite(string Name) {
        Sprite sprite = null;
        for (int i = 0; i < Sprites.Length; i++) {
            for (int i2 = 0; i2 < Sprites[i].Sprites.Length; i2++) {
                if (Sprites[i].Sprites[i2].name == Name) {
                    sprite = Sprites[i].Sprites[i2];
                }
            }
        }
        return sprite;
    }
}

[System.Serializable]
public class SpriteGroupData {
    public string RussianName;
    public Sprite ImageSpriteGroup;
    public Sprite[] Sprites;
}
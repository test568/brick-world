﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " MaterialData", menuName = "Custom Data/ Material Item")]
public class MaterialDataItem : ScriptableObject {
    public string RussianName;
    public Material Material;
    public Sprite ImageMaterial;
    public float Density;
    public TypeMaterialDetail Type;
}

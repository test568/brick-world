﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public enum TypeMaterialDetail {
    Plastic = 1,
    Metal = 2,
    Metal1 = 3,
    Metal2 = 4,
    Metal3 = 5,
    Carbon = 6,
    Copper = 7,
    Foam = 8,
    Glass = 9,
    Gold = 10,
    Metal4 = 11,
    Metal5 = 12,
    Metal6 = 13,
    Rubber = 14,
    Rust1 = 15,
    Rust2 = 16,
    Wood1 = 17,
    Wood2 = 18
}


public enum TypeMaterialGroup {
    Plastic,
    Metal,
    Wood,
    Glass,
    Rubber,
}

[CreateAssetMenu(fileName = " MaterialData", menuName = "Custom Data/ Material DataBase")]
public class MaterialDataBase : ScriptableObject {

    public MaterialGroupData[] GroupDatas;
    public static MaterialDataBase DataBase {
        get {
            if (_materialDataBase == null) {
                _materialDataBase = Resources.Load<MaterialDataBase>("DataBase/MaterialData");
            }
            return _materialDataBase;
        }
    }

    private static MaterialDataBase _materialDataBase;

    public MaterialDataItem FindMaterila(TypeMaterialDetail type) {
        for (int i = 0; i < GroupDatas.Length; i++) {
            for (int i2 = 0; i2 < GroupDatas[i].Materials.Length; i2++) {
                if (GroupDatas[i].Materials[i2].Type == type)
                    return GroupDatas[i].Materials[i2];
            }
        }
        return default;
    }
}

[System.Serializable]
public class MaterialGroupData {
    public TypeMaterialGroup Type;
    public string RussianName;
    public Sprite ImageMaterialGroup;
    public MaterialDataItem[] Materials;
}
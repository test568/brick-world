﻿using UnityEngine;


[CreateAssetMenu(fileName = "New Wing Curves", menuName = "Wing Curve")]
public class WingCurves : ScriptableObject {

    [SerializeField]
    private AnimationCurve lift = new AnimationCurve(new Keyframe(0.0f, 0.0f),
        new Keyframe(16f, 1.1f),
        new Keyframe(20f, 0.6f),
        new Keyframe(135f, -1.0f),
        new Keyframe(160f, -0.6f),
        new Keyframe(164f, -1.1f),
        new Keyframe(180f, 0.0f));

    [SerializeField]
    private AnimationCurve drag = new AnimationCurve(new Keyframe(0.0f, 0.025f),
        new Keyframe(90f, 1f),
        new Keyframe(180f, 0.025f));


    public float GetLiftAtAOA(float aoa) {
        return lift.Evaluate(aoa);
    }


    public float GetDragAtAOA(float aoa) {
        return drag.Evaluate(aoa);
    }
}

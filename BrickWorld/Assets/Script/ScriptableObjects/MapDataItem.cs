﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


[CreateAssetMenu(fileName = " MapItem", menuName = "Custom Data/ Map Item")]
public class MapDataItem: ScriptableObject { //Элемент базы данных для списка карт.
    public string NameMapBuild;
    public string Name;
    public Sprite IconMap;

    }

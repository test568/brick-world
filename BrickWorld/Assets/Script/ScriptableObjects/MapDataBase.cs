﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " MapData", menuName = "Custom Data/ Map DataBase")]
public class MapDataBase: ScriptableObject { // База данных для хранения доступных игроку карт
    public MapDataItem[] ListMaps;
    }

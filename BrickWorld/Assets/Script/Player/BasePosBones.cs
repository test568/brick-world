﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


public class BasePosBones : MonoBehaviour {
    [Header("Save points")]

    public List<Transform> Transforms = new List<Transform>();

    [Header("Stored points")]
    public List<SaveTransform> Saves = new List<SaveTransform>();

    public void Save() {
        Saves.Clear();
        for (int i = 0; i < Transforms.Count; i++) {
            SaveTransform svt = new SaveTransform();

            svt.Position = Transforms[i].localPosition;
            svt.Rotation = Transforms[i].localRotation;
            svt.Scale = Transforms[i].localScale;
            svt.SaveObject = Transforms[i].gameObject;

            Saves.Add(svt);
        }
    }


    public void SetStandartPositions() {
        for (int i = 0; i < Saves.Count; i++) {
            if (Saves[i].SaveObject != null) {
                Saves[i].SaveObject.transform.localPosition = Saves[i].Position;
                Saves[i].SaveObject.transform.localRotation = Saves[i].Rotation;
                Saves[i].SaveObject.transform.localScale = Saves[i].Scale;
            }
        }
    }

}

[System.Serializable]
public class SaveTransform {
    public Vector3 Position;
    public Quaternion Rotation;
    public Vector3 Scale;

    public GameObject SaveObject;
}
#if UNITY_EDITOR
[CustomEditor(typeof(BasePosBones))]
public class EditorPosBones : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        BasePosBones basePos = (BasePosBones) target;

        bool isSave = GUILayout.Button("Save");

        if (isSave) {
            basePos.Save();
            Repaint();
        }
    }
}
#endif
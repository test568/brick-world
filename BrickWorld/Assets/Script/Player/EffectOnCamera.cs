﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
public class EffectOnCamera: MonoBehaviour {
    public static EffectOnCamera effectOnCamera;

    public Camera objCamera;
    public AudioListener audioListener;
    public LayerMask LayerMiniMap;
    public LayerMask LayerGame;
    public PostProcessingBehaviour PostProcessing;
    public FlareLayer FlareSun;

    private void Awake() {
        if (effectOnCamera==null) {
            effectOnCamera=this;
            LoadCameraGame();
            }
        else {
            Destroy(gameObject);
            }
        }

    public void LoadCameraGame() {
        audioListener.enabled=true;
        objCamera.clearFlags=CameraClearFlags.Skybox;
        objCamera.cullingMask=LayerGame;
        }
    public void LoadCameraMiniMap() {
        audioListener.enabled=false;
        objCamera.clearFlags=CameraClearFlags.SolidColor;
        objCamera.cullingMask=LayerMiniMap;
        }

    }

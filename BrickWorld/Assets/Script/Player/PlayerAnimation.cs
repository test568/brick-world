﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[System.Serializable]
public enum PlayerMoveSide {
    Forward,
    Back,
    Left,
    Right,
    None
}

public class PlayerAnimation {

    public bool IsRagdoll {
        get {
            return _is_ragdoll;
        }
        set {
            if (value == _is_ragdoll)
                return;
            if (value == true) {
                SetRegdoll();
            }
            else {
                UnSetRegdoll();
            }
            _is_ragdoll = value;

        }
    }


    bool _is_ragdoll = false;

    public float AngleHead {
        get {
            return _angle_head;
        }
        set {
            _angle_head = value;
            if (Bones != null) {
                Bones.HeadTarget.localEulerAngles = new Vector3(_angle_head, 0, 0);
            }
        }
    }

    float _angle_head = 0f;

    public bool AutoUpdate { get; set; }

    public bool IsHandsAnim { get; set; }

    public bool IsSquat { get; set; }

    public bool IsChair { get; set; }

    public PlayerMoveSide SidePlayerMove {
        get {
            return _side_player_move;
        }
        set {

            if (_side_player_move == value || animator == null) {
                _side_player_move = value;
                return;
            }
            if (_side_player_move == PlayerMoveSide.Back) {
                animator.SetBool("MoveBack", false);
            }
            else if (_side_player_move == PlayerMoveSide.Forward) {
                animator.SetBool("MoveForward", false);
            }
            else if (_side_player_move == PlayerMoveSide.Left) {
                animator.SetBool("MoveLeft", false);
            }
            else if (_side_player_move == PlayerMoveSide.Right) {
                animator.SetBool("MoveRight", false);
            }

            if (value == PlayerMoveSide.Forward) {
                animator.SetBool("MoveForward", true);
            }
            else if (value == PlayerMoveSide.Right) {
                animator.SetBool("MoveRight", true);
            }
            else if (value == PlayerMoveSide.Left) {
                animator.SetBool("MoveLeft", true);
            }
            else if (value == PlayerMoveSide.Back) {
                animator.SetBool("MoveBack", true);
            }

            _side_player_move = value;
        }
    }


    PlayerMoveSide _side_player_move = PlayerMoveSide.None;

    public float Speed {
        get {
            return _speed;
        }
        set {
            animator?.SetFloat("SpeedMove", value * 0.3f);
            _speed = value;
        }
    }

    float _speed = 1;

    public BonesAvatar Bones { get; set; }

    private Animator animator;



    public PlayerAnimation() {
        AutoUpdate = true;
    }

    public void Initialization(Animator animator, BonesAvatar bones) {
        Bones = bones;
        this.animator = animator;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {//Синхронизация анимаций
        if (stream.IsWriting) {
            stream.SendNext(Speed);
            stream.SendNext(IsRagdoll);
            stream.SendNext(SidePlayerMove);
            stream.SendNext(AngleHead);
        }
        else if (stream.IsReading) {
            Speed = (float) stream.ReceiveNext();
            IsRagdoll = (bool) stream.ReceiveNext();
            SidePlayerMove = (PlayerMoveSide) stream.ReceiveNext();
            AngleHead = (float) stream.ReceiveNext();
        }
    }

    public void UpdateAnimation() {


    }


    public void SetDefualt() {
        Bones.BasePlayer.transform.position = Bones.RootRig.position + new Vector3(0, 1f, 0);
        Bones.Character.enabled = true;
        for (int i = 0; i < Bones.Rigidbodies.Length; i++) {
            Bones.Rigidbodies[i].isKinematic = true;
        }
        Bones.HeadIK.enabled = true;

        SidePlayerMove = PlayerMoveSide.None;
        Bones.StartPositionsData.SetStandartPositions();

        animator.enabled = true;

    }

    private void SetRegdoll() {
        Bones.Character.enabled = false;
        for (int i = 0; i < Bones.Rigidbodies.Length; i++) {
            Bones.Rigidbodies[i].isKinematic = false;
        }
        Bones.HeadIK.enabled = false;

        animator.enabled = false;
    }

    private void UnSetRegdoll() {
        SetDefualt();
    }
}

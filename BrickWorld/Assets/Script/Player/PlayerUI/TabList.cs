﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon;

public class TabList: MonoBehaviour {

    [SerializeField] GameObject ui_tab;
    [SerializeField] GameObject content_obj;
    [SerializeField] GameObject copy_text_obj;

    private List<Text> texts = new List<Text>();

    private void Start() {
        if (GameRoom.isMultiplayer) {
            int max_player = PhotonNetwork.CurrentRoom.MaxPlayers;
            for (int i = 0; i<max_player; i++) {
                Text text = Instantiate(copy_text_obj, content_obj.transform).GetComponent<Text>();
                texts.Add(text);
                }
            Destroy(copy_text_obj);
            }
        }

    private void Update() {
        if (GameRoom.isMultiplayer&&!ControllerPlayer.PlayerOnPause&&Input.GetKey(KeyCode.Tab)) {
            ui_tab.SetActive(true);
            }
        else {
            ui_tab.SetActive(false);
            }

        for (int i = 0; i<texts.Count; i++) {
            texts[i].text="";
            if (i<GameRoom.Players.Count) {
                texts[i].text=GameRoom.Players[i].NickName;
                }
            }
        }
    }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Healthbar: MonoBehaviour {
    [HideInInspector]
    public float Health;
    [HideInInspector]
    public float MaxHealth;

    private GameObject scrollbare;
    private float hp_dist;
    private void Awake() {
        scrollbare=gameObject;
        hp_dist=1f/MaxHealth;
        }
    private void Update() {
        scrollbare.GetComponent<Scrollbar>().size=hp_dist*Health;
        }
    }

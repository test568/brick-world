﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeviceUI : MonoBehaviour {

    [Header("Speedometer")]
    [SerializeField] private Text speedText;
    [SerializeField] private Text gearText;
    [SerializeField] private GameObject arrowObj;
    [SerializeField] private GameObject speedometer;
    [Header("RPM")]
    [SerializeField] private Text rpmText;
    [SerializeField] private GameObject arrowObjRMP;
    [SerializeField] private GameObject rpmObj;
    [Header("Panel Element")]
    [SerializeField] private GameObject panelElements;
    [SerializeField] private Image altModeDisable;
    [SerializeField] private Image altModeEnable;
    [SerializeField] private Image frontLightDisable;
    [SerializeField] private Image frontLightEnable;
    [SerializeField] private Image rearLightDisable;
    [SerializeField] private Image rearLightEnable;
    [SerializeField] private Image handbrakeDisable;
    [SerializeField] private Image handbrakeEnable;


    public float Speed {
        get {
            return _speed;
        }
        set {
            _speed = value;
            UpdateSpeedometer();
        }
    }

    float _speed;

    public TypeGear Gear {
        get {
            return _gear;
        }
        set {
            _gear = value;
            UpdateSpeedometer();
        }
    }
    TypeGear _gear;

    public float RPM {
        get {
            return _rpm;
        }
        set {
            _rpm = value;
            UpdateRPM();
        }
    }
    float _rpm;



    public bool IsAltMode {
        get {
            return _is_alt_mode;
        }
        set {
            _is_alt_mode = value;
            if (_is_alt_mode) {
                altModeDisable.gameObject.SetActive(false);
                altModeEnable.gameObject.SetActive(true);
            }
            else {
                altModeDisable.gameObject.SetActive(true);
                altModeEnable.gameObject.SetActive(false);
            }
        }
    }

    bool _is_alt_mode;

     public bool IsRearLight {
        get {
            return _is_rear_light;
        }
        set {
            _is_rear_light = value;
            if (!_is_rear_light) {
                rearLightDisable.gameObject.SetActive(true);
                rearLightEnable.gameObject.SetActive(false);
            }
            else {
                rearLightDisable.gameObject.SetActive(false);
                rearLightEnable.gameObject.SetActive(true);
            }
        }
    }

    bool _is_rear_light;

         public bool IsFrontLight {
        get {
            return _is_front_light;
        }
        set {
            _is_front_light = value;
            if (!_is_front_light) {
                frontLightDisable.gameObject.SetActive(true);
                frontLightEnable.gameObject.SetActive(false);
            }
            else {
                frontLightDisable.gameObject.SetActive(false);
                frontLightEnable.gameObject.SetActive(true);
            }
        }
    }

    bool _is_front_light;

             public bool IsHandbrake {
        get {
            return _is_handbrake;
        }
        set {
            _is_handbrake = value;
            if (!_is_handbrake ){
                handbrakeDisable.gameObject.SetActive(true);
                handbrakeEnable.gameObject.SetActive(false);
            }
            else {
                handbrakeDisable.gameObject.SetActive(false);
                handbrakeEnable.gameObject.SetActive(true);
            }
        }
    }

    bool _is_handbrake;


    [HideInInspector]
    public bool IsDrive { get { return _isDrive; } set { _isDrive = value; UpdateSpeedometer(); UpdateRPM(); UpdatePanelElement(); } }

    bool _isDrive;

    private void UpdateSpeedometer() {
        if (( _isDrive ) && ( Speed != -1 || Gear != TypeGear.None )) {
            string gear = Gear.ToString();
            if (gear.IndexOf("Gear") != -1) {
                gear = gear.Remove(gear.IndexOf("Gear"), 4);
            }
            gearText.text = gear;
            speedText.text = "" + (int) ( Speed );
            arrowObj.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, -Mathf.Clamp(( Speed / 180f ) * 180f, 0, 180));
            speedometer.SetActive(true);
        }
        else {
            speedometer.SetActive(false);
        }
    }

    private void UpdateRPM() {
        if (_isDrive && RPM != -1f) {
            rpmText.text = "" + (int) RPM;
            arrowObjRMP.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, -Mathf.Clamp(( (int) RPM / ( 16000f ) ) * 270f, 0f, 270f));
            rpmObj.SetActive(true);
        }
        else {
            rpmObj.SetActive(false);
        }
    }

    private void UpdatePanelElement() {
        if (IsDrive) {
            panelElements.SetActive(true);
        }
        else {
            panelElements.SetActive(false);
        }
    }
}


[System.Serializable]
public enum TypeGear {
    Gear1,
    Gear2,
    Gear3,
    Gear4,
    N,
    R,
    None,
}

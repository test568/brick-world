﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Photon.Pun;
using Photon.Realtime;

public class PlayerMove : MonoBehaviour {
    [SerializeField] PhotonView Viev;

    [SerializeField] private CharacterController character;

    public GameObject Eye;
    public Transform GroundDetect;

    public float SpeedMove;
    public float CameraSpeedMove;
    public float ShiftSpeedMove;
    public float CtrlSpeedMove;
    public float ForseJump = 1.5f;
    public float Mass;

    public LayerMask LayerGround;

    private GameObject player;

    private float add_speed;



    private const float sit_pos_eye = -0.254f;

    //  private const float stend_pos_eye = 0.132f;

    private const float stend_pos_eye = 0.3f;

    private const float ground_detect_radius = 0.3f;

    private bool isGround;

    private float rotX = 0.0f;

    private float start_volume;

    private Vector3 velocity;

    private int time_from_in_free_camera;

    [SerializeField]
    private AudioSource Move;

    void Awake() {
        player = gameObject;
        start_volume = Move.volume;
    }

    private void OnEnable() {
        Vector3 rot = Eye.transform.localRotation.eulerAngles;
        rotX = rot.x;
    }

    void Update() {
        if (!GameRoom.isMultiplayer || Viev.IsMine) {
            float mouseX;
            int w = 0, s = 0, a = 0, d = 0;
            float speed = 0f;
            if (!ControllerPlayer.PlayerOnPause && !ControllerPlayer.LockControll && ControllerPlayer.MainPlayer.Status == PlayerStatus.Move) {
                if (time_from_in_free_camera > 0) {
                    time_from_in_free_camera--;
                }

                mouseX = Input.GetAxis("Mouse Y");

                rotX -= mouseX * CameraSpeedMove * Time.deltaTime;
                rotX = Mathf.Clamp(rotX, -90, 90);
                ControllerPlayer.MainPlayer.AnimationController.AngleHead = rotX;

                player.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * CameraSpeedMove * Time.deltaTime, 0));

                if (Input.GetKey(KeyCode.W)) {
                    ControllerPlayer.MainPlayer.AnimationController.SidePlayerMove = PlayerMoveSide.Forward;
                    w = 1;
                }
                if (Input.GetKey(KeyCode.S)) {
                    ControllerPlayer.MainPlayer.AnimationController.SidePlayerMove = PlayerMoveSide.Back;
                    s = 1;
                }
                if (Input.GetKey(KeyCode.A)) {
                    ControllerPlayer.MainPlayer.AnimationController.SidePlayerMove = PlayerMoveSide.Left;
                    a = 1;
                }
                if (Input.GetKey(KeyCode.D)) {
                    ControllerPlayer.MainPlayer.AnimationController.SidePlayerMove = PlayerMoveSide.Right;
                    d = 1;
                }
                if (w == 0 && s == 0 && d == 0 && a == 0) {
                    ControllerPlayer.MainPlayer.AnimationController.SidePlayerMove = PlayerMoveSide.None;
                }

                if (Input.GetKey(KeyCode.LeftControl)) {
                    speed = CtrlSpeedMove;
                    character.center = new Vector3(0, -0.3f, 0);
                    character.height = 1.4f;
                    // Eye.transform.localPosition = new Vector3(0, sit_pos_eye, 0.086f);
                }
                else if (Input.GetKey(KeyCode.LeftShift)) {
                    speed = ShiftSpeedMove;
                    character.center = new Vector3(0, 0, 0);
                    character.height = 2f;
                    //  Eye.transform.localPosition = new Vector3(0, stend_pos_eye, -0.3f);
                }
                else {
                    speed = SpeedMove;
                    character.center = new Vector3(0, 0, 0);
                    character.height = 2f;
                    // Eye.transform.localPosition = new Vector3(0, stend_pos_eye, -0.3f);
                }

                if (Input.GetKeyDown(KeyCode.Space) && isGround) {
                    velocity.y = Mathf.Sqrt(ForseJump * -2 * Physics.gravity.y * Mass);
                }

                if (ControllerPlayer.MainPlayer.Status == PlayerStatus.Move && Input.GetKeyDown(KeyCode.F) && time_from_in_free_camera <= 0) {
                    time_from_in_free_camera = 20;
                    ControllerPlayer.MainPlayer.Status = PlayerStatus.FreeCamera;
                }
            }
            if (Time.time != 0) {
                isGround = Physics.CheckSphere(GroundDetect.position, ground_detect_radius, LayerGround);
                if (isGround && velocity.y < 0) {
                    ControllerPlayer.MainPlayer.FallDamag(Mathf.Abs(velocity.y));
                    velocity.y = -2f;
                }

                Move.volume = start_volume * AudioPlayer.Audio.AllVolume;

                Vector3 move = transform.right * ( d - a ) + transform.forward * ( w - s );
                character.Move(move * speed * Time.deltaTime);
                ControllerPlayer.MainPlayer.AnimationController.Speed = speed;

                velocity.y += Physics.gravity.y * Mass * Time.deltaTime;
                character.Move(velocity * Time.deltaTime);
            }
        }
    }

    private void OnDisable() {
    }
}

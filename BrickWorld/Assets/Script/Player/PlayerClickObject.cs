﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClickObject : MonoBehaviour {

    private LayerMask layer;
    private void Awake() {
        layer = LayerMask.GetMask(new string[1] { "Interaction" });
    }
    private void Update() {
        if (Input.GetKeyUp(KeyCode.E) && !ControllerPlayer.LockControll && ( ControllerPlayer.MainPlayer.GetPhotonView().IsMine || !GameRoom.isMultiplayer )) {
            RaycastHit hit;
            Ray ray = ControllerPlayer.MainPlayer.MainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit, 2f, layer)) {
                if (hit.transform.gameObject.GetComponents<InEditor>().Length != 0) {
                    hit.transform.gameObject.GetComponent<InEditor>().LoadEditor();
                    return;
                }
            }
            if (Physics.Raycast(ray, out hit, 7f, layer)) {
                if (hit.collider.gameObject.transform.parent.gameObject.GetComponent<PlayerChair>() != null) {
                    hit.collider.gameObject.transform.parent.gameObject.GetComponent<PlayerChair>().SitDown();
                    return;
                }
            }
            if (Physics.Raycast(ray, out hit, 6f, layer)) {
                if (hit.collider.gameObject.transform.parent.gameObject.GetComponent<ConnectorBase>() != null) {
                    ConnectorBase connector = hit.collider.gameObject.transform.parent.gameObject.GetComponent<ConnectorBase>();
                    if (connector.isConnect) {
                        connector.DisconnectJoint();
                        return;
                    }
                    else if (!connector.isConnect) {
                        connector.Fasten = 1f;
                        connector.Fasten = 0f;
                        return;
                    }
                }
                if (hit.collider.gameObject.transform.parent.gameObject.GetComponent<ConnectConSet>() != null) {
                    ConnectorBase connector = hit.collider.gameObject.transform.parent.gameObject.GetComponent<ConnectConSet>().ConnectBase;
                    if (connector) {
                        connector.DisconnectJoint();
                        return;
                    }
                }
            }
        }
    }
}

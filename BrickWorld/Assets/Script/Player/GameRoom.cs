﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.SceneManagement;
using System;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameRoom : MonoBehaviourPunCallbacks {
    public GameObject PlayerPrefab;

    public Transform[] PointSpawn;

    public static GameRoom singleton;

    public static List<PlayerMatch> Players = new List<PlayerMatch>();

    public static bool isMultiplayer = false;

    public static bool isMasterClient = false;

    public static GameObject PlayerObject;

    static List<IObserverPlayerEnterAndLeft> Observers = new List<IObserverPlayerEnterAndLeft>();

    private void Awake() {
        if (!singleton) {
            singleton = this;
        }
        else {
            Destroy(gameObject);
        }

        if (PhotonNetwork.IsConnected || PhotonNetwork.IsMasterClient) {
            isMultiplayer = true;
            if (PhotonNetwork.PlayerList.Length == 1) {
                isMasterClient = true;
            }
        }
        else {
            isMultiplayer = false;
            isMasterClient = true;
        }

        if (isMultiplayer)
            PlayerObject = PhotonNetwork.Instantiate(PlayerPrefab.name, PointSpawn[UnityEngine.Random.Range(0, PointSpawn.Length)].position, Quaternion.identity);
        else
            PlayerObject = Instantiate(PlayerPrefab, PointSpawn[UnityEngine.Random.Range(0, PointSpawn.Length)].position, Quaternion.identity);

        PhotonPeer.RegisterType(typeof(ushort), 241, WritePhotonObjects.UshortSerialize, WritePhotonObjects.UshortDesirealize);
        PhotonPeer.RegisterType(typeof(PlayerMatch), 242, WritePhotonObjects.PlayerMatchSerialize, WritePhotonObjects.PlayerMatchDesirealize);
        PhotonPeer.RegisterType(typeof(Color), 243, WritePhotonObjects.ColorSerialize, WritePhotonObjects.ColorDesirealize);
        PhotonPeer.RegisterType(typeof(ParametersGroup), 244, WritePhotonObjects.PacketBuildSpawnSerialize, WritePhotonObjects.PacketBuildSpawnDesirealize);
        PhotonPeer.RegisterType(typeof(ParametersBuild), 245, WritePhotonObjects.SRPCLoadBuildSerialize, WritePhotonObjects.SRPCLoadBuildDesirealize);
        PhotonPeer.RegisterType(typeof(ArrayObjects), 246, WritePhotonObjects.ArrayObjectSerialize, WritePhotonObjects.ArrayObjectDesirealize);
        PhotonPeer.RegisterType(typeof(RotationForceSerializable), 247, WritePhotonObjects.RotationForceSerialize, WritePhotonObjects.RotationForceDesirealize);

        PhotonNetwork.SendRate = 15;
        PhotonNetwork.SerializationRate = 15;
    }

    public void LeftRoom() {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom() {
        SceneManager.LoadScene(0);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer) {
        Debug.LogFormat("Player {0} entered room", newPlayer.NickName);
        for (int i = 0; i < Observers.Count; i++) {
            Observers[i].EntryPlayer(newPlayer);
        }
    }

    public override void OnPlayerLeftRoom(Player Player) {
        Debug.LogFormat("Player {0} left room", Player.NickName);
        for (int i = 0; i < Observers.Count; i++) {
            Observers[i].LeftPlayer(Player);
        }
    }

    public override void OnMasterClientSwitched(Player newMasterClient) {
        //if (PhotonNetwork.IsMasterClient) {
        //    isMasterClient=PhotonNetwork.IsMasterClient;
        //    ControllerPlayer.MainPlayer.UpdateMasterClient();
        //    PhysicsMapShync.Singleton.UpdateMasterClient();
        //    }
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(0);
    }

    public static void AddObserver(IObserverPlayerEnterAndLeft observer) {
        Observers.Add(observer);
    }

    public static void RemoveObserver(IObserverPlayerEnterAndLeft observer) {
        Observers.Remove(observer);
    }

    public void RespawnPlayerPosition() {
        PlayerObject.transform.position = PointSpawn[UnityEngine.Random.Range(0, PointSpawn.Length)].position;
    }
}


[System.Serializable]
public class PlayerMatch {
    public ControllerPlayer Controller;
    public int CountKill = 0;
    public int CountDead = 0;
    public int Health = 0;
    public string NickName;
    public string id;
}

public static class WritePhotonObjects {//Класс который содержит статичиские методы для записи обьектов в байты и чтения. Для передачи между пользователями.

    #region Запись и чтение PlayerMatch
    public static byte[] PlayerMatchSerialize(object obj) {
        byte[] bytes = new byte[42];
        PlayerMatch player = (PlayerMatch) obj;
        BitConverter.GetBytes(player.CountKill).CopyTo(bytes, 0);
        BitConverter.GetBytes(player.CountDead).CopyTo(bytes, 4);
        BitConverter.GetBytes(player.Health).CopyTo(bytes, 8);
        char[] nik = new char[15];
        for (int i = 0; i < nik.Length; i++) {
            if (i < player.NickName.Length) {
                nik[i] = player.NickName[i];
            }
        }
        char[] id = new char[15];
        for (int i = 0; i < id.Length; i++) {
            if (i < player.id.Length) {
                id[i] = player.id[i];
            }
        }
        Encoding.UTF8.GetBytes(nik, 0, 15).CopyTo(bytes, 12);
        Encoding.UTF8.GetBytes(id, 0, 15).CopyTo(bytes, 27);
        return bytes;

    }

    public static object PlayerMatchDesirealize(byte[] data) {
        PlayerMatch player = new PlayerMatch();
        player.CountKill = BitConverter.ToInt32(data, 0);
        player.CountDead = BitConverter.ToInt32(data, 4);
        player.Health = BitConverter.ToInt32(data, 8);
        player.NickName = Encoding.UTF8.GetString(data, 12, 15).Trim(new char[1]);
        player.id = Encoding.UTF8.GetString(data, 27, 15).Trim(new char[1]);
        return player;
    }
    #endregion

    #region Запись и чтение Color

    public static byte[] ColorSerialize(object obj) {
        byte[] bytes = new byte[16];
        BitConverter.GetBytes(( (Color) obj ).a).CopyTo(bytes, 0);
        BitConverter.GetBytes(( (Color) obj ).r).CopyTo(bytes, 4);
        BitConverter.GetBytes(( (Color) obj ).g).CopyTo(bytes, 8);
        BitConverter.GetBytes(( (Color) obj ).b).CopyTo(bytes, 12);
        return bytes;
    }


    public static object ColorDesirealize(byte[] data) {
        Color color = new Color();
        color.a = BitConverter.ToSingle(data, 0);
        color.r = BitConverter.ToSingle(data, 4);
        color.g = BitConverter.ToSingle(data, 8);
        color.b = BitConverter.ToSingle(data, 12);
        return color;
    }
    #endregion

    #region Запись и чтение PacketBuildSpawn
    public static byte[] PacketBuildSpawnSerialize(object obj) {
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);
        return ms.ToArray();
    }


    public static object PacketBuildSpawnDesirealize(byte[] data) {
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        ms.Write(data, 0, data.Length);
        ms.Seek(0, SeekOrigin.Begin);
        return bf.Deserialize(ms);
    }



    #endregion

    #region Запись и чтение ArrayObject
    public static byte[] ArrayObjectSerialize(object obj) {
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);
        return ms.ToArray();
    }


    public static object ArrayObjectDesirealize(byte[] data) {
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        ms.Write(data, 0, data.Length);
        ms.Seek(0, SeekOrigin.Begin);
        return bf.Deserialize(ms);
    }

    #endregion

    #region Запись и чтение RotationForce
    public static byte[] RotationForceSerialize(object obj) {
        byte[] bytes = new byte[8];
        RotationForceSerializable r = (RotationForceSerializable) obj;
        BitConverter.GetBytes(r.Forse).CopyTo(bytes, 0);
        BitConverter.GetBytes(r.Revs).CopyTo(bytes, 4);
        return bytes;
    }


    public static object RotationForceDesirealize(byte[] data) {
        RotationForceSerializable r = new RotationForceSerializable();
        r.Forse = BitConverter.ToSingle(data, 0);
        r.Revs = BitConverter.ToSingle(data, 4);
        return r;
    }
    #endregion

    #region Запись и чтение ushort
    public static byte[] UshortSerialize(object obj) {
        return BitConverter.GetBytes((ushort) obj);
    }


    public static object UshortDesirealize(byte[] data) {
        return (ushort) BitConverter.ToInt16(data, 0);
    }

    #endregion

    #region SRPCLoadBuild

    public static byte[] SRPCLoadBuildSerialize(object obj) {
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);
        return ms.ToArray();
    }


    public static object SRPCLoadBuildDesirealize(byte[] data) {
        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        ms.Write(data, 0, data.Length);
        ms.Seek(0, SeekOrigin.Begin);
        return bf.Deserialize(ms);
    }

    #endregion


}
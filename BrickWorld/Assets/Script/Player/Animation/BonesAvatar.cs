﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonesAvatar : MonoBehaviour {
    public BasePosBones StartPositionsData;

    public Transform RootRig;

    public Transform HeadTarget;
    public FastIKFabric HeadIK;

    public Rigidbody[] Rigidbodies;
    public Joint[] Joints;
    public CharacterController Character;

    public  Transform BasePlayer;

}

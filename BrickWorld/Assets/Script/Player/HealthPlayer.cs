﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPlayer: MonoBehaviour {
    public float MaxHealth;
    public float Health;
    public Healthbar UIHealth;
    private void Awake() {
        UIHealth.MaxHealth=MaxHealth;
        }
    private void Update() {
        UIHealth.Health=Health;
        if (Health<=0) {
            ControllerPlayer.MainPlayer.Status=PlayerStatus.Dead;
            Health=MaxHealth;
            }
        }
    private void OnCollisionEnter(Collision collision) {
        if (collision.relativeVelocity.magnitude>12f) {
            Health-=( collision.relativeVelocity.magnitude-12f )*5f;
            }
        }
    }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerWeapon: MonoBehaviour {

    protected GameObject ThisPlayer;

    protected GameObject WeaponPos;

    protected GameObject MainCamera;

    [SerializeField] protected GameObject FirePos;

    public string NameWeapon;

    public bool IsTake = false;

    public void Init(GameObject Player, GameObject WeaponPos, GameObject Camera) {
        ThisPlayer=Player;
        this.WeaponPos=WeaponPos;
        MainCamera=Camera;
        }

    public virtual void Attack() {
        }

    public virtual void UpdateWeapon() {
        }

    public virtual void TakeWeapom() {
        gameObject.SetActive(true);
        IsTake=true;
        }

    public virtual void PutWeapon() {
        gameObject.SetActive(false);
        IsTake=false;
        }
    }

public abstract class GunShotWeapon: PlayerWeapon {

    public int MaxCountAmmo;

    public int MaxAmmoStore;

    public float DamagShot;

    public int CountAmmo;

    public WeaponDamagType TypeDamag;

    public virtual void Reload() {

        }
    }

public abstract class ArmeBlanche: PlayerWeapon {
    public float Damag;

    public float SpeedAttack;

    }

public enum WeaponDamagType {
    Explosive,
    Percussive
    }



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hands: ArmeBlanche {
    public override void UpdateWeapon() {
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            LayerMask layer = LayerMask.GetMask(new string[1] { "PhysicsMap" });
            if (Physics.Raycast(MainCamera.transform.position, MainCamera.transform.forward*2f, out hit, 2f, layer))

                if (HitHandler.CheckHit(hit)) {
                    Debug.Log("Attack Hands");
                    HitHandler.HandlingHit(hit, Damag, WeaponDamagType.Percussive);

                    }
            }
        }
    }

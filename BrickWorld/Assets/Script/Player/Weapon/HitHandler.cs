﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class HitHandler {

    public static readonly string[] ContactLayer = new string[2] { "Player", "PhysicsMap" };

    public static bool CheckHit(RaycastHit hit) {
        if (hit.transform!=null&&ContactLayer.Where((x) => x==LayerMask.LayerToName(hit.transform.gameObject.layer)).First()!=null)
            return true;
        return false;
        }

    public static void HandlingHit(RaycastHit hit, float damag, WeaponDamagType typeDamag) {
        string layer_name = LayerMask.LayerToName(hit.transform.gameObject.layer);
        if (typeDamag==WeaponDamagType.Explosive) {
            damag*=10f;
            }
        if (layer_name==ContactLayer[1]) {
            int index_object = hit.rigidbody.GetComponent<PhysicsObjectMap>().IndexObject;
            PhysicsMapShync.Singleton.ObjectPush(hit.point*damag, Vector3.zero, index_object);
            }
        }
    }

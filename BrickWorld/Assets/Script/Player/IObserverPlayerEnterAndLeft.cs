﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObserverPlayerEnterAndLeft {
    void LeftPlayer(Player player);
    void EntryPlayer(Player player);
}

﻿using System.Collections;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

public enum PlayerStatus {
    inEdtior = 1,
    inChair = 2,
    Dead = 3,
    Move = 4,
    SpawnBuild = 5,
    FreeCamera,
}
public class ControllerPlayer : MonoBehaviour, IPunObservable, IUpdateConnectPlayer, IGetPhotonView { //Этот класс необходим для того что б контролировать в каком игрок состоянии, он имеет доступ ко всем ключевым обьектам игрока и имеет доступ через Singleton

    [Header("NetworkContent-OnlyMyComponent")]
    public Behaviour[] MainComponent;
    public GameObject[] MainObject;

    public GameObject MainCamera;
    public GameObject ParentCameraEditor;
    public GameObject ParentCameraPlayer;
    public GameObject ParentCameraSpawnBuild;

    public GameObject PlayerObject;

    [SerializeField] PlayerWeapon[] WeaponsList;

    [Header("Component")]
    [SerializeField] GameObject UIPlayer;
    [SerializeField] GameObject SpawnBuildObject;
    [SerializeField] DeviceUI Device;
    [SerializeField] ClickMouseEditor MouseEditor;
    [SerializeField] ClickKeyboardEditor KeyboardEditor;
    [SerializeField] MoveCameraEditor CameraEditor;
    [SerializeField] UIEditor uIEditor;
    [SerializeField] PlayerMove MovePlayer;
    [SerializeField] UIOptionsGame OptionGame;
    [SerializeField] PhotonView photon_viev;
    [SerializeField] PlayerMove playerMove;
    [SerializeField] Healthbar healthbar;
    [SerializeField] Animator animator;
    [SerializeField] GameObject model;
    [SerializeField] BonesAvatar bones;
    [SerializeField] CharacterController character;

    [SerializeField] GameObject WeaponPos;

    [Header("FreeCamera")]
    [SerializeField] GameObject FreeCameraObject;
    [SerializeField] MoveFreeCamera FreeCamera;

    public DeviceUI Devices { get => Device; }

    private List<PlayerWeapon> Weapons = new List<PlayerWeapon>();

    private PlayerWeapon TakeWeapon;

    public static ControllerPlayer MainPlayer;

    public static bool PlayerOnPause { get => UIOptionsGame.IsPause; }

    public static bool LockControll { get; set; }

    public PlayerAnimation AnimationController { get; private set; }

    private float Health {
        get => _health; set {
            player.Health = (int) value;
            healthbar.Health = (int) value;
            _health = (int) value;
        }
    }

    float _health = 100;

    [SerializeField] private PlayerMatch player;

    public PlayerStatus Status { //Это свойство необходимо для того что б изменять состояние игрока, оно выполняет выгрузку нынешнего состояние и загружает указаное. Так же может выполнять доп условия.
        get => _status;
        set {
            if (_status == PlayerStatus.Move && value != PlayerStatus.Move) {
                UnLoadPlayer();
            }
            else if (_status == PlayerStatus.FreeCamera && value != PlayerStatus.FreeCamera && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                UnLoadFreeCamera();
            }
            else if (_status == PlayerStatus.inEdtior && value != PlayerStatus.inEdtior && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                UnLoadEditor();
            }
            else if (_status == PlayerStatus.SpawnBuild && value != PlayerStatus.SpawnBuild && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                UnLoadSpawnBuild();
            }
            else if (_status == PlayerStatus.inChair && value != PlayerStatus.inChair && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                UnLoadChair();
            }

            if (value == PlayerStatus.Move && _status != PlayerStatus.Move) {
                LoadPlayer();
                _status = value;
            }
            else if (value == PlayerStatus.FreeCamera && _status != PlayerStatus.FreeCamera && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                LoadFreeCamera();
                _status = value;
            }
            else if (value == PlayerStatus.inEdtior && _status != PlayerStatus.inEdtior && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                LoadEditor();
                _status = value;
            }
            else if (value == PlayerStatus.SpawnBuild && _status != PlayerStatus.SpawnBuild && _status == PlayerStatus.inEdtior && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                LoadSpawnBuild();
                _status = value;
            }
            else if (value == PlayerStatus.Dead) {
                LoadDead();
                _status = value;
            }
            else if (value == PlayerStatus.inChair && _status != PlayerStatus.inChair && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
                LoadChair();
                _status = value;
            }
        }
    }

    private PlayerStatus _status = PlayerStatus.Move;

    private void Start() {//Singleton
        if (MainPlayer == null && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
            MainPlayer = this;
        }
        else if (MainPlayer == this && ( !GameRoom.isMultiplayer || photon_viev.IsMine )) {
            Destroy(gameObject);
        }

        string nick = "";
        string id = "";
        if (photon_viev.IsMine) {
            nick = PhotonNetwork.NickName;
            id = PhotonNetwork.LocalPlayer.UserId;
        }
        player = new PlayerMatch { NickName = nick, id = id, Health = 100, Controller = this };
        GameRoom.Players.Add(player);

        if (GameRoom.isMultiplayer && !photon_viev.IsMine) {
            for (int i = 0; i < MainComponent.Length; i++) {
                MainComponent[i].enabled = false;
            }
            for (int i = 0; i < MainObject.Length; i++) {
                Destroy(MainObject[i]);
            }
        }

        AnimationController = new PlayerAnimation();
        AnimationController.Initialization(animator, bones);
        AnimationController.SetDefualt();


        UpdateMasterClient();

        if (GameRoom.isMultiplayer && !photon_viev.IsMine) {
            photon_viev.RPC("ConnectThisObject", RpcTarget.Others, new object[1] { PhotonNetwork.LocalPlayer });
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {//Синхронизация списка игроков
        if (stream.IsWriting) {
            stream.SendNext(player);
        }
        else if (photon_viev && !photon_viev.IsMine && stream.IsReading) {
            PlayerMatch p = (PlayerMatch) stream.ReceiveNext();
            player.CountDead = p.CountDead;
            player.CountKill = p.CountKill;
            player.Health = p.Health;
            player.id = p.id;
            player.NickName = p.NickName;
        }

        AnimationController.OnPhotonSerializeView(stream, info);
    }

    public void UpdateMasterClient() {
        if (GameRoom.isMasterClient) {
            OptionGame.UpdateMasterOptions(true);
        }
        else {
            OptionGame.UpdateMasterOptions(false);
        }
    }

    public void ShowAndHidePlayerUI(bool param) {
        if (param == true) {
            UIPlayer.SetActive(true);
        }
        else {
            UIPlayer.SetActive(false);
        }
    }

    public void TeleportPlayer(Vector3 position) {//Телепортирует игрока в указаное место и переходит в режим ходьбы.
        character.enabled = false;
        Status = PlayerStatus.Move;

        transform.localPosition = position + new Vector3(0, 3f, 0);
        character.enabled = true;
    }

    public void TeleportPlayerNoEditStatus(Vector3 position) {//Телепортирует игрока в указаное место и переходит в режим ходьбы.
        character.enabled = false;
        transform.localPosition = position + new Vector3(0, 3f, 0);
        character.enabled = true;
    }

    public void FallDamag(float Force) {//Дамаг от падения
        if (Force > 20f) {
            Force -= 20f;
            Force *= 2f;
            Health -= (int) Force;
            PlayerDamagEvent();
        }
    }

    public void TakeDamag(float Damag) {

        PlayerDamagEvent();
    }

    private void PlayerDamagEvent() {
        if (Health <= 0) {
            Status = PlayerStatus.Dead;
        }
    }

    [PunRPC]
    private void LoadStartParams(string[] ListWeaponsName, string TakeWeapon) {//Отправляеться игру который зашел

    }

    [PunRPC]
    private void TakeOtherWeapon(string NewWeapon) {

    }

    [PunRPC]
    public void ConnectThisObject(Player newPlayer) {//Вызываеться если новый игрок подключился и ему нужно обновить обьект
        if (!photon_viev.IsMine)
            return;


        if (Status == PlayerStatus.Dead) {
            photon_viev.RPC("PlayerSetDead", newPlayer);
        }
        else if (Status == PlayerStatus.inChair) {
            photon_viev.RPC("PlayerSetChair", newPlayer);
        }
    }


    #region Состояния игрока
    private void UnLoadPlayer() {
        // LockControll = true;
    }

    private void LoadPlayer() {
        //LockControll = false;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    private void LoadFreeCamera() {
        MainCamera.transform.parent = FreeCamera.transform;
        MainCamera.transform.localPosition = Vector3.zero;
        MainCamera.transform.localEulerAngles = Vector3.zero;
        FreeCamera.transform.parent = default;
        FreeCamera.transform.position = PlayerObject.transform.position;
        FreeCamera.enabled = true;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    private void UnLoadFreeCamera() {
        MainCamera.transform.parent = ParentCameraPlayer.transform;
        MainCamera.transform.localPosition = Vector3.zero;
        MainCamera.transform.localEulerAngles = Vector3.zero;
        FreeCamera.transform.parent = transform;
        FreeCamera.transform.localPosition = Vector3.zero;
        FreeCamera.transform.localEulerAngles = Vector3.zero;
        FreeCamera.enabled = false;
    }

    private void LoadEditor() {
        Editor_Controller.singleton.gameObject.SetActive(true);
        //     Editor_Controller.singleton.enabled = true;
        MainCamera.transform.parent = CameraEditor.transform;
        MainCamera.transform.localPosition = Vector3.zero;
        MainCamera.transform.localEulerAngles = Vector3.zero;
        CameraEditor.transform.parent = default;
        CameraEditor.transform.position = Editor_Controller.singleton.transform.position;
        CameraEditor.enabled = true;
        KeyboardEditor.enabled = true;
        MouseEditor.enabled = true;
        uIEditor.enabled = true;
        uIEditor.CanvasEditor.SetActive(true);
        UIPlayer.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void UnLoadEditor() {
        Editor_Controller.singleton.gameObject.SetActive(false);
        //  Editor_Controller.singleton.enabled = false;
        MainCamera.transform.parent = ParentCameraPlayer.transform;
        MainCamera.transform.localPosition = Vector3.zero;
        MainCamera.transform.localEulerAngles = Vector3.zero;
        CameraEditor.transform.parent = transform;
        CameraEditor.transform.position = PlayerObject.transform.position;
        CameraEditor.enabled = false;
        KeyboardEditor.enabled = false;
        MouseEditor.enabled = false;
        uIEditor.enabled = false;
        uIEditor.CanvasEditor.SetActive(false);
        UIPlayer.SetActive(true);
    }

    private void LoadSpawnBuild() {
        MainCamera.transform.parent = ParentCameraSpawnBuild.transform;
        MainCamera.transform.localPosition = Vector3.zero;
        MainCamera.transform.localEulerAngles = Vector3.zero;
        ParentCameraSpawnBuild.transform.parent = default;
        ParentCameraSpawnBuild.transform.position = PlayerObject.transform.position;
        SpawnBuildObject.SetActive(true);
        SyncBuilds.singleton.CreateNewBuilding(Editor_Controller.singleton.DetailList);
    }

    private void UnLoadSpawnBuild() {
        SpawnBuildObject.SetActive(false);
        ParentCameraSpawnBuild.transform.parent = transform;
        ParentCameraSpawnBuild.transform.localPosition = Vector3.zero;
        MainCamera.transform.parent = ParentCameraPlayer.transform;
        MainCamera.transform.localPosition = Vector3.zero;
        MainCamera.transform.localEulerAngles = Vector3.zero;
    }

    private void LoadDead() {
        playerMove.enabled = false;
        AnimationController.IsRagdoll = true;
        StartCoroutine(TimeDead());
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;

    }

    private void LoadChair() {
        GetComponent<CharacterController>().enabled = false;
        WeaponPos.SetActive(false);
        model.SetActive(false);
        MovePlayer.enabled = false;
        if (GameRoom.isMultiplayer)
            photon_viev.RPC("PlayerSetChair", RpcTarget.Others);
    }

    private void UnLoadChair() {
        GetComponent<CharacterController>().enabled = true;
        MainCamera.transform.parent = ParentCameraPlayer.transform;
        MainCamera.transform.localPosition = Vector3.zero;
        MainCamera.transform.localEulerAngles = Vector3.zero;
        model.SetActive(true);
        WeaponPos.SetActive(true);
        MovePlayer.enabled = true;
        if (GameRoom.isMultiplayer)
            photon_viev.RPC("PlayerSetMove", RpcTarget.Others);
    }

    IEnumerator TimeDead() {//Отсчет времени после смерти для спавна
        if (GameRoom.isMultiplayer)
            photon_viev.RPC("PlayerSetDead", RpcTarget.Others);
        for (int i = 0; i < 10; i++) {
            if (!GameRoom.isMultiplayer && PlayerOnPause)
                i--;
            yield return new WaitForSeconds(1f);
            if (i == 9) {
                Status = PlayerStatus.Move;
                playerMove.enabled = true;
                AnimationController.IsRagdoll = false;
                Health = 100;
                if (GameRoom.isMultiplayer)
                    photon_viev.RPC("PlayerSetMove", RpcTarget.Others);
            }
        }
    }

    [PunRPC]
    private void PlayerSetDead() {//Убить игрока у клиета
        if (!photon_viev.IsMine) {
            AnimationController.IsRagdoll = true;
            WeaponPos.SetActive(false);
        }
    }

    [PunRPC]
    private void PlayerSetChair() {//Посадить игрока в сидушку
        if (!photon_viev.IsMine) {
            model.SetActive(false);
            GetComponent<CharacterController>().enabled = false;
            WeaponPos.SetActive(false);
        }
    }

    [PunRPC]
    private void PlayerSetMove() {//Оживить игрока у клиента
        if (!photon_viev.IsMine) {
            model.SetActive(true);
            AnimationController.IsRagdoll = false;
            WeaponPos.SetActive(true);
        }
    }


    #endregion

    #region


    #endregion


    private void OnDestroy() {
        if (photon_viev.IsMine) {
            if (PlayerChair.ChairFromPlayer != null) {
                PlayerChair.ChairFromPlayer.GetUp();
            }
            Status = PlayerStatus.Move;
        }
    }

    public PhotonView GetPhotonView() {//Передать обьект фотона
        return photon_viev;
    }
}

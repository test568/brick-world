﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.Linq;
public class PhysicsMapShync : MonoBehaviour {

    [SerializeField] private PhotonView View;
    [SerializeField] private PhysicsObjectMap[] PhysicsObjects;

    [Range(0, 10)]
    [SerializeField] private float SendRate;

    public static PhysicsMapShync Singleton { get; private set; }

    private void Start() {
        if (Singleton != null) {
            Destroy(gameObject);
        }
        else {
            Singleton = this;
        }

        for (int i = 0; i < PhysicsObjects.Length; i++)
            PhysicsObjects[i].GetComponent<PhysicsObjectMap>().Init(i);

        UpdateMasterClient();
        for (int i = 0; i < PhysicsObjects.Length; i++)
            if (i == 0)
                View.ObservedComponents[i] = PhysicsObjects[i];
            else
                View.ObservedComponents.Add(PhysicsObjects[i]);
    }

    public void UpdateMasterClient() {
        if (GameRoom.isMultiplayer && !GameRoom.isMasterClient)
            Physics.autoSimulation = false;
        else
            Physics.autoSimulation = true;

        if (GameRoom.isMultiplayer && GameRoom.isMasterClient) {
            StartCoroutine(UpdatePhysc());
        }
    }



    IEnumerator UpdatePhysc() {
        while (GameRoom.isMasterClient) {
            View.RPC("RPCSetRigdibodies", RpcTarget.Others, new object[2] { PhysicsObjects.Select((x) => x.transform.position).ToArray(), PhysicsObjects.Select((x) => x.transform.eulerAngles).ToArray() });
            yield return new WaitForSeconds(1f / SendRate);
        }
    }

    [PunRPC]
    private void RPCSetRigdibodies(Vector3[] pos, Vector3[] ang) {
        for (int i = 0; i < pos.Length; i++) {
            //   PhysicsObjects[i].NewPosition=pos[i];
            //  PhysicsObjects[i].NewRotation=ang[i];
        }
    }

    [PunRPC]
    private void PushObjectForce(Vector3 force, Vector3 position, int index_object) {
        PhysicsObjects[index_object].RigidbodyObj.AddForceAtPosition(force, position, ForceMode.Force);
    }

    public void ObjectPush(Vector3 force, Vector3 position, int index_object) {
        if (GameRoom.isMultiplayer)
            View.RPC("PushObjectForce", RpcTarget.MasterClient, new object[3] { force, position, index_object });
        else
            PushObjectForce(force, position, index_object);
    }

}


public interface IGetPhotonView {
    PhotonView GetPhotonView();
}
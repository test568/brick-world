﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshRenderer))]
public class TraficController : MonoBehaviour {
    [SerializeField] bool isRed;
    [SerializeField] bool isGreen;
    [SerializeField] bool isBlue;

    [SerializeField] Material material;

    private void Awake() {
        material = GetComponent<MeshRenderer>().material;
    }
    private void Update() {
        if (isRed) {
            material.SetFloat("_IsR", 1);       
        }
        else {
            material.SetFloat("_IsR", 0);
        }

        if (isGreen) {
            material.SetFloat("_IsG", 1);
        }
        else {
            material.SetFloat("_IsG", 0);
        }

        if (isBlue) {
            material.SetFloat("_IsB", 1);
        }
        else {
            material.SetFloat("_IsB", 0);
        }
    }
}

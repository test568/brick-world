﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon;

public class PhysicsObjectMap : MonoBehaviour, IPunObservable {
    public int IndexObject { get; private set; }

    private Vector3 new_pos;

    private Vector3 new_rot;

    private float dist;

    private float ang;

    public Rigidbody RigidbodyObj;

    public float step;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (GameRoom.isMasterClient && stream.IsWriting) {
            stream.SendNext(transform.position);
            stream.SendNext(transform.eulerAngles);
        }
        else if (!GameRoom.isMasterClient && stream.IsReading) {
            new_pos = (Vector3) stream.ReceiveNext();
            new_rot = (Vector3) stream.ReceiveNext();
            dist = Vector3.Distance(new_pos, transform.position);
            ang = Quaternion.Angle(transform.rotation, Quaternion.Euler(new_rot));
        }
    }

    public void Init(int index) {
        IndexObject = index;
    }

    private void FixedUpdate() {
        if (!GameRoom.isMasterClient) {
            transform.position = Vector3.MoveTowards(transform.position, new_pos, dist * ( 2f / PhotonNetwork.SerializationRate ));
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new_rot), ang * ( 2f / PhotonNetwork.SerializationRate ));
        }
    }


}

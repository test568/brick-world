﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using System.Text.RegularExpressions;
using System;
using TMPro;
using Photon.Pun.Demo.SlotRacer.Utils;

public class ElectroWire : MonoBehaviour {

    [SerializeField] GameObject[] points;

    [SerializeField] GameObject line_demo;

    [Range(0, 120f)]
    [SerializeField] float max_distance;

    [Range(0, 180f)]
    [SerializeField] float min_angle;

    [SerializeField] List<ElectroWire> finds_connect = new List<ElectroWire>();

    public Vector3[] WirePoints { get => points.Select((x) => x.transform.position).ToArray(); }

    static List<ElectroWire> all_wire_connects = new List<ElectroWire>();

    List<ConnectWire> connect_wires = new List<ConnectWire>();
    private void Awake() {
        all_wire_connects.Add(this);
    }
    void Start() {
        for (int i = 0; i < all_wire_connects.Count; i++) {
            GameObject obj = all_wire_connects[i].gameObject;
            if (all_wire_connects[i] != this && finds_connect.IndexOf(all_wire_connects[i]) == -1 && Vector3.Distance(obj.transform.position, transform.position) <= max_distance) {
                bool t = true;
                for (int i2 = 0; i2 < finds_connect.Count; i2++) {
                    Vector3 main_facing_n = ( finds_connect[i2].transform.position - transform.position ).normalized;
                    Vector3 direct_main_n = ( all_wire_connects[i].transform.position - transform.position ).normalized;
                    if (( Mathf.Acos(Vector3.Dot(main_facing_n, direct_main_n)) * 180 / Math.PI ) < min_angle) {
                        t = false;
                        break;
                    }
                }
                if (t) {
                    finds_connect.Add(all_wire_connects[i]);
                    ConnectWire cw = all_wire_connects[i].InitConnect(this);
                    connect_wires.Add(cw);
                }
            }
        }
    }

    private ConnectWire InitConnect(ElectroWire wire_two) {
        ConnectWire cw = new ConnectWire();
        cw.Wire1 = this;
        cw.Wire2 = wire_two;
        Vector3[] wire_two_points = wire_two.WirePoints;
        for (int i = 0; i < points.Length; i++) {
            if (i < wire_two_points.Length) {
                GameObject obj = Instantiate(line_demo, transform);
                obj.GetComponent<LineRenderer>().SetPositions(new Vector3[2] { points[i].transform.position, wire_two_points[i] });
                cw.WiresObj.Add(obj);
            }
        }
        return cw;
    }

    private void OnDisable() {
        all_wire_connects.Remove(this);
    }

    private class ConnectWire {
        public ElectroWire Wire1;
        public ElectroWire Wire2;
        public List<GameObject> WiresObj = new List<GameObject>();

        public void CreateWires() {

        }

        public void DestroyWires() {

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Photon.Pun;
using Photon;
using Photon.Realtime;

public class WeatherController: MonoBehaviour, IPunObservable {

    [SerializeField] PhotonView photon_view;

    [Range(0, 1440)]
    public float TimeDay;

    [Range(0, 1440)]
    public float StartTime;

    [Range(0f, 1.0f)]
    public float Density;
    [Range(0f, 5f)]
    public float LightingDrop;

    public GameObject DirectionalLight;

    public GameObject BaseClouds;
    public MeshRenderer CouldLow;
    public MeshRenderer CouldCenter;
    public MeshRenderer CouldHigh;

    public AnimationCurve RandomColorCoulds = AnimationCurve.Linear(0f, 0f, 1f, 1f);

    public AnimationCurve RandomDensityColorCoulds = AnimationCurve.Linear(0f, 0f, 1f, 1f);

    public AnimationCurve RainForceInDensityClouds = AnimationCurve.Linear(0f, 0f, 1f, 1f);

    public Gradient ColdsColor;

    public Gradient DensityColorCoulds;

    public GameObject SoundWind;

    public GameObject EffectRain;

    public AudioClip[] Thunders;

    [Range(0f, 1f)]
    public float UpdateDelayTime = 0.3f;

    [Range(0f, 1f)]
    public float SpeedTime = 0.15f;

    [Range(0f, 1f)]
    public float SpeedDensity = 0.001f;

    [Range(0f, 1f)]
    public float Wind = 0.001f;

    [Range(0f, 1f)]
    public float SpeedWind = 0.001f;


    private const float rotation_sum = 0.25f;

    private const float wind_volume = 0.15f;

    private const float min_rain_rate_over_time = 0;
    private const float max_rain_rate_over_time = 4000;

    private const float rain_height = 5f;

    private float fore_rain;

    [SerializeField] float density_1;
    [SerializeField] float density_2;
    [SerializeField] float density_3;

    [SerializeField] Color color1;
    [SerializeField] Color color2;
    [SerializeField] Color color3;

    private float light_intensity;

    private float start_light;

    private AudioSource audio_wind;

    private ParticleSystem RainParticle;

    private AudioSource RainSound;

    public static WeatherController Weater;

    public bool SimulateTime {
        get {
            return _simulate_time;
            }
        set {
            bool t = false;
            if (_simulate_time==false) {
                t=true;
                }
            _simulate_time=value;
            if (value==true&&t) {
                StartCoroutine(TimeGo());
                StartCoroutine(SpeedRandomDensity());
                }
            }
        }
    bool _simulate_time = true;

    private void Awake() {
        if (Weater==null) {
            Weater=this;
            }
        else {
            Destroy(this);
            return;
            }
        start_light=DirectionalLight.GetComponent<Light>().intensity;
        }

    private void Start() {//Запуск куротин и создания дождя
        TimeDay=StartTime;
        StartCoroutine(TimeGo());
        StartCoroutine(SpeedRandomDensity());
        StartCoroutine(wind_random());
        StartCoroutine(density_random());

        GameObject obj = Instantiate(SoundWind, BaseClouds.transform);
        obj.transform.localPosition=Vector3.zero;
        audio_wind=obj.GetComponent<AudioSource>();

        GameObject rain = Instantiate(EffectRain, BaseClouds.transform);
        RainParticle=rain.GetComponent<ParticleSystem>();
        RainSound=rain.GetComponent<AudioSource>();
        RainSound.transform.localPosition=new Vector3(0, rain_height, 0);
        }

    private void Update() {
        if (ControllerPlayer.MainPlayer&&ControllerPlayer.MainPlayer.Status!=PlayerStatus.inEdtior&&!ShowMiniMap.PlayerLookMiniMap) {//Контроль звука и доп эфектов
            audio_wind.volume=Wind*( AudioPlayer.Audio.EffectVolume*wind_volume );
            if (fore_rain<=0.0f) {
                RainParticle.gameObject.SetActive(false);
                }
            else {
                RainParticle.gameObject.SetActive(true);
                RainSound.volume=fore_rain*AudioPlayer.Audio.EffectVolume;
                }
            }
        else {
            audio_wind.volume=0;
            RainParticle.gameObject.SetActive(false);
            }

        if (GameRoom.isMasterClient||!GameRoom.isMultiplayer)
            RandomThunder();
        }


    [PunRPC]
    private void SetWeather(Color color1, Color color2, Color color3, float density1, float density2, float density3, float d_light_intensity, float time_day, float wind, bool simulate_time) {//Установка погоды на сцену
        DirectionalLight.transform.rotation=Quaternion.identity;
        DirectionalLight.transform.Rotate(new Vector3(TimeDay*rotation_sum-90f, 0, 0));

        this.color1=color1;
        this.color2=color2;
        this.color3=color3;
        density_1=density1;
        density_2=density2;
        density_3=density3;

        fore_rain=RainForceInDensityClouds.Evaluate(density1);

        if (RainParticle)
            RainParticle.emissionRate=Mathf.Clamp(fore_rain*max_rain_rate_over_time, min_rain_rate_over_time, max_rain_rate_over_time);

        TimeDay=time_day;
        Wind=wind;
        SimulateTime=simulate_time;
        DirectionalLight.GetComponent<Light>().intensity=d_light_intensity;

        CouldLow.material.SetColor("_CloudColor", color1);
        CouldCenter.material.SetColor("_CloudColor", color2);
        CouldHigh.material.SetColor("_CloudColor", color3);

        CouldLow.material.SetFloat("_Density", 2f-( density1*1.9f ));
        CouldCenter.material.SetFloat("_Density", 2f-( density2*1.9f ));
        CouldHigh.material.SetFloat("_Density", 2f-( density3*1.9f ));
        }

    IEnumerator TimeGo() {//Обновление погоды и отправка параметров клиентам.
        while (true) {
            if (GameRoom.isMasterClient) {
                if (TimeDay>=1439) {
                    TimeDay=0;
                    }
                else {
                    TimeDay+=SpeedTime;
                    }

                light_intensity=start_light-LightingDrop*density_1;
                DirectionalLight.GetComponent<Light>().intensity=light_intensity;

                if (GameRoom.isMultiplayer) {
                    photon_view.RPC("SetWeather", RpcTarget.Others, new object[10] { color1, color2, color3, density_1, density_2, density_3, light_intensity, TimeDay, Wind, SimulateTime });
                    }
                SetWeather(color1, color2, color3, density_1, density_2, density_3, light_intensity, TimeDay, Wind, SimulateTime);

                if (SimulateTime==false) {
                    yield break;
                    }
                }
            yield return new WaitForSeconds(UpdateDelayTime*Time.timeScale);
            }
        }

    IEnumerator SpeedRandomDensity() {//Установка рандомной густоты облаков
        float add_random_1 = 0;
        float add_random_2 = 0;
        float add_random_3 = 0;
        int point_random = 0;
        while (true) {
            if (GameRoom.isMasterClient) {
                if (point_random==0) {
                    point_random=Random.Range(1700, 3000);
                    add_random_1=Random.Range(-0.2f, 0.2f);
                    add_random_2=Random.Range(-0.2f, 0.2f);
                    add_random_3=Random.Range(-0.2f, 0.2f);
                    }

                float mins_1 = Mathf.Clamp(( ( Density+add_random_1 )-density_1 )*100000, -1f, 1f);
                density_1=Mathf.Clamp(density_1+( mins_1*SpeedDensity*Time.deltaTime ), 0f, 1f);

                float mins_2 = Mathf.Clamp(( ( Density+add_random_2 )-density_2 )*100000, -1f, 1f);
                density_2=Mathf.Clamp(density_2+( mins_2*SpeedDensity*Time.deltaTime ), 0f, 1f);

                float mins_3 = Mathf.Clamp(( ( Density+add_random_3 )-density_3 )*100000, -1f, 1f);
                density_3=Mathf.Clamp(density_3+( mins_3*SpeedDensity*Time.deltaTime ), 0f, 1f);


                Color color1 = DensityColorCoulds.Evaluate(RandomDensityColorCoulds.Evaluate(density_1));
                Color color2 = DensityColorCoulds.Evaluate(RandomDensityColorCoulds.Evaluate(density_2));
                Color color3 = DensityColorCoulds.Evaluate(RandomDensityColorCoulds.Evaluate(density_3));

                Color color21 = ColdsColor.Evaluate(TimeDay/1440+RandomColorCoulds.Evaluate(Random.Range(0f, 1f))*0.001f);
                Color color22 = ColdsColor.Evaluate(TimeDay/1440+RandomColorCoulds.Evaluate(Random.Range(0f, 1f))*0.001f);
                Color color23 = ColdsColor.Evaluate(TimeDay/1440+RandomColorCoulds.Evaluate(Random.Range(0f, 1f))*0.001f);

                this.color1=( color21-color1 )*new Color(1, 1, 1, 0)+new Color(0, 0, 0, color21.a);
                this.color2=( color22-color2 )*new Color(1, 1, 1, 0)+new Color(0, 0, 0, color21.a);
                this.color3=( color23-color3 )*new Color(1, 1, 1, 0)+new Color(0, 0, 0, color21.a);

                point_random--;
                if (SimulateTime==false) {
                    yield break;
                    }
                }
            yield return null;
            }
        }

    IEnumerator wind_random() {//установка рандомного ветра
        float wind_add = 0;
        int point_random = 0;

        while (true) {
            if (GameRoom.isMasterClient) {
                if (point_random==0) {
                    point_random=Random.Range(3000, 12000);
                    wind_add=Random.Range(0, 1f);
                    }
                float wind = Mathf.Clamp(( wind_add-Wind )*100000, -1f, 1f);
                Wind=Mathf.Clamp(Wind+( wind*SpeedWind*Time.deltaTime ), 0f, 1f);

                point_random--;
                }
            yield return null;

            }
        }

    IEnumerator density_random() {//Установка общей густоты облаков
        float density_add = 0;
        int point_random = 0;

        while (true) {
            if (GameRoom.isMasterClient) {
                if (point_random==0) {
                    point_random=Random.Range(1700, 5000);
                    density_add=Random.Range(0, 1f);
                    }
                float density = Mathf.Clamp(( density_add-Density )*10000, -1f, 1f);
                Density=Mathf.Clamp(Density+( density*SpeedDensity*Time.deltaTime )-( Wind/6000f ), 0f, 1f);

                point_random--;
                }
            yield return null;
            }
        }


    private void RandomThunder() {//Рандомное создание грома
        if (fore_rain>0.5f) {
            int rand = Random.Range(0, 1400);
            if (rand==10) {
                int number_audio = Random.Range(0, Thunders.Length);
                if (GameRoom.isMultiplayer) {
                    photon_view.RPC("ThunderLoad", RpcTarget.All, new object[1] { number_audio });
                    }
                else {
                    ThunderLoad(number_audio);
                    }
                }
            }
        }


    [PunRPC]
    private void ThunderLoad(int NumberAudio) {//Вызов грома у всех клиентов
        StartCoroutine(Thunder(Thunders[NumberAudio]));
        }

    IEnumerator Thunder(AudioClip audio) {//Создание и проигрование грома
        GameObject obj = new GameObject("Thunder");
        obj.transform.parent=BaseClouds.transform;
        obj.transform.localPosition=Vector3.zero;
        AudioSource source = obj.AddComponent<AudioSource>();
        source.clip=audio;
        source.volume=AudioPlayer.Audio.EffectVolume;
        source.Play();
        yield return new WaitForSeconds(audio.length);
        Destroy(obj);
        yield break;
        }

    private void LateUpdate() {
        BaseClouds.transform.position=ControllerPlayer.MainPlayer.MainCamera.transform.position;//Изменение позиции для облаков
        }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {

        }
    }

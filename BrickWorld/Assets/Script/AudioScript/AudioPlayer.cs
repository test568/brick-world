﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class AudioPlayer: MonoBehaviour {
    public static AudioPlayer Audio;
    public float AllVolume;
    public float EngineVolume;
    public float EffectVolume;
    public float BlastVolume;

    private void Awake() {
        if (Audio==null) {
            Audio=this;
            }
        else {
            Destroy(gameObject);
            }
        }
    }

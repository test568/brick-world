﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fps: MonoBehaviour {
    public Text text;
    private float a = 0;
    private float FPS = 0;
    private float time = 0;

    void Update() {
        time+=Time.deltaTime;
        a++;
        if (time>=1) {
            time=0;
            FPS=a;
            a=0;
            text.text=FPS+"";
            }
        }
    }

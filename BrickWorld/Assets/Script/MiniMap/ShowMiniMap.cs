﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMiniMap : MonoBehaviour {//Загрузка мини карты
    public static bool PlayerLookMiniMap;
    public static ShowMiniMap MiniMap;

    [SerializeField] GameObject CameraMove;
    [SerializeField] GameObject CameraPosSet;

    [SerializeField] Vector2 LimitMoveCamera;
    [SerializeField] float SpeedMoveCamera;
    [SerializeField] float SpeedRotateCamera;

    private Transform old_ParentCam;
    private Quaternion old_rotate;
    private Vector3 old_pos;

    private Vector3 size_mini_map;
    private Vector3 size_terrain;
    private Vector3 convert_size;

    private Vector3 terrain_center;
    private float add_height;

    [SerializeField] GameObject PlayerIcon;
    [SerializeField] Terrain Map;
    [SerializeField] GameObject MiniMapObject;

    private LayerMask hit_layer;

    private void Awake() {
        hit_layer = LayerMask.GetMask(new string[1] { "MiniMap" });
    }

    private void Start() {
        if (MiniMap == null) {
            MiniMap = this;

            Vector3 mesh_size = MiniMapObject.GetComponent<MeshFilter>().mesh.bounds.size;
            Vector3 obj_size = MiniMapObject.transform.localScale;
            size_mini_map = new Vector3(mesh_size.x * obj_size.x, mesh_size.y * obj_size.y, mesh_size.z * obj_size.z);
            size_terrain = Map.terrainData.size;

            Vector3[] verticers = MiniMapObject.GetComponent<MeshFilter>().mesh.vertices;
            add_height = 1000;
            for (int i = 0; i < verticers.Length; i++) {
                if (add_height > verticers[i].y) {
                    add_height = verticers[i].y;
                }
            }
            convert_size = new Vector3(size_mini_map.x / size_terrain.x, ( mesh_size.y + add_height ) / size_terrain.y, size_mini_map.z / size_terrain.z);
            terrain_center = new Vector3(size_terrain.x / 2f, 0, size_terrain.z / 2f);
        }
        else {
            Destroy(gameObject);
        }
    }
    private void Update() {
        if (ControllerPlayer.MainPlayer && PlayerLookMiniMap && !ControllerPlayer.PlayerOnPause) {
            if (Input.GetMouseButton(1)) {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.lockState = CursorLockMode.Locked;
                CameraMove.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * SpeedRotateCamera * Time.deltaTime, 0));
                if (Input.GetKey(KeyCode.D)) {
                    CameraMove.transform.position += CameraMove.transform.forward * SpeedMoveCamera * Time.deltaTime;
                }
                if (Input.GetKey(KeyCode.A)) {
                    CameraMove.transform.position -= CameraMove.transform.forward * SpeedMoveCamera * Time.deltaTime;
                }
                if (Input.GetKey(KeyCode.S)) {
                    CameraMove.transform.position += CameraMove.transform.right * SpeedMoveCamera * Time.deltaTime;
                }
                if (Input.GetKey(KeyCode.W)) {
                    CameraMove.transform.position -= CameraMove.transform.right * SpeedMoveCamera * Time.deltaTime;
                }
                CameraMove.transform.localPosition = new Vector3(Mathf.Clamp(CameraMove.transform.localPosition.x, -LimitMoveCamera.x, LimitMoveCamera.x), 0, Mathf.Clamp(CameraMove.transform.localPosition.z, -LimitMoveCamera.y, LimitMoveCamera.y));
            }
            else if (Input.GetMouseButtonDown(0)) {
                RaycastHit hit;
                Ray ray = EffectOnCamera.effectOnCamera.objCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 10f, hit_layer)) {
                    Vector3 pos = ( hit.point - MiniMapObject.transform.position + new Vector3(0, add_height, 0) );
                    Vector3 teleport_pos = ( new Vector3(pos.x / convert_size.x, pos.y / convert_size.y, pos.z / convert_size.z) + terrain_center );
                    if (Physics.Raycast(teleport_pos + new Vector3(0, 1000f, 0), -Vector3.up * 10000, out hit, Mathf.Infinity)) {
                        ControllerPlayer.MainPlayer.TeleportPlayer(hit.point);
                    }
                }
            }
            else if (!Input.GetMouseButton(1)) {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            PlayerPointSet();
        }
        if (ControllerPlayer.MainPlayer && Input.GetKeyDown(KeyCode.M) && ( ControllerPlayer.MainPlayer.Status == PlayerStatus.inChair || ControllerPlayer.MainPlayer.Status == PlayerStatus.Move ) && !UIOptionsGame.IsPause) {
            if (PlayerLookMiniMap) {
                UnLoadMiniMap();
            }
            else {
                PlayerPointSet();
                LoadMiniMap();
            }
        }
    }

    public void LoadMiniMap() {//Загрузка мини карты
        PlayerLookMiniMap = true;
        //  Debug.Log("Загрузить мини карту");
        ControllerPlayer.LockControll = true;
        EffectOnCamera.effectOnCamera.LoadCameraMiniMap();

        ControllerPlayer.MainPlayer.ShowAndHidePlayerUI(false);
        GameObject camera = EffectOnCamera.effectOnCamera.objCamera.gameObject;
        old_rotate = camera.transform.localRotation;
        old_pos = camera.transform.localPosition;

        camera.transform.position = CameraPosSet.transform.position;
        old_ParentCam = camera.transform.parent;
        camera.transform.parent = CameraPosSet.transform;
        camera.transform.rotation = Quaternion.LookRotation(( CameraMove.transform.position - camera.transform.position ).normalized, Vector3.up);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        CameraMove.transform.position = new Vector3(PlayerIcon.transform.position.x, 0, PlayerIcon.transform.position.z);
        CameraMove.transform.localPosition = new Vector3(Mathf.Clamp(CameraMove.transform.localPosition.x, -LimitMoveCamera.x, LimitMoveCamera.x), 0, Mathf.Clamp(CameraMove.transform.localPosition.z, -LimitMoveCamera.y, LimitMoveCamera.y));


    }

    public void UnLoadMiniMap() {//Отключение мини карты
        PlayerLookMiniMap = false;
        //  Debug.Log("Убрать мини карту");
        ControllerPlayer.LockControll = false;
        EffectOnCamera.effectOnCamera.LoadCameraGame();

        ControllerPlayer.MainPlayer.ShowAndHidePlayerUI(true);
        GameObject camera = EffectOnCamera.effectOnCamera.objCamera.gameObject;
        camera.transform.parent = old_ParentCam;
        camera.transform.localPosition = old_pos;
        camera.transform.localRotation = old_rotate;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void PlayerPointSet() {//Установка иконки игрока на мини карте
        GameObject player;
        int rot;
        if (ControllerPlayer.MainPlayer.Status == PlayerStatus.inChair) {//Разворот иконки игрока если он в сиденьии.
            player = PlayerChair.ChairFromPlayer.gameObject;
            rot = 180;
        }
        else {
            player = ControllerPlayer.MainPlayer.PlayerObject;
            rot = 0;
        }
        Vector3 pos = player.transform.position;
        PlayerIcon.transform.localPosition = new Vector3(( pos - terrain_center ).x * convert_size.x, pos.y * convert_size.y, ( pos - terrain_center ).z * convert_size.z);
        PlayerIcon.transform.localRotation = Quaternion.Euler(new Vector3(0, player.transform.eulerAngles.y - rot, 0));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
using System.IO;
public class MiniMapMeshGenerator: ScriptableWizard {

    public Terrain Map;
    public GameObject MeshGameObject;
    public int SizeMap;
    public float HeightMap;
    public float DistMap;

    TerrainData terrain_data;

    Mesh mesh;

    Vector3[] vertices;
    int[] triangles;
    Vector2[] uv;

    [MenuItem("Terrain/MiniMap/Mesh generator")]
    static void MiniMapGenerator() {
        DisplayWizard<MiniMapMeshGenerator>("Mesh generator", "Generate");
        }

    MiniMapMeshGenerator() {
        for (int i = 0; i<OptionGeneratorMiniMap.Params.Length; i++) {
            OptionGeneratorMiniMap.Params[i].SetValue(this, OptionGeneratorMiniMap.ValueParams[i]);
            }
        }

    private void OnWizardCreate() {
        try {
            EditorUtility.DisplayProgressBar("Building", "", 0.15f);
            if (SizeMap==0||!Map||MeshGameObject.GetComponent<MeshFilter>()==null||MeshGameObject.GetComponent<MeshRenderer>()==null
                ||MeshGameObject.GetComponent<MeshRenderer>().sharedMaterial==null) {
                return;
                }
            terrain_data=Map.terrainData;

            mesh=new Mesh();
            MeshGameObject.GetComponent<MeshFilter>().mesh=mesh;

            vertices=new Vector3[( SizeMap+1 )*( SizeMap+1 )];

            float size_terrain = terrain_data.heightmapResolution/( SizeMap+1 );
            float size_height = HeightMap/terrain_data.size.y;

            uv=new Vector2[vertices.Length];

            for (int i = 0, z = 0; z<=SizeMap; z++) {
                for (int x = 0; x<=SizeMap; x++) {
                    vertices[i]=new Vector3(x*DistMap, terrain_data.GetHeight((int)( x*size_terrain ), (int)( z*size_terrain ))*size_height, z*DistMap);
                    uv[i]=new Vector2((float)x/SizeMap, (float)z/SizeMap);
                    i++;
                    }
                }
            EditorUtility.DisplayProgressBar("Building", "", 0.3f);
            Vector3 center_pos = new Vector3();
            int count = 0;
            for (int i = 0; i<vertices.Length; i++) {
                center_pos+=vertices[i];
                count++;
                }
            EditorUtility.DisplayProgressBar("Building", "", 0.5f);
            center_pos=center_pos/count;
            for (int i = 0; i<vertices.Length; i++) {
                vertices[i]-=new Vector3(center_pos.x, 0, center_pos.z);
                }

            triangles=new int[SizeMap*SizeMap*6];
            EditorUtility.DisplayProgressBar("Building", "", 0.7f);
            int vert = 0;
            int tris = 0;
            for (int z = 0; z<SizeMap; z++) {
                for (int x = 0; x<SizeMap; x++) {
                    triangles[tris+0]=vert+0;
                    triangles[tris+1]=vert+SizeMap+1;
                    triangles[tris+2]=vert+1;
                    triangles[tris+3]=vert+1;
                    triangles[tris+4]=vert+SizeMap+1;
                    triangles[tris+5]=vert+SizeMap+2;

                    vert++;
                    tris+=6;
                    }
                vert++;
                }

            mesh.Clear();

            mesh.vertices=vertices;
            mesh.triangles=triangles;
            mesh.uv=uv;
            mesh.RecalculateNormals();
            EditorUtility.DisplayProgressBar("Building", "", 0.9f);
            string patch_obj = AssetDatabase.GetAssetPath(MeshGameObject);
            int index = patch_obj.IndexOf(MeshGameObject.name+".prefab");
            string patch = patch_obj.Remove(index);

            AssetDatabase.CreateAsset(mesh, patch+MeshGameObject.name+"Mesh");
            AssetDatabase.SaveAssets();
            EditorUtility.DisplayProgressBar("Building", "", 1f);
            EditorUtility.ClearProgressBar();
            }
        catch (Exception ex) {
            EditorUtility.DisplayDialog("Error", ex.Message, "Ok");
            EditorUtility.ClearProgressBar();
            }
        }

    static class OptionGeneratorMiniMap {
        public static FieldInfo[] Params = new FieldInfo[0];
        public static object[] ValueParams = new object[0];
        }
    private void OnWizardUpdate() {
        Type type = this.GetType();
        FieldInfo[] params_type = type.GetFields();
        OptionGeneratorMiniMap.Params=params_type;
        OptionGeneratorMiniMap.ValueParams=new object[params_type.Length];
        for (int i = 0; i<params_type.Length; i++) {
            OptionGeneratorMiniMap.ValueParams[i]=params_type[i].GetValue(this);
            }
        }
    }

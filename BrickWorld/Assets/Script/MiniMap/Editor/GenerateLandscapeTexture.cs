﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
using System.IO;

public class GeneratorLandscapeTexture: ScriptableWizard {
    public Texture2D SplatMap;
    public Texture2D Texture;
    public int SizeTexture;
    public Gradient GradientColor;

    GeneratorLandscapeTexture() {
        for (int i = 0; i<ParamsLandscapeTexture.Params.Length; i++) {
            ParamsLandscapeTexture.Params[i].SetValue(this, ParamsLandscapeTexture.ValueParams[i]);
            }
        }

    [MenuItem("Terrain/MiniMap/Landscape texture")]
    public static void Landscape() {
        DisplayWizard<GeneratorLandscapeTexture>("Landscape texture", "Generate");
        }

    private void OnWizardCreate() {
        try {
            EditorUtility.DisplayProgressBar("Building", "", 0.1f);
            string new_patch = AssetDatabase.GetAssetPath(Texture);
            Texture=new Texture2D(SizeTexture*2, SizeTexture*2);
            Color[] colors = new Color[Texture.width*Texture.width];
            float size_texture = (float)SplatMap.width/(float)Texture.width;

            int c = 0;

            for (int i = 0; i<Texture.width; i++) {
                for (int i2 = 0; i2<Texture.height; i2++) {
                    Color color;
                    color=SplatMap.GetPixel((int)( i*size_texture ), (int)( i2*size_texture ));
                    float v, h, s;

                    Color.RGBToHSV(color, out h, out s, out v);

                    color=GradientColor.Evaluate(h);
                    colors[c]=color;
                    c++;
                    }
                }
            EditorUtility.DisplayProgressBar("Building", "", 0.5f);
            Texture.SetPixels(colors);
            Texture.Apply();

            byte[] bytes = Texture.EncodeToPNG();
            File.WriteAllBytes(new_patch, bytes);
            EditorUtility.DisplayProgressBar("Building", "", 1f);
            EditorUtility.ClearProgressBar();
            }
        catch (Exception ex) {
            EditorUtility.DisplayDialog("Error", ex.Message+"\n"+ex.Source, "Ok");
            EditorUtility.ClearProgressBar();
            }
        }
    private void OnWizardUpdate() {
        Type type = this.GetType();
        FieldInfo[] params_type = type.GetFields();
        ParamsLandscapeTexture.Params=params_type;
        ParamsLandscapeTexture.ValueParams=new object[params_type.Length];
        for (int i = 0; i<params_type.Length; i++) {
            ParamsLandscapeTexture.ValueParams[i]=params_type[i].GetValue(this);
            }
        }

    private static class ParamsLandscapeTexture {
        public static FieldInfo[] Params = new FieldInfo[0];
        public static object[] ValueParams = new object[0];
        }
    }

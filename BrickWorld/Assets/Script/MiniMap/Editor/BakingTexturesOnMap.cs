﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using System.Reflection;
using System.IO;

public class BakingTexturesOnMap: ScriptableWizard {

    public Terrain Map;
    public Texture2D ColorTexture;
    public GameObject[] ObjectsTexture;
    public LayerMask Layer;

    BakingTexturesOnMap() {
        for (int i = 0; i<ParamsBakingTextures.Params.Length; i++) {
            ParamsBakingTextures.Params[i].SetValue(this, ParamsBakingTextures.ValueParams[i]);
            }
        }

    [MenuItem("Terrain/MiniMap/Baking textures on a map color")]
    public static void BakingTextures() {
        DisplayWizard<BakingTexturesOnMap>("Baking textures on a map color", "Generate");
        }

    private void OnWizardCreate() {
        try {
            List<Collider> colliders = new List<Collider>();
            for (int i = 0; i<ObjectsTexture.Length; i++) {
                Collider[] cols = ObjectsTexture[i].GetComponents<Collider>();
                for (int i2 = 0; i2<cols.Length; i2++) {
                    if (cols[i2].enabled) {
                        colliders.Add(cols[i2]);
                        }
                    }
                }
            for (int i = 0; i<colliders.Count; i++) {
                colliders[i].enabled=false;
                }

            bool[] is_rigidbody = new bool[ObjectsTexture.Length];
            Rigidbody[] rigidbodies = new Rigidbody[ObjectsTexture.Length];
            MeshCollider[] new_colider = new MeshCollider[ObjectsTexture.Length];
            for (int i = 0; i<ObjectsTexture.Length; i++) {
                if (ObjectsTexture[i].GetComponent<Rigidbody>()) {
                    rigidbodies[i]=ObjectsTexture[i].GetComponent<Rigidbody>();
                    rigidbodies[i].isKinematic=true;
                    is_rigidbody[i]=false;
                    }
                else {
                    rigidbodies[i]=ObjectsTexture[i].AddComponent<Rigidbody>();
                    rigidbodies[i].isKinematic=true;
                    is_rigidbody[i]=true;
                    }

                new_colider[i]=rigidbodies[i].gameObject.AddComponent<MeshCollider>();
                if (rigidbodies[i].GetComponent<MeshFilter>()) {
                    new_colider[i].sharedMesh=rigidbodies[i].GetComponent<MeshFilter>().sharedMesh;
                    }
                }
            //

            RaycastHit hit;
            EditorUtility.DisplayProgressBar("Building", "", 0.3f);
            string new_patch = AssetDatabase.GetAssetPath(ColorTexture);
            Vector3 terrain_pos = Map.transform.position;
            Vector3 size_pos = Map.terrainData.size;
            Vector3 convert = new Vector3(size_pos.x/ColorTexture.width, 1, size_pos.z/ColorTexture.height);
            Color[] colors = ColorTexture.GetPixels();
            int c = 0;
            for (int i = 0; i<ColorTexture.width; i++) {
                for (int i2 = 0; i2<ColorTexture.height; i2++) {
                    if (Physics.Raycast(new Vector3(terrain_pos.x+( i*convert.x ), 10000, terrain_pos.z+( i2*convert.z )), -Vector3.up*100000f, out hit, 100000f, Layer)) {
                        Vector2 pos_texture = hit.textureCoord;
                        Texture2D text = null;
                        Color color = default;
                        if (hit.collider.gameObject.GetComponent<Renderer>()) {
                            text=(Texture2D)hit.collider.gameObject.GetComponent<Renderer>().sharedMaterial.mainTexture;
                            color=hit.collider.gameObject.GetComponent<Renderer>().sharedMaterial.color;
                            }
                        if (text) {
                            color=text.GetPixel((int)pos_texture.x, (int)pos_texture.y);
                            }
                        colors[c]=color;
                        }

                    c++;
                    }
                }
            EditorUtility.DisplayProgressBar("Building", "", 0.6f);
            ColorTexture.SetPixels(colors);
            ColorTexture.Apply();

            byte[] bytes = ColorTexture.EncodeToPNG();
            File.WriteAllBytes(new_patch, bytes);
            //
            for (int i = 0; i<is_rigidbody.Length; i++) {
                if (is_rigidbody[i]) {
                    DestroyImmediate(rigidbodies[i]);
                    }
                DestroyImmediate(new_colider[i]);
                }
            for (int i = 0; i<colliders.Count; i++) {
                colliders[i].enabled=true;
                }
            EditorUtility.DisplayProgressBar("Building", "", 1f);
            EditorUtility.ClearProgressBar();
            }
        catch (Exception ex) {
            EditorUtility.ClearProgressBar();
            EditorUtility.DisplayDialog("Error", ex.Message+"\n"+ex.Source, "Ok");
            }
        }
    private void OnWizardUpdate() {
        Type type = this.GetType();
        FieldInfo[] params_type = type.GetFields();
        ParamsBakingTextures.Params=params_type;
        ParamsBakingTextures.ValueParams=new object[params_type.Length];
        for (int i = 0; i<params_type.Length; i++) {
            ParamsBakingTextures.ValueParams[i]=params_type[i].GetValue(this);
            }
        }

    private static class ParamsBakingTextures {
        public static FieldInfo[] Params = new FieldInfo[0];
        public static object[] ValueParams = new object[0];
        }
    }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Reflection;
using System;

public class BakingModelsMesh: ScriptableWizard {
    public MeshFilter MapMesh;
    public Terrain MapTerrain;
    public Texture2D TextureSave;
    public bool TerrainObjects;
    public GameObject[] ThirdPartyObjects;


    BakingModelsMesh() {
        for (int i = 0; i<ParamsBakingModels.Params.Length; i++) {
            ParamsBakingModels.Params[i].SetValue(this, ParamsBakingModels.ValueParams[i]);
            }
        }

    [MenuItem("Terrain/MiniMap/Baking models on mesh")]
    public static void BakingModel() {
        DisplayWizard<BakingModelsMesh>("Baking models on mesh", "Generate");
        }

    private void OnWizardCreate() {
        try {
            EditorUtility.DisplayProgressBar("Building", "", 0.1f);
            GameObject obj = MapMesh.gameObject;
            GameObject extra_mesh = null;
            int count_child = obj.transform.childCount;
            for (int i = 0; i<count_child; i++) {
                if (obj.transform.GetChild(i).gameObject.GetComponent<MeshFilter>()) {
                    extra_mesh=obj.transform.GetChild(i).gameObject;
                    break;
                    }
                }
            if (extra_mesh) {
                Mesh mesh;
                mesh=extra_mesh.GetComponent<MeshFilter>().sharedMesh;
                if (!mesh) {
                    mesh=new Mesh();
                    }
                List<Vector3> pos = new List<Vector3>();
                List<Quaternion> rot = new List<Quaternion>();
                List<GameObject> low_poly = new List<GameObject>();
                for (int i = 0; i<ThirdPartyObjects.Length; i++) {
                    if (ThirdPartyObjects[i].GetComponent<LowPolyModelLink>()) {
                        low_poly.Add(ThirdPartyObjects[i].GetComponent<LowPolyModelLink>().LowPolyModel);
                        pos.Add(ThirdPartyObjects[i].transform.position);
                        rot.Add(ThirdPartyObjects[i].transform.rotation);
                        }
                    }
                if (TerrainObjects) {
                    TreeInstance[] trees = MapTerrain.terrainData.treeInstances;
                    for (int i = 0; i<trees.Length; i++) {
                        GameObject tree = MapTerrain.terrainData.treePrototypes[trees[i].prototypeIndex].prefab;
                        if (tree.GetComponent<LowPolyModelLink>()) {
                            low_poly.Add(tree.GetComponent<LowPolyModelLink>().LowPolyModel);
                            pos.Add(trees[i].position);
                            rot.Add(new Quaternion(trees[i].rotation, 0, trees[i].rotation,0));
                            }
                        }
                    }

                Mesh[] meshs = new Mesh[low_poly.Count];
                CombineInstance[] combineInstance = new CombineInstance[low_poly.Count];
                Material mat;

                CombineInstance[] cm = Combine(low_poly.ToArray(), pos.ToArray(), rot.ToArray(), true, TextureFormat.RGB24, out mat);
                mesh.CombineMeshes(cm);
                extra_mesh.GetComponent<MeshFilter>().sharedMesh=mesh;
                extra_mesh.GetComponent<MeshRenderer>().sharedMaterial.mainTexture=TextureSave;


                float add_height = 1000;
                for (int i = 0; i<MapMesh.sharedMesh.vertexCount; i++) {
                    if (add_height>MapMesh.sharedMesh.vertices[i].y) {
                        add_height=MapMesh.sharedMesh.vertices[i].y;
                        }
                    }
                Vector3 size_terrain = MapTerrain.terrainData.size;
                Vector3 size_map = ( MapMesh.sharedMesh.bounds.size+new Vector3(0, add_height, 0) );
                extra_mesh.transform.localScale=new Vector3(size_map.x/size_terrain.x, ( size_map.y+( ( size_map.x/size_map.y )-( size_terrain.x/size_terrain.y ) ) )/size_terrain.y, size_map.z/size_terrain.z);

                extra_mesh.transform.localPosition=new Vector3(-size_map.x/2f, 0, -size_map.z/2f);

                string patch_obj = AssetDatabase.GetAssetPath(MapMesh);
                int index = patch_obj.IndexOf(MapMesh.name+".prefab");
                string patch = patch_obj.Remove(index);
                AssetDatabase.CreateAsset(mesh, patch+MapMesh.name+"MeshExtra");
                AssetDatabase.SaveAssets();
                }
            else {
                EditorUtility.DisplayDialog("Error", "No extra mesh", "Ok");
                }
            EditorUtility.DisplayProgressBar("Building", "", 1f);
            EditorUtility.ClearProgressBar();
            }
        catch (Exception ex) {
            EditorUtility.ClearProgressBar();
            EditorUtility.DisplayDialog("Error", ex.Message+"\n"+ex.Source, "Ok");
            }
        }

    private void OnWizardUpdate() {
        Type type = this.GetType();
        FieldInfo[] params_type = type.GetFields();
        ParamsBakingModels.Params=params_type;
        ParamsBakingModels.ValueParams=new object[params_type.Length];
        for (int i = 0; i<params_type.Length; i++) {
            ParamsBakingModels.ValueParams[i]=params_type[i].GetValue(this);
            }
        }

    static class ParamsBakingModels {
        public static FieldInfo[] Params = new FieldInfo[0];
        public static object[] ValueParams = new object[0];
        }

    private CombineInstance[] Combine(GameObject[] objectsToCombine, Vector3[] pos, Quaternion[] rot, bool useMipMaps, TextureFormat textureFormat, out Material material) {

        int size;
        int originalSize;
        int pow2;
        Texture2D texture;
        Mesh mesh;
        Hashtable textureAtlas = new Hashtable();

        if (objectsToCombine.Length>0) {
            string new_patch = AssetDatabase.GetAssetPath(TextureSave);
            originalSize=objectsToCombine[0].GetComponent<MeshRenderer>().sharedMaterial.mainTexture.width;
            pow2=GetTextureSize(objectsToCombine);
            size=pow2*originalSize;
            TextureSave=new Texture2D(size, size, textureFormat, useMipMaps);

            // Create the combined texture (remember to ensure the total size of the texture isn't
            // larger than the platform supports)
            for (int i = 0; i<objectsToCombine.Length; i++) {
                texture=(Texture2D)objectsToCombine[i].GetComponent<MeshRenderer>().sharedMaterial.mainTexture;
                if (!textureAtlas.ContainsKey(texture)) {
                    TextureSave.SetPixels(( i%pow2 )*originalSize, ( i/pow2 )*originalSize, originalSize, originalSize, texture.GetPixels());
                    textureAtlas.Add(texture, new Vector2(i%pow2, i/pow2));
                    }
                }
            TextureSave.Apply();
            material=new Material(objectsToCombine[0].GetComponent<MeshRenderer>().sharedMaterial);
            material.mainTexture=TextureSave;

            byte[] bytes = TextureSave.EncodeToPNG();
            File.WriteAllBytes(new_patch, bytes);

            // Update texture co-ords for each mesh (this will only work for meshes with coords betwen 0 and 1).
            for (int i = 0; i<objectsToCombine.Length; i++) {
                mesh=objectsToCombine[i].GetComponent<MeshFilter>().sharedMesh;
                Vector2[] uv = new Vector2[mesh.uv.Length];
                Vector2 offset;
                if (textureAtlas.ContainsKey(objectsToCombine[i].GetComponent<MeshRenderer>().sharedMaterial.mainTexture)) {
                    offset=(Vector2)textureAtlas[objectsToCombine[i].GetComponent<MeshRenderer>().sharedMaterial.mainTexture];
                    for (int u = 0; u<mesh.uv.Length; u++) {
                        uv[u]=mesh.uv[u]/(float)pow2;
                        uv[u].x+=( (float)offset.x )/(float)pow2;
                        uv[u].y+=( (float)offset.y )/(float)pow2;
                        }
                    }
                mesh.uv=uv;
                }

            // Combine each mesh marked as static
            CombineInstance[] combine = new CombineInstance[objectsToCombine.Length];
            for (int i = 0; i<objectsToCombine.Length; i++) {
                combine[i].mesh=objectsToCombine[i].GetComponent<MeshFilter>().sharedMesh;
                objectsToCombine[i].transform.position=pos[i];
                objectsToCombine[i].transform.rotation=rot[i];
                combine[i].transform=objectsToCombine[i].transform.localToWorldMatrix;
                }
            return combine;
            }
        material=new Material(Shader.Find("Standard"));
        ;
        return new CombineInstance[0];
        }

    private int GetTextureSize(GameObject[] o) {
        ArrayList textures = new ArrayList();
        // Find unique textures
        for (int i = 0; i<o.Length; i++) {
            if (!textures.Contains(o[i].GetComponent<MeshRenderer>().sharedMaterial.mainTexture)) {
                textures.Add(o[i].GetComponent<MeshRenderer>().sharedMaterial.mainTexture);
                }
            }
        if (textures.Count==1)
            return 1;
        if (textures.Count<5)
            return 2;
        if (textures.Count<17)
            return 4;
        if (textures.Count<65)
            return 8;
        // Doesn't handle more than 64 different textures but I think you can see how to extend
        return 0;
        }
    }

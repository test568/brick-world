﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFreeCamera : MonoBehaviour { //Скрипт Отвечает за управление камерой в свободном режиме и ие выключение

    [Range(0, 40)]
    public float Speed;
    [Range(40, 140)]
    public float SpeedRotate;


    private float rotY = 0.0f;
    private float rotX = 0.0f;

    private int time_from_in_free_camera;

    private void OnEnable() {
        gameObject.transform.localRotation = new Quaternion();
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
        time_from_in_free_camera = 20;
    }

    private void Update() {
        if (time_from_in_free_camera > 0) ;
        time_from_in_free_camera--;
        if (!ControllerPlayer.PlayerOnPause) {
            if (Input.GetKey(KeyCode.W)) {
                gameObject.transform.position += gameObject.transform.forward * ( Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.S)) {
                gameObject.transform.position += gameObject.transform.forward * ( -Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.A)) {
                gameObject.transform.position += gameObject.transform.right * ( -Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.D)) {
                gameObject.transform.position += gameObject.transform.right * ( Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.Space)) {
                gameObject.transform.position += gameObject.transform.up * ( Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.LeftAlt)) {
                gameObject.transform.position += gameObject.transform.up * ( -Speed * Time.deltaTime );
            }

            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = -Input.GetAxis("Mouse Y");

            rotY += mouseX * SpeedRotate * Time.deltaTime;
            rotX += mouseY * SpeedRotate * Time.deltaTime;
            rotX = Mathf.Clamp(rotX, -90, 90);
            Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
            transform.rotation = localRotation;

            if (Input.GetMouseButtonDown(0)) {
                LayerMask layer = LayerMask.GetMask("Ground");
                Ray ray = EffectOnCamera.effectOnCamera.objCamera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, layer) && Vector3.Dot(hit.normal, Vector3.up) > 0.3f) {
                    ControllerPlayer.MainPlayer.TeleportPlayerNoEditStatus(hit.point);
                }
            }

        }
    }

    private void LateUpdate() {
        if (!ControllerPlayer.PlayerOnPause && ControllerPlayer.MainPlayer.Status == PlayerStatus.FreeCamera && Input.GetKeyDown(KeyCode.F) && time_from_in_free_camera <= 0) {
            ControllerPlayer.MainPlayer.Status = PlayerStatus.Move;
        }
    }

    private void OnDisable() {
        gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }
}
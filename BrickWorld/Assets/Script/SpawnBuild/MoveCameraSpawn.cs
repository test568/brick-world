﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraSpawn : MonoBehaviour {
    [Range(0, 40)]
    public float Speed;
    [Range(40, 140)]
    public float SpeedRotate;

    private float rotY = 0.0f; // rotation around the up/y axis
    private float rotX = 0.0f; // rotation around the right/x axis

    private void OnEnable() {
        gameObject.transform.localRotation = new Quaternion();
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
    }

    private void Update() {
        if (Time.timeScale != 0f && ControllerPlayer.MainPlayer.Status == PlayerStatus.SpawnBuild && SyncBuilds.singleton.SpawnBuild) {
            if (Input.GetKey(KeyCode.W)) {
                gameObject.transform.position += gameObject.transform.forward * ( Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.S)) {
                gameObject.transform.position += gameObject.transform.forward * ( -Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.A)) {
                gameObject.transform.position += gameObject.transform.right * ( -Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.D)) {
                gameObject.transform.position += gameObject.transform.right * ( Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.Space)) {
                gameObject.transform.position += gameObject.transform.up * ( Speed * Time.deltaTime );
            }
            if (Input.GetKey(KeyCode.LeftAlt)) {
                gameObject.transform.position += gameObject.transform.up * ( -Speed * Time.deltaTime );
            }

            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = -Input.GetAxis("Mouse Y");

            rotY += mouseX * SpeedRotate * Time.deltaTime;
            rotX += mouseY * SpeedRotate * Time.deltaTime;
            rotX = Mathf.Clamp(rotX, -90, 90);
            Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
            transform.rotation = localRotation;
            if (Input.GetMouseButtonDown(0)) {
                SyncBuilds.singleton.SpawnBuilding();
            }
            else if (Input.GetMouseButtonDown(1)) {
                SyncBuilds.singleton.NoSpawnBuilding();
            }

        }
    }

    private void LateUpdate() {
        if (Time.timeScale != 0f && ControllerPlayer.MainPlayer.Status == PlayerStatus.SpawnBuild && SyncBuilds.singleton.SpawnBuild) {
            SyncBuilds.singleton.SpawnBuild.BuildObject.transform.position = gameObject.transform.forward * 10 + gameObject.transform.position;
            SyncBuilds.singleton.SpawnBuild.BuildObject.transform.Rotate(new Vector3(0,Input.GetAxis("Mouse ScrollWheel")* 1200 * Time.deltaTime,0));
        }
    }

    private void OnDisable() {
        gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Photon.Pun;
using Photon;
using Photon.Realtime;
using System.Linq;

public static class BuildSpawn {

    public static void CollectBuilding(GameObject BuildPR, GameObject BuidlObj, Vector3[] Positions, Vector3[] Rotations, PointOnDetail[] Points, string[] Cods, SaveEditorBuild.ObjectSave[] Parameters) {
        ParametersBuild parameters = new ParametersBuild();
        parameters.Cods = Cods;
        parameters.Parameters = Parameters;
        parameters.Points = Points;
        parameters.Positions = Positions.Select((x) => x.GetVector3Serializable()).ToArray();
        parameters.Rotarions = Rotations.Select((x) => x.GetVector3Serializable()).ToArray();
        BuildPR.GetComponent<BuildingProperties>().Parameters = parameters;

        List<Vector3> pos = new List<Vector3>();
        Vector3 center = new Vector3();
        for (int i = 0; i < Positions.Length; i++) {
            center += Positions[i];
        }
        center /= Positions.Length;
        for (int i = 0; i < Positions.Length; i++) {
            pos.Add(Positions[i] - center);
        }

        List<GameObject> objs = new List<GameObject>();
        List<GameObject> scalabe_objs = new List<GameObject>();
        List<GameObject> objs_controll = new List<GameObject>();
        List<TankWheel> tank_wheels_update = new List<TankWheel>();

        for (int i = 0; i < Positions.Length; i++) {
            int n;
            GameObject go = Editor_Controller.singleton.FullDatabase.FindGroup(Points[i].Group).ToArray[Points[i].Number];
            GameObject obj = UnityEngine.Object.Instantiate(go, pos[i], Quaternion.Euler(Rotations[i]));

            obj.layer = LayerMask.NameToLayer("BuildSpawn");


            obj.transform.parent = BuidlObj.transform;

            obj.transform.localPosition = pos[i];
            obj.transform.localRotation = Quaternion.Euler(Rotations[i]);

            DetailDate detailDate = obj.GetComponent<DetailDate>();
            detailDate.enabled = false;
            object t = detailDate.DetailComponent;
            detailDate.DetailComponent.Cod = Cods[i];



            Type[] interfaces = t.GetType().GetInterfaces();
            for (int i3 = 0; i3 < interfaces.Length; i3++) {
                PropertyInfo[] properties = interfaces[i3].GetProperties();
                for (int i4 = 0; i4 < properties.Length; i4++) {
                    int number_interfase = ( (NumberInterfaceAttribute) interfaces[i3].GetCustomAttribute(typeof(NumberInterfaceAttribute)) ).Number;
                    int index_interface = Parameters[i].NumbersInterfaces.IndexOf((byte) number_interfase);
                    if (index_interface != -1) {
                        int num_property = ( (NumberPropertyAttribute) properties[i4].GetCustomAttribute(typeof(NumberPropertyAttribute)) ).Number;
                        if (properties[i4].CanWrite) {
                            for (int i5 = 0; i5 < Parameters[i].Save.Count; i5++) {
                                if (Parameters[i].NumbersInterfaces[i5] == number_interfase && Parameters[i].NumbersPropertysfaces[i5] == num_property) {
                                    properties[i4].SetValue(t, Parameters[i].Save[i5]);
                                }
                            }
                        }
                    }
                }
            }



            UnityEngine.Object.Destroy(detailDate.Highlight);

            detailDate.DetailMesh.material = MaterialDataBase.DataBase.FindMaterila(detailDate.DetailComponent.TypeMaterial).Material;
            detailDate.DetailMesh.material.color = ColorSerializable.ToColor(detailDate.DetailComponent.ColorDetail);
            detailDate.enabled = false;
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            obj.GetComponent<Rigidbody>().mass = detailDate.DetailComponent.Weight;
            rb.isKinematic = false;
            rb.useGravity = false;
            rb.constraints = RigidbodyConstraints.FreezeAll;

            obj.AddComponent<DetailBuild>().Building = BuildPR.GetComponent<BuildingProperties>();
            DetailPhysics dp = new DetailPhysics();
            dp.Armor = detailDate.DetailComponent.ArmorDetail;
            dp.Buoyancy = detailDate.DetailComponent.Buoyancy;
            dp.Mass = detailDate.DetailComponent.Weight;
            dp.Health = detailDate.DetailComponent.ArmorDetail;
            dp.BondStrength = dp.Mass * dp.Health * 20f;
            obj.GetComponent<DetailBuild>().Physics = dp;
            obj.GetComponent<DetailBuild>().Cod = Cods[i];




            if (detailDate.Type == DetailDate.TypeDetail.Chair) {
                obj.AddComponent<PlayerChair>().Cod = detailDate.DetailComponent.Cod;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Lamp) {
                obj.AddComponent<Lamp>().Cod = detailDate.DetailComponent.Cod;
                Lamp lamp = obj.GetComponent<Lamp>();
                lamp.Angle = ( (ILamp) detailDate.DetailComponent ).Angle;
                lamp.Intensive = ( (ILamp) detailDate.DetailComponent ).Intensive;
                lamp.Distance = ( (ILamp) detailDate.DetailComponent ).Distance;
                lamp.color =
                    ColorSerializable.ToColor(( (ILamp) detailDate.DetailComponent ).LampColor);
                lamp.GenerateLamp();
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Axis) {
                obj.AddComponent<AxisConnect>().Cod =
                    ( (DetailStandart) detailDate.DetailComponent ).Cod;
                AxisConnect axis = obj.GetComponent<AxisConnect>();
                axis.PosObject = new GameObject("PosAxis");
                axis.PosObject.transform.parent = obj.transform;
                axis.PosObject.transform.localPosition = detailDate.LocalAxisPos;
                axis.PosObject.transform.localRotation = Quaternion.Euler(detailDate.LocalAxisRot);

                axis.Spring = ( (ISpring) detailDate.DetailComponent ).Spring;
                axis.Elasticity = ( (ISpring) detailDate.DetailComponent ).Elasticity;
                axis.Damping = ( (ISpring) detailDate.DetailComponent ).Damping;

                axis.Limit = ( (ILimits) detailDate.DetailComponent ).Active;
                axis.Min = ( (ILimits) detailDate.DetailComponent ).Min;
                axis.Max = ( (ILimits) detailDate.DetailComponent ).Max;
                axis.RotateToTransmission = ( (IRotateAxis) detailDate.DetailComponent ).RotateToTransmission;
                if (!axis.RotateToTransmission && ( (IRotateAxis) detailDate.DetailComponent ).RotationDrivers) {
                    axis.Rotate = ( (IRotateAxis) detailDate.DetailComponent ).RotationDrivers;
                    axis.Speed = ( (IRotateAxis) detailDate.DetailComponent ).SpeedRotate;
                    axis.UseTargetAngle = true;
                }
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Wheel) {
                MeshCollider[] meshs = obj.GetComponents<MeshCollider>();
                obj.GetComponent<SphereCollider>().enabled = false;
                obj.GetComponent<SphereCollider>().isTrigger = false;
                WheelCollision wheel = obj.AddComponent<WheelCollision>();
                wheel.Friction = ( (ITire) detailDate.DetailComponent ).TireResilience * 3.4f;
                wheel.TypeWheel = TypeWheel.CarWheel;
                wheel.Radius = obj.GetComponent<SphereCollider>().radius;

                PhysicMaterial physicMaterial = new PhysicMaterial();
                physicMaterial.dynamicFriction = wheel.Friction;
                physicMaterial.staticFriction = wheel.Friction;
                physicMaterial.bounciness = ( (ITire) detailDate.DetailComponent ).TireResilience / 4f;
                physicMaterial.frictionCombine = PhysicMaterialCombine.Average;
                physicMaterial.bounceCombine = PhysicMaterialCombine.Average;
                physicMaterial.name = "Wheel";


                for (int i2 = 0; i2 < meshs.Length; i2++) {
                    meshs[i2].material = physicMaterial;
                }
                obj.GetComponent<SphereCollider>().material = physicMaterial;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.TankWheel) {
                TankWheel wheel = obj.AddComponent<TankWheel>();
                wheel.Friction = 1;
                wheel.Type = ( (ITankWheel) detailDate.DetailComponent ).Track;
                wheel.TypeWheel = TypeWheel.TankWheel;
                wheel.IsLeader = ( (ITankWheel) detailDate.DetailComponent ).IsLead;
                wheel.Height = ( (ITankWheel) detailDate.DetailComponent ).Height;
                wheel.Widht = ( (ITankWheel) detailDate.DetailComponent ).Widht;
                wheel.Radius = ( (TankWheelData) detailDate.ParameterDetail ).Radius;

                wheel.IsLeadTankWheel = ( (TankWheelData) detailDate.ParameterDetail ).IsLeadTankWheel;

                tank_wheels_update.Add(wheel);




                obj.AddComponent<SphereCollider>().enabled = false;
                obj.GetComponent<SphereCollider>().isTrigger = false;
                PhysicMaterial physicMaterial = new PhysicMaterial();
                physicMaterial.dynamicFriction = wheel.Friction;
                physicMaterial.staticFriction = wheel.Friction;
                physicMaterial.bounciness = wheel.Friction;
                physicMaterial.frictionCombine = PhysicMaterialCombine.Average;
                physicMaterial.bounceCombine = PhysicMaterialCombine.Average;
                physicMaterial.name = "WheelTank";
                obj.GetComponent<SphereCollider>().material = physicMaterial;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Engine) {
                Engine engine = obj.AddComponent<Engine>();
                engine.Cod = detailDate.DetailComponent.Cod;
                engine.Power = ( (EngineData) detailDate.ParameterDetail ).Power;
                engine.minRPM = ( (EngineData) detailDate.ParameterDetail ).minRPM;
                engine.maxRPM = ( (EngineData) detailDate.ParameterDetail ).maxRPM;
                engine.Torque = ( (EngineData) detailDate.ParameterDetail ).Torque;
                engine.AudioEngine = ( (EngineData) detailDate.ParameterDetail ).SoundEngine;
                engine.TypeFuel = ( (EngineData) detailDate.ParameterDetail ).Fuel;
                engine.FuelConsumption = ( (EngineData) detailDate.ParameterDetail ).FuelConsumption;
                engine.EffectObj = ( (EngineData) detailDate.ParameterDetail ).EffectObject;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Gearbox) {
                Gearbox gearbox = obj.AddComponent<Gearbox>();
                gearbox.Cod = detailDate.DetailComponent.Cod;
                GearboxData gearboxData = (GearboxData) detailDate.ParameterDetail;
                DetailDate.All_R_GearboxParams gears = (DetailDate.All_R_GearboxParams) detailDate.DetailComponent;
                gearbox.CountGear = gearboxData.CountGear;
                gearbox.MaxTorque = gearboxData.MaxTorque;
                gearbox.Automatic = gearboxData.Automatic;
                gearbox.Gears[0] = gears.GearR;
                gearbox.Gears[1] = 0f;
                gearbox.Gears[2] = gears.Gear1;
                gearbox.Gears[3] = gears.Gear2;
                gearbox.Gears[4] = gears.Gear3;
                gearbox.Gears[5] = gears.Gear4;
                gearbox.Gears[6] = gears.Gear5;
                gearbox.Gears[7] = gears.Gear6;
                gearbox.Gears[8] = gears.Gear7;
                gearbox.Gears[9] = gears.Gear8;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Differential) {
                Differential differential = obj.AddComponent<Differential>();
                differential.Cod = detailDate.DetailComponent.Cod;
                differential.GearFinal = ( (IDifferentialParam) detailDate.DetailComponent ).GearFinal;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Scalabe) {
                scalabe_objs.Add(obj);
                obj.GetComponent<ScalabeData>().SetScale(Vector3Serializable.ToVector3(( (IScalabe) detailDate.DetailComponent ).Scale));
            }
            else if (detailDate.Type == DetailDate.TypeDetail.FuelTank) {
                FuelTank fuelTank = obj.AddComponent<FuelTank>();
                fuelTank.FuelType = ( (IFuelTank) detailDate.DetailComponent ).FuelType;
                fuelTank.Capacity = ( (IFuelTank) detailDate.DetailComponent ).Capacity;
                fuelTank.Count = fuelTank.Capacity;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Beep) {
                Beep beep = obj.AddComponent<Beep>();
                beep.Sound = ( (BeepData) detailDate.ParameterDetail ).Sound;
                beep.Init();
            }
            else if (detailDate.Type == DetailDate.TypeDetail.ConnectorBase) {
                ConnectorBase connector = obj.AddComponent<ConnectorBase>();
                connector.PosObject = new GameObject("PosConnector");
                connector.PosObject.transform.parent = obj.transform;
                connector.PosObject.transform.localPosition = detailDate.LocalAxisPos;
                connector.PosObject.transform.localRotation = Quaternion.Euler(detailDate.LocalAxisRot);
            }
            else if (detailDate.Type == DetailDate.TypeDetail.ConnectorCon) {
                ConnectConSet connectCon = obj.AddComponent<ConnectConSet>();
                connectCon.PosObject = new GameObject("PosConnector");
                connectCon.PosObject.transform.parent = obj.transform;
                connectCon.PosObject.transform.localPosition = detailDate.LocalAxisPos;
                connectCon.PosObject.transform.localRotation = Quaternion.Euler(detailDate.LocalAxisRot);
            }
            else if (detailDate.Type == DetailDate.TypeDetail.ChipSpeedometer) {
                SensorChip sensor = obj.AddComponent<SensorChip>();
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Extractor) {
                obj.AddComponent<Effector>();
            }
            else if (detailDate.Type == DetailDate.TypeDetail.WheelAxis) {
                WheelAxis axis = obj.AddComponent<WheelAxis>();
                IWheelAxis wheelAxis = (IWheelAxis) detailDate.DetailComponent;
                axis.Damper = wheelAxis.Damper;
                axis.Spring = wheelAxis.Spring;
                axis.RotateSpeed = wheelAxis.SpeedRotate;
                axis.DistanceSuspension = wheelAxis.DistanceSuspension;
                axis.ForceBreak = wheelAxis.ForceBreak;
                axis.SteerAngle = wheelAxis.SteerAngle;
                axis.CenterPos = detailDate.LocalAxisPos;
                axis.Revers = 1;
                axis.TrackSide = ( (ISide) detailDate.DetailComponent ).TrackSide;
                if (wheelAxis.Revers)
                    axis.Revers = -1;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Wing) {
                Wing wing = obj.AddComponent<Wing>();
                wing.Area = ( (WingData) detailDate.ParameterDetail ).Area * ( (IWing) detailDate.DetailComponent ).Intensity;
                wing.WingCurve = ( (WingData) detailDate.ParameterDetail ).Wing;
                wing.LiftMultiplier = 1;
                //      wing.LiftMultiplier = ( (IWing) detailDate.DetailComponent ).Intensity;

            }
            else if (detailDate.Type == DetailDate.TypeDetail.Propeller) {
                Propeller propeller = obj.AddComponent<Propeller>();
                propeller.CountBlade = (int) ( (IPropeller) detailDate.DetailComponent ).CountBlade;
                propeller.Widht = ( (PropellerData) detailDate.ParameterDetail ).Widht;
                propeller.Length = ( (PropellerData) detailDate.ParameterDetail ).Length;
            }
            else if (detailDate.Type == DetailDate.TypeDetail.PropellerAxis) {
                PropellerAxis axis = obj.AddComponent<PropellerAxis>();
            }
            else if (detailDate.Type == DetailDate.TypeDetail.Aileron) {
                Aileron aileron = obj.AddComponent<Aileron>();

                aileron.PosObject = ( (AileronData) detailDate.ParameterDetail ).PosRotate;
                aileron.Angle = ( (AileronData) detailDate.ParameterDetail ).Angle;
                aileron.RotateAxis = ( (AileronData) detailDate.ParameterDetail ).RotateAxis;
                aileron.WingCurve = ( (AileronData) detailDate.ParameterDetail ).Wing;
                aileron.Area = ( (AileronData) detailDate.ParameterDetail ).Area * ( (IAileron) detailDate.DetailComponent ).Intensity;
                aileron.LiftMultiplier = 1;
                aileron.Revers = 1;
                if (( (IAileron) detailDate.DetailComponent ).Revers) {
                    aileron.Revers = -1;
                }

            }




            if (detailDate.DetailComponent.GetType().GetCustomAttribute(typeof(HaveWireConnect)) != null) {
                objs_controll.Add(obj);
            }
            objs.Add(obj);
        }



        BuildingProperties br = BuildPR.GetComponent<BuildingProperties>();
        br.Details = new List<GameObject>(objs);
        br.DetailBuilds = new List<DetailBuild>(objs.Select((x) => x.GetComponent<DetailBuild>()).ToArray());
        for (int i = 0; i < objs_controll.Count; i++) {
            if (objs_controll[i].GetComponent<ISyncParams>() != null) {
                objs_controll[i].GetComponent<ISyncParams>().BaseBuilding = br;
                objs_controll[i].GetComponent<ISyncParams>().Index = br.SyncDetails.Count;
                br.SyncDetails.Add(objs_controll[i].GetComponent<ISyncParams>());
                PunSyncAdapter adapter = br.gameObject.AddComponent<PunSyncAdapter>();
                adapter.DetailSync = objs_controll[i].GetComponent<ISyncParams>();
                br.GetPhotonView().ObservedComponents.Add((Component) adapter);
            }
        }


        //Соединение между катками.
        for (int i = 0; i < tank_wheels_update.Count; i++) {
            DetailDate wheel = tank_wheels_update[i].GetComponent<DetailDate>();
            List<string> cods = new List<string>(( (ITankWheel) wheel.DetailComponent ).CodesTankWheels);
            for (int i2 = 0; i2 < tank_wheels_update.Count; i2++) {
                if (cods.IndexOf(tank_wheels_update[i2].GetComponent<DetailDate>().DetailComponent.Cod) != -1) {
                    tank_wheels_update[i].TankWheels.Add(tank_wheels_update[i2]);
                }
            }
        }//

        //соединение между поинтами
        for (int i = 0; i < objs.Count; i++) {
            Bounds bounds = objs[i].GetComponent<DetailDate>().DetailMesh.bounds;
            bounds.size += Editor_Controller.AddSizeBounds;
            for (int i2 = 0; i2 < objs.Count; i2++) {
                if (objs[i] != objs[i2] && objs[i2].GetComponent<DetailDate>().DetailMesh.bounds.Intersects(bounds)) {
                    DetailDate one = objs[i].GetComponent<DetailDate>();
                    DetailDate two = objs[i2].GetComponent<DetailDate>();
                    for (int i3 = 0; i3 < one.Date.Length; i3++) {
                        for (int i4 = 0; i4 < two.Date.Length; i4++) {
                            bool on = false;
                            if (Bracing.CheckContactBracing(one.Date[i3].Name, two.Date[i4].Name)) {
                                on = true;
                            }
                            if (on) {
                                for (int i5 = 0; i5 < one.Date[i3].Bracing.Length; i5++) {
                                    for (int i6 = 0; i6 < two.Date[i4].Bracing.Length; i6++) {
                                        float ds = Vector3.Distance(one.Date[i3].Bracing[i5].transform.position, two.Date[i4].Bracing[i6].transform.position);
                                        if (ds < Editor_Controller.MaxDistBracing) {
                                            if (one.Date[i3].Name == "convex_weld" && two.Date[i4].Name == "internal_weld") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                            else if (one.Date[i3].Name == "internal_weld" && two.Date[i4].Name == "convex_weld") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                            else if (one.Date[i3].Name == "scalabe_bracing" && two.Date[i4].Name == "convex_weld") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                            else if (one.Date[i3].Name == "scalabe_bracing" && two.Date[i4].Name == "internal_weld") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                            else if (one.Date[i3].Name == "convex_weld" && two.Date[i4].Name == "scalabe_bracing") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                            else if (one.Date[i3].Name == "internal_weld" && two.Date[i4].Name == "scalabe_bracing") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                            else if (one.Date[i3].Name == "scalabe_bracing" && two.Date[i4].Name == "scalabe_bracing") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                            else if (( one.Date[i3].Name == "base_down_axis" && two.Date[i4].Name == "base_top_axis" )
                                                | ( one.Date[i3].Name == "axis_nano_base" && two.Date[i4].Name == "axis_nano_top" )
                                                | ( one.Date[i3].Name == "base_down_axis_m" && two.Date[i4].Name == "base_top_axis_m" )
                                                | ( one.Date[i3].Name == "base_nano_b_axis" && two.Date[i4].Name == "top_nano_b_axis" )) {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    PhysicsConnect connects = new PhysicsConnect();
                                                    connects.BaseDetail = objs[i];
                                                    connects.ConnectDetail = objs[i2];
                                                    connects.Type = TypePhysicsConnect.Axis;
                                                    if (objs[i].GetComponent<DetailBuild>().PhysicsConnects.IndexOf(connects) == -1) {
                                                        PhysicsConnect connect = new PhysicsConnect();
                                                        connect.BaseDetail = objs[i];
                                                        connect.ConnectDetail = objs[i2];
                                                        connect.Type = TypePhysicsConnect.Axis;
                                                        objs[i].GetComponent<DetailBuild>().PhysicsConnects.Add(connect);
                                                    }
                                                }
                                            }
                                            else if (one.Date[i3].Name == "axle_weld_m" && two.Date[i4].Name == "wheel_axle_m") {
                                                objs[i].GetComponent<WheelAxis>().SetWheel(objs[i2].GetComponent<WheelCollision>());
                                            }
                                            else if (one.Date[i3].Name == "connector_base" && two.Date[i4].Name == "connector_con") {
                                                PhysicsConnect connects = new PhysicsConnect();
                                                connects.BaseDetail = objs[i];
                                                connects.ConnectDetail = objs[i2];
                                                connects.Type = TypePhysicsConnect.Connect;
                                                if (objs[i].GetComponent<DetailBuild>().PhysicsConnects.IndexOf(connects) == -1) {
                                                    PhysicsConnect connect = new PhysicsConnect();
                                                    connect.BaseDetail = objs[i];
                                                    connect.ConnectDetail = objs[i2];
                                                    connect.Type = TypePhysicsConnect.Connect;
                                                    objs[i].GetComponent<DetailBuild>().PhysicsConnects.Add(connect);
                                                }
                                            }
                                            else if (one.Date[i3].Name == "propeller_connect_base" && two.Date[i4].Name == "propeller_connect_con") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                    objs[i].GetComponent<PropellerAxis>().SetPropeller(objs[i2].GetComponent<Propeller>());
                                                }
                                            }
                                            else if (one.Date[i3].Name == "propeller_connect_con" && two.Date[i4].Name == "propeller_connect_base") {
                                                if (objs[i].GetComponent<DetailBuild>().Connect.IndexOf(objs[i2]) == -1) {
                                                    objs[i].GetComponent<DetailBuild>().Connect.Add(objs[i2]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        List<MethodInfo> addMetods = new List<MethodInfo>();
        List<object> addDetails = new List<object>();
        for (int i = 0; i < objs_controll.Count; i++) {
            Type type = objs_controll[i].GetComponent<DetailDate>().DetailComponent.GetType();
            if (type.GetCustomAttribute(typeof(HaveWireConnect)) != null) {
                Type[] interfaces = type.GetInterfaces();
                for (int i2 = 0; i2 < interfaces.Length; i2++) {
                    for (int i3 = 0; i3 < interfaces[i2].GetProperties().Length; i3++) {
                        PropertyInfo propertie = interfaces[i2].GetProperties()[i3];
                        if (propertie.GetCustomAttribute(typeof(InputAttribute)) != null) {
                            if (( (WireConnect) propertie.GetValue(objs_controll[i].GetComponent<DetailDate>().DetailComponent) ).CodConnectDetail != null) {
                                string cod_detail_connect = ( (WireConnect) propertie.GetValue(objs_controll[i].GetComponent<DetailDate>().DetailComponent) ).CodConnectDetail;
                                string name_function = ( (OutputAttribute) ( (WireConnect) propertie.GetValue(objs_controll[i].GetComponent<DetailDate>().DetailComponent) ).ConnectParameter.GetCustomAttribute(typeof(OutputAttribute)) ).name_function;
                                string name_event = ( (OutputAttribute) ( (WireConnect) propertie.GetValue(objs_controll[i].GetComponent<DetailDate>().DetailComponent) ).ConnectParameter.GetCustomAttribute(typeof(OutputAttribute)) ).name_event;
                                for (int i4 = 0; i4 < objs_controll.Count; i4++) {
                                    if (objs_controll[i].GetComponent<DetailDate>().DetailComponent.GetType().GetCustomAttribute(typeof(HaveWireConnect)) != null) {
                                        if (objs_controll[i4].GetComponent<DetailDate>().DetailComponent.Cod == cod_detail_connect) {
                                            for (int i5 = 0; i5 < objs_controll[i4].GetComponents<IDetailControll>().Length; i5++) {
                                                Type type_component_detail = objs_controll[i4].GetComponents<IDetailControll>()[i5].GetType();
                                                if (type_component_detail.GetMethod(name_function) != null) {
                                                    Delegate @delegate = ( Delegate.CreateDelegate(typeof(FreedbackWire.LoadMetods), objs_controll[i4].GetComponents<IDetailControll>()[i5], type_component_detail.GetMethod(name_function).Name) );

                                                    if (addMetods.IndexOf(@delegate.Method) == -1 || addDetails.IndexOf(objs_controll[i4].GetComponents<IDetailControll>()[i5]) == -1) {
                                                        objs_controll[i4].GetComponents<IDetailControll>()[i5].UpdateFunctions += (FreedbackWire.LoadMetods) @delegate;
                                                        addMetods.Add(@delegate.Method);
                                                        addDetails.Add(objs_controll[i4].GetComponents<IDetailControll>()[i5]);
                                                    }
                                                    for (int i6 = 0; i6 < objs_controll[i].GetComponents<IDetailControll>().Length; i6++) {
                                                        if (objs_controll[i].GetComponents<IDetailControll>()[i6].GetType().GetProperty(( (InputAttribute) propertie.GetCustomAttribute(typeof(InputAttribute)) ).NameParameter) != null) {
                                                            for (int i7 = 0; i7 < objs_controll[i].GetComponents<IDetailControll>().Length; i7++) {
                                                                FieldInfo fieldInfo = objs_controll[i4].GetComponents<IDetailControll>()[i5].GetType().GetField(name_event);
                                                                PropertyInfo propertyInfo = objs_controll[i].GetComponents<IDetailControll>()[i7].GetType().GetProperty(( (InputAttribute) propertie.GetCustomAttribute(typeof(InputAttribute)) ).NameParameter);
                                                                if (fieldInfo != null && propertyInfo != null) {
                                                                    FreedbackWire freedback = ( (FreedbackWire) fieldInfo.GetValue(objs_controll[i4].GetComponents<IDetailControll>()[i5]) );
                                                                    freedback.PropertyInfos.Add(propertyInfo);
                                                                    freedback.Objects.Add(objs_controll[i].GetComponents<IDetailControll>()[i7]);
                                                                    fieldInfo.SetValue(objs_controll[i4].GetComponents<IDetailControll>()[i5], freedback);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        BuildPR.GetComponent<BuildingProperties>().AssemblyAtStart();
        for (int i = 0; i < objs.Count; i++) {
            objs[i].GetComponent<DetailBuild>().LoadDetailControll();
            UnityEngine.Object.Destroy(objs[i].GetComponent<DetailDate>().ParameterDetail);
            UnityEngine.Object.Destroy(objs[i].GetComponent<DetailDate>());
            UnityEngine.Object.Destroy(objs[i].GetComponent<Rigidbody>());
        }

    }

    public static void CollectBuilding(GameObject BuildPR, GameObject BuildObj, DetailDate[] Details) {

        SaveEditorBuild.ObjectSave[] param = new SaveEditorBuild.ObjectSave[Details.Length];
        PointOnDetail[] points = new PointOnDetail[Details.Length];

        for (int i = 0; i < Details.Length; i++) {
            DetailStandart t = Details[i].DetailComponent;
            points[i] = new PointOnDetail();
            points[i].Group = Details[i].Group;
            int number = 0;
            Editor_Controller.singleton.FullDatabase.FindGroup(Details[i].Group).Find(Details[i].CodeDetail, out number);
            points[i].Number = number;
            param[i] = new SaveEditorBuild.ObjectSave();


            Type[] interafaces = t.GetType().GetInterfaces();
            for (int i2 = 0; i2 < interafaces.Length; i2++) {
                PropertyInfo[] properties = interafaces[i2].GetProperties();
                for (int i3 = 0; i3 < properties.Length; i3++) {
                    if (properties[i3].CanWrite && properties[i3].GetCustomAttribute(typeof(NoSaveAttribute)) == null) {
                        param[i].NumbersInterfaces.Add(( (NumberInterfaceAttribute) interafaces[i2].GetCustomAttribute(typeof(NumberInterfaceAttribute)) ).Number);
                        param[i].NumbersPropertysfaces.Add(( (NumberPropertyAttribute) properties[i3].GetCustomAttribute(typeof(NumberPropertyAttribute)) ).Number);
                        param[i].Save.Add(properties[i3].GetValue(t));
                    }
                }
            }
        }

        CollectBuilding(BuildPR, BuildObj, Details.Select((x) => x.transform.position).ToArray(), Details.Select((x) => x.transform.eulerAngles).ToArray(), points, Details.Select((x) => x.DetailComponent.Cod).ToArray(), param);

    }

    public static void UnFreezeBuilding(GameObject BuildObject) {
        BuildingProperties br = BuildObject.GetComponent<BuildingProperties>();
        for (int i = 0; i < br.Details.Count; i++) {
            br.Details[i].layer = LayerMask.NameToLayer("Detail");
        }
        for (int i = 0; i < br.PhysicsBases.Count; i++) {
            if (br.PhysicsBases[i].GetComponent<Rigidbody>())
                br.PhysicsBases[i].GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}

public class FreedbackWire {
    public List<object> Objects = new List<object>();
    public List<PropertyInfo> PropertyInfos = new List<PropertyInfo>();

    public delegate void LoadMetods();

    public static void UpdateWireConnect(FreedbackWire freedbackWire, object value) {
        for (int i = 0; i < freedbackWire.Objects.Count; i++) {
            if (freedbackWire.Objects[i] != null)
                freedbackWire.PropertyInfos[i].SetValue(freedbackWire.Objects[i], value);
        }
    }

    public static List<object> TakeWireConnect(FreedbackWire freedback) {
        List<object> objs = new List<object>();
        for (int i = 0; i < freedback.Objects.Count; i++) {
            if (freedback.Objects[i] != null) {
                objs.Add(freedback.PropertyInfos[i].GetValue(freedback.Objects[i]));
            }
        }
        return objs;
    }
}
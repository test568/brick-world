﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InEditor: MonoBehaviour {
    public GameObject IconObject;
    public SpriteRenderer Icon;

    [Range(0, 20)]
    public float StartShowDistance;
    [Range(0, 20)]
    public float MaxDistanceShow;
    [Range(0, 20)]
    public float MinDistanceShow;

    private GameObject obj;
    private LayerMask layer;
    private void Awake() {
        layer=LayerMask.NameToLayer("Interaction");
        obj=gameObject;
        obj.layer=layer;
        }
    private void Update() {
        if (Vector3.Distance(obj.transform.position, ControllerPlayer.MainPlayer.PlayerObject.transform.position)<StartShowDistance&&ControllerPlayer.MainPlayer.PlayerObject.activeInHierarchy) {
            IconObject.transform.rotation=Quaternion.LookRotation(ControllerPlayer.MainPlayer.PlayerObject.transform.position-obj.transform.position, Vector3.up);
            if (Vector3.Distance(obj.transform.position, ControllerPlayer.MainPlayer.PlayerObject.transform.position)<MaxDistanceShow) {
                float alpha = ( 1/( MaxDistanceShow-MinDistanceShow ) )*( ( MinDistanceShow-1 )-Vector3.Distance(obj.transform.position, ControllerPlayer.MainPlayer.PlayerObject.transform.position) );
                Icon.color=new Color(1, 1, 1, alpha);
                }
            else {
                Icon.color=new Color(1, 1, 1, 0);
                }
            }
        else {
            Icon.color=new Color(1, 1, 1, 0);
            }
        }
    public void LoadEditor() {
        ControllerPlayer.MainPlayer.Status=PlayerStatus.inEdtior;
        }
    }

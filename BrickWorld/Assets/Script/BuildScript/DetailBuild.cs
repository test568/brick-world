﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DetailBuild : MonoBehaviour {

    public bool DetailConnect {
        get {
            return _detail_connect;
        }
        set {
            if (_detail_connect != value && value == false) {
                Disconnect();
                _detail_connect = false;
            }
        }
    }

    bool _detail_connect = true;

    public bool DetailInBase;
    public bool DetailTakeDamag {
        get {
            return take_damag;
        }
        set {
            take_damag = value;
        }
    }

    bool take_damag;

    public string Cod;

    public List<GameObject> Connect = new List<GameObject>();
    public List<PhysicsConnect> PhysicsConnects = new List<PhysicsConnect>();

    public BuildingProperties Building;

    private BaseInBuild _base;
    public DetailPhysics Physics;

    public PointOnDetail Point;

    public BaseInBuild Base {
        get {
            return _base;
        }
        set {
            if (detail_controls == null) {
                detail_controls = GetComponents<IDetailControll>();
            }
            for (int i = 0; i < detail_controls.Length; i++) {
                detail_controls[i].UpdateThisBase(value);
            }
            _base = value;
        }
    }

    public bool IsDestroy = false;

    private IDetailControll[] detail_controls;
    public void LoadDetailControll() {
        detail_controls = GetComponents<IDetailControll>();
    }

    public List<GameObject> UpdateBase() {
        if (!DetailInBase && DetailConnect) {
            List<GameObject> gms = new List<GameObject>();
            DetailInBase = true;
            gms.Add(gameObject);
            for (int i = 0; i < Connect.Count; i++) {
                gms.AddRange(Connect[i].GetComponent<DetailBuild>().UpdateBase());
            }
            return gms;
        }
        return new List<GameObject>();
    }

    public GameObject[] UpdateDamag(float Force, GameObject gm) {
        if (!IsDestroy && !DetailTakeDamag && Force > 1f) {
            if (Physics.BondStrength > Force) {
                DetailTakeDamag = true;
                Physics.BondStrength -= Force;
                return new GameObject[0];
            }
            else if (Physics.BondStrength == Force && !DetailTakeDamag) {
                DetailTakeDamag = true;
                Physics.BondStrength = 0;
                DetailConnect = false;
                return new GameObject[1] { gameObject };
            }
            else if (!DetailTakeDamag) {
                DetailTakeDamag = true;
                Force -= Physics.BondStrength;
                // float force = Force * 0.7f / ( Connect.Count + PhysicsConnects.Count );
                float force = Force * 0.7f;
                List<GameObject> gms = new List<GameObject>();
                for (int i = 0; i < Connect.Count; i++) {
                    if (Connect[i] != gameObject && Connect[i] != gm && !Connect[i].GetComponent<DetailBuild>().IsDestroy) {
                        gms.AddRange(Connect[i].GetComponent<DetailBuild>().UpdateDamag(force, gameObject));
                    }
                }
                for (int i = 0; i < PhysicsConnects.Count; i++) {
                    if (PhysicsConnects[i].ConnectDetail != gm && !PhysicsConnects[i].ConnectDetail.GetComponent<DetailBuild>().IsDestroy) {
                        gms.AddRange(PhysicsConnects[i].ConnectDetail.GetComponent<DetailBuild>().UpdateDamag(force, gameObject));
                    }
                }
                gms.Add(gameObject);
                DetailConnect = false;
                return gms.ToArray();
            }
            else {
                DetailTakeDamag = true;
                return new GameObject[0];
            }
        }
        else {
            DetailTakeDamag = true;
            return new GameObject[0];
        }
    }

    private void Disconnect() {
        if (gameObject.GetComponent<Rigidbody>()) {
            Destroy(gameObject.GetComponent<Rigidbody>());
        }

        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.mass = Physics.Mass;

        for (int i = 0; i < detail_controls.Length; i++) {
            detail_controls[i].Disconnect = true;
        }
        for (int i = 0; i < Connect.Count; i++) {
            Connect[i].gameObject.GetComponent<DetailBuild>().Connect.Remove(gameObject);
        }
        Connect.Clear();
        PhysicsConnects.Clear();
        IsDestroy = true;
        gameObject.layer = LayerMask.NameToLayer("DestroyDetail");
        Building.SetDetailNoWork(gameObject);

        if (Base != null)
            Base.Details.Remove(this);
        transform.parent = Building.BuildObject.transform;


    }
}


public struct PhysicsConnect {
    public GameObject BaseDetail;
    public GameObject ConnectDetail;
    public Transform GroupPositionSet;
    public TypePhysicsConnect Type;
}

[System.Serializable]
public struct DetailPhysics {
    public float Mass;
    public float Buoyancy;
    public float Health;
    public float Armor;
    public float BondStrength;
}

[System.Serializable]
public enum TypePhysicsConnect {
    Axis,
    Connect,
}
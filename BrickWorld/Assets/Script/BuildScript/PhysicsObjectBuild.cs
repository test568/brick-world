﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PhysicsObjectBuild : MonoBehaviour {
    public BuildingProperties Build;
    public BaseInBuild Base;

    public int Number;

    public bool EnbaleSync { get; private set; }

    private Vector3 new_pos;

    private Vector3 new_rot;

    private float dist;

    private float ang;

    private float old_time_damag;

    private void Awake() {
        EnbaleSync = true;
    }

    public void UpdateNetworkPos(Vector3 pos, Vector3 rot) {
        new_pos = pos;
        new_rot = rot;
        dist = Vector3.Distance(new_pos, transform.position);
        ang = Quaternion.Angle(transform.rotation, Quaternion.Euler(new_rot));
    }

    public void DisableSync() {
        EnbaleSync = false;
        if (Base != null)
            Base.rigidbody.isKinematic = true;
        else
            GetComponent<Rigidbody>().isKinematic = true;
    }

    public void EnableSync() {
        EnbaleSync = true;
        if (Base != null)
            Base.rigidbody.isKinematic = false;
        else
            GetComponent<Rigidbody>().isKinematic = false;
    }

    private void FixedUpdate() {
        if (EnbaleSync) {
            transform.position = Vector3.MoveTowards(transform.position, new_pos, dist * ( 1.8f / PhotonNetwork.SerializationRate ));
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new_rot), ang * ( 1.8f / PhotonNetwork.SerializationRate ));
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.impulse.magnitude > 60f && Time.realtimeSinceStartup - old_time_damag > 1f) {
            old_time_damag = Time.realtimeSinceStartup;
            Build.TakeCollisionEnter(collision);
        }
    }
}

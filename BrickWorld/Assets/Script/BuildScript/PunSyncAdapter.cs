﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Sirenix.OdinInspector;

public class PunSyncAdapter : MonoBehaviour, IPunObservable {
    [ShowInInspector]
    public ISyncParams DetailSync { get; set; }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (DetailSync != null) {
            DetailSync.ObservableSync(stream, info);
        }
    }
}

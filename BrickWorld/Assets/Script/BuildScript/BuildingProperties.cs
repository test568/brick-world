﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;
using System;
using Sirenix.OdinInspector;

public class BuildingProperties : MonoBehaviour, IGetPhotonView, IPunObservable {
    public string Cod;

    public int NumberPlayer;
    public GameObject BuildObject;
    public List<GameObject> Details = new List<GameObject>();
    public List<DetailBuild> DetailBuilds = new List<DetailBuild>();


    public List<GameObject> PhysicsBases = new List<GameObject>();
    public List<BaseInBuild> BaseInBuilds = new List<BaseInBuild>();
    public List<JointBuild> Joints = new List<JointBuild>();

    public List<ISyncParams> SyncDetails = new List<ISyncParams>();

    List<GameObject> work_details = new List<GameObject>();

    [ShowInInspector]
    public ParametersBuild Parameters {
        get { return _params; }
        set {
            if (_params.Cods == null) _params = value;
        }
    }

    ParametersBuild _params;

    public bool IsMainBuilding {
        get {
            return ( !GameRoom.isMultiplayer || photonView.IsMine );
        }
    }

    public ParametersGroup GroupsParameters { get; private set; }

    [SerializeField] PhotonView photonView;

    private PhysicsObjectBuild[] physics_object_builds = new PhysicsObjectBuild[0];

    bool no_rebuild;

    bool update_new_copy = false;

    bool is_start_load_physics = false;

    int add_bases;

    ushort[] update_client_damag = new ushort[0];

    private void Awake() {
        if (GameRoom.isMultiplayer) {
            photonView.ObservedComponents[0] = this;
        }
        BuildObject = new GameObject("BuildObject");
    }

    private void OnDestroy() {
        Destroy(BuildObject);
        SyncBuilds.singleton.Builds.Remove(this);
    }

    private void Start() {
        SyncBuilds.singleton.Builds.Add(this);
        if (!IsMainBuilding) {
            photonView.RpcSecure("RPCCreateCopy", photonView.Controller, false, new object[1] { PhotonNetwork.LocalPlayer });
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {//Синхронизация позиций групп
        if (stream.IsWriting) {
            stream.SendNext(update_client_damag);
            if (update_client_damag.Length != 0) {
                Array.Resize(ref update_client_damag, 0);
            }
            stream.SendNext(BaseInBuilds.Count);
            for (int i = 0; i < BaseInBuilds.Count; i++) {
                stream.SendNext(BaseInBuilds[i].NumberGroup);
                stream.SendNext(BaseInBuilds[i].gameObject.transform.position);
                stream.SendNext(BaseInBuilds[i].gameObject.transform.eulerAngles);
            }
        }
        else {
            ushort[] bytes = (ushort[]) stream.ReceiveNext();
            if (bytes.Length != 0) {
                UpdatePhysicsDamag(bytes);
            }
            int count = (int) stream.ReceiveNext();
            for (int i = 0; i < count; i++) {
                int num = (int) stream.ReceiveNext();
                Vector3 pos = (Vector3) stream.ReceiveNext();
                Vector3 rot = (Vector3) stream.ReceiveNext();
                for (int i2 = 0; i2 < physics_object_builds.Length; i2++) {
                    if (physics_object_builds[i2].Number == num) {
                        physics_object_builds[i2].UpdateNetworkPos(pos, rot);
                        break;
                    }
                }
            }
        }
    }

    #region Client


    [PunRPC]
    private void RPCInitNewCopy(ParametersBuild packet, string cod, int number_actors) {
        Cod = cod;
        NumberPlayer = number_actors;
        gameObject.name = "Building: " + cod;
        no_rebuild = true;
        BuildSpawn.CollectBuilding(gameObject, BuildObject, packet.Positions.Select((x) => Vector3Serializable.ToVector3(x)).ToArray(),
            packet.Rotarions.Select((x) => Vector3Serializable.ToVector3(x)).ToArray(), packet.Points, packet.Cods, packet.Parameters);
        BuildSpawn.UnFreezeBuilding(this.gameObject);
        if (SyncBuilds.singleton.SpawnBuild == null) {
            no_rebuild = false;
        }
    }


    [PunRPC]
    private void RPCSynhDetailNewCopy(int[] indexes, ArrayObjects[] objects) {

        if (!update_new_copy) {
            StartCoroutine(IsUpdateCopyAll(indexes, objects));
            return;
        }
        for (int i = 0; i < indexes.Length; i++) {
            SyncDetails[indexes[i]].AcceptStartParameters(objects[i].objects);
        }
    }

    [PunRPC]
    private void RPCLoadPhysicsAssembly(ParametersGroup packet) {
        if (!is_start_load_physics) {
            StartCoroutine(IsRebuildAll(packet));
            return;
        }
        update_new_copy = true;
        List<PhysicsObjectBuild> physics = new List<PhysicsObjectBuild>(physics_object_builds);
        for (int i = 0; i < packet.CodeDetails.Length; i++) {
            for (int i2 = i; i2 < DetailBuilds.Count; i2++) {
                if (packet.CodeDetails[i] == DetailBuilds[i2].Cod) {
                    if (packet.VelocytiDestroy[i] != new Vector3Serializable(-1, -1, -1)) {
                        DetailBuilds[i2].DetailConnect = false;
                        DetailBuilds[i2].transform.position = Vector3Serializable.ToVector3(packet.Positions[i]);
                        DetailBuilds[i2].transform.eulerAngles = Vector3Serializable.ToVector3(packet.Rotations[i]);
                        DetailBuilds[i2].GetComponent<Rigidbody>().velocity = Vector3Serializable.ToVector3(packet.VelocytiDestroy[i]);
                        DetailBuilds[i2].gameObject.layer = LayerMask.NameToLayer("DestroyDetail");
                    }
                    else {
                        PhysicsObjectBuild b = null;
                        for (int i3 = 0; i3 < physics.Count; i3++) {
                            if (physics[i3].Number == packet.NumberGroup[i]) {
                                b = physics[i3];
                                break;
                            }

                        }
                        if (b == null) {
                            b = new GameObject("Physics").AddComponent<PhysicsObjectBuild>();
                            b.transform.parent = BuildObject.transform;
                            b.transform.localPosition = Vector3.zero;
                            b.Number = packet.NumberGroup[i];
                            b.Build = this;
                            b.gameObject.AddComponent<Rigidbody>().isKinematic = true;
                            physics.Add(b);
                        }
                        DetailBuilds[i2].transform.parent = b.transform;
                        DetailBuilds[i2].transform.localPosition = Vector3Serializable.ToVector3(packet.Positions[i]);
                        DetailBuilds[i2].transform.localEulerAngles = Vector3Serializable.ToVector3(packet.Rotations[i]);
                    }

                    break;
                }
            }
        }
        physics_object_builds = physics.ToArray();
    }

    private void UpdatePhysicsDamag(ushort[] num_groups) {
        if (num_groups.Length > DetailBuilds.Count)
            return;
        List<PhysicsObjectBuild> physics = new List<PhysicsObjectBuild>(physics_object_builds);
        List<PhysicsObjectBuild> check_groups_disconnect_detail = new List<PhysicsObjectBuild>();
        List<GameObject> update_groups = new List<GameObject>();

        for (int i = 0; i < num_groups.Length; i++) {
            if (num_groups[i] == ushort.MaxValue) {
                PhysicsObjectBuild b = DetailBuilds[i].transform.parent.GetComponent<PhysicsObjectBuild>();
                if (b != null && check_groups_disconnect_detail.IndexOf(b) == -1)
                    check_groups_disconnect_detail.Add(b);
                DetailBuilds[i].transform.parent = BuildObject.transform;
                if (DetailBuilds[i].GetComponent<Rigidbody>()) {
                    DetailBuilds[i].gameObject.layer = LayerMask.NameToLayer("DestroyDetail");
                }
                else {
                    DetailBuilds[i].DetailConnect = false;
                    DetailBuilds[i].gameObject.layer = LayerMask.NameToLayer("DestroyDetail");
                }
            }
            else {
                PhysicsObjectBuild b = null;
                for (int i2 = 0; i2 < physics.Count; i2++) {
                    if (physics[i2].Number == (int) num_groups[i]) {
                        b = physics[i2];
                    }
                }
                if (b == null) {
                    b = new GameObject("Physics").AddComponent<PhysicsObjectBuild>();
                    b.transform.parent = BuildObject.transform;
                    b.transform.position = DetailBuilds[i].transform.position;
                    Rigidbody r = b.gameObject.AddComponent<Rigidbody>();
                    r.isKinematic = true;
                    b.Number = num_groups[i];
                    b.Build = this;
                    DetailBuilds[i].transform.parent = b.transform;
                    physics.Add(b);
                    update_groups.Add(b.gameObject);
                }
                else if (b != null) {
                    DetailBuilds[i].transform.parent = b.transform;
                    if (update_groups.IndexOf(b.gameObject) == -1) {
                        update_groups.Add(b.gameObject);
                    }
                }
            }
        }

        for (int i = 0; i < check_groups_disconnect_detail.Count; i++) {
            if (check_groups_disconnect_detail[i].transform && check_groups_disconnect_detail[i].transform.childCount == 0) {
                physics.Remove(check_groups_disconnect_detail[i]);
                Destroy(check_groups_disconnect_detail[i].gameObject);
            }
        }
        physics_object_builds = physics.ToArray();
    }

    IEnumerator IsRebuildAll(ParametersGroup packet) {
        while (true) {
            if (!no_rebuild) {
                yield return null;
                is_start_load_physics = true;
                RPCLoadPhysicsAssembly(packet);
                update_new_copy = true;
                break;
            }
            yield return null;
        }
        yield break;
    }

    IEnumerator IsUpdateCopyAll(int[] indexes, ArrayObjects[] objects) {
        while (true) {
            if (update_new_copy) {
                RPCSynhDetailNewCopy(indexes, objects);
                break;
            }
            yield return null;
        }
        yield break;
    }

    public void SyncParamsDetail(ISyncParams detail, int value_index, object value) {
        if (GameRoom.isMultiplayer && !no_rebuild) {
            photonView.RpcSecure("RPCSyncDetail", RpcTarget.Others, false, detail.Index, value_index, value);
        }
    }

    [PunRPC]
    private void RPCSyncDetail(int index, int value_index, object value) {
        if (index < SyncDetails.Count) {
            SyncDetails[index].UpdateParams(value_index, value);
        }
    }

    #endregion

    #region OwnerBuild

    [PunRPC]
    private void RPCCreateCopy(Player newPlayer) {
        StartCoroutine(IsLoadAll(newPlayer));
    }

    IEnumerator IsLoadAll(Player newPlayer) {
        while (true) {
            if (Parameters.Cods != null) {
                photonView.RpcSecure("RPCInitNewCopy", newPlayer, false, new object[3] { Parameters, Cod, PhotonNetwork.LocalPlayer.ActorNumber });
                photonView.RpcSecure("RPCLoadPhysicsAssembly", newPlayer, false, new object[1] { GroupsParameters });
                ArrayObjects[] objects = new ArrayObjects[SyncDetails.Count];
                for (int i = 0; i < objects.Length; i++) {
                    objects[i] = new ArrayObjects();
                    objects[i].objects = SyncDetails[i].SendUploaded();
                }
                photonView.RpcSecure("RPCSynhDetailNewCopy", newPlayer, false, SyncDetails.Select((x) => x.Index).ToArray(), objects);
                break;
            }
            yield return null;
        }
        yield break;
    }

    public void AssemblyAtStart() {//Стартовое построение обьекта
        if (IsMainBuilding) {
            work_details.AddRange(Details.ToArray());
            for (int i = 0; i < Details.Count; i++) {
                Collider[] colliders = Details[i].GetComponents<Collider>();
                for (int i2 = 0; i2 < colliders.Length; i2++) {
                    colliders[i2].isTrigger = false;
                }

                if (!Details[i].GetComponent<DetailBuild>().DetailInBase) {
                    PhysicsBases.Add(new GameObject("PhysicsBases " + PhysicsBases.Count));
                    BaseInBuild bs = new BaseInBuild();
                    bs.gameObject = PhysicsBases[PhysicsBases.Count - 1];
                    bs.gameObject.transform.position = Details[i].transform.position;
                    bs.gameObject.transform.rotation = Details[i].transform.rotation;
                    bs.gameObject.AddComponent<PhysicsObjectBuild>().Build = this;
                    bs.gameObject.GetComponent<PhysicsObjectBuild>().Base = bs;
                    bs.rigidbody = bs.gameObject.AddComponent<Rigidbody>();
                    bs.rigidbody.isKinematic = true;
                    bs.Base = this;
                    bs.NumberGroup = BaseInBuilds.Count;
                    add_bases = BaseInBuilds.Count;
                    BaseInBuilds.Add(bs);

                    GameObject[] gms = Details[i].GetComponent<DetailBuild>().UpdateBase().ToArray();

                    for (int i2 = 0; i2 < gms.Length; i2++) {

                        DetailBuild d = gms[i2].GetComponent<DetailBuild>();
                        d.DetailInBase = true;
                        d.transform.parent = bs.gameObject.transform;
                        bs.Details.Add(d);
                        d.Base = bs;
                    }
                    PhysicsBases[PhysicsBases.Count - 1].transform.parent = BuildObject.transform;
                    bs.UpdateRigidbody();
                }

            }
            for (int i = 0; i < Details.Count; i++) {
                if (Details[i].GetComponent<DetailBuild>().PhysicsConnects.Count != 0) {
                    for (int i2 = 0; i2 < Details[i].GetComponent<DetailBuild>().PhysicsConnects.Count; i2++) {
                        if (Details[i].GetComponent<DetailBuild>().Base.gameObject != Details[i].GetComponent<DetailBuild>().PhysicsConnects[i2].ConnectDetail.GetComponent<DetailBuild>().Base.gameObject) {
                            JointBuild jb = new JointBuild();
                            jb.XCod = UnityEngine.Random.Range(0, 99999999) + "";
                            PhysicsConnect pc = Details[i].GetComponent<DetailBuild>().PhysicsConnects[i2];
                            jb.BaseDetail = pc.BaseDetail.GetComponent<DetailBuild>();
                            jb.ConnectDetail = pc.ConnectDetail.GetComponent<DetailBuild>();
                            jb.Type = pc.Type;
                            jb.Create();
                            Joints.Add(jb);
                        }
                    }
                }
            }

            if (GameRoom.isMultiplayer) {
                UpdateGroupsParameters();
            }
        }
    }


    public void UpdatePhysicsBaces(BaseInBuild PhysicsBase, DetailBuild Detail) {//Обновление построение при уроне       
        if (IsMainBuilding) {
            for (int i = 0; i < Detail.Connect.Count; i++) {
                Detail.Connect[i].GetComponent<DetailBuild>().Connect.Remove(Detail.gameObject);
            }
            SetDetailNoWork(Detail.gameObject);
            PhysicsBase.Details.Remove(Detail);

            if (PhysicsBase.Details.Count == 0) {
                PhysicsBases.Remove(PhysicsBase.gameObject);
                BaseInBuilds.Remove(BaseInBuilds.Where((x) => x.NumberGroup == PhysicsBase.NumberGroup).FirstOrDefault());
                Destroy(PhysicsBase.gameObject);
                return;
            }
            else {
                for (int i = 0; i < PhysicsBase.Details.Count; i++) {
                    PhysicsBase.Details[i].DetailInBase = false;
                }

                List<DetailBuild> list = new List<DetailBuild>(PhysicsBase.Details);
                PhysicsBase.Details = new List<DetailBuild>();
                for (int i = 0; i < list.Count; i++) {
                    if (!list[i].DetailInBase && list[i].DetailConnect) {
                        if (i == 0) {
                            GameObject[] gms = list[i].GetComponent<DetailBuild>().UpdateBase().ToArray();
                            if (gms.Length < 4) {
                                for (int i2 = 0; i2 < gms.Length; i2++) {
                                    gms[i2].GetComponent<DetailBuild>().DetailConnect = false;
                                }
                            }
                            else {
                                for (int i2 = 0; i2 < gms.Length; i2++) {
                                    DetailBuild d = gms[i2].GetComponent<DetailBuild>();
                                    d.DetailInBase = true;
                                    d.transform.parent = PhysicsBase.gameObject.transform;
                                    PhysicsBase.Details.Add(d);
                                    d.Base = PhysicsBase;
                                }
                            }
                            PhysicsBase.UpdateRigidbody();
                        }
                        else {
                            GameObject[] gms = list[i].GetComponent<DetailBuild>().UpdateBase().ToArray();
                            if (gms.Length < 8) {
                                for (int i2 = 0; i2 < gms.Length; i2++) {
                                    gms[i2].GetComponent<DetailBuild>().DetailConnect = false;
                                }
                            }
                            else {
                                GameObject b = new GameObject("PhysicsBases " + PhysicsBases.Count);
                                PhysicsBases.Add(b);
                                BaseInBuild bs = new BaseInBuild();
                                bs.gameObject = b;
                                b.transform.position = list[i].transform.position;
                                b.transform.rotation = list[i].transform.rotation;
                                bs.gameObject.AddComponent<PhysicsObjectBuild>().Build = this;
                                bs.gameObject.GetComponent<PhysicsObjectBuild>().Base = bs;
                                bs.rigidbody = bs.gameObject.AddComponent<Rigidbody>();
                                bs.rigidbody.isKinematic = false;
                                bs.rigidbody.velocity = PhysicsBase.rigidbody.velocity;
                                bs.Base = this;
                                add_bases++;
                                bs.NumberGroup = add_bases;
                                bs.gameObject.GetComponent<PhysicsObjectBuild>().Number = add_bases;
                                BaseInBuilds.Add(bs);

                                for (int i2 = 0; i2 < gms.Length; i2++) {
                                    DetailBuild d = gms[i2].GetComponent<DetailBuild>();
                                    d.DetailInBase = true;
                                    d.transform.parent = bs.gameObject.transform;
                                    bs.Details.Add(d);
                                    d.Base = bs;
                                }

                                bs.gameObject.transform.parent = BuildObject.transform;
                                bs.UpdateRigidbody();
                            }
                        }
                    }
                }

            }
            if (PhysicsBase.Details.Count == 0) {
                PhysicsBases.Remove(PhysicsBase.gameObject);
                BaseInBuilds.Remove(BaseInBuilds.Where((x) => x.NumberGroup == PhysicsBase.NumberGroup).FirstOrDefault());
                Destroy(PhysicsBase.gameObject);
            }
        }
    }

    public void TakeCollisionEnter(Collision collision) {//Нанесение урона постройке и проверка на то нужно ли перестроить обьект

        if (IsMainBuilding) {
            bool is_very_damag = false;


            float Force = collision.impulse.magnitude * 0.6f;
            List<GameObject> gms = new List<GameObject>();


            for (int i = 0; i < collision.contactCount; i++) {
                GameObject obj_contact = collision.contacts[0].thisCollider.gameObject;
                gms.AddRange(obj_contact.GetComponent<DetailBuild>().UpdateDamag(Force, obj_contact));
            }

            for (int i = 0; i < Details.Count; i++) {
                Details[i].GetComponent<DetailBuild>().DetailTakeDamag = false;
            }
            List<BaseInBuild> builds_update = new List<BaseInBuild>();

            for (int i = 0; i < gms.Count; i++) {
                is_very_damag = true;

                DetailBuild db = gms[i].GetComponent<DetailBuild>();
                if (builds_update.IndexOf(db.Base) == -1) {
                    builds_update.Add(db.Base);
                    UpdatePhysicsBaces(db.Base, db);
                }
            }
            if (gms.Count != 0)
                for (int i = 0; i < Joints.Count; i++) {
                    Joints[i].UpdateJoint();
                }


            if (is_very_damag && IsMainBuilding && GameRoom.isMultiplayer) {
                UpdateGroupsParameters();
                ushort[] num_groups = new ushort[DetailBuilds.Count];
                for (int i = 0; i < DetailBuilds.Count; i++) {
                    if (DetailBuilds[i].DetailConnect) {
                        num_groups[i] = (ushort) DetailBuilds[i].Base.NumberGroup;
                    }
                    else {
                        num_groups[i] = ushort.MaxValue;
                    }
                }
                update_client_damag = num_groups;
            }
        }
    }

    public void SetDetailNoWork(GameObject Detail) {
        work_details.Remove(Detail);
    }

    #endregion
    public PhotonView GetPhotonView() {
        return photonView;
    }

    private void UpdateGroupsParameters() {
        ParametersGroup p = new ParametersGroup();
        p.NumberGroup = DetailBuilds.Select((x) => x.Base.NumberGroup).ToArray();
        p.CodeDetails = DetailBuilds.Select((x) => x.Cod).ToArray();
        p.Positions = DetailBuilds.Select((x) => x.transform.localPosition.GetVector3Serializable()).ToArray();
        p.Rotations = DetailBuilds.Select((x) => x.transform.localEulerAngles.GetVector3Serializable()).ToArray();
        p.VelocytiDestroy = new Vector3Serializable[p.NumberGroup.Length];
        for (int i = 0; i < DetailBuilds.Count; i++) {
            p.VelocytiDestroy[i] = new Vector3Serializable(-1, -1, -1);
            if (!DetailBuilds[i].DetailConnect) {
                p.Positions[i] = DetailBuilds[i].transform.position.GetVector3Serializable();
                p.Rotations[i] = DetailBuilds[i].transform.eulerAngles.GetVector3Serializable();
                p.VelocytiDestroy[i] = DetailBuilds[i].GetComponent<Rigidbody>().velocity.GetVector3Serializable();
            }
        }
        GroupsParameters = p;
    }
}


public class BaseInBuild {
    public BuildingProperties Base;
    public string XCod;
    public int NumberGroup;


    public GameObject gameObject;
    public Rigidbody rigidbody;

    public List<DetailBuild> Details = new List<DetailBuild>();

    public List<FuelTank> Fuels = new List<FuelTank>();


    public void UpdateRigidbody() {
        float mass = 0;
        Vector3 centerMass = Vector3.zero;
        for (int i = 0; i < Details.Count; i++) {
            mass += Details[i].Physics.Mass;
            centerMass += Details[i].transform.localPosition * Details[i].Physics.Mass;
        }

        rigidbody.mass = mass;
        rigidbody.centerOfMass = centerMass / mass;

    }


    public bool CheckCanTakeFuel(float count, TypeFuel typeFuel) {
        for (int i = 0; i < Fuels.Count; i++) {
            if (typeFuel == Fuels[i].FuelType) {
                if (Fuels[i].Count < count) {
                    count -= Fuels[i].Count;
                    Fuels[i].Count = 0;
                }
                else if (Fuels[i].Count == count) {
                    Fuels[i].Count = 0;
                    return true;
                }
                else if (Fuels[i].Count > count) {
                    Fuels[i].Count -= count;
                    return true;
                }
            }
        }
        if (count <= 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public float TakeCountFuel(TypeFuel typeFuel) {
        float count = 0;
        for (int i = 0; i < Fuels.Count; i++) {
            if (typeFuel == Fuels[i].FuelType) {
                count = Fuels[i].Count;
            }
        }
        return count;
    }
}

public class JointBuild {
    public DetailBuild BaseDetail;
    public DetailBuild ConnectDetail;
    public BaseInBuild MainBase;
    public BaseInBuild ConnectBase;

    public TypePhysicsConnect Type;

    public string XCod;

    public Joint JointConnect;

    private IDetailJoint detail_joint;

    private float minL, maxL;
    private bool newL;
    public void Create() {
        if (Type == TypePhysicsConnect.Axis) {
            detail_joint = BaseDetail.GetComponent<AxisConnect>();
            AxisConnect axis = (AxisConnect) detail_joint;
            if (BaseDetail.Base.rigidbody.mass < ConnectDetail.Base.rigidbody.mass) {

                MainBase = ConnectDetail.Base;
                ConnectBase = BaseDetail.Base;
            }
            else {

                MainBase = BaseDetail.Base;
                ConnectBase = ConnectDetail.Base;
            }


            axis.ConnectGroup = ConnectBase;
            axis.BaseGroup = MainBase;

            JointConnect = MainBase.gameObject.AddComponent<HingeJoint>();

            JointConnect.autoConfigureConnectedAnchor = false;
            JointConnect.axis = MainBase.gameObject.transform.InverseTransformDirection(axis.PosObject.transform.up);

            JointConnect.anchor = MainBase.gameObject.transform.InverseTransformPoint(axis.PosObject.transform.position);
            JointConnect.connectedAnchor = ConnectBase.gameObject.transform.InverseTransformPoint(axis.PosObject.transform.position);


            JointConnect.connectedBody = ConnectBase.rigidbody;


            JointSpring joints;
            joints = new JointSpring();
            joints.spring = axis.Elasticity;
            joints.damper = axis.Damping;
            ( (HingeJoint) JointConnect ).useSpring = axis.Spring;
            ( (HingeJoint) JointConnect ).spring = joints;

            if (!newL) {
                JointLimits joint_limit = new JointLimits();
                ( (HingeJoint) JointConnect ).useLimits = axis.Limit;
                joint_limit.min = axis.Min;
                joint_limit.max = axis.Max;
                ( (HingeJoint) JointConnect ).limits = joint_limit;
            }
            else {
                JointLimits joint_limit = new JointLimits();
                ( (HingeJoint) JointConnect ).useLimits = axis.Limit;
                joint_limit.min = minL;
                joint_limit.max = maxL;
                axis.TargetAngle = 0;
                axis.Max = maxL;
                axis.Min = minL;
                ( (HingeJoint) JointConnect ).limits = joint_limit;
            }

            ( (HingeJoint) JointConnect ).useMotor = axis.Rotate;
            JointConnect.enableCollision = false;

            axis.Hinge = ( (HingeJoint) JointConnect );

            detail_joint.CreateJoint();
            // axis.Rebuild();
        }
        else if (Type == TypePhysicsConnect.Connect) {
            MainBase = BaseDetail.Base;
            ConnectBase = ConnectDetail.Base;
            detail_joint = BaseDetail.GetComponent<ConnectorBase>();
            ConnectorBase connector = (ConnectorBase) detail_joint;

            JointConnect = MainBase.gameObject.AddComponent<ConfigurableJoint>();

            JointConnect.autoConfigureConnectedAnchor = false;
            JointConnect.axis = connector.PosObject.transform.up.normalized;

            JointConnect.connectedAnchor = ConnectBase.gameObject.transform.InverseTransformPoint(connector.PosObject.transform.position);
            JointConnect.anchor = MainBase.gameObject.transform.InverseTransformPoint(connector.PosObject.transform.position);

            JointConnect.connectedBody = ConnectBase.rigidbody;

            ConnectDetail.GetComponent<ConnectConSet>().ConnectBase = connector;

            ConfigurableJoint character = (ConfigurableJoint) JointConnect;
            character.xMotion = ConfigurableJointMotion.Locked;
            character.yMotion = ConfigurableJointMotion.Locked;
            character.zMotion = ConfigurableJointMotion.Locked;

            connector.Joint = this;
            connector.isConnect = true;
            detail_joint.CreateJoint();
        }
    }
    public void UpdateJoint() {

        if (!BaseDetail.DetailConnect) {
            if (Type == TypePhysicsConnect.Axis)
                ( (AxisConnect) detail_joint ).ConnectGroup = null;
            UnityEngine.Object.Destroy(JointConnect);
            detail_joint.DestroyJoint();
        }
        else if (!ConnectDetail.DetailConnect) {
            if (Type == TypePhysicsConnect.Axis)
                ( (AxisConnect) detail_joint ).ConnectGroup = null;
            UnityEngine.Object.Destroy(JointConnect);
            detail_joint.DestroyJoint();
        }
        else if (BaseDetail.Base.gameObject != MainBase.gameObject && BaseDetail.Base.gameObject != ConnectBase.gameObject) {
            NewLimits();
            UnityEngine.Object.Destroy(JointConnect);
            detail_joint.DestroyJoint();
            Create();
        }
        else if (ConnectDetail.Base.gameObject != ConnectBase.gameObject && ConnectDetail.Base.gameObject != MainBase.gameObject) {
            NewLimits();
            UnityEngine.Object.Destroy(JointConnect);
            detail_joint.DestroyJoint();
            Create();
        }
    }

    private void NewLimits() {
        if (Type == TypePhysicsConnect.Axis && JointConnect && ( (HingeJoint) JointConnect ).useLimits) {
            newL = true;

            minL = ( (AxisConnect) detail_joint ).Min - ( (AxisConnect) detail_joint ).TargetAngle;
            maxL = ( (AxisConnect) detail_joint ).Max - ( (AxisConnect) detail_joint ).TargetAngle;
        }
    }

    public void DestroyJoint() {
        BaseDetail.Building.Joints.Remove(this);
        UnityEngine.Object.Destroy(JointConnect);
        if (Type == TypePhysicsConnect.Connect) {
            ( (ConnectorBase) detail_joint ).isConnect = false;
        }
        detail_joint.DestroyJoint();
    }
}

[System.Serializable]
public struct ParametersGroup {
    public string[] CodeDetails;
    public Vector3Serializable[] Positions;
    public Vector3Serializable[] Rotations;
    public int[] NumberGroup;
    public Vector3Serializable[] VelocytiDestroy;
}

[System.Serializable]
public struct ParametersBuild {
    public Vector3Serializable[] Positions;
    public Vector3Serializable[] Rotarions;
    public PointOnDetail[] Points;
    public string[] Cods;
    public SaveEditorBuild.ObjectSave[] Parameters;
}

[System.Serializable]
public struct PointOnDetail {
    public GroupBase Group;
    public int Number;

    public PointOnDetail(GroupBase Group, int Number) {
        this.Group = Group;
        this.Number = Number;
    }
}

[System.Serializable]
public struct ArrayObjects {
    public object[] objects;
}

public interface IDetailJoint {

    void CreateJoint();

    void DestroyJoint();

}
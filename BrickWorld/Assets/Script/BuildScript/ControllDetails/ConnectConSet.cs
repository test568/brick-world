﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectConSet: MonoBehaviour, IDetailControll {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
            }
        set {
            if (value==false) {
                PosConnects.Remove(this);
                ConnectBase.DisconnectJoint();
                }
            _disconnect=value;
            }
        }

    bool _disconnect;

    public ConnectorBase ConnectBase;

    public static List<ConnectConSet> PosConnects = new List<ConnectConSet>();

    public GameObject PosObject;

    public GameObject Trigger;

    private void Awake() {
        Trigger=new GameObject("Trigger");
        Trigger.transform.parent=gameObject.transform;
        Trigger.transform.localRotation=Quaternion.Euler(Vector3.zero);
        Trigger.transform.localPosition=Vector3.zero;
        Trigger.layer=LayerMask.NameToLayer("Interaction");
        BoxCollider[] boxs = GetComponents<BoxCollider>();
        for (int i = 0; i<boxs.Length; i++) {
            BoxCollider box = Trigger.AddComponent<BoxCollider>();
            box.size=boxs[i].size;
            box.center=boxs[i].center;
            box.isTrigger=true;
            }
        }

    public void UpdateThisBase(BaseInBuild new_base) {
        }

    private void OnEnable() {
        PosConnects.Add(this);
        }

    private void OnDisable() {
        PosConnects?.Remove(this);
        ConnectBase?.DisconnectJoint();
        }
    }

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aileron : MonoBehaviour, IDetailControll {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (value == true)
                rigid = GetComponent<Rigidbody>();
            _disconnect = value;
        }
    }

    bool _disconnect;

    public int Revers;

    public float LiftMultiplier = 1f;

    public float DragMultiplier = 1f;

    public GameObject PosObject;

    public Vector3 RotateAxis;

    public float Angle;

    public WingCurves WingCurve;

    public float Area;


    Vector3 start_rot;
    public float Rotate {
        get {
            return _rotate;
        }
        set {
            _rotate = value;
            PosObject.transform.localEulerAngles = start_rot + ( RotateAxis * Angle ) * value * Revers;
        }
    }

    float _rotate = 0f;

    BaseInBuild _base;

    Rigidbody rigid;

    private void Start() {
        start_rot = PosObject.transform.localEulerAngles;
    }

    public void UpdateThisBase(BaseInBuild new_base) {
        rigid = new_base.rigidbody;
        _base = new_base;
    }

    private void FixedUpdate() {
        if (_base != null && _base.Base.IsMainBuilding) {

            Vector3 force = Wing.CalculateForce(rigid.GetPointVelocity(PosObject.transform.position), WingCurve, PosObject.transform, Area, LiftMultiplier, DragMultiplier);
            //force = Vector3.ClampMagnitude(force, rigid.velocity.magnitude * rigid.mass);
            rigid.AddForceAtPosition(force, PosObject.transform.position, ForceMode.Force);
        }
    }
}

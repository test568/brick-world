﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beep : MonoBehaviour, IDetailControll, ISyncParams {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (GameRoom.isMasterClient) {
                _disconnect = value;
                if (_disconnect) {
                    Activated = 0f;
                    BaseBuilding.SyncParamsDetail(this, 1, true);
                }
            }
        }
    }

    bool _disconnect;

    public float Activated {
        get {
            return _activated;
        }
        set {
            if (value == 0 && _activated != 0) {
                audio?.Stop();
            }
            else if (value != 0 && _activated == 0 && !Disconnect) {
                audio?.Play();
            }
            _activated = value;
        }
    }

    public BuildingProperties BaseBuilding { get; set; }
    public int Index { get; set; }

    float _activated;

    public GameObject Sound;

    AudioSource audio;

    public void UpdateParams(int index_param, object param) {
        if (index_param == 1)
            _disconnect = (bool) param;
    }

    public void AcceptStartParameters(object[] objects) {
        Activated = (float) objects[0];
        _disconnect = (bool) objects[1];
        if ((bool) objects[1]) {
            Activated = 0f;
        }
    }

    public object[] SendUploaded() {
        return new object[2] { _activated, Disconnect };
    }

    public void UpdateThisBase(BaseInBuild new_base) {

    }
    public void Init() {
        GameObject obj = Instantiate(Sound, transform);
        obj.transform.localPosition = Vector3.zero;
        audio = obj.GetComponent<AudioSource>();
    }

    private void Update() {
        if (audio)
            audio.volume = AudioPlayer.Audio.EffectVolume * AudioPlayer.Audio.AllVolume;
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RailJoint : MonoBehaviour
{
    public Joint Connection;
    public Vector3 Axe = Vector3.up;

    Vector3 StartConnectedAnchor;
    Transform Tr;
    Transform CTr;
    Vector3 ConnectedAxe;
    Rigidbody Rigid;
    float RailLength;
    [HideInInspector]
    public ConfigurableJoint TargetJoint;

    private void Start()
    {
        Tr = transform;
        CTr = Connection.connectedBody.transform;
        Rigid = GetComponent<Rigidbody>();
        RailLength = Axe.magnitude;
        StartConnectedAnchor = Connection.connectedAnchor;
        ConnectedAxe = CTr.InverseTransformDirection(Tr.TransformDirection(Axe));
        if(Application.isPlaying)
            Connection.autoConfigureConnectedAnchor = false;
    }

    private void OnDrawGizmosSelected()
    {
        if (Connection)
        {
            if(!Application.isPlaying)
                Start();

            Vector3 anh = CTr.TransformPoint(StartConnectedAnchor);
            Vector3 axe = CTr.TransformDirection(ConnectedAxe) * 0.5f;
            Vector3 cross = Vector3.Cross(axe, Vector3.up);
            Gizmos.DrawLine(anh + axe, anh - axe);
            Gizmos.color = new Color(0.8f, 0.5f, 0.2f, 1);
            Gizmos.DrawLine(anh + axe + cross * 0.1f, anh + axe - cross * 0.1f);
            Gizmos.DrawLine(anh - axe + cross * 0.1f, anh - axe - cross * 0.1f);
            anh = CTr.TransformPoint(Connection.connectedAnchor);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(anh + cross * 0.1f, anh - cross * 0.1f);
        }
    }

    readonly float offsetLimit = 0.1f;
    readonly float MaxForce = 10000;
    void Scync()
    {
        Vector3 anhPos = CTr.TransformPoint(StartConnectedAnchor);
        Vector3 worldAnchor = Tr.TransformPoint(Connection.anchor);
        Vector3 localAnchorPos = GetInverse(CTr.rotation) * (worldAnchor - anhPos);
        Connection.connectedAnchor = StartConnectedAnchor + ConnectedAxe.normalized * Mathf.Clamp(localAnchorPos.z, -RailLength * 0.5f, RailLength * 0.5f);
        Vector3 direction = CTr.TransformPoint(Connection.connectedAnchor) - worldAnchor;

        if (!Rigid.isKinematic)
            Tr.position = Vector3.MoveTowards(Tr.position + direction, Tr.position, offsetLimit);
    }

    private void LateUpdate()
    {
        if(Connection)
            Scync();
    }

    public static Quaternion GetInverse(Quaternion rotation)
    {
        return Quaternion.Inverse(rotation);
    }

    public void BakeAsConfigurableJoint()
    {
#if UNITY_EDITOR
        Undo.RecordObject(gameObject, "BakeAsConfigurableJoint");
        Undo.RecordObject(this, "BakeAsConfigurableJoint");
        if (Connection)
            Undo.RecordObject(Connection, "BakeAsConfigurableJoint");
#endif
        Rigidbody cb = null;
        if (Connection)
        {
            cb = Connection.connectedBody;
            DestroyImmediate(Connection);
        }
        ConfigurableJoint TargetJoint = GetComponent<ConfigurableJoint>();
        if (!TargetJoint)
            TargetJoint = gameObject.AddComponent<ConfigurableJoint>();

        TargetJoint.connectedBody = cb;

        TargetJoint.xMotion = ConfigurableJointMotion.Locked;
        TargetJoint.yMotion = ConfigurableJointMotion.Locked;
        TargetJoint.zMotion = ConfigurableJointMotion.Locked;
        TargetJoint.angularXMotion = ConfigurableJointMotion.Locked;
        TargetJoint.angularYMotion = ConfigurableJointMotion.Locked;
        TargetJoint.angularZMotion = ConfigurableJointMotion.Locked;

        if (Mathf.Abs(Axe.x) > Mathf.Abs(Axe.y) && Mathf.Abs(Axe.x) > Mathf.Abs(Axe.z))
        {
            TargetJoint.xMotion = ConfigurableJointMotion.Limited;
        }
        else if (Mathf.Abs(Axe.y) > Mathf.Abs(Axe.x) && Mathf.Abs(Axe.y) > Mathf.Abs(Axe.z))
        {
            TargetJoint.yMotion = ConfigurableJointMotion.Limited;
        }
        else if (Mathf.Abs(Axe.z) > Mathf.Abs(Axe.x) && Mathf.Abs(Axe.z) > Mathf.Abs(Axe.y))
        {
            TargetJoint.zMotion = ConfigurableJointMotion.Limited;
        }

        var limit = TargetJoint.linearLimit;
        limit.limit = Axe.magnitude / 2;
        TargetJoint.linearLimit = limit;

#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
        EditorUtility.SetDirty(gameObject);
#endif

        DestroyImmediate(this);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(RailJoint))]
public class RailJointEditor : Editor
{
    public RailJoint Script;

    private void OnEnable()
    {
        Script = (RailJoint)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Bake As Configurable Joint"))
        {
            Script.BakeAsConfigurableJoint();
        }
    }
}
#endif
﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngineInternal;

public class WheelAxis : MonoBehaviour, IDetailControll, ISyncParams {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }

    public FreedbackWire RotateTransmission = new FreedbackWire();

    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (value == true && wheel != null) {
                _disconnect = value;
                wheel.transform.parent.GetComponent<Rigidbody>().velocity = Vector3.zero;
                wheel.transform.parent.GetComponent<PhysicsObjectBuild>().EnableSync();
                wheel.gameObject.GetComponent<SphereCollider>().enabled = true;
                wheel.transform.parent.position = transform.position;
                if (is_tank && wheel) {
                    ( (TankWheel) wheel ).DestroyTrack();
                }
                if (BaseBuilding.IsMainBuilding) {
                    BaseBuilding.SyncParamsDetail(this, 0, true);
                }
            }

            _disconnect = value;
        }
    }

    bool _disconnect;

    BaseInBuild base_phys;

    public float Force;

    public void UpdateThisBase(BaseInBuild new_base) {
        base_phys = new_base;
    }

    public float Steering {
        get {
            return 0;
        }
        set {
            _steering = value;
        }
    }

    float _steering = 0f;

    public float Break {
        get {
            return 0;
        }
        set {
            _break = value;
        }
    }

    public Side TrackSide;

    public BuildingProperties BaseBuilding { get; set; }

    public int Index { get; set; }

    float _break;

    bool is_tank = false;

    bool is_create_track {
        set {
            if (!_is_create_track) {
                ( (TankWheel) wheel ).BuildTrack();
                _is_create_track = true;
            }
        }
    }
    bool _is_create_track;

    WheelAxis dependent_axis;


    [HideInInspector]
    public float Spring;

    [HideInInspector]
    public float Damper;

    [HideInInspector]
    public float RotateSpeed;

    [HideInInspector]
    public float SteerAngle;

    [HideInInspector]
    public float DistanceSuspension;

    [HideInInspector]
    public float ForceBreak;

    [HideInInspector]
    public Vector3 CenterPos;

    //[HideInInspector]
    public int Revers;

    private WheelCollision wheel;

    private Transform m_dummyWheel;
    private Transform track_wheel_point;
    private Rigidbody m_rigidbody;

    private WheelFrictionCurveSource m_forwardFriction; //Properties of tire friction in the direction the wheel is pointing in.
    private WheelFrictionCurveSource m_sidewaysFriction; //Properties of tire friction in the sideways direction.
    private float m_forwardSlip;
    private float m_sidewaysSlip;
    private Vector3 m_totalForce;

    private Vector3 m_prevPosition;
    private bool m_isGrounded; //Indicates whether the wheel currently collides with something (Read Only).

    private float m_wheelBrakeTorque; //Brake torque. Must be positive.
    private float m_wheelSteerAngle; //Steering angle in degrees, always around the local y-axis.
    private float m_wheelAngularVelocity; //Current wheel axle rotation speed, in rotations per minute (Read Only).
    private float m_wheelRotationAngle;
    private float m_wheelRadius {
        get {
            return wheel.GetRadius();
        }
    } //The radius of the wheel, measured in local space.

    private float m_wheelMass; //The mass of the wheel. Must be larger than zero.

    private RaycastHit m_raycastHit;
    private float m_suspensionCompression;
    private float m_suspensionCompressionPrev;
    private JointSpringSource m_suspensionSpring; //The parameters of wheel's suspension. The suspension attempts to reach a target position
    private GameObject m_wheel;


    private Vector3 network_pos;
    private Quaternion network_rot;
    private float dist;
    private float ang;

    private LayerMask PhysicsLayer;

    //Debugging color data
    private Color GizmoColor;

    private RotationForce rotation;

    bool start_synh = false;

    // Called once per physics update

    public void FixedUpdate() {
        if (!Disconnect && BaseBuilding.IsMainBuilding) {
            UpdateSuspension();

            if (!wheel.Disconnect) {
                UpdateWheel();
            }
            else { }
            if (m_isGrounded) {
                CalculateSlips();
                CalculateForcesFromSlips();

                if (Time.timeScale != 0) {
                    base_phys.rigidbody.AddForceAtPosition(m_totalForce, transform.position);
                }
            }


            float break_rotate = 0;

            if (!is_tank) {
                if (_steering != 0)
                    m_wheelSteerAngle = Mathf.Clamp(m_wheelSteerAngle + ( RotateSpeed * Mathf.Sign(_steering) ) * Time.deltaTime, -SteerAngle, SteerAngle);
                if (_steering == 0)
                    m_wheelSteerAngle = Mathf.Clamp(m_wheelSteerAngle + ( RotateSpeed * Mathf.Clamp(0 - m_wheelSteerAngle, -1, 1) ) * Time.deltaTime, -SteerAngle, SteerAngle);
            }
            else {
                if (_steering < 0 && TrackSide == Side.Left) {
                    break_rotate = 1;
                }
                else if (_steering > 0 && TrackSide == Side.Right) {
                    break_rotate = 1;
                }
            }


            if (_break != 0 || break_rotate != 0)
                m_wheelBrakeTorque = Mathf.Clamp(ForceBreak, 0, 100);
            else
                m_wheelBrakeTorque = 1;
        }
        if (!BaseBuilding.IsMainBuilding && !Disconnect) {
            m_wheel.transform.localPosition = Vector3.MoveTowards(m_wheel.transform.localPosition, network_pos, dist * ( 1.5f / PhotonNetwork.SerializationRate ));
            m_wheel.transform.localRotation = Quaternion.RotateTowards(m_wheel.transform.localRotation, network_rot, ang * ( 1.5f / PhotonNetwork.SerializationRate ));
            if (is_tank)
                track_wheel_point.localPosition = m_wheel.transform.localPosition;
        }
    }

    private void LateUpdate() {
        if (UpdateFunctions != null && ( !GameRoom.isMultiplayer || BaseBuilding.GetPhotonView().IsMine )) {
            UpdateFunctions();
        }
    }

    private void Update() {
        if (!Disconnect && BaseBuilding.IsMainBuilding) {
            wheel.BasePhys.gameObject.transform.position = m_wheel.transform.position;
            wheel.BasePhys.gameObject.transform.eulerAngles = m_wheel.transform.eulerAngles;
        }
        else if (!Disconnect && !BaseBuilding.IsMainBuilding && start_synh) {
            wheel.transform.parent.position = m_wheel.transform.position;
            wheel.transform.parent.eulerAngles = m_wheel.transform.eulerAngles;
        }
        if (is_tank && ( (TankWheel) wheel ).IsLeader) {
            ( (TankWheel) wheel ).SpeedTrack = m_wheelAngularVelocity * wheel.GetRadius();
        }
        if (is_tank) {
            is_create_track = true;
        }
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(m_wheel.transform.localPosition);
            stream.SendNext(m_wheel.transform.localEulerAngles);
            if (is_tank) {
                stream.SendNext(m_wheelAngularVelocity);
            }
        }
        else if (stream.IsReading) {
            network_pos = (Vector3) stream.ReceiveNext();
            network_rot = Quaternion.Euler((Vector3) stream.ReceiveNext());
            dist = Vector3.Distance(m_wheel.transform.localPosition, network_pos);
            ang = Quaternion.Angle(network_rot, m_wheel.transform.localRotation);
            if (is_tank) {
                m_wheelAngularVelocity = (float) stream.ReceiveNext();
            }
        }
    }

    public void UpdateParams(int index_param, object param) {
        if (index_param == 0) {
            Disconnect = (bool) param;
        }
    }

    public void AcceptStartParameters(object[] objects) {
        start_synh = true;
        Disconnect = (bool) objects[0];
    }

    public object[] SendUploaded() {
        return new object[1] { Disconnect };
    }

    public void TransmissionUpdate() {
        RotationForce rt = new RotationForce();
        rt.Forse = -1f;
        List<object> rts = FreedbackWire.TakeWireConnect(RotateTransmission);
        for (int i = 0; i < rts.Count; i++) {
            rt.Revs += ( (RotationForce) rts[i] ).Revs;
            rt.Forse += ( (RotationForce) rts[i] ).Forse;
        }
        if (float.IsNaN(rt.Revs) || float.IsInfinity(rt.Revs))
            rt.Revs = 0f;
        if (float.IsNaN(rt.Forse) || float.IsInfinity(rt.Forse))
            rt.Forse = -1f;
        rotation = rt;

    }

    // Use this for initialization
    public void SetWheel(WheelCollision wheel) {
        if (this.wheel == null) {
            this.wheel = wheel;
            m_wheel = new GameObject("Wheel");
            m_wheel.layer = LayerMask.NameToLayer("Wheel");
            m_wheel.transform.parent = transform;
        }
        else
            return;

        if (wheel.TypeWheel == TypeWheel.TankWheel) {
            ( (TankWheel) wheel ).ConnectAxis = this;

            track_wheel_point = new GameObject("TrackPoint").transform;
            track_wheel_point.parent = transform;
            track_wheel_point.localEulerAngles = new Vector3(90, 0, -90);
            track_wheel_point.position = wheel.gameObject.transform.position;
            ( (TankWheel) wheel ).PointWheelAxis = track_wheel_point;

            is_tank = true;
        }

        if (!( !GameRoom.isMultiplayer || BaseBuilding.GetPhotonView().IsMine ))
            return;



        m_dummyWheel = new GameObject("DummyWheel").transform;
        m_dummyWheel.parent = transform;
        m_dummyWheel.position = wheel.gameObject.transform.position;
        m_dummyWheel.localEulerAngles = Vector3.zero;

        m_suspensionCompression = 0;
        m_wheelMass = wheel.GetComponent<Rigidbody>().mass;

        m_forwardFriction = new WheelFrictionCurveSource();
        m_sidewaysFriction = new WheelFrictionCurveSource();
        m_forwardFriction.Stiffness = wheel.gameObject.GetComponent<SphereCollider>().material.dynamicFriction;
        m_sidewaysFriction.Stiffness = wheel.gameObject.GetComponent<SphereCollider>().material.dynamicFriction;

        m_suspensionSpring = new JointSpringSource();
        m_suspensionSpring.Spring = Spring;
        m_suspensionSpring.Damper = Damper;
        m_suspensionSpring.TargetPosition = 0;

        wheel.gameObject.layer = LayerMask.NameToLayer("Wheel");
        PhysicsLayer = LayerMask.GetMask(new string[] { "Ground" });
        wheel.gameObject.GetComponent<MeshCollider>().enabled = false;


    }

    public void SetTrackParams(WheelAxis dependent_axis) {
        this.dependent_axis = dependent_axis;
    }

    public void DestroyTrack() {
        dependent_axis = null;
    }

    public void OnDrawGizmosSelected() {
        Gizmos.color = GizmoColor;

        Vector3 point1;
        Vector3 point0 = transform.TransformPoint(m_wheelRadius * new Vector3(0, Mathf.Sin(0), Mathf.Cos(0)));
        for (int i = 1; i <= 20; ++i) {
            point1 = transform.TransformPoint(m_wheelRadius * new Vector3(0, Mathf.Sin(i / 20.0f * Mathf.PI * 2.0f), Mathf.Cos(i / 20.0f * Mathf.PI * 2.0f)));
            Gizmos.DrawLine(point0, point1);
            point0 = point1;
        }
        Gizmos.color = Color.white;
    }

    public bool GetGroundHit(out WheelHitSource wheelHit) {
        wheelHit = new WheelHitSource();
        if (m_isGrounded) {
            wheelHit.Collider = m_raycastHit.collider;
            wheelHit.Point = m_raycastHit.point;
            wheelHit.Normal = m_raycastHit.normal;
            wheelHit.ForwardDir = m_dummyWheel.forward;
            wheelHit.SidewaysDir = m_dummyWheel.right;
            wheelHit.Force = m_totalForce;
            wheelHit.ForwardSlip = m_forwardSlip;
            wheelHit.SidewaysSlip = m_sidewaysSlip;
        }

        return m_isGrounded;
    }

    private void UpdateSuspension() {
        //Raycast down along the suspension to find out how far the ground is to the wheel
        bool result = false;
        if (Physics.SphereCast(new Ray(m_dummyWheel.position, -m_dummyWheel.up), m_wheelRadius, out m_raycastHit, DistanceSuspension, PhysicsLayer)) {
            result = true;
        }
        else if (Physics.Raycast(new Ray(m_dummyWheel.position, -m_dummyWheel.up), out m_raycastHit, DistanceSuspension + m_wheelRadius, PhysicsLayer)) {
            result = true;
        }


        if (result) //The wheel is in contact with the ground
        {
            if (!m_isGrounded) //If not previously grounded, set the prevPosition value to the wheel's current position.
            {
                m_prevPosition = m_dummyWheel.position;
            }
            GizmoColor = Color.green;
            m_isGrounded = true;

            //Store the previous suspension compression for the damping calculation
            m_suspensionCompressionPrev = m_suspensionCompression;

            //Update the suspension compression
            m_suspensionCompression = DistanceSuspension + m_wheelRadius - ( m_raycastHit.point - m_dummyWheel.position ).magnitude;
            if (m_suspensionCompression > DistanceSuspension) {
                GizmoColor = Color.red;
            }
        }
        else //The wheel is in the air
        {
            m_suspensionCompression = 0;
            GizmoColor = Color.blue;
            m_isGrounded = false;
        }
    }

    private void UpdateWheel() {
        m_wheelRotationAngle = Mathf.Repeat(m_wheelRotationAngle, 360);
        //Set steering angle of the wheel dummy
        m_dummyWheel.localEulerAngles = new Vector3(0, m_wheelSteerAngle, 0);



        //Set the wheel's position given the current suspension compression
        m_wheel.transform.localPosition = m_dummyWheel.localPosition - Vector3.up * ( DistanceSuspension - m_suspensionCompression );
        if (is_tank)
            track_wheel_point.localPosition = m_wheel.transform.localPosition;


        if (!dependent_axis) {
            //Apply rolling force to tires if they are grounded and don't have motor torque applied to them.
            if (m_isGrounded) {
                //Apply angular force to wheel from slip
                m_wheelAngularVelocity -= Mathf.Sign(m_forwardSlip) * m_forwardFriction.Evaluate(m_forwardSlip) / ( Mathf.PI * 2.0f * m_wheelRadius ) / m_wheelMass * 40f * Time.deltaTime;
            }

            //Apply motor torque
            float force = 0;
            if (rotation.Forse != -1) {
                force = ( rotation.Forse * rotation.Revs ) * Revers;
                Force = force;
            }
            else {
                Force = 0;
            }

            m_wheelAngularVelocity += force / m_wheelRadius / m_wheelMass * Time.deltaTime;

            //Apply brake torque
            //       m_wheelAngularVelocity -= Mathf.Sign(force) * Mathf.Min(m_wheelBrakeTorque * 10000000 * m_wheelRadius / m_wheelMass * Time.deltaTime, Mathf.Abs(m_wheelAngularVelocity));
            m_wheelAngularVelocity = m_wheelAngularVelocity - ( m_wheelAngularVelocity / 100 ) * m_wheelBrakeTorque;
            //Calculate the wheel's rotation given it's angular velocity


            if (force != 0)
                m_wheelAngularVelocity = Mathf.Clamp(m_wheelAngularVelocity, -Mathf.Abs(rotation.Revs), Mathf.Abs(rotation.Revs));
        }
        else {
            m_wheelAngularVelocity = dependent_axis.GetAngularVelocity();
        }

        m_wheelRotationAngle += m_wheelAngularVelocity * Time.deltaTime;

        //Set the rotation and steer angle of the wheel model
        //   m_wheelRotationAngle = Mathf.Repeat(m_wheelRotationAngle, 360);
        if (m_wheelRotationAngle > 10000000f || m_wheelRotationAngle < -10000000f) {
            m_wheelRotationAngle = 0f;
        }
        m_wheel.transform.localRotation = Quaternion.Euler(new Vector3(m_wheelRotationAngle, m_wheelSteerAngle, -90));
    }

    private float GetAngularVelocity() {
        return m_wheelAngularVelocity;
    }

    private void CalculateSlips() {
        //Calculate the wheel's linear velocity
        Vector3 velocity = ( m_dummyWheel.position - m_prevPosition ) / Time.deltaTime;
        m_prevPosition = m_dummyWheel.position;

        //Store the forward and sideways direction to improve performance
        //   Vector3 forward = m_dummyWheel.forward;
        //    Vector3 sideways = -m_dummyWheel.right;

        Vector3 forward = Vector3.Cross(m_dummyWheel.right, m_raycastHit.normal.normalized);
        Vector3 sideways = -m_dummyWheel.right;

        //Calculate the forward and sideways velocity components relative to the wheel
        Vector3 forwardVelocity = Vector3.Dot(velocity, forward) * forward;
        Vector3 sidewaysVelocity = Vector3.Dot(velocity, sideways) * sideways;

        //Calculate the slip velocities. 
        //Note that these values are different from the standard slip calculation.
        m_forwardSlip = -Mathf.Sign(Vector3.Dot(forward, forwardVelocity)) * forwardVelocity.magnitude + ( m_wheelAngularVelocity * Mathf.PI / 180.0f * m_wheelRadius );
        m_sidewaysSlip = -Mathf.Sign(Vector3.Dot(sideways, sidewaysVelocity)) * sidewaysVelocity.magnitude;

    }

    private void CalculateForcesFromSlips() {
        //Forward slip force
        m_totalForce = new Vector3();

        //m_totalForce = m_dummyWheel.forward * Mathf.Sign(m_forwardSlip) * m_forwardFriction.Evaluate(m_forwardSlip) * 2.7f;
        m_totalForce = Vector3.Cross(m_dummyWheel.right, m_raycastHit.normal.normalized) * Mathf.Sign(m_forwardSlip) * m_forwardFriction.Evaluate(m_forwardSlip) * 2.7f;
        //Lateral slip force
        m_totalForce -= m_dummyWheel.right * Mathf.Sign(m_sidewaysSlip) * m_forwardFriction.Evaluate(m_sidewaysSlip);

        //Spring force
        m_totalForce += m_raycastHit.normal * ( m_suspensionCompression - DistanceSuspension * ( m_suspensionSpring.TargetPosition ) ) * m_suspensionSpring.Spring;

        //Spring damping force
        m_totalForce += m_raycastHit.normal * ( m_suspensionCompression - m_suspensionCompressionPrev ) / Time.deltaTime * m_suspensionSpring.Damper;


        //  Debug.Log(m_suspensionCompression + " | " + DistanceSuspension);
    }
}

public struct WheelHitSource {
    public Collider Collider; //The other Collider the wheel is hitting.
    public Vector3 Point; //The point of contact between the wheel and the ground.
    public Vector3 Normal; //The normal at the point of contact.
    public Vector3 ForwardDir; //The direction the wheel is pointing in.
    public Vector3 SidewaysDir; //The sideways direction of the wheel.
    public Vector3 Force; //The magnitude of the force being applied for the contact.
    public float ForwardSlip; //Tire slip in the rolling direction. Acceleration slip is negative, braking slip is positive.
    public float SidewaysSlip; //Tire slip in the sideways direction.
}

public struct JointSpringSource {
    private float m_spring; //The spring forces used to reach the target position
    private float m_damper; //The damper force uses to dampen the spring
    private float m_targetPosition; //The target position the joint attempts to reach.


    public float Spring {
        get {
            return m_spring;
        }
        set {
            m_spring = Mathf.Max(0, value);
        }
    }
    public float Damper {
        get {
            return m_damper;
        }
        set {
            m_damper = Mathf.Max(0, value);
        }
    }
    public float TargetPosition {
        get {
            return m_targetPosition;
        }
        set {
            m_targetPosition = Mathf.Clamp01(value);
        }
    }
}

public class WheelFrictionCurveSource {
    private struct WheelFrictionCurvePoint {
        public float TValue;
        public Vector2 SlipForcePoint;
    }
    private float m_extremumSlip; //Extremum point slip (default 3).
    private float m_extremumValue; //Force at the extremum slip (default 6000).
    private float m_asymptoteSlip; //Asymptote point slip (default 4).
    private float m_asymptoteValue; //Force at the asymptote slip (default 5500).
    private float m_stiffness; //Multiplier for the extremumValue and asymptoteValue values (default 1).

    private int m_arraySize;
    private WheelFrictionCurvePoint[] m_extremePoints;
    private WheelFrictionCurvePoint[] m_asymptotePoints;

    public float ExtremumSlip {
        get {
            return m_extremumSlip;
        }
        set {
            m_extremumSlip = value;
            UpdateArrays();
        }
    }
    public float ExtremumValue {
        get {
            return m_extremumValue;
        }
        set {
            m_extremumValue = value;
            UpdateArrays();
        }
    }
    public float AsymptoteSlip {
        get {
            return m_asymptoteSlip;
        }
        set {
            m_asymptoteSlip = value;
            UpdateArrays();
        }
    }
    public float AsymptoteValue {
        get {
            return m_asymptoteValue;
        }
        set {
            m_asymptoteValue = value;
            UpdateArrays();
        }
    }
    public float Stiffness {
        get {
            return m_stiffness;
        }
        set {
            m_stiffness = value;
        }
    }

    public WheelFrictionCurveSource() {
        m_extremumSlip = 3;
        m_extremumValue = 6000;
        m_asymptoteSlip = 4;
        m_asymptoteValue = 5500;
        m_stiffness = 1;

        m_arraySize = 50;
        m_extremePoints = new WheelFrictionCurvePoint[m_arraySize];
        m_asymptotePoints = new WheelFrictionCurvePoint[m_arraySize];

        UpdateArrays();
    }

    private void UpdateArrays() {
        //Generate the arrays
        for (int i = 0; i < m_arraySize; ++i) {
            m_extremePoints[i].TValue = (float) i / (float) m_arraySize;
            m_extremePoints[i].SlipForcePoint = Hermite(
                    (float) i / (float) m_arraySize,
                    Vector2.zero,
                    new Vector2(m_extremumSlip, m_extremumValue),
                    Vector2.zero,
                    new Vector2(m_extremumSlip * 0.5f + 1, 0)
                );

            m_asymptotePoints[i].TValue = (float) i / (float) m_arraySize;
            m_asymptotePoints[i].SlipForcePoint = Hermite(
                (float) i / (float) m_arraySize,
                    new Vector2(m_extremumSlip, m_extremumValue),
                    new Vector2(m_asymptoteSlip, m_asymptoteValue),
                    new Vector2(( m_asymptoteSlip - m_extremumSlip ) * 0.5f + 1, 0),
                    new Vector2(( m_asymptoteSlip - m_extremumSlip ) * 0.5f + 1, 0)
                );
        }
    }

    public float Evaluate(float slip) {
        //The slip value must be positive.
        slip = Mathf.Abs(slip);

        if (slip < m_extremumSlip) {
            return Evaluate(slip, m_extremePoints) * m_stiffness;
        }
        else if (slip < m_asymptoteSlip) {
            return Evaluate(slip, m_asymptotePoints) * m_stiffness;
        }
        else {
            return m_asymptoteValue * m_stiffness;
        }
    }

    private float Evaluate(float slip, WheelFrictionCurvePoint[] curvePoints) {
        int top = m_arraySize - 1;
        int bottom = 0;
        int index = (int) ( ( top + bottom ) * 0.5f );

        WheelFrictionCurvePoint result = curvePoints[index];

        //Binary search the curve to find the corresponding t value for the given slip
        while (( top != bottom && top - bottom > 1 )) {
            if (result.SlipForcePoint.x <= slip) {
                bottom = index;
            }
            else if (result.SlipForcePoint.x >= slip) {
                top = index;
            }

            index = (int) ( ( top + bottom ) * 0.5f );
            result = curvePoints[index];
        }

        float slip1 = curvePoints[bottom].SlipForcePoint.x;
        float slip2 = curvePoints[top].SlipForcePoint.x;
        float force1 = curvePoints[bottom].SlipForcePoint.y;
        float force2 = curvePoints[top].SlipForcePoint.y;

        float slipFraction = ( slip - slip1 ) / ( slip2 - slip1 );

        return force1 * ( 1 - slipFraction ) + force2 * ( slipFraction );
        ;
    }

    private Vector2 Hermite(float t, Vector2 p0, Vector2 p1, Vector2 m0, Vector2 m1) {
        float t2 = t * t;
        float t3 = t2 * t;

        return
            ( 2 * t3 - 3 * t2 + 1 ) * p0 +
            ( t3 - 2 * t2 + t ) * m0 +
            ( -2 * t3 + 3 * t2 ) * p1 +
            ( t3 - t2 ) * m1;
    }
}
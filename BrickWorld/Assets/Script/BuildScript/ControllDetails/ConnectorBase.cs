﻿using Photon.Pun;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectorBase : MonoBehaviour, IDetailControll, ISyncParams, IDetailJoint {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (value == false) {
                DisconnectJoint();
                Destroy(Trigger);
            }
            _disconnect = value;
        }
    }

    bool _disconnect;

    public GameObject PosObject;

    public JointBuild Joint;

    public bool isConnect;

    private bool attemp_fasten;
    public void UpdateThisBase(BaseInBuild new_base) {
    }

    public float Fasten {
        get {
            return _fasten;
        }
        set {
            if (value != 0 && _fasten == 0 && !isConnect) {
                _fasten = value;
                Connected();
                return;
            }
            _fasten = value;
        }
    }

    float _fasten = 0f;
    public float Unfasten {
        get {
            return _unfasten;
        }
        set {

            if (value != 0 && _unfasten == 0 && isConnect) {
                _unfasten = value;
                Joint.DestroyJoint();
                return;
            }
            _unfasten = value;
        }
    }

    public BuildingProperties BaseBuilding { get; set; }
    public int Index { get; set; }

    float _unfasten = 0f;

    public GameObject Trigger;

    private void Awake() {
        Trigger = new GameObject("Trigger");
        Trigger.transform.parent = gameObject.transform;
        Trigger.transform.localRotation = Quaternion.Euler(Vector3.zero);
        Trigger.transform.localPosition = Vector3.zero;
        Trigger.layer = LayerMask.NameToLayer("Interaction");
        BoxCollider[] boxs = GetComponents<BoxCollider>();
        for (int i = 0; i < boxs.Length; i++) {
            BoxCollider box = Trigger.AddComponent<BoxCollider>();
            box.size = boxs[i].size;
            box.center = boxs[i].center;
            box.isTrigger = true;
        }
    }

    public void Connected() {
        if (BaseBuilding.IsMainBuilding && !isConnect && !attemp_fasten) {
            ConnectConSet[] connects = ConnectConSet.PosConnects.ToArray();
            float dist = 100;
            int index = -1;
            for (int i = 0; i < connects.Length; i++) {
                float d = Vector3.Distance(connects[i].PosObject.transform.position, PosObject.transform.position);
                if (d < 1f && dist > d) {
                    dist = d;
                    index = i;
                }
            }
            if (index != -1) {
                StartCoroutine(AttempFasten(index));
            }
        }
        else {
            BaseBuilding.SyncParamsDetail(this, 0, _fasten);
        }
    }

    IEnumerator AttempFasten(int index) {
        attemp_fasten = true;
        Rigidbody rb = ConnectConSet.PosConnects.ToArray()[index].GetComponent<DetailBuild>().Base.rigidbody;
        GameObject point = ConnectConSet.PosConnects.ToArray()[index].PosObject;
        for (int i = 0; i < 100; i++) {
            if (Vector3.Distance(point.transform.position, PosObject.transform.position) < 0.009f) {
                PhysicsConnect pc = new PhysicsConnect();
                pc.Type = TypePhysicsConnect.Connect;
                pc.BaseDetail = gameObject;
                pc.ConnectDetail = ConnectConSet.PosConnects.ToArray()[index].gameObject;
                GetComponent<DetailBuild>().PhysicsConnects.Add(pc);

                JointBuild jb = new JointBuild();
                jb.XCod = UnityEngine.Random.Range(0, 99999999) + "";
                jb.BaseDetail = pc.BaseDetail.GetComponent<DetailBuild>();
                jb.ConnectDetail = pc.ConnectDetail.GetComponent<DetailBuild>();
                jb.Type = pc.Type;
                jb.Create();
                GetComponent<DetailBuild>().Building.Joints.Add(jb);
                attemp_fasten = false;
                yield break;
            }
            if (!rb) {
                attemp_fasten = false;
                yield break;
            }
            rb.AddForceAtPosition(( PosObject.transform.position - point.transform.position ).normalized * rb.mass / 2f, point.transform.position, ForceMode.Impulse);
            yield return new WaitForFixedUpdate();
        }
        attemp_fasten = false;
        yield break;
    }

    public void DisconnectJoint() {
        if (BaseBuilding.IsMainBuilding) {
            Joint.DestroyJoint();
            isConnect = false;
            GetComponent<DetailBuild>().PhysicsConnects.Clear();
            attemp_fasten = false;
        }
        else {
            BaseBuilding.SyncParamsDetail(this, 1, _unfasten);
        }
    }

    private void OnDisable() {
        DisconnectJoint();
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {

    }

    public void UpdateParams(int index_param, object param) {
        if (index_param == 0) {
            if (BaseBuilding.IsMainBuilding)
                Fasten = (float) param;
            else
                _fasten = (float) param;
        }
        else {
            if (BaseBuilding.IsMainBuilding)
                Unfasten = (float) param;
            else
                _unfasten = (float) param;
        }
    }

    public void AcceptStartParameters(object[] objects) {
        _fasten = (float) objects[0];
        _unfasten = (float) objects[1];
    }

    public object[] SendUploaded() {
        return new object[2] { _fasten, _unfasten };
    }

    public void CreateJoint() {

    }

    public void DestroyJoint() {

    }
}

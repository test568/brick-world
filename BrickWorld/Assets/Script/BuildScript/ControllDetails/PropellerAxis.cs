﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerAxis : MonoBehaviour, IDetailControll, ISyncParams {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect { get; set; }
    public BuildingProperties BaseBuilding { get; set; }
    public int Index { get; set; }

    public FreedbackWire RotateTransmission = new FreedbackWire();

    Propeller propeller;

    RotationForce rotation;

    BaseInBuild _base;

    float rotate;

    private void Update() {
        if (UpdateFunctions != null && !Disconnect && _base != null && _base.Base.IsMainBuilding) {
            UpdateFunctions();
        }
    }

    private void FixedUpdate() {
        if (BaseBuilding.IsMainBuilding && !Disconnect && propeller) {
            Rigidbody rb = _base.rigidbody;
            float max = ( propeller.GetForce() * Mathf.Abs(rotation.Revs) ) * 0.05f;

            float current = ( Vector3.Dot(rb.velocity.normalized, transform.up) * rb.velocity.sqrMagnitude );
            rotate = rotation.Revs * propeller.GetForce() * 0.1f;
            propeller.transform.Rotate(new Vector3(0, rotate, 0), Space.Self);
            rb.AddForceAtPosition(propeller.transform.up * Mathf.Clamp(( max - current ), 0, max) * 800f * -Mathf.Sign(rotation.Revs), propeller.transform.position);
        }
        else if (!BaseBuilding.IsMainBuilding) {
            propeller.transform.Rotate(new Vector3(0, rotate, 0), Space.Self);
        }
        else {
            rotate = 0;
        }
    }

    public void TransmissionUpdate() {
        RotationForce rt = new RotationForce();
        rt.Forse = -1f;
        List<object> rts = FreedbackWire.TakeWireConnect(RotateTransmission);
        for (int i = 0; i < rts.Count; i++) {
            rt.Revs += ( (RotationForce) rts[i] ).Revs;
            rt.Forse += ( (RotationForce) rts[i] ).Forse;
        }
        if (float.IsNaN(rt.Revs) || float.IsInfinity(rt.Revs))
            rt.Revs = 0f;
        if (float.IsNaN(rt.Forse) || float.IsInfinity(rt.Forse))
            rt.Forse = -1f;
        rotation = rt;
    }

    public void UpdateThisBase(BaseInBuild new_base) {
        _base = new_base;
    }

    public void SetPropeller(Propeller propeller) {
        this.propeller = propeller;
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(rotate);
        }
        else {
            rotate = (float) stream.ReceiveNext();
        }
    }

    public void UpdateParams(int index_param, object param) {

    }

    public void AcceptStartParameters(object[] objects) {

    }

    public object[] SendUploaded() {
        return new object[0];
    }
}

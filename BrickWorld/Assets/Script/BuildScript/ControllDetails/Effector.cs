﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effector : MonoBehaviour, IDetailControll {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect { get; set; }

    public EffectEmite GetExtractor {
        get {
            return _effectEmite;
        }
        set {
            _effectEmite = value;
            if (!Disconnect && effect == null) {
                GameObject obj = Instantiate(value.Effect, transform);
                effect = obj.GetComponent<ParticleSystem>();
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.identity;
                effect.Play();
            }
            if (!Disconnect)
                effect.emissionRate = value.Force * 10f;
            else {
                if (effect)
                    Destroy(effect.gameObject);
            }
        }
    }

    EffectEmite _effectEmite;

    ParticleSystem effect;

    public void UpdateThisBase(BaseInBuild new_base) {
    }
}


public struct EffectEmite {
    public GameObject Effect;
    public float Force;
}
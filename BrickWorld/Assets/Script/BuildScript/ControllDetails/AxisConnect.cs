﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisConnect : MonoBehaviour, IDetailControll, ISyncParams, IDetailJoint {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }

    public FreedbackWire RotateTransmission = new FreedbackWire();

    public string Cod;

    public GameObject PosObject;

    public BaseInBuild BaseGroup;
    public BaseInBuild ConnectGroup;

    public HingeJoint Hinge;


    public bool Spring;
    public float Elasticity;
    public float Damping;

    public bool Limit;
    public float Min;
    public float Max;

    public bool Rotate;
    public float Speed;

    public bool RotateToTransmission;

    public float revs;
    public float forse;


    public float LeftRotate {
        get {
            return _left_rot;
        }
        set {
            _left_rot = value;
        }
    }

    float _left_rot;

    public float RightRotate {
        get {
            return _right_rot;
        }
        set {
            _right_rot = value;
        }
    }

    public bool Disconnect { get; set; }
    public BuildingProperties BaseBuilding { get; set; }
    public int Index { get; set; }

    private FixedJoint fixed_joint;

    float _right_rot;

    private GameObject old_base_group;
    private GameObject old_connect_group;

    public float TargetAngle;
    private Vector3 ReletiveAxe;



    private Vector3 ReletiveCrossAxe;
    private Transform BaseGroupTransform;
    private Transform ConnectGroupTransform;
    private Rigidbody ConnectGroupRigidbody;
    public bool UseTargetAngle;

    Vector3 connect_local_axis;

    Vector3 axis;
    Vector3 anhor;
    Vector3 connectAnhor;

    Vector3 cross_local_axis;
    Vector3 cross_base_axis;

    private void Awake() {
        if (Rotate) {
            Stop();
        }
    }

    private void Update() {
        if (BaseBuilding.IsMainBuilding) {

            if (Hinge) {
                if (Rotate && !RotateToTransmission && _right_rot != 0) {
                    NoStop();
                    RotateRight();
                }
                else if (Rotate && !RotateToTransmission && _left_rot != 0) {
                    NoStop();
                    RotateLeft();
                }
                else {
                    Stop();
                }
            }


            if (RotateToTransmission && !Disconnect) {
                TransmissionUpdate();
            }
        }
    }
    private void LateUpdate() {
        // old_base_group = BaseGroup.gameObject;
        //old_connect_group = ConnectGroup.gameObject;
    }

    private void OnDestroy() {
        Destroy(Hinge);
    }

    public void TransmissionUpdate() {
        RotationForce rt = new RotationForce();
        List<object> rts = FreedbackWire.TakeWireConnect(RotateTransmission);
        for (int i = 0; i < rts.Count; i++) {
            revs = ( (RotationForce) rts[i] ).Revs;
            rt.Revs += ( (RotationForce) rts[i] ).Revs;
            rt.Forse += ( (RotationForce) rts[i] ).Forse;
        }
        JointMotor motor = new JointMotor();
        motor.freeSpin = true;
        motor.force = rt.Forse * 30;
        motor.targetVelocity = rt.Revs * 3f;
        ( (HingeJoint) Hinge ).useMotor = true;
        ( (HingeJoint) Hinge ).motor = motor;
        forse = rt.Forse * 30f;
        revs = rt.Revs * 3f;
    }

    public void CreateJoint() {
        if (Rotate) {
            Stop();
        }
    }

    public void DestroyJoint() {
        if (Rotate) {
            NoStop();
        }
    }

    public void Rebuild() {
        ConnectGroupRigidbody = ConnectGroup.rigidbody;
        ConnectGroupTransform = ConnectGroup.gameObject.transform;
        BaseGroupTransform = BaseGroup.gameObject.transform;

        if (!Rotate) {
            axis = Hinge.axis;
            anhor = Hinge.anchor;
            connectAnhor = Hinge.connectedAnchor;
        }
        else {
            axis = BaseGroup.gameObject.transform.InverseTransformDirection(PosObject.transform.up);
            anhor = BaseGroup.gameObject.transform.InverseTransformPoint(PosObject.transform.position);
            connectAnhor = ConnectGroup.gameObject.transform.InverseTransformPoint(PosObject.transform.position);
        }





        Vector3 cross_axe;
        if (Mathf.Abs(Vector3.Dot(axis, Vector3.forward)) != 1)
            cross_axe = Vector3.Cross(axis, Vector3.forward);
        else
            cross_axe = Vector3.Cross(axis, Vector3.up);

        cross_base_axis = cross_axe;

        connect_local_axis = ConnectGroupTransform.InverseTransformDirection(BaseGroupTransform.TransformDirection(axis));
        cross_local_axis = ConnectGroupTransform.InverseTransformDirection(BaseGroupTransform.TransformDirection(cross_axe));




        ReletiveCrossAxe = BaseGroupTransform.InverseTransformDirection(ConnectGroupTransform.TransformDirection(cross_axe));

    }


    public void Scync(float value) {

        if (BaseGroup != null && ConnectGroup != null && !Disconnect) {



            Quaternion additionalRotation = Quaternion.Inverse(Quaternion.LookRotation(connect_local_axis, cross_local_axis));



            Vector3 axis = ConnectGroupTransform.TransformDirection(connect_local_axis);
            Vector3 anhor;
            if (UseTargetAngle) {
                anhor = Quaternion.AngleAxis(-TargetAngle, axis) * BaseGroupTransform.TransformDirection(ReletiveCrossAxe);
            }
            else {
                anhor = ConnectGroupTransform.TransformDirection(cross_local_axis);
            }
            Vector3 base_pos = BaseGroupTransform.TransformPoint(this.anhor);
            Vector3 connect_pos = ConnectGroupTransform.TransformPoint(connectAnhor);

            ConnectGroupTransform.rotation = Quaternion.LookRotation(axis, anhor) * additionalRotation;
            ConnectGroupTransform.position += ( base_pos - connect_pos );

            if (Hinge) {
                Hinge.anchor = BaseGroup.gameObject.transform.InverseTransformPoint(PosObject.transform.position);
                //  Hinge.connectedAnchor = ConnectGroup.gameObject.transform.InverseTransformPoint(PosObject.transform.position);
                //    Hinge.axis = BaseGroup.gameObject.transform.InverseTransformDirection(PosObject.transform.up);
            }

        }
    }

    private void RotateLeft() {

        // TargetAngle += Speed / 1000f;
        // if (Hinge && Limit)
        //     TargetAngle = Mathf.Clamp(TargetAngle, Min + 1f, Max - 1f);
        JointMotor motor = new JointMotor();
        motor.targetVelocity = Speed;
        motor.force = BaseGroup.rigidbody.mass * ConnectGroup.rigidbody.mass;
        Hinge.motor = motor;

    }
    private void RotateRight() {
        // TargetAngle -= Speed / 1000f;
        // if (Hinge && Limit)
        //   TargetAngle = Mathf.Clamp(TargetAngle, Min + 1f, Max - 1f);

        JointMotor motor = new JointMotor();
        motor.targetVelocity = -Speed;
        motor.force = BaseGroup.rigidbody.mass * ConnectGroup.rigidbody.mass;
        Hinge.motor = motor;

    }
    private void Stop() {
        if (!fixed_joint) {
            if (Hinge)
                Hinge.useMotor = false;
            fixed_joint = BaseGroup.gameObject.AddComponent<FixedJoint>();
            fixed_joint.connectedBody = ConnectGroup.rigidbody;
        }
    }
    private void NoStop() {
        if (fixed_joint) {
            Destroy(fixed_joint);
            if (Hinge)
                Hinge.useMotor = true;
        }
    }

    Vector3 GetAxis() {
        return ConnectGroupTransform.TransformDirection(cross_local_axis);
    }

    Vector3 GetCrossAxis() {
        if (UseTargetAngle) {
            return Quaternion.AngleAxis(-TargetAngle, GetAxis()) * BaseGroupTransform.TransformDirection(ReletiveCrossAxe);
        }
        return ConnectGroupTransform.TransformDirection(cross_local_axis);
    }

    public void UpdateThisBase(BaseInBuild new_base) {
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {

    }

    public void UpdateParams(int index_param, object param) {

    }

    public void AcceptStartParameters(object[] objects) {

    }

    public object[] SendUploaded() {
        return new object[0];
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TypeWheel {
    CarWheel,
    TankWheel
}

public class WheelCollision : MonoBehaviour, IDetailControll {

    public BaseInBuild BasePhys;

    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            _disconnect = value;
            UpdateDisconnect();
        }
    }

    bool _disconnect = false;

    public float Friction = 1f;

    public TypeWheel TypeWheel;

    public float Radius;

    protected virtual void UpdateBase(BaseInBuild new_base) {
    }

    protected virtual void UpdateDisconnect() {

    }

    public void UpdateThisBase(BaseInBuild new_base) {
        UpdateBase(new_base);
        BasePhys = new_base;
    }

    public virtual float GetRadius() {
        return Radius;
    }
}



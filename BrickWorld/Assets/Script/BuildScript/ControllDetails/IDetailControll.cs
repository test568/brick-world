﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDetailControll {

    FreedbackWire.LoadMetods UpdateFunctions { get; set; }

    bool Disconnect { get; set; }

    void UpdateThisBase(BaseInBuild new_base);
    }

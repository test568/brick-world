﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propeller : MonoBehaviour, IDetailControll {

    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }

    public bool Disconnect { get; set; }

    public int CountBlade;

    public float Widht;

    public float Length;

    float force {
        get {
            if (_force == -1) {
                _force = Widht * Length * CountBlade;
            }
            return _force;
        }
    }

    float _force = -1;

    public void UpdateThisBase(BaseInBuild new_base) {

    }

    public float GetForce() {
        return force;
    }
}

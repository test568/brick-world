﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class PlayerChair : MonoBehaviour, IDetailControll, ISyncParams, IObserverPlayerEnterAndLeft {

    public string Cod { get; set; }
    public bool isSeat { get; private set; }

    [HideInInspector]
    public GameObject Trigger;

    public BuildingProperties BaseBuilding {
        get {
            return bs;
        }
        set {
            bs = value;
        }
    }
    BuildingProperties bs;

    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            _disconnect = value;
            if (value == true) {
                if (UpdateFunctions != null)
                    UpdateFunctions();
            }
        }
    }

    private bool _disconnect;

    public float Speed {
        get {
            return _speed;

        }
        set {
            if (BaseBuilding.IsMainBuilding)
                _speed = value;
        }
    }

    float _speed = -1;

    public float RPM {
        get {
            return _rpm;
        }
        set {
            if (BaseBuilding.IsMainBuilding)
                _rpm = value;
        }
    }

    float _rpm = -1;

    public TypeGear Gear {
        get {
            return _gear;
        }
        set {
            if (BaseBuilding.IsMainBuilding)
                _gear = value;
        }
    }

    TypeGear _gear = TypeGear.None;

    public FreedbackWire FWire = new FreedbackWire();
    public FreedbackWire AWire = new FreedbackWire();
    public FreedbackWire DWire = new FreedbackWire();
    public FreedbackWire SWire = new FreedbackWire();
    public FreedbackWire WWire = new FreedbackWire();
    public FreedbackWire SteeringWire = new FreedbackWire();
    public FreedbackWire ThrottleWire = new FreedbackWire();
    public FreedbackWire BrakeWire = new FreedbackWire();
    public FreedbackWire GearShiftWire = new FreedbackWire();
    public FreedbackWire EngineActiveWire = new FreedbackWire();
    public FreedbackWire TechnicalModeWire = new FreedbackWire();
    public FreedbackWire FrontLightWire = new FreedbackWire();
    public FreedbackWire RearLightWire = new FreedbackWire();



    private float x_ang;
    private float dist_camera;

    public string IdSetPlayer {
        get {
            return _id_player;
        }
        private set {
            _id_player = value;
            BaseBuilding.SyncParamsDetail(this, 0, value);
        }
    }

    [SerializeField] string _id_player = "";

    public static PlayerChair ChairFromPlayer { get; private set; }

    public int Index { get; set; }

    public enum TypeCamera {
        Forward = 1,
        ThirdPerson = 2,
        Camera = 3
    }

    public TypeCamera SetCamera;

    private void Awake() {
        GameRoom.AddObserver(this);
        x_ang = 0;
        dist_camera = 4f;
        SetCamera = TypeCamera.ThirdPerson;
        Trigger = new GameObject("Trigger");
        Trigger.transform.parent = gameObject.transform;
        Trigger.transform.localRotation = Quaternion.Euler(Vector3.zero);
        Trigger.transform.localPosition = Vector3.zero;
        Trigger.layer = LayerMask.NameToLayer("Interaction");
        Trigger.AddComponent<BoxCollider>();
        Trigger.GetComponent<BoxCollider>().center = gameObject.GetComponent<BoxCollider>().center;
        Trigger.GetComponent<BoxCollider>().size = gameObject.GetComponent<BoxCollider>().size;
        Trigger.GetComponent<BoxCollider>().isTrigger = true;
    }

    private void Update() {
        if (ChairFromPlayer == this && !ControllerPlayer.PlayerOnPause) {
            if (isSeat) {
                if (SetCamera == TypeCamera.Forward && Input.GetKeyDown(KeyCode.V)) {
                    SetCamera = TypeCamera.ThirdPerson;
                }
                else if (SetCamera == TypeCamera.ThirdPerson && Input.GetKeyDown(KeyCode.V)) {
                    SetCamera = TypeCamera.Forward;
                    ControllerPlayer.MainPlayer.MainCamera.transform.localPosition = new Vector3(0, 0.8f, 0);
                }
            }
            if (isSeat && Input.GetKeyDown(KeyCode.E)) {
                GetUp();
            }

            if (UpdateFunctions != null && !Disconnect) {
                UpdateFunctions();
            }
            UpdateDevices();
        }
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {

            stream.SendNext(Gear);
            stream.SendNext(RPM);
            stream.SendNext(Speed);
        }
        else {

            _gear = (TypeGear) stream.ReceiveNext();
            _rpm = (float) stream.ReceiveNext();
            _speed = (float) stream.ReceiveNext();
        }
    }

    private void LateUpdate() {

        if (ControllerPlayer.MainPlayer.Status == PlayerStatus.inChair && isSeat && !ControllerPlayer.PlayerOnPause && !ControllerPlayer.LockControll) {
            if (SetCamera == TypeCamera.Forward) {
                ControllerPlayer.MainPlayer.MainCamera.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * 140f * Time.deltaTime, 0));
                if (x_ang - Input.GetAxis("Mouse Y") * 140f * Time.deltaTime < 80f && x_ang - Input.GetAxis("Mouse Y") * 140f * Time.deltaTime > -80f) {
                    x_ang -= Input.GetAxis("Mouse Y") * 140f * Time.deltaTime;
                    ControllerPlayer.MainPlayer.MainCamera.transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y") * 140f * Time.deltaTime, 0, 0));
                }
                ControllerPlayer.MainPlayer.MainCamera.transform.localRotation = Quaternion.Euler(new Vector3(ControllerPlayer.MainPlayer.MainCamera.transform.localEulerAngles.x, ControllerPlayer.MainPlayer.MainCamera.transform.localEulerAngles.y, 0));
            }
            else if (SetCamera == TypeCamera.ThirdPerson) {
                ControllerPlayer.MainPlayer.MainCamera.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * 140f * Time.deltaTime, 0));
                if (x_ang - Input.GetAxis("Mouse Y") * 140f * Time.deltaTime < 80f && x_ang - Input.GetAxis("Mouse Y") * 140f * Time.deltaTime > -80f) {
                    x_ang -= Input.GetAxis("Mouse Y") * 140f * Time.deltaTime;
                    ControllerPlayer.MainPlayer.MainCamera.transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y") * 140f * Time.deltaTime, 0, 0));
                }
                ControllerPlayer.MainPlayer.MainCamera.transform.localRotation = Quaternion.Euler(new Vector3(ControllerPlayer.MainPlayer.MainCamera.transform.localEulerAngles.x, ControllerPlayer.MainPlayer.MainCamera.transform.localEulerAngles.y, 0));
                RaycastHit hit;
                if (Physics.Raycast(gameObject.transform.position + new Vector3(0, 0.2f, 0), ( -ControllerPlayer.MainPlayer.MainCamera.transform.forward ).normalized, out hit, dist_camera, LayerMask.GetMask(new string[1] { "Ground" }))) {
                    ControllerPlayer.MainPlayer.MainCamera.transform.position = hit.point;
                }
                else {
                    ControllerPlayer.MainPlayer.MainCamera.transform.position = gameObject.transform.position + ( -ControllerPlayer.MainPlayer.MainCamera.transform.forward * dist_camera );
                }
            }
        }
    }

    public void UpdateParams(int index_param, object param) {
        if (index_param == 0) {
            _id_player = (string) param;
        }
        else if (index_param == 1) {
            _is_f = (float) param;
            FreedbackWire.UpdateWireConnect(FWire, _is_f);
        }
        else if (index_param == 2) {
            _is_w = (float) param;
            FreedbackWire.UpdateWireConnect(WWire, _is_w);
        }
        else if (index_param == 3) {
            _is_s = (float) param;
            FreedbackWire.UpdateWireConnect(SWire, _is_s);
        }
        else if (index_param == 4) {
            _is_a = (float) param;
            FreedbackWire.UpdateWireConnect(AWire, _is_a);
        }
        else if (index_param == 5) {
            _is_a = (float) param;
            FreedbackWire.UpdateWireConnect(DWire, _is_a);
        }

        else if (index_param == 6) {
            _is_steering = (float) param;
            FreedbackWire.UpdateWireConnect(SteeringWire, _is_steering);
        }
        else if (index_param == 7) {
            _is_throttle = (float) param;
            FreedbackWire.UpdateWireConnect(ThrottleWire, _is_throttle);
        }
        else if (index_param == 8) {
            _is_brake = (float) param;
            FreedbackWire.UpdateWireConnect(BrakeWire, _is_brake);
        }
        else if (index_param == 9) {
            _is_gear_shift = (float) param;
            FreedbackWire.UpdateWireConnect(GearShiftWire, _is_gear_shift);
        }
        else if (index_param == 10) {
            _is_engine_active = (float) param;
            FreedbackWire.UpdateWireConnect(EngineActiveWire, _is_engine_active);
        }
        else if (index_param == 11) {
            _is_technical_mode = (float) param;
            FreedbackWire.UpdateWireConnect(TechnicalModeWire, _is_technical_mode);
        }
        else if (index_param == 12) {
            _is_front_light = (float) param;
            FreedbackWire.UpdateWireConnect(FrontLightWire, _is_front_light);
        }
        else if (index_param == 13) {
            _is_rear_light = (float) param;
            FreedbackWire.UpdateWireConnect(RearLightWire, _is_rear_light);
        }
    }

    public void UpdateThisBase(BaseInBuild new_base) {
    }

    public void AcceptStartParameters(object[] objects) {
        _id_player = (string) objects[0];
        if (IdSetPlayer != "") {
            isSeat = true;
        }
    }

    public object[] SendUploaded() {
        return new object[1] { IdSetPlayer };
    }

    public void GetUp() {
        ChairFromPlayer = null;
        isSeat = false;
        if (GameRoom.isMultiplayer) {
            IdSetPlayer = "";
        }
        ControllerPlayer.MainPlayer.Devices.IsDrive = false;
        ControllerPlayer.MainPlayer.PlayerObject.transform.position = transform.position + new Vector3(0, 3, 0);
        ControllerPlayer.MainPlayer.Status = PlayerStatus.Move;
        if (UpdateFunctions != null && !Disconnect)
            UpdateFunctions();
    }
    public void SitDown() {
        if (IdSetPlayer == "") {
            ChairFromPlayer = this;
            ControllerPlayer.MainPlayer.Status = PlayerStatus.inChair;
            isSeat = true;
            ControllerPlayer.MainPlayer.MainCamera.transform.parent = gameObject.transform;
            ControllerPlayer.MainPlayer.MainCamera.transform.localPosition = new Vector3(0, 0.8f, 0);
            ControllerPlayer.MainPlayer.MainCamera.transform.localRotation = Quaternion.identity;
            x_ang = 0;
            ControllerPlayer.MainPlayer.Devices.IsDrive = true;
            if (GameRoom.isMultiplayer) {
                IdSetPlayer = PhotonNetwork.LocalPlayer.ActorNumber + "";
            }
        }
    }

    #region KeyF

    public void GetButtonF() {
        if (Input.GetKey(KeyCode.F) && isSeat && !Disconnect && !ControllerPlayer.LockControll)
            is_f = 1f;
        else
            is_f = 0f;
    }

    private float is_f {
        get {
            return _is_f;
        }
        set {
            if (value != _is_f) {
                Debug.Log(BaseBuilding);
                FreedbackWire.UpdateWireConnect(FWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 1, value);
                }
            }
            _is_f = value;
        }
    }

    float _is_f = 0f;

    #endregion

    #region KeyW

    public void GetButtonW() {
        if (Input.GetKey(KeyCode.W) && isSeat && !Disconnect && !ControllerPlayer.LockControll)
            is_w = 1f;
        else
            is_w = 0f;
    }

    private float is_w {
        get {
            return _is_w;
        }
        set {
            if (value != _is_w) {
                FreedbackWire.UpdateWireConnect(WWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 2, value);
                }
            }
            _is_w = value;
        }
    }
    float _is_w = 0f;

    #endregion

    #region KeyS

    public void GetButtonS() {
        if (Input.GetKey(KeyCode.S) && isSeat && !Disconnect && !ControllerPlayer.LockControll)
            is_s = 1f;
        else
            is_s = 0f;
    }

    private float is_s {
        get {
            return _is_s;
        }
        set {
            if (value != _is_s) {
                FreedbackWire.UpdateWireConnect(SWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 3, value);
                }
            }
            _is_s = value;
        }
    }
    float _is_s = 0f;

    #endregion

    #region KeyA

    public void GetButtonA() {
        if (Input.GetKey(KeyCode.A) && isSeat && !Disconnect && !ControllerPlayer.LockControll)
            is_a = 1f;
        else
            is_a = 0f;
    }

    private float is_a {
        get {
            return _is_a;
        }
        set {
            if (value != _is_a) {
                FreedbackWire.UpdateWireConnect(AWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 4, value);
                }
            }
            _is_a = value;
        }
    }
    float _is_a = 0f;

    #endregion

    #region KeyD

    public void GetButtonD() {
        if (Input.GetKey(KeyCode.D) && isSeat && !Disconnect && !ControllerPlayer.LockControll)
            is_d = 1f;
        else
            is_d = 0f;
    }

    private float is_d {
        get {
            return _is_d;
        }
        set {
            if (value != _is_d) {
                FreedbackWire.UpdateWireConnect(DWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 5, value);
                }
            }
            _is_d = value;
        }
    }
    float _is_d = 0f;

    #endregion

    #region KeySteering

    public void GetSteering() {
        if (isSeat && !Disconnect && !ControllerPlayer.LockControll) {
            if (Input.GetKey(KeyCode.D))
                is_steering = 1f;
            else if (Input.GetKey(KeyCode.A))
                is_steering = -1f;
            else
                is_steering = 0f;
        }
        else
            is_steering = 0f;
    }

    private float is_steering {
        get {
            return _is_steering;
        }
        set {
            if (value != _is_steering) {
                FreedbackWire.UpdateWireConnect(SteeringWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 6, value);
                }
            }
            _is_steering = value;
        }
    }
    float _is_steering = 0f;

    #endregion



    #region KeyThrottle

    public void GetThrottle() {
        if (isSeat && !Disconnect && !ControllerPlayer.LockControll) {
            if (Input.GetKey(KeyCode.W))
                is_throttle = 1f;
            else if (Input.GetKey(KeyCode.S))
                is_throttle = -1f;
            else
                is_throttle = 0f;
        }
        else
            is_throttle = 0f;
    }

    private float is_throttle {
        get {
            return _is_throttle;
        }
        set {
            if (value != _is_throttle) {
                FreedbackWire.UpdateWireConnect(ThrottleWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 7, value);
                }
            }
            _is_throttle = value;
        }
    }
    float _is_throttle = 0f;

    #endregion


    #region KeyBrake

    public void GetBrake() {
        if (isSeat && !Disconnect && !ControllerPlayer.LockControll) {
            if (Input.GetKey(KeyCode.Space))
                is_brake = 1f;
            else
                is_brake = 0f;
        }
        else
            is_brake = 0f;
    }

    private float is_brake {
        get {
            return _is_brake;
        }
        set {
            if (value != _is_brake) {
                FreedbackWire.UpdateWireConnect(BrakeWire, value);

                if (value > 0) {
                    ControllerPlayer.MainPlayer.Devices.IsHandbrake = true;
                }
                else {
                    ControllerPlayer.MainPlayer.Devices.IsHandbrake = false;
                }
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 8, value);
                }
            }
            _is_brake = value;
        }
    }
    float _is_brake = 0f;

    #endregion


    #region KeyGearShift

    public void GetGearShift() {
        if (isSeat && !Disconnect && !ControllerPlayer.LockControll) {
            if (Input.GetKey(KeyCode.LeftShift))
                is_gear_shift = 1f;
            else if (Input.GetKey(KeyCode.LeftControl))
                is_gear_shift = -1f;
            else
                is_gear_shift = 0f;
        }
        else
            is_gear_shift = 0f;
    }

    private float is_gear_shift {
        get {
            return _is_gear_shift;
        }
        set {
            if (value != _is_gear_shift) {
                FreedbackWire.UpdateWireConnect(GearShiftWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 9, value);
                }
            }
            _is_gear_shift = value;
        }
    }
    float _is_gear_shift = 0f;

    #endregion



    #region KeyEngineActive

    public void GetEngineActive() {
        if (isSeat) {
            is_engine_active = 1f;
        }
        else {
            is_engine_active = 0f;
        }
    }

    private float is_engine_active {
        get {
            return _is_engine_active;
        }
        set {
            if (value != _is_engine_active) {
                FreedbackWire.UpdateWireConnect(EngineActiveWire, value);
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 10, value);
                }
            }
            _is_engine_active = value;
        }
    }
    float _is_engine_active = 0f;

    #endregion


    #region KeyTechnicalMode

    public void GetTechnicalMode() {
        if (Input.GetKeyDown(KeyCode.LeftAlt)) {
            if (is_technical_mode > 0) {
                is_technical_mode = 0;
            }
            else {
                is_technical_mode = 1;
            }
        }
    }

    private float is_technical_mode {
        get {
            return _is_technical_mode;
        }
        set {
            if (value != _is_technical_mode) {
                FreedbackWire.UpdateWireConnect(TechnicalModeWire, value);

                if (value > 0) {
                    ControllerPlayer.MainPlayer.Devices.IsAltMode = true;
                }
                else {
                    ControllerPlayer.MainPlayer.Devices.IsAltMode = false;
                }
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 11, value);
                }
            }
            _is_technical_mode = value;
        }
    }
    float _is_technical_mode = 0f;

    #endregion


    #region KeyFrontLight

    public void GetFrontLight() {
        if (Input.GetKeyDown(KeyCode.L)) {
            Debug.Log(200);
            if (is_front_light > 0) {
                is_front_light = 0;
            }
            else {
                is_front_light = 1;
            }
        }
    }

    private float is_front_light {
        get {
            return _is_front_light;
        }
        set {
            if (value != _is_front_light) {
                FreedbackWire.UpdateWireConnect(FrontLightWire, value);

                if (value > 0) {
                    ControllerPlayer.MainPlayer.Devices.IsFrontLight = true;
                }
                else {
                    ControllerPlayer.MainPlayer.Devices.IsFrontLight = false;
                }
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 12, value);
                }
            }
            _is_front_light = value;
        }
    }
    float _is_front_light = 0f;

    #endregion


    #region KeyRearLight

    public void GetRearLight() {
        if (Input.GetKeyDown(KeyCode.J)) {
            if (is_rear_light > 0) {
                is_rear_light = 0;
            }
            else {
                is_rear_light = 1;
            }
        }
    }

    private float is_rear_light {
        get {
            return _is_rear_light;
        }
        set {
            if (value != _is_rear_light) {
                FreedbackWire.UpdateWireConnect(RearLightWire, value);

                if (value > 0) {
                    ControllerPlayer.MainPlayer.Devices.IsRearLight = true;
                }
                else {
                    ControllerPlayer.MainPlayer.Devices.IsRearLight = false;
                }
                if (GameRoom.isMultiplayer) {
                    BaseBuilding.SyncParamsDetail(this, 13, value);
                }
            }
            _is_rear_light = value;
        }
    }
    float _is_rear_light = 0f;

    #endregion


    private void UpdateDevices() {
        if (isSeat && !Disconnect) {
            ControllerPlayer.MainPlayer.Devices.Gear = Gear;
            ControllerPlayer.MainPlayer.Devices.Speed = Speed;
            ControllerPlayer.MainPlayer.Devices.RPM = RPM;
        }
    }

    private void OnDestroy() {
        if (isSeat) {
            GetUp();
        }
        GameRoom.RemoveObserver(this);
    }

    public void LeftPlayer(Player player) {
        Debug.Log("Left PLayer" + " " + player.ActorNumber + " | " + IdSetPlayer);
        if (player.ActorNumber.ToString() == IdSetPlayer)
            IdSetPlayer = "";
    }

    public void EntryPlayer(Player player) {

    }
}


﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Differential : MonoBehaviour, IDetailControll, ISyncParams {

    public string Cod;

    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }

    public FreedbackWire GetRotateWire = new FreedbackWire();

    public float GearFinal;

    [SerializeField] float CurrentRPM;

    [SerializeField] float CurrentTorque;


    private Engine base_engine;

    public RotationForce LeftRotate {
        get {
            if (!Disconnect) {
                _l_rotate.Revs = CurrentRPM;
                _l_rotate.Forse = CurrentTorque / 2f;
                _l_rotate.BaseEngine = base_engine;
            }
            else {
                _l_rotate.Revs = 0;
                _l_rotate.Forse = 0;
                _l_rotate.BaseEngine = default;
            }
            return _l_rotate;
        }
        private set { }
    }

    RotationForce _l_rotate;

    public RotationForce RightRotate {
        get {
            if (!Disconnect) {
                _r_rotate.Revs = CurrentRPM;
                _r_rotate.Forse = CurrentTorque / 2f;
                _r_rotate.BaseEngine = base_engine;
            }
            else {
                _r_rotate.Revs = 0;
                _r_rotate.Forse = 0;
                _r_rotate.BaseEngine = default;
            }
            return _r_rotate;
        }
        private set { }
    }

    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (BaseBuilding.IsMainBuilding) {
                _disconnect = value;
                if (_disconnect) {
                    CurrentRPM = 0;
                    CurrentTorque = 0;
                    BaseBuilding.SyncParamsDetail(this, 2, true);
                }
            }
        }
    }

    bool _disconnect;
    public BuildingProperties BaseBuilding { get; set; }
    public int Index { get; set; }

    RotationForce _r_rotate;

    private void Update() {
        if (!Disconnect)
            GetRotate();
    }

    public void GetRotate() {
        CurrentRPM = 0;
        CurrentTorque = 0;
        RotationForce rt = new RotationForce();
        List<object> rts = FreedbackWire.TakeWireConnect(GetRotateWire);
        for (int i = 0; i < rts.Count; i++) {
            rt.Revs += ( (RotationForce) rts[i] ).Revs;
            rt.Forse += ( (RotationForce) rts[i] ).Forse;
        }
        CurrentRPM = rt.Revs * GearFinal;
        CurrentTorque = rt.Forse / Mathf.Abs(GearFinal);
        if (rts.Count != 0) {
            base_engine = ( (RotationForce) rts[0] ).BaseEngine;
        }
    }

    public void UpdateThisBase(BaseInBuild new_base) {
    }

    public void UpdateParams(int index_param, object param) {
       if (index_param == 0) {
            _disconnect = (bool) param;
        }
    }

    public void AcceptStartParameters(object[] objects) {
        Disconnect = (bool) objects[0];
    }

    public object[] SendUploaded() {
        return new object[1] { Disconnect };
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {
    }
}

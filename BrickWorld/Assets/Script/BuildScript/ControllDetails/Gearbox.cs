﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gearbox : MonoBehaviour, IDetailControll, ISyncParams {
    public string Cod;
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }

    public FreedbackWire GetRotateWire = new FreedbackWire();

    public FreedbackWire GetGearWire = new FreedbackWire();

    public float[] Gears = new float[10] { -1, 0, 1, 2, 3, 4, 5, 6, 7, 8 };

    public TypeGear GearT = TypeGear.N;

    public float MaxTorque;

    public bool Automatic;

    public float CurrentRPM;

    public float CurrentTorque;

    public float GearShift {
        get {
            return _gear_shift;
        }
        set {
            if (BaseBuilding.IsMainBuilding && !Disconnect) {
                if (value > 0f) {
                    GearShifted();
                    CurrentGear++;
                    CurrentGear = Mathf.Clamp(CurrentGear, 0, CountGear + 1);
                    _gear_shift = 1f;
                }
                else if (value < 0f) {
                    GearShifted();
                    CurrentGear--;
                    CurrentGear = Mathf.Clamp(CurrentGear, 0, CountGear);
                    _gear_down = 1f;
                }
                else {
                    _gear_shift = 0f;
                }

            }

        }
    }
    float _gear_shift = 0f;

    float _gear_down;
    public float Clutch {
        get {
            return _clutch;
        }
        set {
            if (BaseBuilding.IsMainBuilding) {
                if (_clutch == 0 && value != 0 && !Disconnect) {
                    _clutch = 1f;
                }
                else if (_clutch != 0 && value == 0) {
                    _clutch = 0f;
                }
            }
        }
    }

    public RotationForce Rotate {
        get {
            if (!Disconnect) {
                return _rotate;
            }
            else {
                return default;
            }
        }
        set => _rotate = value;
    }

    public int CountGear;
    int _currentGear = 1;


    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (BaseBuilding.IsMainBuilding) {
                _disconnect = value;
                if (_disconnect) {
                    BaseBuilding.SyncParamsDetail(this, 2, true);
                }
            }
        }
    }

    bool _disconnect;

    RotationForce _rotate;

    float _clutch;

    public int CurrentGear {
        get {
            return _currentGear;
        }
        set {
            _currentGear = value;
            if (value == 0) {
                GearT = TypeGear.R;
            }
            else if (value == 1) {
                GearT = TypeGear.N;
            }
            else if (value == 2) {
                GearT = TypeGear.Gear1;
            }
            else if (value == 3) {
                GearT = TypeGear.Gear2;
            }
            else if (value == 4) {
                GearT = TypeGear.Gear3;
            }
            else if (value == 5) {
                GearT = TypeGear.Gear4;
            }
        }
    }

    public BuildingProperties BaseBuilding { get; set; }
    public int Index { get; set; }

    public float SlowdownRMP;

    private void Update() {
        if (UpdateFunctions != null)
            UpdateFunctions();
    }

    public void GetRotate() {
        if (BaseBuilding.IsMainBuilding) {
            if (!Disconnect) {
                CurrentRPM = 0;
                CurrentTorque = 0;
                RotationForce rt = new RotationForce();
                List<object> rts = FreedbackWire.TakeWireConnect(GetRotateWire);
                for (int i = 0; i < rts.Count; i++) {
                    rt.Revs += ( (RotationForce) rts[i] ).Revs;
                    rt.Forse += ( (RotationForce) rts[i] ).Forse;
                }
                rt.Revs /= rts.Count;
                CurrentRPM = rt.Revs * Gears[CurrentGear];
                CurrentTorque = rt.Forse / Mathf.Abs(Gears[CurrentGear]);
                SlowdownRMP = 1f - Mathf.Abs(Gears[CurrentGear]);
                if (Clutch != 0) {
                    CurrentRPM = 0;
                    CurrentTorque = 0f;
                    SlowdownRMP = 1f;
                }
                if (rts.Count != 0) {
                    rt.BaseEngine = ( (RotationForce) rts[0] ).BaseEngine;
                }
                rt.Revs = CurrentRPM;
                rt.Forse = CurrentTorque;
                Rotate = rt;
            }
            else {
                RotationForce rt = new RotationForce();
                rt.Revs = 0;
                rt.Forse = 0;
                rt.BaseEngine = null;
                Rotate = rt;
            }
        }
    }

    public void GetGear() {
        FreedbackWire.UpdateWireConnect(GetGearWire, GearT);
    }

    private void GearShifted() {
        List<object> rts = FreedbackWire.TakeWireConnect(GetRotateWire);
        for (int i = 0; i < rts.Count; i++) {
            if (( (RotationForce) rts[i] ).BaseEngine) {
                ( (RotationForce) rts[i] ).BaseEngine.RPMDrop();
                ( (RotationForce) rts[i] ).BaseEngine.SlowdownRMP = SlowdownRMP;
            }
        }
    }

    public void UpdateThisBase(BaseInBuild new_base) {
    }

    public void UpdateParams(int index_param, object param) {
        if (index_param == 0) {
            _disconnect = (bool) param;
        }
    }

    public void AcceptStartParameters(object[] objects) {
        _disconnect = (bool) objects[0];
    }

    public object[] SendUploaded() {
        return new object[1] { _disconnect };
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {
    }
}

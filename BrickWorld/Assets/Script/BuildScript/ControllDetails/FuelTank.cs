﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelTank : MonoBehaviour, IDetailControll {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (value == true)
                this_base?.Fuels.Remove(this);
            _disconnect = value;
        }
    }

    bool _disconnect;

    public TypeFuel FuelType;

    private BaseInBuild this_base;

    public float Capacity;

    public float Count;
    public void UpdateThisBase(BaseInBuild new_base) {
        this_base?.Fuels.Remove(this);
        new_base.Fuels.Add(this);
        this_base = new_base;
    }
}

﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public interface ISyncParams {

    BuildingProperties BaseBuilding { get; set; }

    int Index { get; set; }

    void ObservableSync(PhotonStream stream, PhotonMessageInfo info);

    void UpdateParams(int index_param, object param);

    void AcceptStartParameters(object[] objects);

    object[] SendUploaded();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wing : MonoBehaviour, IDetailControll {
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }
    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (value == true)
                rigid = GetComponent<Rigidbody>();
            _disconnect = value;
        }
    }

    bool _disconnect;

    public float Area;


    public float LiftMultiplier = 1f;

    public float DragMultiplier = 1f;

    public WingCurves WingCurve;

    private BaseInBuild _base;

    Rigidbody rigid;
    public void UpdateThisBase(BaseInBuild new_base) {
        rigid = new_base.rigidbody;
        _base = new_base;
    }


    public static Vector3 CalculateForce(Vector3 velocity, WingCurves wing, Transform transform, float Area, float liftMultiplier, float dragMultiplier) {

        Vector3 localVelocity = transform.InverseTransformDirection(velocity);
        localVelocity.x = 0;



        //  float angleOfAttack = Vector3.Angle(Vector3.forward, localVelocity);
        float angleOfAttack = Vector3.Angle(transform.forward, velocity);
        float liftCoefficient = wing.GetLiftAtAOA(angleOfAttack);
        float dragCoefficient = wing.GetDragAtAOA(angleOfAttack);

        // Calculate lift/drag.
        float liftForce = localVelocity.sqrMagnitude * liftCoefficient * Area * liftMultiplier * 10f;
        float dragForce = localVelocity.sqrMagnitude * dragCoefficient * Area * dragMultiplier * 10f;

        // Vector3.Angle always returns a positive value, so add the sign back in.

        liftForce *= -Mathf.Sign(localVelocity.y);

        // Lift is always perpendicular to air flow.


        Vector3 liftDirection = Vector3.Cross(velocity, transform.right).normalized;

        Vector3 force = ( liftDirection * liftForce ) + ( -velocity.normalized * dragForce );
        //Vector3 force = ( liftDirection * liftForce );

        Debug.DrawRay(transform.position, ( liftDirection * liftForce ) * 0.001f, Color.red);

        Debug.DrawRay(transform.position, ( -velocity.normalized * dragForce ) * 0.001f, Color.blue);

        return force;
    }

    private void FixedUpdate() {
        if (_base != null && _base.Base.IsMainBuilding) {


            Vector3 force = CalculateForce(rigid.GetPointVelocity(transform.position), WingCurve, transform, Area, LiftMultiplier, DragMultiplier);
               force = Vector3.ClampMagnitude(force, rigid.velocity.magnitude * rigid.mass);

            rigid.AddForceAtPosition(force, transform.position, ForceMode.Force);
        }
    }
}

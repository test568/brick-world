﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour, IDetailControll, ISyncParams {
    public string Cod;

    public float Angle;
    public float Distance;
    public float Intensive;
    public Color color;
    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }


    public float Active {
        get {
            return _active;
        }
        set {
            if (parent_gameobject && value == 0f && parent_gameobject.activeInHierarchy) {
                parent_gameobject.SetActive(false);
            }
            else if (parent_gameobject && value != 0f && !parent_gameobject.activeInHierarchy && !Disconnect) {
                parent_gameobject.SetActive(true);
            }
            _active = value;
        }
    }

    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (GameRoom.isMasterClient) {
                _disconnect = value;
                if (_disconnect) {
                    Active = 0f;
                    BaseBuilding.SyncParamsDetail(this, 1, true);
                }
            }
        }
    }

    bool _disconnect;
    public int Index { get; set; }
    public BuildingProperties BaseBuilding { get; set; }

    float _active;

    GameObject parent_gameobject;
    GameObject lamp1;

    public void UpdateParams(int index_param, object param) {
        if (index_param == 0)
            Active = (float) param;
        else if (index_param == 1)
            _disconnect = (bool) param;
    }

    public void AcceptStartParameters(object[] objects) {
        Active = (float) objects[0];
        _disconnect = (bool) objects[1];
        if ((bool) objects[1])
            Active = 0f;
    }

    public object[] SendUploaded() {
        return new object[2] { _active, Disconnect };
    }

    public void GenerateLamp() {
        parent_gameobject = new GameObject("Lamps");
        parent_gameobject.transform.parent = gameObject.transform;
        parent_gameobject.transform.localPosition = Vector3.zero;
        parent_gameobject.transform.localRotation = Quaternion.identity;

        lamp1 = new GameObject("Lamp1");
        lamp1.transform.parent = parent_gameobject.transform;
        lamp1.transform.localPosition = Vector3.zero;
        lamp1.transform.localRotation = Quaternion.Euler(new Vector3(-90, 0, 0));

        if (Angle <= 180f) {
            lamp1.AddComponent<Light>().type = LightType.Spot;
            lamp1.GetComponent<Light>().spotAngle = Angle;
        }
        else if (Angle == 360f) {
            lamp1.AddComponent<Light>().type = LightType.Point;
        }

        lamp1.GetComponent<Light>().range = Distance;
        lamp1.GetComponent<Light>().intensity = Intensive;
        lamp1.GetComponent<Light>().color = color;
        lamp1.GetComponent<Light>().renderMode = LightRenderMode.ForcePixel;

        parent_gameobject.SetActive(false);
    }

    public void UpdateThisBase(BaseInBuild new_base) {
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {
    }
}

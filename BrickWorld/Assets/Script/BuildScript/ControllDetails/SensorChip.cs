﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorChip : MonoBehaviour, IDetailControll, ISyncParams {

    public FreedbackWire.LoadMetods UpdateFunctions { get; set; }

    public bool Disconnect {
        get {
            return _disconnect;
        }
        set {
            if (GameRoom.isMasterClient) {
                _disconnect = value;
                BaseBuilding.SyncParamsDetail(this, 0, true);
            }
        }
    }

    bool _disconnect;

    public FreedbackWire GetSpeedWire = new FreedbackWire();

    public void UpdateThisBase(BaseInBuild new_base) {
        this_base = new_base;
    }

    private BaseInBuild this_base;

    public float Speed { get; set; }
    public BuildingProperties BaseBuilding { get; set; }
    public int Index { get; set; }

    private void Update() {
        if (!Disconnect && BaseBuilding.IsMainBuilding && GameRoom.isMultiplayer) {
            BaseBuilding.SyncParamsDetail(this, 1, Speed);
        }
        if (UpdateFunctions != null) {
            UpdateFunctions();
        }
    }

    public void GetSpeed() {
        if (BaseBuilding.IsMainBuilding)
            if (!Disconnect) {
                Speed = this_base.rigidbody.velocity.magnitude * 3.6f;
                FreedbackWire.UpdateWireConnect(GetSpeedWire, Speed);
            }
            else {
                Speed = 0;
                FreedbackWire.UpdateWireConnect(GetSpeedWire, Speed);
            }
        else
            FreedbackWire.UpdateWireConnect(GetSpeedWire, Speed);
    }

    public void UpdateParams(int index_param, object param) {
        if (index_param == 0)
            _disconnect = (bool) param;
        else
            Speed = (float) param;
    }

    public void AcceptStartParameters(object[] objects) {
        _disconnect = (bool) objects[0];
        Speed = (float) objects[1];
    }

    public object[] SendUploaded() {
        return new object[2] { _disconnect, Speed };
    }

    public void ObservableSync(PhotonStream stream, PhotonMessageInfo info) {
    }
}

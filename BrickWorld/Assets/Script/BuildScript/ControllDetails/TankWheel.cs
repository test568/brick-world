﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TankWheel : WheelCollision, IDetailControll {
    public bool IsLeadTankWheel = false;

    public bool IsLeader = false;

    public List<TankWheel> TankWheels = new List<TankWheel>();

    public WheelAxis ConnectAxis;

    public float Widht;

    public float Height;

    public TypeTrack Type;

    public Transform PointWheelAxis;

    public float SpeedTrack;

    [SerializeField]
    private TankTrackBuild Track;

    private TankWheelTrack[] Wheels;



    private void Update() {
        if (IsLeader && Track != null) {
            Track.UpdateTrack();
            Track.MoveUpdate(SpeedTrack);
        }
    }

    public void BuildTrack() {
        if (TankWheels.Count > 1) {
            if (!IsLeadTankWheel) {
                for (int i = 0; i < TankWheels.Count; i++) {
                    if (TankWheels[i].IsLeadTankWheel) {
                        IsLeader = false;
                        TankWheels[i].IsLeader = true;
                        TankWheels[i].BuildTrack();
                        return;
                    }
                }
            }
            if (Track == null) {
                for (int i = 0; i < TankWheels.Count; i++) {
                    if (TankWheels[i] != this) {
                        TankWheels[i].ConnectAxis.SetTrackParams(ConnectAxis);
                    }
                }

                Track = new TankTrackBuild();

                List<TankWheelTrack> wheels = new List<TankWheelTrack>();
                for (int i = 0; i < TankWheels.Count; i++) {

                    TankWheelTrack wheel = new TankWheelTrack();
                    TankWheels[i].Track = Track;

                    wheel.Height = Height;
                    wheel.Widht = Widht;
                    wheel.Type = Type;
                    wheel.transform = TankWheels[i].PointWheelAxis;
                    wheel.WheelObject = TankWheels[i].PointWheelAxis.gameObject;
                    wheel.Radius = Radius;
                    wheels.Add(wheel);

                }
                Wheels = wheels.ToArray();
                Track.CreateNewTrack(wheels.ToArray());
            }
        }
    }

    public void DestroyTrack() {
        if (!IsLeader) {
            for (int i = 0; i < TankWheels.Count; i++) {
                if (TankWheels[i].IsLeader) {
                    TankWheels[i].DestroyTrack();
                    return;
                }
            }
        }
        for (int i = 0; i < TankWheels.Count; i++) {
            TankWheels[i].ConnectAxis.DestroyTrack();
        }

        if (Track != null) {
            Track.DestroyTrack();
            Track = null;
        }
    }

    protected override void UpdateDisconnect() {
        if (Disconnect) {
            DestroyTrack();
        }
    }

    public override float GetRadius() {
        if (Track != null && Track.IsTrack) {
            return Radius + Height / TankTrack.HeightDivider * 2f;
        }
        else {
            return Radius;
        }
    }

    private void OnDestroy() {
        if (IsLeader && Track != null) {
            Track.DestroyTrack();
        }
    }
}

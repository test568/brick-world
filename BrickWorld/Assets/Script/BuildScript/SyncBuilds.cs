﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class SyncBuilds : MonoBehaviour, IGetPhotonView {

    public BuildingProperties SpawnBuild { get; private set; }

    [ShowInInspector]
    public List<BuildingProperties> Builds { get; private set; }

    [SerializeField] PhotonView photon_view;

    [SerializeField] GameObject build_prefab;

    public static SyncBuilds singleton;

    private void Awake() {
        if (singleton == null)
            singleton = this;

        Builds = new List<BuildingProperties>();
    }

    public PhotonView GetPhotonView() {
        return photon_view;
    }

    [PunRPC]
    private void RPCRemoveBuild(string cod_build) {
        PhotonNetwork.Destroy(Builds.Where((x) => x.Cod == cod_build).FirstOrDefault().GetPhotonView());
    }

    public void DestroyAllTechnic() {
        for (int i = 0; i < Builds.Count; i++) {
            if (GameRoom.isMultiplayer) {
                PhotonNetwork.Destroy(Builds[i].GetPhotonView());
            }
            else {
                Destroy(Builds[i].gameObject);
            }
        }
    }

    public void DestroyThisTechnic() {
        if (GameRoom.isMultiplayer) {
            if (ControllerPlayer.MainPlayer.Status == PlayerStatus.inChair && PlayerChair.ChairFromPlayer.BaseBuilding.GetPhotonView().Controller == PhotonNetwork.LocalPlayer) {
          //      photon_view.RpcSecure("RPCRemoveBuild", PlayerChair.ChairFromPlayer.BaseBuilding.GetPhotonView().Controller, false, PlayerChair.ChairFromPlayer.BaseBuilding.Cod);
                PhotonNetwork.Destroy(PlayerChair.ChairFromPlayer.BaseBuilding.GetPhotonView());
            }
        }
        else {
            if (ControllerPlayer.MainPlayer.Status == PlayerStatus.inChair) {
                Destroy(PlayerChair.ChairFromPlayer.BaseBuilding.gameObject);
            }
        }
    }

    public void DestroyAllMyTechnic() {
        for (int i = 0; i < Builds.Count; i++) {
            if (GameRoom.isMultiplayer) {
                if (Builds[i].GetPhotonView().Controller == PhotonNetwork.LocalPlayer) {
                    PhotonNetwork.Destroy(Builds[i].GetPhotonView());
                }
            }
            else {
                Destroy(Builds[i].gameObject);
            }
        }
    }

    public void CreateNewBuilding(List<DetailDate> details) {
        if (GameRoom.isMultiplayer) {
            string cod = UnityEngine.Random.Range(0, int.MaxValue).ToString();
            BuildingProperties br = PhotonNetwork.Instantiate(build_prefab.name, new Vector3(0, -1000f, 0), Quaternion.identity).GetComponent<BuildingProperties>();
            br.Cod = cod;
            BuildSpawn.CollectBuilding(br.gameObject, br.BuildObject, details.ToArray());
            SpawnBuild = br;
        }
        else {
            SpawnBuild = Instantiate(build_prefab, new Vector3(0, -1000f, 0), Quaternion.identity).GetComponent<BuildingProperties>();
            string cod = UnityEngine.Random.Range(0, int.MaxValue).ToString();
            BuildSpawn.CollectBuilding(SpawnBuild.gameObject, SpawnBuild.BuildObject, details.ToArray());
            SpawnBuild.Cod = cod;
        }
    }

    public void SpawnBuilding() {
        BuildSpawn.UnFreezeBuilding(SpawnBuild.gameObject);
        SpawnBuild = null;
        ControllerPlayer.MainPlayer.Status = PlayerStatus.Move;
    }

    public void NoSpawnBuilding() {
        if (SpawnBuild) {
            if (GameRoom.isMultiplayer) {
                PhotonNetwork.Destroy(SpawnBuild.gameObject);
            }
            else {
                Destroy(SpawnBuild.gameObject);
            }
            SpawnBuild = null;
            ControllerPlayer.MainPlayer.Status = PlayerStatus.Move;
        }
    }
}

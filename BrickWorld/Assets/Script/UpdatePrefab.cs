﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using HighlightPlus;

public class UpdatePrefab: MonoBehaviour {
    public GameObject obj;
    public FullDatabase FullDatabase;
    public string[] NameBracing;

    public void PrefabSet() {
        for (int i = 0; i<FullDatabase.ToArray.Length; i++) {
            for (int i2 = 0; i2<FullDatabase.ToArray[i].ToArray.Length; i2++) {
                GameObject detail = FullDatabase.ToArray[i].ToArray[i2];
                DetailDate date = detail.GetComponent<DetailDate>();
                HighlightEffect[] effects = date.DetailMesh.gameObject.GetComponents<HighlightEffect>();
                HighlightEffect no_delet = effects[0];
                for (int i3 = 0; i3<effects.Length; i3++) {
                    if (effects[i3]!=no_delet) {
                        DestroyImmediate(effects[i3],true);
                        }
                    }
                date.Highlight=no_delet;
                ////GameObject detail = obj;

                //int count_child = detail.transform.childCount;
                //List<GameObject> bracing = new List<GameObject>();
                //for (int i3 = 0; i3<count_child; i3++) {
                //    bool s = false;
                //    for (int i4 = 0; i4<NameBracing.Length; i4++) {
                //        if (detail.transform.GetChild(i3).name.IndexOf(NameBracing[i4])!=-1) {
                //            s=true;
                //            break;
                //            }
                //        }
                //    if (s)
                //        bracing.Add(detail.transform.GetChild(i3).gameObject);
                //    }
                //Vector3 pos = new Vector3(0, 0, 0);
                //if (bracing.Count!=0) {
                //    for (int i3 = 0; i3<bracing.Count; i3++) {
                //        pos+=bracing[i3].transform.localPosition;
                //        }
                //    pos=pos/bracing.Count;

                //    pos.x=pos.x-( pos.x%( 0.07999992f/2f ) );
                //    pos.y=pos.y-( pos.y%( 0.07999992f ) );
                //    pos.z=pos.z-( pos.z%( 0.07999992f/2f ) );

                //    for (int i3 = 0; i3<count_child; i3++) {
                //        detail.transform.GetChild(i3).transform.localPosition-=pos;
                //        }
                //    Collider[] col = detail.GetComponents<Collider>();
                //    for (int i3 = 0; i3<col.Length; i3++) {
                //        try {
                //            ( (BoxCollider)col[i3] ).center-=pos;
                //            }
                //        catch {
                //            }
                //        }
                //    }
                }
            }
        }

    private Component CopyComponent(Component original, GameObject destination) {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields) {
            field.SetValue(copy, field.GetValue(original));
            }
        return copy;
        }
    }

#if UNITY_EDITOR

[CustomEditor(typeof(UpdatePrefab))]
public class UpdatePrefabEditor: Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        if (GUILayout.Button("Обновить")) {
            ( (UpdatePrefab)target ).PrefabSet();
            }
        }
    }
#endif